<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BuildGroupRepository")
 */
class BuildGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bgrId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastEditor;

    ////////////////////////////////////////////////////////////////
    //     Constructor
    ////////////////////////////////////////////////////////////////

    public function __construct()
    {
        $this->modified = new \DateTime('now');
    }

    ////////////////////////////////////////////////////////////////
    //     Getter and Setter
    ////////////////////////////////////////////////////////////////

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBgrId(): ?String
    {
        return $this->bgrId;
    }

    public function setBgrId(string $bgrId): self
    {
        $this->bgrId = $bgrId;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLastEditor(): ?string
    {
        return $this->lastEditor;
    }

    public function setLastEditor(string $lastEditor): self
    {
        $this->lastEditor = $lastEditor;

        return $this;
    }

    /**
     * Set modified
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    public function __toString()
    {
        if (!empty($this->getBgrId())) {
            return $this->getBgrId();
        } else {
            return 'label.new_entry';
        }
    }
}
