<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PowerOfLampRepository")
 */
class PowerOfLamp
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataSheet", mappedBy="powerOfLamp_id")
     */
    private $dataSheets;

    public function __construct()
    {
        $this->dataSheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|DataSheet[]
     */
    public function getDataSheets(): Collection
    {
        return $this->dataSheets;
    }

    public function addDataSheet(DataSheet $dataSheet): self
    {
        if (!$this->dataSheets->contains($dataSheet)) {
            $this->dataSheets[] = $dataSheet;
            $dataSheet->setPowerOfLampId($this);
        }

        return $this;
    }

    public function removeDataSheet(DataSheet $dataSheet): self
    {
        if ($this->dataSheets->contains($dataSheet)) {
            $this->dataSheets->removeElement($dataSheet);
            // set the owning side to null (unless already changed)
            if ($dataSheet->getPowerOfLampId() === $this) {
                $dataSheet->setPowerOfLampId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if (!empty($this->getValue())) {
            return $this->getValue();
        } else {
            return 'label.new_entry';
        }
    }
}
