<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppBundleAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DataSheetRepository")
 * @ORM\HasLifecycleCallbacks
 *
 */
class DataSheet
{
    const IMG_UPLOAD_DIR_SKIZZE = 'uploads/datasheets/skizze/';
    const IMG_UPLOAD_DIR_IMAGE = 'uploads/datasheets/image/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $light_type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FixtureLightOutput", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fixture_light_output_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MaterialOfHousing", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $material_of_housing_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BeamAngle", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $beam_angle_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MaterialOfShade", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $material_of_shade_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LightColour", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $light_colour_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ColourOfHousing", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $colourOfHousing_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cover", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cover_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ColourOfShade", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $colourOfShade_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DimmerType", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dimmerType_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Frequency", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $frequency_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LightPoints", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lightPoints_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NumberOfLamps", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $numberOfLamps_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AdjustingAngle", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adjustingAngle_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PowerOfFitting", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $powerOfFitting_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Socket", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $socket_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeOfLamp", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeOfLamp_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plug", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plug_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeOfBulb", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeOfBulb_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PowerOfLamp", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $powerOfLamp_id;

    /**
     * @ORM\Column(type="text")
     */
    private $remarks;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cri", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cri_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Voltage", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voltage_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="dataSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $supplier_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $articleNo;

    /**
     * Unmapped property to handle file uploads
     *
     * @Assert\Image(
     *  minWidth="500",
     *  minWidthMessage = "min width 500px",
     *  minHeight="500",
     *  minHeightMessage = "min heigth 500px",
     *  mimeTypes = {"image/jpeg"},
     *  mimeTypesMessage = "Please select a .jpg File!"
     *
     * )
     */
    private $fileSkizze;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileSkizzeUrl;

    /**
     * Unmapped property to handle file uploads
     *
     * @Assert\Image(
     *  minWidth="500",
     *  minWidthMessage = "min width 500px",
     *  minHeight="500",
     *  minHeightMessage = "min heigth 500px",
     *  mimeTypes = {"image/jpeg", "image/png"},
     *  mimeTypesMessage = "Please select a valid fileformat!"
     * )
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier")
     * @ORM\JoinColumn(nullable=false)
     */
    private $manufacturer_id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $light_type_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diameter_complete;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diameter_cutout;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mountingDepth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depth_in_sight;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $distance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $connection;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $main_category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subcategory;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subclass;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subclass_description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bk;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $ip_class_x;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $ip_class_y;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $lastEditor;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $revision;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLightType(): ?string
    {
        return $this->light_type;
    }

    public function setLightType(string $light_type): self
    {
        $this->light_type = $light_type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFixtureLightOutputId(): ?FixtureLightOutput
    {
        return $this->fixture_light_output_id;
    }

    public function setFixtureLightOutputId(?FixtureLightOutput $fixture_light_output_id): self
    {
        $this->fixture_light_output_id = $fixture_light_output_id;

        return $this;
    }

    public function getMaterialOfHousingId(): ?MaterialOfHousing
    {
        return $this->material_of_housing_id;
    }

    public function setMaterialOfHousingId(?MaterialOfHousing $material_of_housing_id): self
    {
        $this->material_of_housing_id = $material_of_housing_id;

        return $this;
    }

    public function getBeamAngleId(): ?BeamAngle
    {
        return $this->beam_angle_id;
    }

    public function setBeamAngleId(?BeamAngle $beam_angle_id): self
    {
        $this->beam_angle_id = $beam_angle_id;

        return $this;
    }

    public function getMaterialOfShadeId(): ?MaterialOfShade
    {
        return $this->material_of_shade_id;
    }

    public function setMaterialOfShadeId(?MaterialOfShade $material_of_shade_id): self
    {
        $this->material_of_shade_id = $material_of_shade_id;

        return $this;
    }

    public function getLightColourId(): ?LightColour
    {
        return $this->light_colour_id;
    }

    public function setLightColourId(?LightColour $light_colour_id): self
    {
        $this->light_colour_id = $light_colour_id;

        return $this;
    }

    public function getColourOfHousingId(): ?ColourOfHousing
    {
        return $this->colourOfHousing_id;
    }

    public function setColourOfHousingId(?ColourOfHousing $colourOfHousing_id): self
    {
        $this->colourOfHousing_id = $colourOfHousing_id;

        return $this;
    }

    public function getCoverId(): ?Cover
    {
        return $this->cover_id;
    }

    public function setCoverId(?Cover $cover_id): self
    {
        $this->cover_id = $cover_id;

        return $this;
    }

    public function getColourOfShadeId(): ?ColourOfShade
    {
        return $this->colourOfShade_id;
    }

    public function setColourOfShadeId(?ColourOfShade $colourOfShade_id): self
    {
        $this->colourOfShade_id = $colourOfShade_id;

        return $this;
    }

    public function getDimmerTypeId(): ?DimmerType
    {
        return $this->dimmerType_id;
    }

    public function setDimmerTypeId(?DimmerType $dimmerType_id): self
    {
        $this->dimmerType_id = $dimmerType_id;

        return $this;
    }

    public function getFrequencyId(): ?Frequency
    {
        return $this->frequency_id;
    }

    public function setFrequencyId(?Frequency $frequency_id): self
    {
        $this->frequency_id = $frequency_id;

        return $this;
    }

    public function getLightPointsId(): ?LightPoints
    {
        return $this->lightPoints_id;
    }

    public function setLightPointsId(?LightPoints $lightPoints_id): self
    {
        $this->lightPoints_id = $lightPoints_id;

        return $this;
    }

    public function getNumberOfLampsId(): ?NumberOfLamps
    {
        return $this->numberOfLamps_id;
    }

    public function setNumberOfLampsId(?NumberOfLamps $numberOfLamps_id): self
    {
        $this->numberOfLamps_id = $numberOfLamps_id;

        return $this;
    }

    public function getAdjustingAngleId(): ?AdjustingAngle
    {
        return $this->adjustingAngle_id;
    }

    public function setAdjustingAngleId(?AdjustingAngle $adjustingAngle_id): self
    {
        $this->adjustingAngle_id = $adjustingAngle_id;

        return $this;
    }

    public function getPowerOfFittingId(): ?PowerOfFitting
    {
        return $this->powerOfFitting_id;
    }

    public function setPowerOfFittingId(?PowerOfFitting $powerOfFitting_id): self
    {
        $this->powerOfFitting_id = $powerOfFitting_id;

        return $this;
    }

    public function getSocketId(): ?Socket
    {
        return $this->socket_id;
    }

    public function setSocketId(?Socket $socket_id): self
    {
        $this->socket_id = $socket_id;

        return $this;
    }

    public function getTypeOfLampId(): ?TypeOfLamp
    {
        return $this->typeOfLamp_id;
    }

    public function setTypeOfLampId(?TypeOfLamp $typeOfLamp_id): self
    {
        $this->typeOfLamp_id = $typeOfLamp_id;

        return $this;
    }

    public function getPlugId(): ?Plug
    {
        return $this->plug_id;
    }

    public function setPlugId(?Plug $plug_id): self
    {
        $this->plug_id = $plug_id;

        return $this;
    }

    public function getTypeOfBulbId(): ?TypeOfBulb
    {
        return $this->typeOfBulb_id;
    }

    public function setTypeOfBulbId(?TypeOfBulb $typeOfBulb_id): self
    {
        $this->typeOfBulb_id = $typeOfBulb_id;

        return $this;
    }

    public function getPowerOfLampId(): ?PowerOfLamp
    {
        return $this->powerOfLamp_id;
    }

    public function setPowerOfLampId(?PowerOfLamp $powerOfLamp_id): self
    {
        $this->powerOfLamp_id = $powerOfLamp_id;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    public function getCriId(): ?Cri
    {
        return $this->cri_id;
    }

    public function setCriId(?Cri $cri_id): self
    {
        $this->cri_id = $cri_id;

        return $this;
    }

    public function getVoltageId(): ?Voltage
    {
        return $this->voltage_id;
    }

    public function setVoltageId(?Voltage $voltage_id): self
    {
        $this->voltage_id = $voltage_id;

        return $this;
    }

    public function getDriver(): ?string
    {
        return $this->driver;
    }

    public function setDriver(string $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getSupplierId(): ?Supplier
    {
        return $this->supplier_id;
    }

    public function setSupplierId(?Supplier $supplier_id): self
    {
        $this->supplier_id = $supplier_id;

        return $this;
    }

    public function getArticleNo(): ?string
    {
        return $this->articleNo;
    }

    public function setArticleNo(string $articleNo): self
    {
        $this->articleNo = $articleNo;

        return $this;
    }

    /**
     * Set createDate
     *
     * @ORM\PrePersist
     */
    public function setCreateDate()
    {
        $this->createDate = new \DateTime();
        $this->updateDate = new \DateTime();
        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdateDate()
    {
        $this->updateDate = new \DateTime();
        return $this;
    }

    public function getManufacturerId(): ?Supplier
    {
        return $this->manufacturer_id;
    }

    public function setManufacturerId(?Supplier $manufacturer_id): self
    {
        $this->manufacturer_id = $manufacturer_id;

        return $this;
    }

    /**
     * Get empUpdate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    public function getFileSkizzeUrl(): ?string
    {
        return $this->fileSkizzeUrl;
    }

    public function setFileSkizzeUrl(string $fileSkizzeUrl): self
    {
        $this->fileSkizzeUrl = '/'.self::IMG_UPLOAD_DIR_SKIZZE.$fileSkizzeUrl;

        return $this;
    }

    /**
     * @param UploadedFile $fileSkizze
     */
    public function setFileSkizze(UploadedFile $fileSkizze = null)
    {
        $this->fileSkizze = $fileSkizze;
    }

    /**
     * @return UploadedFile
     */
    public function getFileSkizze()
    {
        return $this->fileSkizze;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(string $imageUrl): self
    {
        $this->imageUrl = '/'.self::IMG_UPLOAD_DIR_IMAGE.$imageUrl;

        return $this;
    }

    /**
     * @param UploadedFile $image
     */
    public function setImage(UploadedFile $image = null)
    {
        $this->image = $image;
    }

    /**
     * @return UploadedFile
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {

        // the file property can be empty if the field is not required
        if (null != $this->getFileSkizze()) {


            // we use the original file name here but you should
            // sanitize it at least to avoid any security issues

            // move takes the target directory and target filename as params
            $this->getFileSkizze()->move(
                self::IMG_UPLOAD_DIR_SKIZZE,
                $this->getFileSkizze()->getClientOriginalName()
            );

            // set the path property to the filename where you've saved the file
            $this->fileSkizzeUrl = $this->getFileSkizze()->getClientOriginalName();

            // clean up the file property as you won't need it anymore
            $this->setFileSkizze(null);
        } elseif(null != $this->getImage()) {


            // we use the original file name here but you should
            // sanitize it at least to avoid any security issues

            // move takes the target directory and target filename as params
            $this->getImage()->move(
                self::IMG_UPLOAD_DIR_IMAGE,
                $this->getImage()->getClientOriginalName()
            );

            // set the path property to the filename where you've saved the file
            $this->imageUrl = $this->getImage()->getClientOriginalName();

            // clean up the file property as you won't need it anymore
            $this->setImage(null);
        } else {
            return;
        }

    }

    /**
     * lifecycleFileUpload
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     */
    public function preAction() {
        $this->preUpload();
    }

    /**
     * preUpload
     */
    public function preUpload() {
        if ($this->getFileSkizze() instanceof UploadedFile) {
            $filenameSkizze = $this->getFileSkizze()->getClientOriginalName();//notify image field, that there is a change
            $this->setFileSkizzeUrl($filenameSkizze);
        }

        if ($this->getImage() instanceof UploadedFile) {
            $filenameImage = $this->getImage()->getClientOriginalName();//notify image field, that there is a change
            $this->setImageUrl($filenameImage);
        }
    }

    /**
     * Lifecycle callback to upload the file to the server.
     *
     * @ORM\PostUpdate
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire.
     */
    public function refreshUpdated()
    {
        $this->setUpdateDate(new \DateTime("now"));
    }

    public function getLightTypeId(): ?string
    {
        return $this->light_type_id;
    }

    public function setLightTypeId(string $light_type_id): self
    {
        $this->light_type_id = $light_type_id;

        return $this;
    }

    public function __toString()
    {
        if (!empty($this->getLightTypeId())) {
            return $this->getLightTypeId() . " - " .$this->getDescription();
        } else {
            return 'label.new_entry';
        }
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(?string $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getDepth(): ?string
    {
        return $this->depth;
    }

    public function setDepth(?string $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getDiameterComplete(): ?string
    {
        return $this->diameter_complete;
    }

    public function setDiameterComplete(?string $diameter_complete): self
    {
        $this->diameter_complete = $diameter_complete;

        return $this;
    }

    public function getDiameterCutout(): ?string
    {
        return $this->diameter_cutout;
    }

    public function setDiameterCutout(?string $diameter_cutout): self
    {
        $this->diameter_cutout = $diameter_cutout;

        return $this;
    }

    public function getMountingDepth(): ?string
    {
        return $this->mountingDepth;
    }

    public function setMountingDepth(?string $mountingDepth): self
    {
        $this->mountingDepth = $mountingDepth;

        return $this;
    }

    public function getDepthInSight(): ?string
    {
        return $this->depth_in_sight;
    }

    public function setDepthInSight(string $depth_in_sight): self
    {
        $this->depth_in_sight = $depth_in_sight;

        return $this;
    }

    public function getDistance(): ?string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getConnection(): ?string
    {
        return $this->connection;
    }

    public function setConnection(?string $connection): self
    {
        $this->connection = $connection;

        return $this;
    }

    public function getMainCategory(): ?string
    {
        return $this->main_category;
    }

    public function setMainCategory(string $main_category): self
    {
        $this->main_category = $main_category;

        return $this;
    }

    public function getSubcategory(): ?string
    {
        return $this->subcategory;
    }

    public function setSubcategory(string $subcategory): self
    {
        $this->subcategory = $subcategory;

        return $this;
    }

    public function getSubclass(): ?string
    {
        return $this->subclass;
    }

    public function setSubclass(string $subclass): self
    {
        $this->subclass = $subclass;

        return $this;
    }

    public function getSubclassDescription(): ?string
    {
        return $this->subclass_description;
    }

    public function setSubclassDescription(string $subclass_description): self
    {
        $this->subclass_description = $subclass_description;

        return $this;
    }

    public function getBk(): ?string
    {
        return $this->bk;
    }

    public function setBk(string $bk): self
    {
        $this->bk = $bk;

        return $this;
    }

    public function getIpClassX(): ?string
    {
        return $this->ip_class_x;
    }

    public function setIpClassX(string $ip_class_x): self
    {
        $this->ip_class_x = $ip_class_x;

        return $this;
    }

    public function getIpClassY(): ?string
    {
        return $this->ip_class_y;
    }

    public function setIpClassY(string $ip_class_y): self
    {
        $this->ip_class_y = $ip_class_y;

        return $this;
    }

    public function getLastEditor(): ?User
    {
        return $this->lastEditor;
    }

    public function setLastEditor(?User $lastEditor): self
    {
        $this->lastEditor = $lastEditor;

        return $this;
    }

    public function getRevision(): ?string
    {
        return $this->revision;
    }

    public function setRevision(string $revision): self
    {
        $this->revision = $revision;

        return $this;
    }
}
