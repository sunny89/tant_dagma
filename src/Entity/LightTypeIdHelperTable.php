<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LightTypeIdHelperTableRepository")
 */
class LightTypeIdHelperTable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $light_type;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $entry_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLightType(): ?string
    {
        return $this->light_type;
    }

    public function setLightType(string $light_type): self
    {
        $this->light_type = $light_type;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getEntryType(): ?string
    {
        return $this->entry_type;
    }

    public function setEntryType(string $entry_type): self
    {
        $this->entry_type = $entry_type;

        return $this;
    }
}
