<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FixtureLightOutputRepository")
 */
class FixtureLightOutput
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataSheet", mappedBy="fixture_light_output_id")
     */
    private $dataSheets;

    public function __construct()
    {
        $this->dataSheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|DataSheet[]
     */
    public function getDataSheets(): Collection
    {
        return $this->dataSheets;
    }

    public function addDataSheet(DataSheet $dataSheet): self
    {
        if (!$this->dataSheets->contains($dataSheet)) {
            $this->dataSheets[] = $dataSheet;
            $dataSheet->setFixtureLightOutputId($this);
        }

        return $this;
    }

    public function removeDataSheet(DataSheet $dataSheet): self
    {
        if ($this->dataSheets->contains($dataSheet)) {
            $this->dataSheets->removeElement($dataSheet);
            // set the owning side to null (unless already changed)
            if ($dataSheet->getFixtureLightOutputId() === $this) {
                $dataSheet->setFixtureLightOutputId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if (!empty($this->getValue())) {
            return $this->getValue();
        } else {
            return 'label.new_entry';
        }
    }
}
