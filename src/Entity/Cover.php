<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoverRepository")
 */
class Cover
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataSheet", mappedBy="cover_id")
     */
    private $dataSheets;

    public function __construct()
    {
        $this->dataSheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|DataSheet[]
     */
    public function getDataSheets(): Collection
    {
        return $this->dataSheets;
    }

    public function addDataSheet(DataSheet $dataSheet): self
    {
        if (!$this->dataSheets->contains($dataSheet)) {
            $this->dataSheets[] = $dataSheet;
            $dataSheet->setCoverId($this);
        }

        return $this;
    }

    public function removeDataSheet(DataSheet $dataSheet): self
    {
        if ($this->dataSheets->contains($dataSheet)) {
            $this->dataSheets->removeElement($dataSheet);
            // set the owning side to null (unless already changed)
            if ($dataSheet->getCoverId() === $this) {
                $dataSheet->setCoverId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if (!empty($this->getValue())) {
            return $this->getValue();
        } else {
            return 'label.new_entry';
        }
    }
}
