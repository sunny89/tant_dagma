<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DimmerTypeRepository")
 */
class DimmerType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $typ;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cable_type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataSheet", mappedBy="dimmerType_id")
     */
    private $dataSheets;

    public function __construct()
    {
        $this->dataSheets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTyp(): ?string
    {
        return $this->typ;
    }

    public function setTyp(string $typ): self
    {
        $this->typ = $typ;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCableType(): ?string
    {
        return $this->cable_type;
    }

    public function setCableType(string $cable_type): self
    {
        $this->cable_type = $cable_type;

        return $this;
    }

    /**
     * @return Collection|DataSheet[]
     */
    public function getDataSheets(): Collection
    {
        return $this->dataSheets;
    }

    public function addDataSheet(DataSheet $dataSheet): self
    {
        if (!$this->dataSheets->contains($dataSheet)) {
            $this->dataSheets[] = $dataSheet;
            $dataSheet->setDimmerTypeId($this);
        }

        return $this;
    }

    public function removeDataSheet(DataSheet $dataSheet): self
    {
        if ($this->dataSheets->contains($dataSheet)) {
            $this->dataSheets->removeElement($dataSheet);
            // set the owning side to null (unless already changed)
            if ($dataSheet->getDimmerTypeId() === $this) {
                $dataSheet->setDimmerTypeId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if (!empty($this->getDescription())) {
            return $this->getDescription();
        } else {
            return 'label.new_entry';
        }
    }
}
