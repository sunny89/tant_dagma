<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use DH\DoctrineAuditBundle\Reader\AuditReader;

class DefaultController extends Controller {

    /**
     * @Route("/", name="index")
     */
    public function index() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/dimmerrack", name="dimmerrack")
     */
    public function dimmerrack() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/dimmerracksou", name="dimmerracksou")
     */
    public function dimmerracksou() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/lightPointCalculation", name="lightPointCalculation")
     */
    public function lightPointCalculation() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/powerLoadConsumtion", name="powerLoadConsumtion")
     */
    public function powerLoadConsumtion() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/dimmingChannelCalculation", name="dimmingChannelCalculation")
     */
    public function dimmingChannelCalculation() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/videoTutorials", name="videoTutorials")
     */
    public function videoTutorials() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }

    /**
     * @Route("/admin/manuals", name="manuals")
     */
    public function manuals() {
        return $this->redirectToRoute('sonata_admin_redirect', [], 301);
    }
}