<?php

namespace App\Controller;

use App\Entity\DataSheet;
use App\Service\LightTypeIdHelperService;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\VarDumper\Cloner\Data;
use DH\DoctrineAuditBundle\Reader\AuditReader;
use DH\DoctrineAuditBundle\Exception\AccessDeniedException;
use DH\DoctrineAuditBundle\Exception\InvalidArgumentException;
use DH\DoctrineAuditBundle\Helper\AuditHelper;
use DH\DoctrineAuditBundle\Reader\AuditEntry;

class DatasheetActionsController extends CRUDController
{
    public function createPDFAction($id)
    {

        $datasheet = $this->getDatasheet($id);

        $pdfName = $datasheet->getLightTypeId();

        $datasheet = array(
            'logo' => '/assets/images/Dagma_Logo_small.jpg',
            'id' => $datasheet->getLightTypeId(),
            'description' => $datasheet->getDescription(),
            'voltage' => $datasheet->getVoltageId()->getValue(),
            'dimmerType' => $datasheet->getDimmerTypeId()->getDescription(),
            'frequency' => $datasheet->getFrequencyId()->getValue(),
            'plug' => $datasheet->getPlugId()->getValue(),
            'ip_class' => "IP".$datasheet->getIpClassX().$datasheet->getIpClassY(),
            'driver' => $datasheet->getDriver(),
            'power_of_fitting' => $datasheet->getPowerOfFittingId()->getValue(),
            'number_of_lamp' => $datasheet->getNumberOfLampsId()->getValue(),
            'socket' => $datasheet->getSocketId()->getValue(),
            'type_of_bulb' => $datasheet->getTypeOfBulbId()->getValue(),
            'type_of_lamp' => $datasheet->getTypeOfLampId()->getValue(),
            'power_of_lamp' => $datasheet->getPowerOfLampId()->getValue(),
            'lightpoints' => $datasheet->getLightPointsId()->getValue(),
            'cri' => $datasheet->getCriId()->getValue(),
            'light_colour' => $datasheet->getLightColourId()->getValue(),
            'cover' => $datasheet->getCoverId()->getValue(),
            'beam_angle' => $datasheet->getBeamAngleId()->getValue(),
            'colour_of_shade' => $datasheet->getColourOfShadeId()->getValue(),
            'material_of_shade' => $datasheet->getMaterialOfShadeId()->getValue(),
            'colour_of_housing' => $datasheet->getColourOfHousingId()->getValue(),
            'material_of_housing' => $datasheet->getMaterialOfHousingId()->getValue(),
            'manufacturer' => $datasheet->getManufacturerId()->getName(),
            'supplier' => $datasheet->getSupplierId()->getName(),
            'supplier_street' => $datasheet->getSupplierId()->getStreet(),
            'supplier_location' => $datasheet->getSupplierId()->getLocation(),
            'supplier_country' => $datasheet->getSupplierId()->getCountry(),
            'article_no' => $datasheet->getArticleNo(),
            'supplier_phone' => $datasheet->getSupplierId()->getPhone(),
            'supplier_fax' => $datasheet->getSupplierId()->getFax(),
            'supplier_mail' => $datasheet->getSupplierId()->getEmail(),
            'supplier_zipcode' => $datasheet->getSupplierId()->getZipcode(),
            'skizze' => $datasheet->getFileSkizzeUrl(),
            'image' => $datasheet->getImageUrl(),
            'fixture_light_output' => $datasheet->getFixtureLightOutputId()->getValue(),
            'length' => $datasheet->getLength(),
            'width' => $datasheet->getWidth(),
            'height' => $datasheet->getHeight(),
            'depth' => $datasheet->getDepth(),
            'diameter_complete' => $datasheet->getDiameterComplete(),
            'diameter_cutout' => $datasheet->getDiameterCutout(),
            'mountingDepth' => $datasheet->getMountingDepth(),
            'depth_in_sight' => $datasheet->getDepthInSight(),
            'distance' => $datasheet->getDistance(),
            'connection' => $datasheet->getConnection(),
            'adjustingAngle' => $datasheet->getAdjustingAngleId()->getValue(),
            'modified' => $datasheet->getUpdateDate(),
            'lastEditor' => $datasheet->getLastEditor()->getUsername(),
            'revision' => $datasheet->getRevision()
        );

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('isRemoteEnabled', TRUE);

        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('DataSheet/datasheet_pdf.html.twig', ['datasheet'=>$datasheet]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($pdfName.".pdf", [
            "Attachment" => false
        ]);

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * @param $id
     */
    public function copyDatasheetAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        // Be careful, you may need to overload the __clone method of your object
        // to set its id to null !
        $clonedObject = clone $object;

        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'Cloned successfully');

        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $clonedObject->getId())));
    }

    /**
     * @param $id
     */
    public function auditListAction($id) {

        $entity = AuditHelper::paramToNamespace('App-Entity-DataSheet');

        /** @var AuditReader $reader */
        $reader = $this->container->get('dh_doctrine_audit.reader');

        if (!$reader->getConfiguration()->isAuditable($entity)) {
            throw $this->createNotFoundException();
        }

        try {
            /** @var AuditEntry $entries */
            $entries = $reader->getAudits($entity,$id);
        } catch (AccessDeniedException $e) {
            throw $this->createAccessDeniedException();
        }

        $sortId = array_column($entries, 'id');
        array_multisort($sortId, SORT_DESC, $entries);

        /** @var DataSheet $datasheet */
        $datasheet = $this->getDatasheet($entries[0]->getObjectId());

        /**
         * @var  $key
         * @var  AuditEntry $entry
         */
        foreach($entries as $key => $entry) {
            $revision = $entry->getDiffs();
            if(isset($revision['revision']['new'])) {
                $revision = $revision['revision']['new'];
            } else {
                $revision = '0';
            }
            $pdfUrl = "/audit_pdf/".$id."/".$entry->getTransactionHash();
            $links[] = array(
                'link' => $pdfUrl,
                'date' => $entry->getCreatedAt(),
                'status' => $entry->getType(),
                'revision' => $revision,
            );
        }

        return $this->render('DataSheet/revisionList_datasheet.html.twig', [
            'links' => $links,
            'lightType' => $datasheet->getLightTypeId(),
            'description' => $datasheet->getDescription()
        ]);
    }

    public function getDatasheet($id) {
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $datasheetRepo = $em->getRepository(DataSheet::class);

        /** @var $datasheet DataSheet*/
        $datasheet = $datasheetRepo->findOneBy(['id' => $id]);

        return $datasheet;
    }
}
