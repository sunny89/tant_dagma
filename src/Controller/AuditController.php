<?php

namespace App\Controller;

use App\Entity\DataSheet;
use App\Service\HelperService;
use DH\DoctrineAuditBundle\Reader\AuditEntry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use DH\DoctrineAuditBundle\Reader\AuditReader;
use DH\DoctrineAuditBundle\Exception\AccessDeniedException;
use DH\DoctrineAuditBundle\Exception\InvalidArgumentException;
use DH\DoctrineAuditBundle\Helper\AuditHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;

class AuditController extends Controller {

    /**
     * @Route("/audit_new/{entity}/{id}", name="app_doctrine_audit_show_entity_history", methods={"GET"})
     *
     * @param Request    $request
     * @param string     $entity
     * @param int|string $id
     *
     * @return Response
     */
    public function getHistory(Request $request, string $entity, $id = null): Response
    {
        $entity = AuditHelper::paramToNamespace($entity);

        $reader = $this->container->get('dh_doctrine_audit.reader');

        if (!$reader->getConfiguration()->isAuditable($entity)) {
            throw $this->createNotFoundException();
        }

        try {
            /** @var AuditEntry $entries */
            $entries = $reader->getAudits($entity,$id);
        } catch (AccessDeniedException $e) {
            throw $this->createAccessDeniedException();
        }

        echo "<p>Anzahl: ".count($entries)."</p>";
        $id = array_column($entries, 'id');
        array_multisort($id, SORT_ASC, $entries);

        foreach($entries as $key => $entry) {
            echo "ID: ".$entry->getId()."<br/>";
            /** @var HelperService $helperService */
            $helperService = new HelperService();
            $revisionNumber = $helperService->revisionNumber($key);
            echo "Revisionsnummer: ".$revisionNumber."<br />";
            //$helperService = new HelperService($key);
            foreach ($entry->getDiffs() as $key => $valueVersion) {
                echo $key.": ".$valueVersion['new']."<br />";
            }
            echo "<hr />";
            var_dump($entry);
            echo "<hr />";
        }

        die();
        /*return $this->render('@DHDoctrineAudit/Audit/entity_history.html.twig', [
            'id' => $id,
            'entity' => $entity,
            'entries' => $entries,
        ]);*/
    }

    /**
     * @Route("/audit_pdf/{id}/{transactionHash}", name="get_history_pdf", methods={"GET"})
     * @param $id
     * @param $transactionHash
     * @return RedirectResponse
     */
    public function getHistoryPDF($id, $transactionHash) {

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $datasheetRepo = $em->getRepository(DataSheet::class);

        /** @var $datasheet DataSheet*/
        $datasheet = $datasheetRepo->findOneBy(['id' => $id]);

        $pdfName = $datasheet->getLightTypeId();

        $datasheet = array(
            'logo' => '/assets/images/Dagma_Logo_small.jpg',
            'id' => $datasheet->getLightTypeId(),
            'description' => $datasheet->getDescription(),
            'voltage' => $datasheet->getVoltageId()->getValue(),
            'dimmerType' => $datasheet->getDimmerTypeId()->getDescription(),
            'frequency' => $datasheet->getFrequencyId()->getValue(),
            'plug' => $datasheet->getPlugId()->getValue(),
            'ip_class' => "IP".$datasheet->getIpClassX().$datasheet->getIpClassY(),
            'driver' => $datasheet->getDriver(),
            'power_of_fitting' => $datasheet->getPowerOfFittingId()->getValue(),
            'number_of_lamp' => $datasheet->getNumberOfLampsId()->getValue(),
            'socket' => $datasheet->getSocketId()->getValue(),
            'type_of_bulb' => $datasheet->getTypeOfBulbId()->getValue(),
            'type_of_lamp' => $datasheet->getTypeOfLampId()->getValue(),
            'power_of_lamp' => $datasheet->getPowerOfLampId()->getValue(),
            'lightpoints' => $datasheet->getLightPointsId()->getValue(),
            'cri' => $datasheet->getCriId()->getValue(),
            'light_colour' => $datasheet->getLightColourId()->getValue(),
            'cover' => $datasheet->getCoverId()->getValue(),
            'beam_angle' => $datasheet->getBeamAngleId()->getValue(),
            'colour_of_shade' => $datasheet->getColourOfShadeId()->getValue(),
            'material_of_shade' => $datasheet->getMaterialOfShadeId()->getValue(),
            'colour_of_housing' => $datasheet->getColourOfHousingId()->getValue(),
            'material_of_housing' => $datasheet->getMaterialOfHousingId()->getValue(),
            'manufacturer' => $datasheet->getManufacturerId()->getName(),
            'supplier' => $datasheet->getSupplierId()->getName(),
            'supplier_street' => $datasheet->getSupplierId()->getStreet(),
            'supplier_location' => $datasheet->getSupplierId()->getLocation(),
            'supplier_country' => $datasheet->getSupplierId()->getCountry(),
            'article_no' => $datasheet->getArticleNo(),
            'supplier_phone' => $datasheet->getSupplierId()->getPhone(),
            'supplier_fax' => $datasheet->getSupplierId()->getFax(),
            'supplier_mail' => $datasheet->getSupplierId()->getEmail(),
            'supplier_zipcode' => $datasheet->getSupplierId()->getZipcode(),
            'skizze' => $datasheet->getFileSkizzeUrl(),
            'image' => $datasheet->getImageUrl(),
            'fixture_light_output' => $datasheet->getFixtureLightOutputId()->getValue(),
            'length' => $datasheet->getLength(),
            'width' => $datasheet->getWidth(),
            'height' => $datasheet->getHeight(),
            'depth' => $datasheet->getDepth(),
            'diameter_complete' => $datasheet->getDiameterComplete(),
            'diameter_cutout' => $datasheet->getDiameterCutout(),
            'mountingDepth' => $datasheet->getMountingDepth(),
            'depth_in_sight' => $datasheet->getDepthInSight(),
            'distance' => $datasheet->getDistance(),
            'connection' => $datasheet->getConnection(),
            'adjustingAngle' => $datasheet->getAdjustingAngleId()->getValue(),
            'modified' => $datasheet->getUpdateDate(),
            'lastEditor' => $datasheet->getLastEditor()->getUsername(),
            'revision' => $datasheet->getRevision()
        );

        $entity = AuditHelper::paramToNamespace('App-Entity-DataSheet');

        /** @var AuditReader $reader */
        $reader = $this->container->get('dh_doctrine_audit.reader');

        if (!$reader->getConfiguration()->isAuditable($entity)) {
            throw $this->createNotFoundException();
        }

        try {
            /** @var Audits $entry */
            $entry = $reader->getAuditsByTransactionHash($transactionHash);
        } catch (AccessDeniedException $e) {
            throw $this->createAccessDeniedException();
        }

        foreach($entry as $audit) {
            foreach ($audit as $diffs) {
                foreach ($diffs->getDiffs() as $key => $valueVersion) {
                    $datasheet[$key] = $valueVersion['new'];
                }
            }
        }
        
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('isRemoteEnabled', TRUE);

        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('DataSheet/datasheet_pdf.html.twig', ['datasheet'=>$datasheet]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($pdfName.".pdf", [
            "Attachment" => false
        ]);

        return new RedirectResponse('/');
    }
}