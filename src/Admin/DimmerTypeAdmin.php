<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class DimmerTypeAdmin extends AbstractAdmin
{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('typ', null, array('label' => 'label.typ', 'required' => true))
            ->add('description', null, array('label' => 'label.description', 'required' => true))
            ->add('cable_type', null, array('label' => 'label.cable_typ', 'required' => true));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('typ')
            ->add('description')
            ->add('cable_type');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('typ')
            ->addIdentifier('description')
            ->addIdentifier('cable_type')
            ->addIdentifier('_action', 'actions', array(
                'label' => 'label.action',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }
}