<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class IPClassAdmin extends AbstractAdmin
{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('ipClassId', null, array('label' => 'label.ip_class_id', 'required' => true))
            ->add('description', null, array('label' => 'label.description', 'required' => true));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('ipClassId');
        $datagridMapper->add('description');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('ipClassId')
            ->addIdentifier('description')
            ->addIdentifier('_action', 'actions', array(
                'label' => 'label.action',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }
}