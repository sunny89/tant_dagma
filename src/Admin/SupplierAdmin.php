<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

final class SupplierAdmin extends AbstractAdmin
{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label' => 'label.name', 'required' => true))
            ->add('street', null, array('label' => 'label.street', 'required' => true))
            ->add('location', null, array('label' => 'label.location', 'required' => true))
            ->add('zipcode', null, array('label' => 'label.zipcode', 'required' => true))
            ->add('phone', null, array('label' => 'label.phone', 'required' => true))
            ->add('fax', null, array('label' => 'label.fax', 'required' => true))
            ->add('email', null, array('label' => 'label.email', 'required' => true))
            ->add('country', null, array('label' => 'label.country', 'required' => true));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('email')
            ->addIdentifier('_action', 'actions', array(
                'label' => 'label.action',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }
}