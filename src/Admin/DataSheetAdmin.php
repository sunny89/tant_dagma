<?php
namespace App\Admin;

use App\Entity\DataSheet;
use App\Entity\User;
use App\Service\HelperService;
use App\Service\LightTypeIdHelperService;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\VarDumper\Cloner\Data;

final class DataSheetAdmin extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('createPDF', $this->getRouterIdParameter().'/createPDF')
            ->add('copyDatasheet', $this->getRouterIdParameter().'/copyDatasheet')
            ->add('auditList', $this->getRouterIdParameter().'/auditList')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Image instance
        $dataSheet = $this->getSubject();
        $fileFieldOptionsSkizze = $this->getFileFieldOptionsSkizze($dataSheet);
        $fileFieldOptionsImage = $this->getFileFieldOptionsImage($dataSheet);

        $formMapper
            ->with('label.electrical_and_related_attributes', array('class' => 'col-md-6'))
            ->add('main_category', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('subcategory', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('subclass', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('subclass_description', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('bk', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('light_type_id', null, array('label' => 'label.light_type_id', 'attr' => array('readonly' => true)))
            ->add('light_type', null, array('label' => 'label.light_type', 'attr' => array('readonly' => true)))
            ->add('description', null, array('label' => 'label.description'))
            ->add('voltage_id', null, array('label' => 'label.voltage'))
            ->add('frequency_id', null, array('label' => 'label.frequency'))
            ->add('powerOfFitting_id', null, array('label' => 'label.power_of_fitting'))
            ->add('socket_id', null, array('label' => 'label.socket'))
            ->add('typeOfLamp_id', null, array('label' => 'label.type_of_lamp'))
            ->add('dimmerType_id', null, array('label' => 'label.dimmer_type'))
            ->add('plug_id', null, array('label' => 'label.plug'))
            ->add('driver', null, array('label' => 'label.driver'))
            ->add('numberOfLamps_id', null, array('label' => 'label.number_of_lamps'))
            ->add('typeOfBulb_id', null, array('label' => 'label.type_of_bulb'))
            ->add('powerOfLamp_id', null, array('label' => 'label.power_of_lamp'))
            ->add('lightPoints_id', null, array('label' => 'label.lightpoints'))
            ->add('adjustingAngle_id', null, array('label' => 'label.adjusting_angle'))
            ->end();

        $formMapper
            ->with('label.light_information', array('class' => 'col-md-6 col-xs-12'))
            ->add('cri_id', null, array('label' => 'label.cri'))
            ->add('cover_id', null, array('label' => 'label.cover'))
            ->add('colourOfShade_id', null, array('label' => 'label.colour_of_shade'))
            ->add('colourOfHousing_id', null, array('label' => 'label.colour_of_housing'))
            ->add('light_colour_id', null, array('label' => 'label.light_colour'))
            ->add('beam_angle_id', null, array('label' => 'label.beam_angle'))
            ->add('material_of_shade_id', null, array('label' => 'label.material_of_shade'))
            ->add('material_of_housing_id', null, array('label' => 'label.material_of_housing'))
            ->add('fixture_light_output_id', null, array('label' => 'label.fixture_light_output'))
            ->end();

        $formMapper
            ->with('label.remarks', array('class' => 'col-md-6 col-xs-12'))
            ->add('remarks', null, array(
                'label' => 'label.remarks',
                'attr' => array(
                    'rows' => '4'
                )))
            ->end();

        $formMapper
            ->with('label.contact', array('class' => 'col-md-6 col-xs-12'))
            ->add('manufacturer_id', null, array('label' => 'label.manufacturer'))
            ->add('supplier_id', null, array('label' => 'label.supplier'))
            ->add('articleNo', null, array('label' => 'label.articleNo'))
            ->end();

        $formMapper
            ->with('label.ip_class', array('class' => 'col-md-12 col-xs-12'))
            ->add('ip_class_x',ChoiceType::class, array(
                'label' => 'label.ip_class_x',
                'choices' => [
                    'label.ip_class_x_0' => '0',
                    'label.ip_class_x_1' => '1',
                    'label.ip_class_x_2' => '2',
                    'label.ip_class_x_3' => '3',
                    'label.ip_class_x_4' => '4',
                    'label.ip_class_x_5' => '5',
                    'label.ip_class_x_6' => '6'
                ]
            ))
            ->add('ip_class_y', ChoiceType::class, array(
                'label' => 'label.ip_class_y',
                'choices' => [
                    "label.ip_class_y_0" => "0",
                    "label.ip_class_y_1" => "1",
                    "label.ip_class_y_2" => "2",
                    "label.ip_class_y_3° gegen die Senkrechte" => "3",
                    "label.ip_class_y_4" => "4",
                    "label.ip_class_y_4K" => "4K",
                    "label.ip_class_y_5" => "5",
                    "label.ip_class_y_6" => "6",
                    "label.ip_class_y_6K" => "6K",
                    "label.ip_class_y_7" => "7",
                    "label.ip_class_y_8" => "8",
                    "label.ip_class_y_9" => "9"
                ]
            ))
            ->end();

        $formMapper
            ->with('label.dimensions', array('class' => 'col-md-6 col-xs-12 clear'))
            ->add('length', null, array('label' => 'label.length'))
            ->add('width', null, array('label' => 'label.width'))
            ->add('height', null, array('label' => 'label.height'))
            ->add('depth', null, array('label' => 'label.depth'))
            ->add('diameter_complete', null, array('label' => 'label.diameter_complete'))
            ->add('diameter_cutout', null, array('label' => 'label.diameter_cutout'))
            ->add('mountingDepth', null, array('label' => 'label.mounting_depth'))
            ->add('depth_in_sight', null, array('label' => 'label.depth_in_sight'))
            ->add('distance', null, array('label' => 'label.distance'))
            ->add('connection', null, array('label' => 'label.connection'))
            ->end();

        $formMapper
            ->with('label.dimensions_skizze', array('class' => 'col-md-6 col-xs-12 dimensions-skizze'))
            ->end();

        $formMapper
            ->with('label.skizze', array('class' => 'col-md-6 col-xs-12 clear'))
            ->add('fileSkizze', FileType::class, $fileFieldOptionsSkizze)
            ->end();

        $formMapper
            ->with('label.image', array('class' => 'col-md-6 col-xs-12'))
            ->add('image', FileType::class, $fileFieldOptionsImage)
            ->end();

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('label.electrical_and_related_attributes', array('class' => 'col-md-6'))
            ->add('main_category', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('subcategory', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('subclass', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('subclass_description', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('bk', HiddenType::class, array('attr' => array('hidden' => true)))
            ->add('light_type_id', null, array('label' => 'label.light_type_id', 'attr' => array('readonly' => true)))
            ->add('light_type', null, array('label' => 'label.light_type', 'attr' => array('readonly' => true)))
            ->add('description', null, array('label' => 'label.description'))
            ->add('voltage_id', null, array('label' => 'label.voltage'))
            ->add('frequency_id', null, array('label' => 'label.frequency'))
            ->add('powerOfFitting_id', null, array('label' => 'label.power_of_fitting'))
            ->add('socket_id', null, array('label' => 'label.socket'))
            ->add('typeOfLamp_id', null, array('label' => 'label.type_of_lamp'))
            ->add('dimmerType_id', null, array('label' => 'label.dimmer_type'))
            ->add('plug_id', null, array('label' => 'label.plug'))
            ->add('driver', null, array('label' => 'label.driver'))
            ->add('numberOfLamps_id', null, array('label' => 'label.number_of_lamps'))
            ->add('typeOfBulb_id', null, array('label' => 'label.type_of_bulb'))
            ->add('powerOfLamp_id', null, array('label' => 'label.power_of_lamp'))
            ->add('lightPoints_id', null, array('label' => 'label.lightpoints'))
            ->add('adjustingAngle_id', null, array('label' => 'label.adjusting_angle'))
            ->end();

        $showMapper
            ->with('label.light_information', array('class' => 'col-md-6 col-xs-12'))
            ->add('cri_id', null, array('label' => 'label.cri'))
            ->add('cover_id', null, array('label' => 'label.cover'))
            ->add('colourOfShade_id', null, array('label' => 'label.colour_of_shade'))
            ->add('colourOfHousing_id', null, array('label' => 'label.colour_of_housing'))
            ->add('light_colour_id', null, array('label' => 'label.light_colour'))
            ->add('beam_angle_id', null, array('label' => 'label.beam_angle'))
            ->add('material_of_shade_id', null, array('label' => 'label.material_of_shade'))
            ->add('material_of_housing_id', null, array('label' => 'label.material_of_housing'))
            ->add('fixture_light_output_id', null, array('label' => 'label.fixture_light_output'))
            ->end();

        $showMapper
            ->with('label.remarks', array('class' => 'col-md-6 col-xs-12'))
            ->add('remarks', null, array(
                'label' => 'label.remarks',
                'attr' => array(
                    'rows' => '4'
                )))
            ->end();

        $showMapper
            ->with('label.contact', array('class' => 'col-md-6 col-xs-12'))
            ->add('manufacturer_id', null, array('label' => 'label.manufacturer'))
            ->add('supplier_id', null, array('label' => 'label.supplier'))
            ->add('articleNo', null, array('label' => 'label.articleNo'))
            ->end();

        $showMapper
            ->with('label.ip_class', array('class' => 'col-md-12 col-xs-12'))
            ->add('ip_class_x',ChoiceType::class, array(
                'label' => 'label.ip_class_x',
                'choices' => [
                    'label.ip_class_x_0' => '0',
                    'label.ip_class_x_1' => '1',
                    'label.ip_class_x_2' => '2',
                    'label.ip_class_x_3' => '3',
                    'label.ip_class_x_4' => '4',
                    'label.ip_class_x_5' => '5',
                    'label.ip_class_x_6' => '6'
                ]
            ))
            ->add('ip_class_y', ChoiceType::class, array(
                'label' => 'label.ip_class_y',
                'choices' => [
                    "label.ip_class_y_0" => "0",
                    "label.ip_class_y_1" => "1",
                    "label.ip_class_y_2" => "2",
                    "label.ip_class_y_3° gegen die Senkrechte" => "3",
                    "label.ip_class_y_4" => "4",
                    "label.ip_class_y_4K" => "4K",
                    "label.ip_class_y_5" => "5",
                    "label.ip_class_y_6" => "6",
                    "label.ip_class_y_6K" => "6K",
                    "label.ip_class_y_7" => "7",
                    "label.ip_class_y_8" => "8",
                    "label.ip_class_y_9" => "9"
                ]
            ))
            ->end();

        $showMapper
            ->with('label.dimensions', array('class' => 'col-md-6 col-xs-12 clear'))
            ->add('length', null, array('label' => 'label.length'))
            ->add('width', null, array('label' => 'label.width'))
            ->add('height', null, array('label' => 'label.height'))
            ->add('depth', null, array('label' => 'label.depth'))
            ->add('diameter_complete', null, array('label' => 'label.diameter_complete'))
            ->add('diameter_cutout', null, array('label' => 'label.diameter_cutout'))
            ->add('mountingDepth', null, array('label' => 'label.mounting_depth'))
            ->add('depth_in_sight', null, array('label' => 'label.depth_in_sight'))
            ->add('distance', null, array('label' => 'label.distance'))
            ->add('connection', null, array('label' => 'label.connection'))
            ->end();

        $showMapper
            ->with('label.dimensions_skizze', array('class' => 'col-md-6 col-xs-12 dimensions-skizze'))
            ->end();

        $showMapper
            ->with('label.skizze', array('class' => 'col-md-6 col-xs-12 clear'))
            ->add('fileSkizze', FileType::class)
            ->end();

        $showMapper
            ->with('label.image', array('class' => 'col-md-6 col-xs-12'))
            ->add('image', FileType::class)
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('light_type_id', null, array('label' => 'label.light_type_id'))
            ->add('light_type', null, array('label' => 'label.light_type'))
            ->add('description', null, array('label' => 'label.description'))
            ->add('voltage_id', null, array('label' => 'label.voltage'))
            ->add('frequency_id', null, array('label' => 'label.frequency'))
            ->add('powerOfFitting_id', null, array('label' => 'label.power_of_fitting'))
            ->add('socket_id', null, array('label' => 'label.socket'))
            ->add('typeOfLamp_id', null, array('label' => 'label.type_of_lamp'))
            ->add('dimmerType_id', null, array('label' => 'label.dimmer_type'))
            ->add('plug_id', null, array('label' => 'label.plug'))
            ->add('driver', null, array('label' => 'label.driver'))
            ->add('numberOfLamps_id', null, array('label' => 'label.number_of_lamps'))
            ->add('typeOfBulb_id', null, array('label' => 'label.type_of_bulb'))
            ->add('powerOfLamp_id', null, array('label' => 'label.power_of_lamp'))
            ->add('lightPoints_id', null, array('label' => 'label.lightpoints'))
            ->add('adjustingAngle_id', null, array('label' => 'label.adjusting_angle'))
            ->add('cri_id', null, array('label' => 'label.cri'))
            ->add('cover_id', null, array('label' => 'label.cover'))
            ->add('colourOfShade_id', null, array('label' => 'label.colour_of_shade'))
            ->add('colourOfHousing_id', null, array('label' => 'label.colour_of_housing'))
            ->add('light_colour_id', null, array('label' => 'label.light_colour'))
            ->add('beam_angle_id', null, array('label' => 'label.beam_angle'))
            ->add('material_of_shade_id', null, array('label' => 'label.material_of_shade'))
            ->add('material_of_housing_id', null, array('label' => 'label.material_of_housing'))
            ->add('fixture_light_output_id', null, array('label' => 'label.fixture_light_output'))
            ->add('ip_class_x', null, array('label' => 'label.ip_class_x'))
            ->add('ip_class_y', null, array('label' => 'label.ip_class_y'))
            ->add('length', null, array('label' => 'label.length'))
            ->add('width', null, array('label' => 'label.width'))
            ->add('height', null, array('label' => 'label.height'))
            ->add('depth', null, array('label' => 'label.depth'))
            ->add('diameter_complete', null, array('label' => 'label.diameter_complete'))
            ->add('diameter_cutout', null, array('label' => 'label.diameter_cutout'))
            ->add('mountingDepth', null, array('label' => 'label.mounting_depth'))
            ->add('depth_in_sight', null, array('label' => 'label.depth_in_sight'))
            ->add('distance', null, array('label' => 'label.distance'))
            ->add('connection', null, array('label' => 'label.connection'))
        ;

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('light_type_id')
            ->addIdentifier('description')
            ->addIdentifier('_action', 'actions', array(
                'label' => 'label.action',
                'actions' => array(
                    'show' => arraY(),
                    'edit' => array(),
                    'delete' => array(),
                    'actions' => [
                        'template' => 'CRUD/list__action_datasheet.html.twig',
                    ]
                )
            ));
    }

    /**
     * @param $dataSheet DataSheet
     */
    public function prePersist($dataSheet)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $entityManager = $container->get('doctrine.orm.entity_manager');

        /** @var $user User */
        $user= $container->get('security.token_storage')->getToken()->getUser();

        /** @var LightTypeIdHelperService $helperService */
        $helperService = new LightTypeIdHelperService($entityManager);

        $helperService->actionLightTypeId($dataSheet,'create');

        $this->manageFileUpload($dataSheet);

        $dataSheet->setLastEditor($user);

        $dataSheet->setRevision('0');

        $entityManager->flush();
    }

    /**
     * @param $dataSheet DataSheet
     */
    public function preUpdate($dataSheet)
    {
        /** Manage File Upload */
        $this->manageFileUpload($dataSheet);

        $container = $this->getConfigurationPool()->getContainer();
        /** @var $user User */
        $user= $container->get('security.token_storage')->getToken()->getUser();
        $dataSheet->setLastEditor($user);

        /** @var HelperService $helperService */
        $helperService = new HelperService();
        $revisionNumber = $helperService->revisionNumber($dataSheet->getRevision());
        $dataSheet->setRevision($revisionNumber);

        /* $em_uow = $this->getModelManager()->getEntityManager($this->getClass());
        $uow = $em_uow->getUnitOfWork();
        $changeset = $uow->getEntityChangeSet($dataSheet);

        $em_uow = $this->getModelManager()->getEntityManager($this->getClass());
        $originalData = $em_uow->getUnitOfWork()->getOriginalEntityData($dataSheet);
        $toArrayEntity = (array) $dataSheet;

        $changes = array_diff_assoc($toArrayEntity, $originalData);

        var_dump($changes);

        if(!empty($changeset)) {

        }*/


    }

    public function preRemove($dataSheet)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $entityManager = $container->get('doctrine.orm.entity_manager');

        /** @var LightTypeIdHelperService $helperService */
        $helperService = new LightTypeIdHelperService($entityManager);

        $helperService->setClearLightTypeId($dataSheet);
    }


    /**
     * @param $image DataSheet
     */
    private function manageFileUpload($dataSheet)
    {
        if ($dataSheet->getFileSkizze() || $dataSheet->getImage()) {
            $dataSheet->refreshUpdated();
        }
    }

    /**
     * @param $fileSkizze DataSheet
     * @return array
     */
    private function getFileFieldOptionsSkizze($fileSkizze)
    {
        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'label.skizze');

        if ($fileSkizze->getFileSkizzeUrl() != '' OR $fileSkizze->getFileSkizzeUrl() != null) {
            $fileFieldOptions['help'] = '<img src="' .  $fileSkizze->getFileSkizzeUrl() . '" class="admin-preview" width="100%"/>';
        } else {
            $fileFieldOptions['help'] = '';
        }

        return $fileFieldOptions;
    }

    /**
     * @param $image DataSheet
     * @return array
     */
    private function getFileFieldOptionsImage($image)
    {
        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'label.image');

        if ($image->getImageUrl() != '' OR $image->getImageUrl() != null) {
            $fileFieldOptions['help'] = '<img src="'  . $image->getImageUrl() . '" class="admin-preview" width="100%"/>';
        } else {
            $fileFieldOptions['help'] = '';
        }

        return $fileFieldOptions;
    }

}