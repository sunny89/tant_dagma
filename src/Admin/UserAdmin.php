<?php
namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\UserBundle\Model\UserManagerInterface;

final class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username', null, array('label' => 'label.username', 'required' => true))
            ->add('firstname', null, array('label' => 'label.firstname', 'required' => true))
            ->add('lastname', null, array('label' => 'label.lastname', 'required' => true))
            ->add('email', null, array('label' => 'label.email', 'required' => true))
            ->add('enabled', null, array('label' => 'label.enable', 'required' => true))
            ->add('role', ChoiceType::class, array(
                'label' => 'label.role',
                'required' => true,
                'choices'  => [
                    'MASTERUSER' => 'ROLE_MASTERUSER',
                    'LIGHTADMIN' => 'ROLE_LIGHTADMIN',
                    'DIMMERADMIN' => 'ROLE_DIMMERADMIN',
                    'USER' => 'ROLE_EDITOR'

                ],
                'expanded' => true,
                'multiple' => false))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Password confirmation')
            ))

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
        $datagridMapper->add('firstname');
        $datagridMapper->add('lastname');
        $datagridMapper->add('email');
        $datagridMapper->add('role');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->addIdentifier('firstname')
            ->addIdentifier('lastname')
            ->addIdentifier('email')
            ->addIdentifier('role')
            ->addIdentifier('enabled')
            ->addIdentifier('_action', 'actions', array(
                'label' => 'label.action',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @param $object User
     */
    public function prePersist($object) {
        if($object->getPlainPassword() != '') {
            $plainPassword = $object->getPlainPassword();
            $container = $this->getConfigurationPool()->getContainer();
            $encoder = $container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($object, $plainPassword);
            $object->setPassword($encoded);
            $this->getUserManager()->updatePassword($object);
        }
        $object->setRoles(array($object->getRole()));
        $this->getUserManager()->updateUser($object);
        $this->getUserManager()->updateCanonicalFields($object);
    }

    /**
     * @param $object User
     */
    public function preUpdate($object) {
        $this->prePersist($object);
    }

    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }
}