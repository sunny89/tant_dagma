<?php

namespace App\Service;

use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Scheb\TwoFactorBundle\Mailer\AuthCodeMailerInterface;

class CustomAuthMailer implements AuthCodeMailerInterface
{
    public function sendAuthCode(TwoFactorInterface $user): void
    {
        $authCode = $user->getEmailAuthCode();

        $message = new \Swift_Message();
        $message
            ->setTo('tina.henkensiefken@me.com')
            ->setFrom('noreply@steuerbar.systems')
            ->setSubject('Authentication Code')
            ->setBody($authCode)
        ;
        $this->mailer->send($message);

    }
}