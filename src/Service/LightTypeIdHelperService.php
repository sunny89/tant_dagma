<?php

namespace App\Service;

use App\Entity\DataSheet;
use App\Entity\LightTypeIdHelperTable;
use App\Repository\LightTypeIdHelperTableRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class LightTypeIdHelperService {

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getLastIdFromType($type) {
        $helperTable = $this->em->getRepository(LightTypeIdHelperTable::class);
        $lastId = $helperTable->findOneBy(array('light_type' => $type, 'entry_type' => 'last'));
        return $lastId;
    }

    public function getClearIdFromType($type) {
        $helperTable = $this->em->getRepository(LightTypeIdHelperTable::class);
        $clearId = $helperTable->findOneBy(array('light_type' => $type, 'entry_type' => 'clear'));
        return $clearId;
    }

    public function removeLightTypeId(LightTypeIdHelperTable $lightTypeId) {
        $this->em->remove($lightTypeId);
        $this->em->flush();
    }

    public function setClearLightTypeId(DataSheet $dataSheet) {
        $clearId = $dataSheet->getLightTypeId();
        $clearId = strstr($clearId, '0');
        $lightTypeIdObject = new LightTypeIdHelperTable();
        $lightTypeIdObject->setLightType($dataSheet->getLightType());
        $lightTypeIdObject->setEntryType('clear');
        $lightTypeIdObject->setNumber($clearId);
        $this->em->persist($lightTypeIdObject);
    }

    public function setNewLightTypeId($oldLightTypeId, $lightType) {
        $newLightTypeId = ($oldLightTypeId);

        $newLightTypeId = str_pad($newLightTypeId, 6 ,'0', STR_PAD_LEFT);

        $lightTypeIdObject = new LightTypeIdHelperTable();
        $lightTypeIdObject->setLightType($lightType);
        $lightTypeIdObject->setEntryType('last');
        $lightTypeIdObject->setNumber($newLightTypeId);
        $this->em->persist($lightTypeIdObject);
    }

    public function actionLightTypeId($datasheet) {
        $lightType = $datasheet->getLightType();

        /**
         * @var LightTypeIdHelperTable $lightTypeId
         */
        $lastClearId = $this->getClearIdFromType($lightType);

        /**
         * @var LightTypeIdHelperTable $lightTypeId
         */
        $lastId = $this->getLastIdFromType($lightType);

        if(!empty($lastClearId)) {
            $this->removeLightTypeId($lastClearId);
            $datasheet->setLightTypeId($datasheet->getLightType().$lastClearId->getNumber());
            $this->em->persist($datasheet);
            $this->em->flush();
            return;
        }

        if(!empty($lastId)) {
            $this->removeLightTypeId($lastId);
            $newId = ($lastId->getNumber()+1);
            $newId = str_pad($newId, 6 ,'0', STR_PAD_LEFT);
            $this->setNewLightTypeId($newId, $datasheet->getLightType());
            $datasheet->setLightTypeId($datasheet->getLightType().$newId);
            $this->em->persist($datasheet);
            $this->em->flush();
            return;
        }

        /** Check removeId isnt available else create new */
        if(empty($lastId) && empty($lastClearId)) {
            $lightTypeIdObject = new LightTypeIdHelperTable();
            $lightTypeIdObject->setLightType($lightType);
            $lightTypeIdObject->setEntryType('last');
            $lightTypeIdObject->setNumber('000001');
            $this->em->persist($lightTypeIdObject);

            $datasheet->setLightTypeId($lightType.$lightTypeIdObject->getNumber());
            $this->em->persist($datasheet);
            $this->em->flush();
        }
    }
}