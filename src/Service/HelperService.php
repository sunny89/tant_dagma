<?php

namespace App\Service;

class HelperService {
    public function revisionNumber($lastRevision) {
        $revisionNumber = array(
            0,
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            'AA',
            'AB',
            'AC',
            'AD',
            'AE'
        );

        $lastRevisionKey = array_search($lastRevision, $revisionNumber);
        $newRevisionNumber = $revisionNumber[$lastRevisionKey+1];

        return $newRevisionNumber;
    }
}