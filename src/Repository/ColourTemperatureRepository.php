<?php

namespace App\Repository;

use App\Entity\ColourTemperature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ColourTemperature|null find($id, $lockMode = null, $lockVersion = null)
 * @method ColourTemperature|null findOneBy(array $criteria, array $orderBy = null)
 * @method ColourTemperature[]    findAll()
 * @method ColourTemperature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ColourTemperatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ColourTemperature::class);
    }

    // /**
    //  * @return ColourTemperature[] Returns an array of ColourTemperature objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ColourTemperature
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
