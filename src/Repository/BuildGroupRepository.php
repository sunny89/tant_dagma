<?php

namespace App\Repository;

use App\Entity\BuildGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BuildGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method BuildGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method BuildGroup[]    findAll()
 * @method BuildGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BuildGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BuildGroup::class);
    }

    // /**
    //  * @return BuildGroup[] Returns an array of BuildGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BuildGroup
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
