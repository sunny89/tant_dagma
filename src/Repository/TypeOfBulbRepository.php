<?php

namespace App\Repository;

use App\Entity\TypeOfBulb;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeOfBulb|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfBulb|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfBulb[]    findAll()
 * @method TypeOfBulb[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfBulbRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeOfBulb::class);
    }

    // /**
    //  * @return TypeOfBulb[] Returns an array of TypeOfBulb objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOfBulb
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
