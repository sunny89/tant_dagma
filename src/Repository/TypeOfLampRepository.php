<?php

namespace App\Repository;

use App\Entity\TypeOfLamp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeOfLamp|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfLamp|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfLamp[]    findAll()
 * @method TypeOfLamp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfLampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeOfLamp::class);
    }

    // /**
    //  * @return TypeOfLamp[] Returns an array of TypeOfLamp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeOfLamp
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
