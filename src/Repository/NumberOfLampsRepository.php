<?php

namespace App\Repository;

use App\Entity\NumberOfLamps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NumberOfLamps|null find($id, $lockMode = null, $lockVersion = null)
 * @method NumberOfLamps|null findOneBy(array $criteria, array $orderBy = null)
 * @method NumberOfLamps[]    findAll()
 * @method NumberOfLamps[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NumberOfLampsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NumberOfLamps::class);
    }

    // /**
    //  * @return NumberOfLamps[] Returns an array of NumberOfLamps objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NumberOfLamps
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
