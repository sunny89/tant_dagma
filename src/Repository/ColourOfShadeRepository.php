<?php

namespace App\Repository;

use App\Entity\ColourOfShade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ColourOfShade|null find($id, $lockMode = null, $lockVersion = null)
 * @method ColourOfShade|null findOneBy(array $criteria, array $orderBy = null)
 * @method ColourOfShade[]    findAll()
 * @method ColourOfShade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ColourOfShadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ColourOfShade::class);
    }

    // /**
    //  * @return ColourOfShade[] Returns an array of ColourOfShade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ColourOfShade
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
