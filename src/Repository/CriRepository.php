<?php

namespace App\Repository;

use App\Entity\Cri;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cri|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cri|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cri[]    findAll()
 * @method Cri[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CriRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cri::class);
    }

    // /**
    //  * @return Cri[] Returns an array of Cri objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cri
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
