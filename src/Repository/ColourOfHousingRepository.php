<?php

namespace App\Repository;

use App\Entity\ColourOfHousing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ColourOfHousing|null find($id, $lockMode = null, $lockVersion = null)
 * @method ColourOfHousing|null findOneBy(array $criteria, array $orderBy = null)
 * @method ColourOfHousing[]    findAll()
 * @method ColourOfHousing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ColourOfHousingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ColourOfHousing::class);
    }

    // /**
    //  * @return ColourOfHousing[] Returns an array of ColourOfHousing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ColourOfHousing
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
