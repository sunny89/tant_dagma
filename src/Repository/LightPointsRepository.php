<?php

namespace App\Repository;

use App\Entity\LightPoints;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LightPoints|null find($id, $lockMode = null, $lockVersion = null)
 * @method LightPoints|null findOneBy(array $criteria, array $orderBy = null)
 * @method LightPoints[]    findAll()
 * @method LightPoints[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LightPointsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LightPoints::class);
    }

    // /**
    //  * @return LightPoints[] Returns an array of LightPoints objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LightPoints
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
