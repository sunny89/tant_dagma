<?php

namespace App\Repository;

use App\Entity\DimmerType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DimmerType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DimmerType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DimmerType[]    findAll()
 * @method DimmerType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DimmerTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DimmerType::class);
    }

    // /**
    //  * @return DimmerType[] Returns an array of DimmerType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DimmerType
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
