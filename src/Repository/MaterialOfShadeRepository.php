<?php

namespace App\Repository;

use App\Entity\MaterialOfShade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MaterialOfShade|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialOfShade|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialOfShade[]    findAll()
 * @method MaterialOfShade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialOfShadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaterialOfShade::class);
    }

    // /**
    //  * @return FixtureLightOutput[] Returns an array of FixtureLightOutput objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FixtureLightOutput
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
