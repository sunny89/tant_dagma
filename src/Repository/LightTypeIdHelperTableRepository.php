<?php

namespace App\Repository;

use App\Entity\LightTypeIdHelperTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LightTypeIdHelperTable|null find($id, $lockMode = null, $lockVersion = null)
 * @method LightTypeIdHelperTable|null findOneBy(array $criteria, array $orderBy = null)
 * @method LightTypeIdHelperTable[]    findAll()
 * @method LightTypeIdHelperTable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LightTypeIdHelperTableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LightTypeIdHelperTable::class);
    }

    // /**
    //  * @return LightNumberHelpTable[] Returns an array of LightNumberHelpTable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LightNumberHelpTable
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
