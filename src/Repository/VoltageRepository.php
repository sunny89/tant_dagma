<?php

namespace App\Repository;

use App\Entity\Voltage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Voltage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Voltage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Voltage[]    findAll()
 * @method Voltage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoltageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Voltage::class);
    }

    // /**
    //  * @return Voltage[] Returns an array of Voltage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Voltage
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
