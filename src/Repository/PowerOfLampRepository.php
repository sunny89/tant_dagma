<?php

namespace App\Repository;

use App\Entity\PowerOfLamp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PowerOfLamp|null find($id, $lockMode = null, $lockVersion = null)
 * @method PowerOfLamp|null findOneBy(array $criteria, array $orderBy = null)
 * @method PowerOfLamp[]    findAll()
 * @method PowerOfLamp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PowerOfLampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PowerOfLamp::class);
    }

    // /**
    //  * @return PowerOfLamp[] Returns an array of PowerOfLamp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PowerOfLamp
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
