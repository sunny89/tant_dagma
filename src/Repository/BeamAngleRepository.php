<?php

namespace App\Repository;

use App\Entity\BeamAngle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BeamAngle|null find($id, $lockMode = null, $lockVersion = null)
 * @method BeamAngle|null findOneBy(array $criteria, array $orderBy = null)
 * @method BeamAngle[]    findAll()
 * @method BeamAngle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeamAngleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BeamAngle::class);
    }

    // /**
    //  * @return FixtureLightOutput[] Returns an array of FixtureLightOutput objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FixtureLightOutput
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
