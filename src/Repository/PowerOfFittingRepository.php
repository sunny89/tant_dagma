<?php

namespace App\Repository;

use App\Entity\PowerOfFitting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PowerOfFitting|null find($id, $lockMode = null, $lockVersion = null)
 * @method PowerOfFitting|null findOneBy(array $criteria, array $orderBy = null)
 * @method PowerOfFitting[]    findAll()
 * @method PowerOfFitting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PowerOfFittingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PowerOfFitting::class);
    }

    // /**
    //  * @return PowerOfFitting[] Returns an array of PowerOfFitting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PowerOfFitting
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
