<?php

namespace App\Repository;

use App\Entity\AdjustingAngle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AdjustingAngle|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdjustingAngle|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdjustingAngle[]    findAll()
 * @method AdjustingAngle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdjustingAngleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdjustingAngle::class);
    }

    // /**
    //  * @return Adjustangle[] Returns an array of Adjustangle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Adjustangle
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
