$( document ).ready(function() {
    $("#wizard_subcategory").chained("#wizard_maincategory");
    $("#wizard_subclass").chained("#wizard_subcategory");
    $("#wizard_description").chained("#wizard_subclass");
    $("#wizard_bk").chained("#wizard_description");

    $("#wizard_maincategory").change(function() {
        var selectedWizardMainCategory = $(this).children("option:selected").text();
        $("[id$=main_category]").val(selectedWizardMainCategory);
    });

    $("#wizard_subcategory").change(function() {
        var selectedWizardSubCategory = $(this).children("option:selected").text();
        $("[id$=subcategory]").val(selectedWizardSubCategory);
    });

    $("#wizard_subclass").change(function() {
        var selectedWizardSubClass = $(this).children("option:selected").text();
        $("[id$=subclass]").val(selectedWizardSubClass);
    });

    $("#wizard_description").change(function() {
        var selectedWizardDescription = $(this).children("option:selected").text();
        $("[id$=subclass_description]").val(selectedWizardDescription);
    });

    $("#wizard_bk").change(function() {
        var selectedWizardBK = $('#s2id_wizard_bk').select2('data').id;
        var selectedWizardBKData = $(this).children("option:selected").data('end');
        $("[id$=bk]").val(selectedWizardBK);
        $("[id$=light_type]").val(selectedWizardBKData);

        dimensions_color(selectedWizardBKData, selectedWizardBK);
    });

    var url = window.location.href;
    var selectedWizardBKData = $("input[id$=light_type]").val();
    var selectedWizardBK = $('#s2id_wizard_bk').select2('data').id;

    if(url.indexOf("edit") >=0) {
        dimensions_color(selectedWizardBKData, selectedWizardBK);
    };
});

function dimensions_color(light_type, bk_number) {
    var ceiling = ['05','12','13','14','16'];
    var exit = ['31'];
    var bollard = ['18b'];
    var downlight = ['01','02','03','04d','18'];
    var festoon = ['11f'];
    var floodlight = ['15,18f'];
    var floorlamp = ['18f'];
    var led_line = ['17','19l'];
    var led_panel = ['19p'];
    var pendel = ['11p','19p'];
    var tablelamp = ['06','09','10'];
    var tracklight = ['04t'];
    var transformer = ['21','30'];
    var walllamp = ['08','18'];
    var default_group = ['07','19','20','40','90','93','94','95'];
    

    $("[id$=_length].form-group").hide();
    $("[id$=_length] label").addClass('color-darkred');
    $("[id$=_length] div input").addClass('border-color-darkred');

    $("[id$=_width].form-group").hide();
    $("[id$=_width] label").addClass('color-blue');
    $("[id$=_width] div input").addClass('border-color-blue');

    $("[id$=_height].form-group").hide();
    $("[id$=_height] label").addClass('color-red');
    $("[id$=_height] div input").addClass('border-color-red');

    $("[id$=_depth].form-group").hide();
    $("[id$=_depth] label").addClass('color-darkgreen');
    $("[id$=_depth] div input").addClass('border-color-darkgreen');

    $("[id$=_diameter_complete].form-group").hide();
    $("[id$=_diameter_complete] label").addClass('color-orange');
    $("[id$=_diameter_complete] div input").addClass('border-color-orange');

    $("[id$=_diameter_cutout].form-group").hide();
    $("[id$=_diameter_cutout] label").addClass('color-yellow');
    $("[id$=_diameter_cutout] div input").addClass('border-color-yellow');

    $("[id$=_mountingDepth].form-group").hide();
    $("[id$=_mountingDepth] label").addClass('color-brown');
    $("[id$=_mountingDepth] div input").addClass('border-color-brown');

    $("[id$=_depth_in_sight].form-group").hide();
    $("[id$=_depth_in_sight] label").addClass('color-purple');
    $("[id$=_depth_in_sight] div input").addClass('border-color-purple');

    $("[id$=_distance].form-group").hide();
    $("[id$=_distance] label").addClass('color-green');
    $("[id$=_distance] div input").addClass('border-color-green');

    $("[id$=_connection].form-group").hide();
    $("[id$=_connection] label").addClass('color-black');
    $("[id$=_connection] div input").addClass('border-color-black');

    if(light_type == 'LCT') {
        if(jQuery.inArray(bk_number, ceiling) != -1) {
            ceilingLamp();
        }

        if(jQuery.inArray(bk_number, exit) != -1) {
            exitLamp();
        }
    }

    if(light_type == 'LCD') {
        if(jQuery.inArray(bk_number, pendel) != -1) {
            pendelLamp();
        }
        if(jQuery.inArray(bk_number, festoon) != -1) {
            festoonLamp();
        }
    }

    if(light_type == 'LCA') {
        if(jQuery.inArray(bk_number, downlight) != -1) {
            downlightLamp();
        }
        if(jQuery.inArray(bk_number, tracklight) != -1) {
            tracklightLamp();
        }
        if(jQuery.inArray(bk_number, ceiling) != -1) {
            ceilingLamp();
        }
    }

    if(light_type == 'LG') {
        if(jQuery.inArray(bk_number, bollard) != -1) {
            bollardLamp()
        }
        if(jQuery.inArray(bk_number, tablelamp) != -1) {
            tableLamp();
        }
        if(jQuery.inArray(bk_number, floodlight) != -1) {
            floodlightLamp();
        }
        if(jQuery.inArray(bk_number, led_line) != -1) {
            ledPanel();
        }
        if(jQuery.inArray(bk_number, floorlamp) != -1) {
            floorLamp();
        }
    }

    if(light_type == 'LW') {
        if(jQuery.inArray(bk_number, tablelamp) != -1) {
            tableLamp();
        }
        if(jQuery.inArray(bk_number, default_group) != -1) {
            transformerLamp();
        }
        if(jQuery.inArray(bk_number, walllamp) != -1) {
            wallLamp();
        }
        if(jQuery.inArray(bk_number, floodlight) != -1) {
            floodlightLamp();
        }

    }

    if(light_type == 'LL') {
        if(jQuery.inArray(bk_number, led_line) != -1) {
            ledLine();
        }
        if(jQuery.inArray(bk_number, led_panel) != -1) {
            ledPanel();
        }
        if(jQuery.inArray(bk_number, default_group) != -1) {
            transformerLamp();
        }
    }

    if(light_type == 'LE') {
        if(jQuery.inArray(bk_number, transformer) != -1) {
            transformerLamp();
        }
        if(jQuery.inArray(bk_number, default_group) != -1) {
            transformerLamp();
        }

    }

    if(light_type == 'LF') {
        if(jQuery.inArray(bk_number, default_group) != -1) {
            transformerLamp();
        }
        if(jQuery.inArray(bk_number, tablelamp) != -1) {
            tableLamp();
        }

    }

    if(light_type == 'LP') {
        if(jQuery.inArray(bk_number, default_group) != -1) {
            transformerLamp();
        }
    }
}

function ceilingLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Ceiling.png'+'"/>');
}

function bollardLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Bollard.png'+'"/>');
}

function downlightLamp() {
    $("[id$=_height]").show();
    $("[id$=_diameter_complete]").show();
    $("[id$=_diameter_cutout]").show();
    $("[id$=_mountingDepth]").show();
    $("[id$=_depth_in_sight]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Downlight.png'+'"/>');
}

function exitLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Exit.png'+'"/>');
}

function festoonLamp() {
    $("[id$=_distance]").show();
    $("[id$=_connection]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Festoon.png'+'"/>');
}

function floodlightLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Floodlight.png'+'"/>');
}

function floorLamp() {
    $("[id$=_height]").show();
    $("[id$=_diameter_complete]").show();
    $("[id$=_diameter_cutout]").show();
    $("[id$=_mountingDepth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Floorlamp.png'+'"/>');
}

function ledLine() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_diameter_complete]").show();
    $("[id$=_mountingDepth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/LED-Linie.png'+'"/>');
}

function ledPanel() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/LED-Panel.png'+'"/>');
}

function pendelLamp() {
    $("[id$=_height]").show();
    $("[id$=_diameter_complete]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Pendel.png'+'"/>');
}

function tableLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Tablelamp.png'+'"/>');
}

function tracklightLamp() {
    $("[id$=_length]").show();
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Tracklight.png'+'"/>');
}

function transformerLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Transformer.png'+'"/>');
}

function wallLamp() {
    $("[id$=_width]").show();
    $("[id$=_height]").show();
    $("[id$=_depth]").show();
    $(".dimensions-skizze").html('<img src="/assets/images/Walllamp.png'+'"/>');
}