# This file is the entry point to configure your own services.
# Files in the packages/ subdirectory configure your dependencies.

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
  locale: en

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        public: false       # Allows optimizing the container by removing unused services; this also means
                            # fetching services directly from the container via $container->get() won't work.
                            # The best practice is to be explicit about your dependencies anyway.

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/*'
        exclude: '../src/{DependencyInjection,Entity,Migrations,Tests,Kernel.php}'

    # controllers are imported separately to make sure services can be injected
    # as action arguments even if you don't extend any base controller class
    App\Controller\:
        resource: '../src/Controller'
        tags: ['controller.service_arguments']

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones
    admin.user:
        class: App\Admin\UserAdmin
        arguments: [~, App\Entity\User, ~]
        calls:
            - method: setUserManager
              arguments:
                  - '@fos_user.user_manager'
        tags:
            - { name: sonata.admin, manager_type: orm, group: "usermanagement", label: label.user_management }

    admin.datasheet:
        class: App\Admin\DataSheetAdmin
        arguments:
            - ~
            - App\Entity\DataSheet
            - App\Controller\DatasheetActionsController
        calls:
            - [setTemplate, ['edit', 'DataSheet/base_edit_datasheet.html.twig']]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet", label: label.datasheet_list }

    #base_data
    admin.adjustingAngle:
        class: App\Admin\AdjustingAngleAdmin
        arguments: [~, App\Entity\AdjustingAngle, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.adjusting_angle }

    admin.beamAngle:
        class: App\Admin\BeamAngleAdmin
        arguments: [~, App\Entity\BeamAngle, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.beam_angle }

    admin.colourOfHousing:
        class: App\Admin\ColourOfHousingAdmin
        arguments: [~, App\Entity\ColourOfHousing, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.colour_of_housing }

    admin.colourOfShade:
        class: App\Admin\ColourOfShadeAdmin
        arguments: [~, App\Entity\ColourOfShade, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.colour_of_shade }

    admin.cover:
        class: App\Admin\CoverAdmin
        arguments: [~, App\Entity\Cover, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.cover }

    admin.fixtureLightOutput:
        class: App\Admin\FixtureLightOutputAdmin
        arguments: [~, App\Entity\FixtureLightOutput, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.fixture_light_output }

    admin.lightColour:
        class: App\Admin\LightColourAdmin
        arguments: [~, App\Entity\LightColour, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.light_colour }

    admin.lightPoints:
        class: App\Admin\LightPointsAdmin
        arguments: [~, App\Entity\LightPoints, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.light_points }

    admin.materialOfHousing:
        class: App\Admin\MaterialOfHousingAdmin
        arguments: [~, App\Entity\MaterialOfHousing, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.material_of_housing }

    admin.materialOfShade:
        class: App\Admin\MaterialOfShadeAdmin
        arguments: [~, App\Entity\MaterialOfShade, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.material_of_shade }

    admin.numberOfLamps:
        class: App\Admin\NumberOfLampsAdmin
        arguments: [~, App\Entity\NumberOfLamps, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.number_of_lamps }

    admin.plug:
        class: App\Admin\PlugAdmin
        arguments: [~, App\Entity\Plug, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.plug }

    admin.powerOfFitting:
        class: App\Admin\PowerOfFittingAdmin
        arguments: [~, App\Entity\PowerOfFitting, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.power_of_fitting }

    admin.powerOfLamp:
        class: App\Admin\PowerOfLampAdmin
        arguments: [~, App\Entity\PowerOfLamp, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.power_of_lamp }

    admin.supplier:
        class: App\Admin\SupplierAdmin
        arguments: [~, App\Entity\Supplier, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.supplier }

    admin.socket:
        class: App\Admin\SocketAdmin
        arguments: [~, App\Entity\Socket, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.socket }

    admin.typeOfLamp:
        class: App\Admin\TypeOfLampAdmin
        arguments: [~, App\Entity\TypeOfLamp, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.type_of_lamp }

    admin.voltage:
        class: App\Admin\VoltageAdmin
        arguments: [~, App\Entity\Voltage, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.voltage }

    admin.typeOfBulb:
        class: App\Admin\TypeOfBulbAdmin
        arguments: [~, App\Entity\TypeOfBulb, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.basedata", label: label.type_of_bulb }

    #no_edit fields
    admin.dimmerType:
        class: App\Admin\DimmerTypeAdmin
        arguments: [~, App\Entity\DimmerType, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.fixeddata", label: label.dimmer_type }

    admin.frequency:
        class: App\Admin\FrequencyAdmin
        arguments: [~, App\Entity\Frequency, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.fixeddata", label: label.frequency }

    admin.cri:
        class: App\Admin\CriAdmin
        arguments: [~, App\Entity\Cri, ~]
        tags:
            - { name: sonata.admin, manager_type: orm, group: "datasheet.fixeddata", label: label.cri }

    service.lightTypeIdHelperService:
        class: App\Service\LightTypeIdHelperService
        arguments: [~, App\Entity\Datasheet, ~]

    service.HelperService:
        class: App\Service\HelperService

    service.custom_mailer_auth:
        class: App\Service\CustomAuthMailer