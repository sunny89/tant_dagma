<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* DataSheet/datasheet_pdf.html.twig */
class __TwigTemplate_2f96e931a6de04ed41963f8e86d1c84c10e7af30ce0310149489e6c64dd47929 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <style>
        table {
            border: 1px solid #E9E9E9;
            width:100%
        }
        tr,td {
            border: 1px solid #E9E9E9;
        }
        .bg_grey {
            background-color:slategray;
            color:white;
        }

        .bg_light_grey {
            background-color:#F6F6F6;
        }
        .center {
            text-align:center;
        }

        h2 {
            font-weight:bold;
            font-size: 18px;
        }
    </style>
    <title>Title of the PDF</title>
</head>
<body>
<h4>Datasheet</h4>
<table>
    <tr>
        <td colspan=\"4\" class=\"bg_grey center\">
            <h2>Electrical and related attributes</h2>
        </td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Description</td>
        <td colspan=\"3\">";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "description", [], "any", false, false, false, 42), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Voltage [V]</td>
        <td>";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "voltage", [], "any", false, false, false, 46), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Dimmer type</td>
        <td>";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "dimmerType", [], "any", false, false, false, 48), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Frequency</td>
        <td>";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "frequency", [], "any", false, false, false, 52), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Plug</td>
        <td>";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "plug", [], "any", false, false, false, 54), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">IP-class</td>
        <td>";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "ip_class", [], "any", false, false, false, 58), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Driver</td>
        <td>";
        // line 60
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "driver", [], "any", false, false, false, 60), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Power of fitting [VA]</td>
        <td>";
        // line 64
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "power_of_fitting", [], "any", false, false, false, 64), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Number of lamps</td>
        <td>";
        // line 66
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "number_of_lamp", [], "any", false, false, false, 66), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Socket</td>
        <td>";
        // line 70
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "socket", [], "any", false, false, false, 70), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Type of bulb</td>
        <td>";
        // line 72
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "type_of_bulb", [], "any", false, false, false, 72), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Type of lamp</td>
        <td>";
        // line 76
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "type_of_lamp", [], "any", false, false, false, 76), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Power of lamp [W]</td>
        <td>";
        // line 78
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "power_of_lamp", [], "any", false, false, false, 78), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td class=\"bg_light_grey\">Lightpoints</td>
        <td>";
        // line 84
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "lightpoints", [], "any", false, false, false, 84), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td colspan=\"4\" class=\"bg_grey center\">Light information</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">CRI</td>
        <td>";
        // line 91
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "cri", [], "any", false, false, false, 91), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Light colour [K]</td>
        <td>";
        // line 93
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "light_colour", [], "any", false, false, false, 93), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Cover</td>
        <td>";
        // line 97
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "cover", [], "any", false, false, false, 97), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Beam angle [o]</td>
        <td>";
        // line 99
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "beam_angle", [], "any", false, false, false, 99), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Colour of shade</td>
        <td>";
        // line 103
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "colour_of_shade", [], "any", false, false, false, 103), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Material of shade</td>
        <td>";
        // line 105
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "material_of_shade", [], "any", false, false, false, 105), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Colour of housing</td>
        <td>";
        // line 109
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "colour_of_housing", [], "any", false, false, false, 109), "html", null, true);
        echo "</td>
        <td class=\"bg_light_grey\">Material of housing</td>
        <td>";
        // line 111
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["datasheet"] ?? null), "material_of_housing", [], "any", false, false, false, 111), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td class=\"bg_light_grey\">Fixture light output [lm]</td>
        <td></td>
    </tr>
    <tr>
        <td colspan=\"4\" class=\"bg_grey center\">Dimensions</td>
    </tr>
    <tr>
        <td colspan=\"2\" class=\"bg_light_grey\">Fixture length, -width, height [mm]</td>
        <td colspan=\"2\"></td>
    </tr>
    <tr>
        <td colspan=\"2\" class=\"bg_light_grey\">Cut out length, -width, -diameter [mm]</td>
        <td colspan=\"2\"></td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Fixture diameter [mm]</td>
        <td></td>
        <td class=\"bg_light_grey\">Weight [kg]</td>
        <td></td>
    </tr>
    <tr>
        <td colspan=\"2\" class=\"bg_light_grey\">Built-in height [mm]</td>
        <td colspan=\"2\"></td>
    </tr>
    <tr>
        <td colspan=\"2\" class=\"bg_light_grey\">Adjusting angle [o]</td>
        <td colspan=\"2\"></td>
    </tr>
    <tr>
        <td colspan=\"4\" class=\"bg_grey center\">
            <h2>Remarks</h2>
        </td>
    </tr>
    <tr>
        <td colspan=\"4\">valie</td>
    </tr>
    <tr>
        <td colspan=\"2\" class=\"bg_light_grey\">Reference</td>
        <td colspan=\"2\"></td>
    </tr>
    <tr>
        <td colspan=\"4\" class=\"bg_grey center\">
            <h2>Picture</h2>
        </td>
    </tr>
    <tr>
        <td colspan=\"2\" class=\"bg_light_grey\">Manufacturer</td>
        <td colspan=\"2\"></td>
    </tr>
    <tr>
        <td colspan=\"4\" class=\"bg_grey center\">
            <h2>Contact</h2>
        </td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Supplier</td>
        <td></td>
        <td class=\"bg_light_grey\">Article No.</td>
        <td></td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Street</td>
        <td></td>
        <td class=\"bg_light_grey\">Telephone</td>
        <td></td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Location</td>
        <td></td>
        <td class=\"bg_light_grey\">Fax</td>
        <td></td>
    </tr>
    <tr>
        <td class=\"bg_light_grey\">Country</td>
        <td></td>
        <td class=\"bg_light_grey\">E-Mail</td>
        <td></td>
    </tr>
</table>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "DataSheet/datasheet_pdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 111,  207 => 109,  200 => 105,  195 => 103,  188 => 99,  183 => 97,  176 => 93,  171 => 91,  161 => 84,  152 => 78,  147 => 76,  140 => 72,  135 => 70,  128 => 66,  123 => 64,  116 => 60,  111 => 58,  104 => 54,  99 => 52,  92 => 48,  87 => 46,  80 => 42,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "DataSheet/datasheet_pdf.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/templates/DataSheet/datasheet_pdf.html.twig");
    }
}
