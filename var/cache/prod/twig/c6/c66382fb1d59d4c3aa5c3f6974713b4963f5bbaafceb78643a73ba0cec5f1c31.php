<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SonataAdmin/CRUD/base_list.html.twig */
class __TwigTemplate_e393325d7318a42444f1536bf5442822aa4aef29696ca9c8146b4e67bb5c5f44 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'actions' => [$this, 'block_actions'],
            'tab_menu' => [$this, 'block_tab_menu'],
            'title' => [$this, 'block_title'],
            'navbar_title' => [$this, 'block_navbar_title'],
            'list_table' => [$this, 'block_list_table'],
            'list_header' => [$this, 'block_list_header'],
            'table_header' => [$this, 'block_table_header'],
            'table_body' => [$this, 'block_table_body'],
            'table_footer' => [$this, 'block_table_footer'],
            'no_result_content' => [$this, 'block_no_result_content'],
            'list_footer' => [$this, 'block_list_footer'],
            'batch' => [$this, 'block_batch'],
            'batch_javascript' => [$this, 'block_batch_javascript'],
            'batch_actions' => [$this, 'block_batch_actions'],
            'pager_results' => [$this, 'block_pager_results'],
            'pager_links' => [$this, 'block_pager_links'],
            'list_filters_actions' => [$this, 'block_list_filters_actions'],
            'list_filters' => [$this, 'block_list_filters'],
            'sonata_list_filter_group_class' => [$this, 'block_sonata_list_filter_group_class'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(($context["base_template"] ?? null), "@SonataAdmin/CRUD/base_list.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        $this->loadTemplate("@SonataAdmin/CRUD/action_buttons.html.twig", "@SonataAdmin/CRUD/base_list.html.twig", 15)->display($context);
    }

    // line 18
    public function block_tab_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "sidemenu", [0 => ($context["action"] ?? null)], "method", false, false, false, 19), ["currentClass" => "active", "template" => $this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("tab_menu_template")], "twig");
    }

    // line 25
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    ";
        // line 30
        echo "
    ";
        // line 31
        if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "isChild", [], "any", false, false, false, 31) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "parent", [], "any", false, false, false, 31), "subject", [], "any", false, false, false, 31))) {
            // line 32
            echo "        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("title_edit", ["%name%" => twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "parent", [], "any", false, false, false, 32), "toString", [0 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "parent", [], "any", false, false, false, 32), "subject", [], "any", false, false, false, 32)], "method", false, false, false, 32), 15)], "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
    }

    // line 36
    public function block_navbar_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
    }

    // line 40
    public function block_list_table($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    <div class=\"col-xs-12 col-md-12\">
        ";
        // line 42
        $context["batchactions"] = twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "batchactions", [], "any", false, false, false, 42);
        // line 43
        echo "        ";
        if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "batch"], "method", false, false, false, 43) && twig_length_filter($this->env, ($context["batchactions"] ?? null)))) {
            // line 44
            echo "            <form action=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "batch", 1 => ["filter" => twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "filterParameters", [], "any", false, false, false, 44)]], "method", false, false, false, 44), "html", null, true);
            echo "\" method=\"POST\" >
            <input type=\"hidden\" name=\"_sonata_csrf_token\" value=\"";
            // line 45
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
            echo "\">
        ";
        }
        // line 47
        echo "
        ";
        // line 49
        echo "        <div class=\"box box-primary\" ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 49), "pager", [], "any", false, false, false, 49), "lastPage", [], "any", false, false, false, 49) == 1)) {
            echo "style=\"margin-bottom: 100px;\"";
        }
        echo ">
            <div class=\"box-body ";
        // line 50
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 50), "results", [], "any", false, false, false, 50)) > 0)) {
            echo "table-responsive no-padding";
        }
        echo "\">
                ";
        // line 51
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sonata.admin.list.table.top", ["admin" => ($context["admin"] ?? null)]]);
        echo "

                ";
        // line 53
        $this->displayBlock('list_header', $context, $blocks);
        // line 54
        echo "
                ";
        // line 55
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 55), "results", [], "any", false, false, false, 55)) > 0)) {
            // line 56
            echo "                    <table class=\"table table-bordered table-striped sonata-ba-list\">
                        ";
            // line 57
            $this->displayBlock('table_header', $context, $blocks);
            // line 96
            echo "
                        ";
            // line 97
            $this->displayBlock('table_body', $context, $blocks);
            // line 102
            echo "
                        ";
            // line 103
            $this->displayBlock('table_footer', $context, $blocks);
            // line 105
            echo "                    </table>
                ";
        } else {
            // line 107
            echo "                    ";
            $this->displayBlock('no_result_content', $context, $blocks);
            // line 125
            echo "                ";
        }
        // line 126
        echo "
                ";
        // line 127
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sonata.admin.list.table.bottom", ["admin" => ($context["admin"] ?? null)]]);
        echo "
            </div>
            ";
        // line 129
        $this->displayBlock('list_footer', $context, $blocks);
        // line 229
        echo "        </div>
        ";
        // line 230
        if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "batch"], "method", false, false, false, 230) && twig_length_filter($this->env, ($context["batchactions"] ?? null)))) {
            // line 231
            echo "            </form>
        ";
        }
        // line 233
        echo "    </div>
";
    }

    // line 53
    public function block_list_header($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 57
    public function block_table_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "                            <thead>
                                <tr class=\"sonata-ba-list-field-header\">
                                    ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "list", [], "any", false, false, false, 60), "elements", [], "any", false, false, false, 60));
        foreach ($context['_seq'] as $context["_key"] => $context["field_description"]) {
            // line 61
            echo "                                        ";
            if (((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "batch"], "method", false, false, false, 61) && (twig_get_attribute($this->env, $this->source, $context["field_description"], "getOption", [0 => "code"], "method", false, false, false, 61) == "_batch")) && (twig_length_filter($this->env, ($context["batchactions"] ?? null)) > 0))) {
                // line 62
                echo "                                            <th class=\"sonata-ba-list-field-header sonata-ba-list-field-header-batch\">
                                              <input type=\"checkbox\" id=\"list_batch_checkbox\">
                                            </th>
                                        ";
            } elseif ((twig_get_attribute($this->env, $this->source,             // line 65
$context["field_description"], "getOption", [0 => "code"], "method", false, false, false, 65) == "_select")) {
                // line 66
                echo "                                            <th class=\"sonata-ba-list-field-header sonata-ba-list-field-header-select\"></th>
                                        ";
            } elseif (((twig_get_attribute($this->env, $this->source,             // line 67
$context["field_description"], "name", [], "any", false, false, false, 67) == "_action") && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 67), "isXmlHttpRequest", [], "any", false, false, false, 67))) {
                // line 68
                echo "                                            ";
                // line 69
                echo "                                        ";
            } elseif (((twig_get_attribute($this->env, $this->source, $context["field_description"], "getOption", [0 => "ajax_hidden"], "method", false, false, false, 69) == true) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 69), "isXmlHttpRequest", [], "any", false, false, false, 69))) {
                // line 70
                echo "                                            ";
                // line 71
                echo "                                        ";
            } else {
                // line 72
                echo "                                            ";
                $context["sortable"] = false;
                // line 73
                echo "                                            ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, true, false, 73), "sortable", [], "any", true, true, false, 73) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, false, false, 73), "sortable", [], "any", false, false, false, 73))) {
                    // line 74
                    echo "                                                ";
                    $context["sortable"] = true;
                    // line 75
                    echo "                                                ";
                    $context["sort_parameters"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "modelmanager", [], "any", false, false, false, 75), "sortparameters", [0 => $context["field_description"], 1 => twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 75)], "method", false, false, false, 75);
                    // line 76
                    echo "                                                ";
                    $context["current"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 76), "values", [], "any", false, false, false, 76), "_sort_by", [], "any", false, false, false, 76) == $context["field_description"]) || (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 76), "values", [], "any", false, false, false, 76), "_sort_by", [], "any", false, false, false, 76), "name", [], "any", false, false, false, 76) == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sort_parameters"] ?? null), "filter", [], "any", false, false, false, 76), "_sort_by", [], "any", false, false, false, 76)));
                    // line 77
                    echo "                                                ";
                    $context["sort_active_class"] = ((($context["current"] ?? null)) ? ("sonata-ba-list-field-order-active") : (""));
                    // line 78
                    echo "                                                ";
                    $context["sort_by"] = ((($context["current"] ?? null)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 78), "values", [], "any", false, false, false, 78), "_sort_order", [], "any", false, false, false, 78)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, false, false, 78), "_sort_order", [], "any", false, false, false, 78)));
                    // line 79
                    echo "                                            ";
                }
                // line 80
                echo "
                                            ";
                // line 81
                ob_start(function () { return ''; });
                // line 82
                echo "                                                <th class=\"sonata-ba-list-field-header-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["field_description"], "type", [], "any", false, false, false, 82), "html", null, true);
                echo " ";
                if (($context["sortable"] ?? null)) {
                    echo " sonata-ba-list-field-header-order-";
                    echo twig_escape_filter($this->env, twig_lower_filter($this->env, ($context["sort_by"] ?? null)), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, ($context["sort_active_class"] ?? null), "html", null, true);
                }
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, true, false, 82), "header_class", [], "any", true, true, false, 82)) {
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, false, false, 82), "header_class", [], "any", false, false, false, 82), "html", null, true);
                }
                echo "\"";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, true, false, 82), "header_style", [], "any", true, true, false, 82)) {
                    echo " style=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["field_description"], "options", [], "any", false, false, false, 82), "header_style", [], "any", false, false, false, 82), "html", null, true);
                    echo "\"";
                }
                echo ">
                                                    ";
                // line 83
                if (($context["sortable"] ?? null)) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "list", 1 => ($context["sort_parameters"] ?? null)], "method", false, false, false, 83), "html", null, true);
                    echo "\">";
                }
                // line 84
                echo "                                                    ";
                if (twig_get_attribute($this->env, $this->source, $context["field_description"], "getOption", [0 => "label_icon"], "method", false, false, false, 84)) {
                    // line 85
                    echo "                                                        <i class=\"sonata-ba-list-field-header-label-icon ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["field_description"], "getOption", [0 => "label_icon"], "method", false, false, false, 85), "html", null, true);
                    echo "\" aria-hidden=\"true\"></i>
                                                    ";
                }
                // line 87
                echo "                                                    ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["field_description"], "label", [], "any", false, false, false, 87), [], twig_get_attribute($this->env, $this->source, $context["field_description"], "translationDomain", [], "any", false, false, false, 87)), "html", null, true);
                echo "
                                                    ";
                // line 88
                if (($context["sortable"] ?? null)) {
                    echo "</a>";
                }
                // line 89
                echo "                                                </th>
                                            ";
                echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
                // line 91
                echo "                                        ";
            }
            // line 92
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field_description'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "                                </tr>
                            </thead>
                        ";
    }

    // line 97
    public function block_table_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 98
        echo "                            <tbody>
                                ";
        // line 99
        $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getAdminTemplate(("outer_list_rows_" . twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "getListMode", [], "method", false, false, false, 99)), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "code", [], "any", false, false, false, 99)), "@SonataAdmin/CRUD/base_list.html.twig", 99)->display($context);
        // line 100
        echo "                            </tbody>
                        ";
    }

    // line 103
    public function block_table_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 104
        echo "                        ";
    }

    // line 107
    public function block_no_result_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 108
        echo "                        <div class=\"info-box\">
                            <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i></span>
                            <div class=\"info-box-content\">
                                <span class=\"info-box-text\">";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("no_result", [], "SonataAdminBundle"), "html", null, true);
        echo "</span>
                                <div class=\"progress\">
                                    <div class=\"progress-bar\" style=\"width: 0%\"></div>
                                </div>
                                <span class=\"progress-description\">
                                    ";
        // line 116
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 116), "xmlHttpRequest", [], "any", false, false, false, 116)) {
            // line 117
            echo "                                    <ul class=\"list-unstyled\">
                                        ";
            // line 118
            $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getAdminTemplate("button_create", twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "code", [], "any", false, false, false, 118)), "@SonataAdmin/CRUD/base_list.html.twig", 118)->display($context);
            // line 119
            echo "                                    </ul>
                                    ";
        }
        // line 121
        echo "                                </span>
                            </div><!-- /.info-box-content -->
                        </div>
                    ";
    }

    // line 129
    public function block_list_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 130
        echo "                ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 130), "results", [], "any", false, false, false, 130)) > 0)) {
            // line 131
            echo "                    <div class=\"box-footer\">
                        <div class=\"form-inline clearfix\">
                            ";
            // line 133
            if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 133), "isXmlHttpRequest", [], "any", false, false, false, 133)) {
                // line 134
                echo "                                <div class=\"pull-left\">
                                    ";
                // line 135
                if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "batch"], "method", false, false, false, 135) && (twig_length_filter($this->env, ($context["batchactions"] ?? null)) > 0))) {
                    // line 136
                    echo "                                        ";
                    $this->displayBlock('batch', $context, $blocks);
                    // line 183
                    echo "                                    ";
                }
                // line 184
                echo "                                </div>


                                ";
                // line 188
                echo "                                ";
                $context["export_formats"] = (((isset($context["export_formats"]) || array_key_exists("export_formats", $context))) ? (_twig_default_filter(($context["export_formats"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "exportFormats", [], "any", false, false, false, 188))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "exportFormats", [], "any", false, false, false, 188)));
                // line 189
                echo "
                                <div class=\"pull-right\">
                                    ";
                // line 191
                if (((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "export"], "method", false, false, false, 191) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "export"], "method", false, false, false, 191)) && twig_length_filter($this->env, ($context["export_formats"] ?? null)))) {
                    // line 192
                    echo "                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                                                <i class=\"fa fa-share-square-o\" aria-hidden=\"true\"></i>
                                                ";
                    // line 195
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("label_export_download", [], "SonataAdminBundle"), "html", null, true);
                    echo "
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\">
                                                ";
                    // line 199
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["export_formats"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["format"]) {
                        // line 200
                        echo "                                                <li>
                                                    <a href=\"";
                        // line 201
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "export", 1 => (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "modelmanager", [], "any", false, false, false, 201), "paginationparameters", [0 => twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 201), 1 => 0], "method", false, false, false, 201) + ["format" => $context["format"]])], "method", false, false, false, 201), "html", null, true);
                        echo "\">
                                                        <i class=\"fa fa-arrow-circle-o-down\" aria-hidden=\"true\"></i>
                                                        ";
                        // line 203
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("export_format_" . $context["format"]), [], "SonataAdminBundle"), "html", null, true);
                        echo "
                                                    </a>
                                                <li>
                                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['format'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 207
                    echo "                                            </ul>
                                        </div>

                                        &nbsp;-&nbsp;
                                    ";
                }
                // line 212
                echo "
                                    ";
                // line 213
                $this->displayBlock('pager_results', $context, $blocks);
                // line 216
                echo "                                </div>
                            ";
            }
            // line 218
            echo "                        </div>

                        ";
            // line 220
            $this->displayBlock('pager_links', $context, $blocks);
            // line 226
            echo "                    </div>
                ";
        }
        // line 228
        echo "            ";
    }

    // line 136
    public function block_batch($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 137
        echo "                                            <script>
                                                ";
        // line 138
        $this->displayBlock('batch_javascript', $context, $blocks);
        // line 165
        echo "                                            </script>

                                        ";
        // line 167
        $this->displayBlock('batch_actions', $context, $blocks);
        // line 180
        echo "
                                            <input type=\"submit\" class=\"btn btn-small btn-primary\" value=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_batch", [], "SonataAdminBundle"), "html", null, true);
        echo "\">
                                        ";
    }

    // line 138
    public function block_batch_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 139
        echo "                                                    jQuery(document).ready(function (\$) {
                                                        // Toggle individual checkboxes when the batch checkbox is changed
                                                        \$('#list_batch_checkbox').on('ifChanged change', function () {
                                                            var checkboxes = \$(this)
                                                                .closest('table')
                                                                .find('td.sonata-ba-list-field-batch input[type=\"checkbox\"], div.sonata-ba-list-field-batch input[type=\"checkbox\"]')
                                                            ;
                                                            if (window.SONATA_CONFIG.USE_ICHECK) {
                                                                checkboxes.iCheck(\$(this).is(':checked') ? 'check' : 'uncheck');
                                                            } else {
                                                                checkboxes.prop('checked', this.checked);
                                                            }
                                                        });

                                                        // Add a CSS class to rows when they are selected
                                                        \$('td.sonata-ba-list-field-batch input[type=\"checkbox\"], div.sonata-ba-list-field-batch input[type=\"checkbox\"]')
                                                            .on('ifChanged change', function () {
                                                                \$(this)
                                                                    .closest('tr, div.sonata-ba-list-field-batch')
                                                                    .toggleClass('sonata-ba-list-row-selected', \$(this).is(':checked'))
                                                                ;
                                                            })
                                                            .trigger('ifChanged')
                                                        ;
                                                    });
                                                ";
    }

    // line 167
    public function block_batch_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 168
        echo "                                            <label class=\"checkbox\" for=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 168), "html", null, true);
        echo "_all_elements\">
                                                <input type=\"checkbox\" name=\"all_elements\" id=\"";
        // line 169
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 169), "html", null, true);
        echo "_all_elements\">
                                                ";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("all_elements", [], "SonataAdminBundle"), "html", null, true);
        echo "
                                                (";
        // line 171
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 171), "pager", [], "any", false, false, false, 171), "nbresults", [], "any", false, false, false, 171), "html", null, true);
        echo ")
                                            </label>

                                            <select name=\"action\" style=\"width: auto; height: auto\" class=\"form-control\">
                                                ";
        // line 175
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["batchactions"] ?? null));
        foreach ($context['_seq'] as $context["action"] => $context["options"]) {
            // line 176
            echo "                                                    <option value=\"";
            echo twig_escape_filter($this->env, $context["action"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["options"], "label", [], "any", false, false, false, 176), [], ((twig_get_attribute($this->env, $this->source, $context["options"], "translation_domain", [], "any", true, true, false, 176)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, $context["options"], "translation_domain", [], "any", false, false, false, 176), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 176))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 176)))), "html", null, true);
            echo "</option>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['action'], $context['options'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 178
        echo "                                            </select>
                                        ";
    }

    // line 213
    public function block_pager_results($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 214
        echo "                                        ";
        $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getAdminTemplate("pager_results", twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "code", [], "any", false, false, false, 214)), "@SonataAdmin/CRUD/base_list.html.twig", 214)->display($context);
        // line 215
        echo "                                    ";
    }

    // line 220
    public function block_pager_links($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 221
        echo "                            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 221), "pager", [], "any", false, false, false, 221), "haveToPaginate", [], "method", false, false, false, 221)) {
            // line 222
            echo "                                <hr/>
                                ";
            // line 223
            $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getAdminTemplate("pager_links", twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "code", [], "any", false, false, false, 223)), "@SonataAdmin/CRUD/base_list.html.twig", 223)->display($context);
            // line 224
            echo "                            ";
        }
        // line 225
        echo "                        ";
    }

    // line 236
    public function block_list_filters_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 237
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 237), "filters", [], "any", false, false, false, 237))) {
            // line 238
            echo "        <ul class=\"nav navbar-nav navbar-right\">

            <li class=\"dropdown sonata-actions\">
                <a href=\"#\" class=\"dropdown-toggle sonata-ba-action\" data-toggle=\"dropdown\">
                    <i class=\"fa fa-filter\" aria-hidden=\"true\"></i>
                    ";
            // line 243
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_filters", [], "SonataAdminBundle"), "html", null, true);
            echo "
                    <span class=\"badge sonata-filter-count\"></span>
                    <b class=\"caret\"></b>
                </a>

                <ul class=\"dropdown-menu\" role=\"menu\">
                    ";
            // line 249
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 249), "filters", [], "any", false, false, false, 249));
            foreach ($context['_seq'] as $context["_key"] => $context["filter"]) {
                if ((((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 249)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["show_filter"] ?? null) : null) === true) || (null === (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 249)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["show_filter"] ?? null) : null)))) {
                    // line 250
                    echo "                        ";
                    $context["filterActive"] = ((twig_get_attribute($this->env, $this->source, $context["filter"], "isActive", [], "method", false, false, false, 250) || (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 250)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["show_filter"] ?? null) : null)) &&  !twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "isDefaultFilter", [0 => twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 250)], "method", false, false, false, 250));
                    // line 251
                    echo "                        <li>
                            <a href=\"#\" class=\"sonata-toggle-filter sonata-ba-action\" filter-target=\"filter-";
                    // line 252
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 252), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["filter"], "name", [], "any", false, false, false, 252), "html", null, true);
                    echo "\" filter-container=\"filter-container-";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "method", false, false, false, 252), "html", null, true);
                    echo "\">
                                <i class=\"fa ";
                    // line 253
                    echo (((twig_get_attribute($this->env, $this->source, $context["filter"], "isActive", [], "method", false, false, false, 253) || (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 253)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["show_filter"] ?? null) : null))) ? ("fa-check-square-o") : ("fa-square-o"));
                    echo "\"></i>";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["filter"], "label", [], "any", false, false, false, 253), [], ((twig_get_attribute($this->env, $this->source, $context["filter"], "translationDomain", [], "any", false, false, false, 253)) ? (twig_get_attribute($this->env, $this->source, $context["filter"], "translationDomain", [], "any", false, false, false, 253)) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 253)))), "html", null, true);
                    echo "
                            </a>
                        </li>
                    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 257
            echo "                </ul>
            </li>
        </ul>
    ";
        }
    }

    // line 263
    public function block_list_filters($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 264
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 264), "filters", [], "any", false, false, false, 264)) {
            // line 265
            echo "        ";
            $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => $this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getAdminTemplate("filter", twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "code", [], "any", false, false, false, 265))], true);
            // line 266
            echo "
        <div class=\"col-xs-12 col-md-12 sonata-filters-box\" style=\"display: ";
            // line 267
            echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 267), "hasDisplayableFilters", [], "any", false, false, false, 267)) ? ("block") : ("none"));
            echo "\" id=\"filter-container-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "method", false, false, false, 267), "html", null, true);
            echo "\">
            <div class=\"box box-primary\" >
                <div class=\"box-body\">
                    <form class=\"sonata-filter-form form-horizontal ";
            // line 270
            echo (((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "isChild", [], "any", false, false, false, 270) && (1 == twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 270), "filters", [], "any", false, false, false, 270))))) ? ("hide") : (""));
            echo "\" action=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "list"], "method", false, false, false, 270), "html", null, true);
            echo "\" method=\"GET\" role=\"form\">
                        ";
            // line 271
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "

                        <div class=\"row\">
                            <div class=\"col-sm-9\">
                                ";
            // line 275
            $context["withAdvancedFilter"] = false;
            // line 276
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 276), "filters", [], "any", false, false, false, 276));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["filter"]) {
                // line 277
                echo "                                    ";
                $context["filterActive"] = (((twig_get_attribute($this->env, $this->source, $context["filter"], "isActive", [], "method", false, false, false, 277) && (null === (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 277)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["show_filter"] ?? null) : null))) || ((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 277)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["show_filter"] ?? null) : null) === true)) &&  !twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "isDefaultFilter", [0 => twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 277)], "method", false, false, false, 277));
                // line 278
                echo "                                    ";
                $context["filterVisible"] = (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 278)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["show_filter"] ?? null) : null) === true) || (null === (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 278)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["show_filter"] ?? null) : null)));
                // line 279
                echo "                                    <div class=\"form-group ";
                $this->displayBlock('sonata_list_filter_group_class', $context, $blocks);
                echo "\" id=\"filter-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 279), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["filter"], "name", [], "any", false, false, false, 279), "html", null, true);
                echo "\" sonata-filter=\"";
                echo ((($context["filterVisible"] ?? null)) ? ("true") : ("false"));
                echo "\" style=\"display: ";
                if (($context["filterActive"] ?? null)) {
                    echo "block";
                } else {
                    echo "none";
                }
                echo "\">
                                        ";
                // line 280
                if ( !(twig_get_attribute($this->env, $this->source, $context["filter"], "label", [], "any", false, false, false, 280) === false)) {
                    // line 281
                    echo "                                            <label for=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = twig_get_attribute($this->env, $this->source, (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["form"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 281)] ?? null) : null), "children", [], "any", false, false, false, 281)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["value"] ?? null) : null), "vars", [], "any", false, false, false, 281), "id", [], "any", false, false, false, 281), "html", null, true);
                    echo "\" class=\"col-sm-3 control-label\">";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["filter"], "label", [], "any", false, false, false, 281), [], ((twig_get_attribute($this->env, $this->source, $context["filter"], "translationDomain", [], "any", false, false, false, 281)) ? (twig_get_attribute($this->env, $this->source, $context["filter"], "translationDomain", [], "any", false, false, false, 281)) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 281)))), "html", null, true);
                    echo "</label>
                                        ";
                }
                // line 283
                echo "                                        ";
                $context["attr"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 283), [], "array", false, true, false, 283), "children", [], "any", false, true, false, 283), "type", [], "array", false, true, false, 283), "vars", [], "any", false, true, false, 283), "attr", [], "any", true, true, false, 283)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 283), [], "array", false, true, false, 283), "children", [], "any", false, true, false, 283), "type", [], "array", false, true, false, 283), "vars", [], "any", false, true, false, 283), "attr", [], "any", false, false, false, 283), [])) : ([]));
                // line 284
                echo "
                                        <div class=\"col-sm-4 advanced-filter\">
                                            ";
                // line 286
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = twig_get_attribute($this->env, $this->source, (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["form"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 286)] ?? null) : null), "children", [], "any", false, false, false, 286)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["type"] ?? null) : null), 'widget', ["attr" => ($context["attr"] ?? null)]);
                echo "
                                        </div>

                                        <div class=\"col-sm-4\">
                                            ";
                // line 290
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = twig_get_attribute($this->env, $this->source, (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["form"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40[twig_get_attribute($this->env, $this->source, $context["filter"], "formName", [], "any", false, false, false, 290)] ?? null) : null), "children", [], "any", false, false, false, 290)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["value"] ?? null) : null), 'widget');
                echo "
                                        </div>

                                        <div class=\"col-sm-1\">
                                            <label class=\"control-label\">
                                                <a href=\"#\" class=\"sonata-toggle-filter sonata-ba-action\" filter-target=\"filter-";
                // line 295
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 295), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["filter"], "name", [], "any", false, false, false, 295), "html", null, true);
                echo "\" filter-container=\"filter-container-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "method", false, false, false, 295), "html", null, true);
                echo "\">
                                                    <i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>
                                                </a>
                                            </label>
                                        </div>
                                    </div>

                                    ";
                // line 302
                if ((($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = twig_get_attribute($this->env, $this->source, $context["filter"], "options", [], "any", false, false, false, 302)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["advanced_filter"] ?? null) : null)) {
                    // line 303
                    echo "                                        ";
                    $context["withAdvancedFilter"] = true;
                    // line 304
                    echo "                                    ";
                }
                // line 305
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 306
            echo "                            </div>
                            <div class=\"col-sm-3 text-center\">
                                <input type=\"hidden\" name=\"filter[_page]\" id=\"filter__page\" value=\"1\">

                                ";
            // line 310
            $context["foo"] = twig_get_attribute($this->env, $this->source, (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["form"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["_page"] ?? null) : null), "setRendered", [], "method", false, false, false, 310);
            // line 311
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "

                                <div class=\"form-group\">
                                    <button type=\"submit\" class=\"btn btn-primary\">
                                        <i class=\"fa fa-filter\" aria-hidden=\"true\"></i> ";
            // line 315
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_filter", [], "SonataAdminBundle"), "html", null, true);
            echo "
                                    </button>

                                    <a class=\"btn btn-default\" href=\"";
            // line 318
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "list", 1 => ["filters" => "reset"]], "method", false, false, false, 318), "html", null, true);
            echo "\">
                                        ";
            // line 319
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_reset_filter", [], "SonataAdminBundle"), "html", null, true);
            echo "
                                    </a>
                                </div>

                                ";
            // line 323
            if (($context["withAdvancedFilter"] ?? null)) {
                // line 324
                echo "                                    <div class=\"form-group\">
                                        <a href=\"#\" data-toggle=\"advanced-filter\">
                                            <i class=\"fa fa-cogs\" aria-hidden=\"true\"></i>
                                            ";
                // line 327
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_advanced_filters", [], "SonataAdminBundle"), "html", null, true);
                echo "
                                        </a>
                                    </div>
                                ";
            }
            // line 331
            echo "                            </div>
                        </div>

                        ";
            // line 334
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "persistentParameters", [], "any", false, false, false, 334));
            foreach ($context['_seq'] as $context["paramKey"] => $context["paramValue"]) {
                // line 335
                echo "                            <input type=\"hidden\" name=\"";
                echo twig_escape_filter($this->env, $context["paramKey"], "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $context["paramValue"], "html", null, true);
                echo "\">
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['paramKey'], $context['paramValue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 337
            echo "                    </form>
                </div>
            </div>
        </div>
    ";
        }
    }

    // line 279
    public function block_sonata_list_filter_group_class($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "@SonataAdmin/CRUD/base_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  918 => 279,  909 => 337,  898 => 335,  894 => 334,  889 => 331,  882 => 327,  877 => 324,  875 => 323,  868 => 319,  864 => 318,  858 => 315,  850 => 311,  848 => 310,  842 => 306,  828 => 305,  825 => 304,  822 => 303,  820 => 302,  806 => 295,  798 => 290,  791 => 286,  787 => 284,  784 => 283,  776 => 281,  774 => 280,  757 => 279,  754 => 278,  751 => 277,  733 => 276,  731 => 275,  724 => 271,  718 => 270,  710 => 267,  707 => 266,  704 => 265,  701 => 264,  697 => 263,  689 => 257,  676 => 253,  668 => 252,  665 => 251,  662 => 250,  657 => 249,  648 => 243,  641 => 238,  639 => 237,  635 => 236,  631 => 225,  628 => 224,  626 => 223,  623 => 222,  620 => 221,  616 => 220,  612 => 215,  609 => 214,  605 => 213,  600 => 178,  589 => 176,  585 => 175,  578 => 171,  574 => 170,  570 => 169,  565 => 168,  561 => 167,  532 => 139,  528 => 138,  522 => 181,  519 => 180,  517 => 167,  513 => 165,  511 => 138,  508 => 137,  504 => 136,  500 => 228,  496 => 226,  494 => 220,  490 => 218,  486 => 216,  484 => 213,  481 => 212,  474 => 207,  464 => 203,  459 => 201,  456 => 200,  452 => 199,  445 => 195,  440 => 192,  438 => 191,  434 => 189,  431 => 188,  426 => 184,  423 => 183,  420 => 136,  418 => 135,  415 => 134,  413 => 133,  409 => 131,  406 => 130,  402 => 129,  395 => 121,  391 => 119,  389 => 118,  386 => 117,  384 => 116,  376 => 111,  371 => 108,  367 => 107,  363 => 104,  359 => 103,  354 => 100,  352 => 99,  349 => 98,  345 => 97,  339 => 93,  333 => 92,  330 => 91,  326 => 89,  322 => 88,  317 => 87,  311 => 85,  308 => 84,  302 => 83,  280 => 82,  278 => 81,  275 => 80,  272 => 79,  269 => 78,  266 => 77,  263 => 76,  260 => 75,  257 => 74,  254 => 73,  251 => 72,  248 => 71,  246 => 70,  243 => 69,  241 => 68,  239 => 67,  236 => 66,  234 => 65,  229 => 62,  226 => 61,  222 => 60,  218 => 58,  214 => 57,  208 => 53,  203 => 233,  199 => 231,  197 => 230,  194 => 229,  192 => 129,  187 => 127,  184 => 126,  181 => 125,  178 => 107,  174 => 105,  172 => 103,  169 => 102,  167 => 97,  164 => 96,  162 => 57,  159 => 56,  157 => 55,  154 => 54,  152 => 53,  147 => 51,  141 => 50,  134 => 49,  131 => 47,  126 => 45,  121 => 44,  118 => 43,  116 => 42,  113 => 41,  109 => 40,  102 => 37,  98 => 36,  90 => 32,  88 => 31,  85 => 30,  83 => 26,  79 => 25,  75 => 19,  71 => 18,  67 => 15,  63 => 14,  53 => 12,);
    }

    public function getSourceContext()
    {
        return new Source("", "@SonataAdmin/CRUD/base_list.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/sonata-project/admin-bundle/src/Resources/views/CRUD/base_list.html.twig");
    }
}
