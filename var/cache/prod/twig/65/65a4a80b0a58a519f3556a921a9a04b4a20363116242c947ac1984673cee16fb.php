<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SonataAdmin/CRUD/list_outer_rows_mosaic.html.twig */
class __TwigTemplate_63ddd3266f130de3809090b88ce0568a8bde788b4c31214628ecd4cb11223373 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'sonata_mosaic_background' => [$this, 'block_sonata_mosaic_background'],
            'sonata_mosaic_default_view' => [$this, 'block_sonata_mosaic_default_view'],
            'sonata_mosaic_hover_view' => [$this, 'block_sonata_mosaic_hover_view'],
            'sonata_mosaic_description' => [$this, 'block_sonata_mosaic_description'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "
<!--
This template can be customized to match your needs. You should only extends the template and used the differents block to customize the view:
    - sonata_mosaic_default_view
    - sonata_mosaic_hover_view
    - sonata_mosaic_description
-->

<tr>
    <td colspan=\"";
        // line 20
        echo twig_escape_filter($this->env, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "list", [], "any", false, false, false, 20), "elements", [], "any", false, false, false, 20)) - ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 20), "isXmlHttpRequest", [], "any", false, false, false, 20)) ? ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "list", [], "any", false, false, false, 20), "has", [0 => "_action"], "method", false, false, false, 20) + twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "list", [], "any", false, false, false, 20), "has", [0 => "batch"], "method", false, false, false, 20))) : (0))), "html", null, true);
        echo "\">
        <div class=\"row\">
            ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "datagrid", [], "any", false, false, false, 22), "results", [], "any", false, false, false, 22));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["object"]) {
            // line 23
            echo "                ";
            $context["meta"] = twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "getObjectMetadata", [0 => $context["object"]], "method", false, false, false, 23);
            // line 24
            echo "                ";
            ob_start(function () { return ''; });
            // line 25
            echo "                    <div class=\"mosaic-box-outter\">
                        <div class=\"mosaic-inner-box\">
                            ";
            // line 30
            echo "
                            <div class=\"mosaic-inner-box-default\">
                                ";
            // line 32
            $this->displayBlock('sonata_mosaic_background', $context, $blocks);
            // line 35
            echo "                                ";
            $this->displayBlock('sonata_mosaic_default_view', $context, $blocks);
            // line 38
            echo "                            </div>

                            ";
            // line 44
            echo "                            <div class=\"mosaic-inner-box-hover\">
                                ";
            // line 45
            $this->displayBlock('sonata_mosaic_hover_view', $context, $blocks);
            // line 49
            echo "                            </div>
                        </div>

                        ";
            // line 53
            echo "                        ";
            $context["export_formats"] = (((isset($context["export_formats"]) || array_key_exists("export_formats", $context))) ? (_twig_default_filter(($context["export_formats"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "getExportFormats", [], "any", false, false, false, 53))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "getExportFormats", [], "any", false, false, false, 53)));
            // line 54
            echo "
                        <div class=\"mosaic-inner-text\">
                            ";
            // line 56
            if (((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "batch"], "method", false, false, false, 56) && (twig_length_filter($this->env, ($context["batchactions"] ?? null)) > 0)) || ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "export"], "method", false, false, false, 56) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "export"], "method", false, false, false, 56)) && twig_length_filter($this->env, ($context["export_formats"] ?? null))))) {
                // line 57
                echo "                                <input type=\"checkbox\" name=\"idx[]\" value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => $context["object"]], "method", false, false, false, 57), "html", null, true);
                echo "\">
                            ";
            } else {
                // line 59
                echo "                                &nbsp;
                            ";
            }
            // line 61
            echo "
                            ";
            // line 62
            $this->displayBlock('sonata_mosaic_description', $context, $blocks);
            // line 65
            echo "                        </div>
                    </div>
                ";
            $context["mosaic_content"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 68
            echo "                <div class=\"col-xs-6 col-sm-3 mosaic-box sonata-ba-list-field-batch sonata-ba-list-field\"
                     objectId=\"";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => $context["object"]], "method", false, false, false, 69), "html", null, true);
            echo "\">
                    ";
            // line 70
            if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "edit", 1 => $context["object"]], "method", false, false, false, 70) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "edit"], "method", false, false, false, 70))) {
                // line 71
                echo "                        <a class=\"mosaic-inner-link\"
                           href=\"";
                // line 72
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "edit", 1 => ["id" => $this->extensions['Sonata\AdminBundle\Twig\Extension\SonataAdminExtension']->getUrlsafeIdentifier($context["object"], ($context["admin"] ?? null))]], "method", false, false, false, 72), "html", null, true);
                echo "\">
                            ";
                // line 73
                echo twig_escape_filter($this->env, ($context["mosaic_content"] ?? null), "html", null, true);
                echo "
                        </a>
                    ";
            } elseif ((twig_get_attribute($this->env, $this->source,             // line 75
($context["admin"] ?? null), "hasAccess", [0 => "show", 1 => $context["object"]], "method", false, false, false, 75) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "show"], "method", false, false, false, 75))) {
                // line 76
                echo "                        <a class=\"mosaic-inner-link\"
                           href=\"";
                // line 77
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "show", 1 => ["id" => $this->extensions['Sonata\AdminBundle\Twig\Extension\SonataAdminExtension']->getUrlsafeIdentifier($context["object"], ($context["admin"] ?? null))]], "method", false, false, false, 77), "html", null, true);
                echo "\">
                            ";
                // line 78
                echo twig_escape_filter($this->env, ($context["mosaic_content"] ?? null), "html", null, true);
                echo "
                        </a>
                    ";
            } else {
                // line 81
                echo "                        ";
                echo twig_escape_filter($this->env, ($context["mosaic_content"] ?? null), "html", null, true);
                echo "
                    ";
            }
            // line 83
            echo "                </div>

                ";
            // line 85
            if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 85) % 4) == 0)) {
                // line 86
                echo "                    <div class=\"clearfix hidden-xs\"></div>
                ";
            }
            // line 88
            echo "                ";
            if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 88) % 2) == 0)) {
                // line 89
                echo "                    <div class=\"clearfix visible-xs\"></div>
                ";
            }
            // line 91
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['object'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "        </div>
    </td>
</tr>
";
    }

    // line 32
    public function block_sonata_mosaic_background($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "                                    <img src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["meta"] ?? null), "image", [], "any", false, false, false, 33), "html", null, true);
        echo "\" alt=\"\" />
                                ";
    }

    // line 35
    public function block_sonata_mosaic_default_view($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "                                    <span class=\"mosaic-box-label label label-primary pull-right\">#";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 36), "html", null, true);
        echo "</span>
                                ";
    }

    // line 45
    public function block_sonata_mosaic_hover_view($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "                                    <span class=\"mosaic-box-label label label-primary pull-right\">#";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 46), "html", null, true);
        echo "</span>
                                    ";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["meta"] ?? null), "description", [], "any", false, false, false, 47), "html", null, true);
        echo "
                                ";
    }

    // line 62
    public function block_sonata_mosaic_description($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "                                ";
        echo twig_escape_filter($this->env, twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["meta"] ?? null), "title", [], "any", false, false, false, 63), 40), "html", null, true);
        echo "
                            ";
    }

    public function getTemplateName()
    {
        return "@SonataAdmin/CRUD/list_outer_rows_mosaic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 63,  253 => 62,  247 => 47,  242 => 46,  238 => 45,  231 => 36,  227 => 35,  220 => 33,  216 => 32,  209 => 92,  195 => 91,  191 => 89,  188 => 88,  184 => 86,  182 => 85,  178 => 83,  172 => 81,  166 => 78,  162 => 77,  159 => 76,  157 => 75,  152 => 73,  148 => 72,  145 => 71,  143 => 70,  139 => 69,  136 => 68,  131 => 65,  129 => 62,  126 => 61,  122 => 59,  116 => 57,  114 => 56,  110 => 54,  107 => 53,  102 => 49,  100 => 45,  97 => 44,  93 => 38,  90 => 35,  88 => 32,  84 => 30,  80 => 25,  77 => 24,  74 => 23,  57 => 22,  52 => 20,  41 => 11,);
    }

    public function getSourceContext()
    {
        return new Source("", "@SonataAdmin/CRUD/list_outer_rows_mosaic.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/sonata-project/admin-bundle/src/Resources/views/CRUD/list_outer_rows_mosaic.html.twig");
    }
}
