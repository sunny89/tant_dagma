<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @KinulabSonataGentellelaTheme/Form/form_admin_fields.html.twig */
class __TwigTemplate_19a70f9799e14bf4fca30fe5964c2f8d2c3654bc6a432ff847ea5bf8d485b417 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'form_errors' => [$this, 'block_form_errors'],
            'sonata_help' => [$this, 'block_sonata_help'],
            'form_widget' => [$this, 'block_form_widget'],
            'form_widget_simple' => [$this, 'block_form_widget_simple'],
            'textarea_widget' => [$this, 'block_textarea_widget'],
            'money_widget' => [$this, 'block_money_widget'],
            'percent_widget' => [$this, 'block_percent_widget'],
            'checkbox_widget' => [$this, 'block_checkbox_widget'],
            'radio_widget' => [$this, 'block_radio_widget'],
            'form_label' => [$this, 'block_form_label'],
            'checkbox_label' => [$this, 'block_checkbox_label'],
            'radio_label' => [$this, 'block_radio_label'],
            'checkbox_radio_label' => [$this, 'block_checkbox_radio_label'],
            'choice_widget_expanded' => [$this, 'block_choice_widget_expanded'],
            'choice_widget_collapsed' => [$this, 'block_choice_widget_collapsed'],
            'date_widget' => [$this, 'block_date_widget'],
            'time_widget' => [$this, 'block_time_widget'],
            'datetime_widget' => [$this, 'block_datetime_widget'],
            'form_row' => [$this, 'block_form_row'],
            'checkbox_row' => [$this, 'block_checkbox_row'],
            'radio_row' => [$this, 'block_radio_row'],
            'sonata_type_native_collection_widget_row' => [$this, 'block_sonata_type_native_collection_widget_row'],
            'sonata_type_native_collection_widget' => [$this, 'block_sonata_type_native_collection_widget'],
            'sonata_type_immutable_array_widget' => [$this, 'block_sonata_type_immutable_array_widget'],
            'sonata_type_immutable_array_widget_row' => [$this, 'block_sonata_type_immutable_array_widget_row'],
            'sonata_type_model_autocomplete_widget' => [$this, 'block_sonata_type_model_autocomplete_widget'],
            'sonata_type_choice_field_mask_widget' => [$this, 'block_sonata_type_choice_field_mask_widget'],
            'sonata_type_choice_multiple_sortable' => [$this, 'block_sonata_type_choice_multiple_sortable'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "@KinulabSonataGentellelaTheme/Form/form_admin_fields.html.twig", 12);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_form_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 16
            echo "        ";
            if ( !twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 16)) {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 17
            echo "            <ul class=\"list-unstyled\">
                ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 19
                echo "                    <li><i class=\"fa fa-exclamation-circle\"></i> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 19), "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "            </ul>
        ";
            // line 22
            if ( !twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 22)) {
                echo "</div>";
            }
            // line 23
            echo "    ";
        }
    }

    // line 26
    public function block_sonata_help($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        ob_start(function () { return ''; });
        // line 28
        if (((isset($context["sonata_help"]) || array_key_exists("sonata_help", $context)) && ($context["sonata_help"] ?? null))) {
            // line 29
            echo "    <span class=\"help-block sonata-ba-field-widget-help\">";
            echo ($context["sonata_help"] ?? null);
            echo "</span>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 34
    public function block_form_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        $this->displayParentBlock("form_widget", $context, $blocks);
        echo "
    ";
        // line 36
        $this->displayBlock("sonata_help", $context, $blocks);
    }

    // line 39
    public function block_form_widget_simple($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "    ";
        $context["type"] = (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 41
        echo "    ";
        if ((($context["type"] ?? null) != "file")) {
            // line 42
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 42)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 42), "")) : ("")) . " form-control")]);
            // line 43
            echo "    ";
        }
        // line 44
        echo "    ";
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 47
    public function block_textarea_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 48)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 48), "")) : ("")) . " form-control")]);
        // line 49
        echo "    ";
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        echo "
";
    }

    // line 52
    public function block_money_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        if ((($context["money_pattern"] ?? null) == "{{ widget }}")) {
            // line 54
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 56
            echo "        ";
            $context["currencySymbol"] = twig_trim_filter(twig_replace_filter(($context["money_pattern"] ?? null), ["{{ widget }}" => ""]));
            // line 57
            echo "        ";
            if (preg_match("/^{{ widget }}/", ($context["money_pattern"] ?? null))) {
                // line 58
                echo "            <div class=\"input-group\">";
                // line 59
                $this->displayBlock("form_widget_simple", $context, $blocks);
                // line 60
                echo "<span class=\"input-group-addon\">";
                echo twig_escape_filter($this->env, ($context["currencySymbol"] ?? null), "html", null, true);
                echo "</span>
            </div>
        ";
            } elseif (preg_match("/{{ widget }}\$/",             // line 62
($context["money_pattern"] ?? null))) {
                // line 63
                echo "            <div class=\"input-group\">
                <span class=\"input-group-addon\">";
                // line 64
                echo twig_escape_filter($this->env, ($context["currencySymbol"] ?? null), "html", null, true);
                echo "</span>";
                // line 65
                $this->displayBlock("form_widget_simple", $context, $blocks);
                // line 66
                echo "</div>
        ";
            }
            // line 68
            echo "    ";
        }
    }

    // line 71
    public function block_percent_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 72
        echo "    ";
        ob_start(function () { return ''; });
        // line 73
        echo "        ";
        $context["type"] = (((isset($context["type"]) || array_key_exists("type", $context))) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 74
        echo "        <div class=\"input-group\">
            ";
        // line 75
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
            <span class=\"input-group-addon\">%</span>
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 81
    public function block_checkbox_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 82
        $context["parent_label_class"] = (((isset($context["parent_label_class"]) || array_key_exists("parent_label_class", $context))) ? (_twig_default_filter(($context["parent_label_class"] ?? null), "")) : (""));
        // line 83
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? null))) {
            // line 84
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)]);
        } else {
            // line 86
            echo "<div class=\"checkbox\">";
            // line 87
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)]);
            // line 88
            echo "</div>";
        }
    }

    // line 92
    public function block_radio_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 93
        $context["parent_label_class"] = (((isset($context["parent_label_class"]) || array_key_exists("parent_label_class", $context))) ? (_twig_default_filter(($context["parent_label_class"] ?? null), "")) : (""));
        // line 94
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? null))) {
            // line 95
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["widget" => $this->renderParentBlock("radio_widget", $context, $blocks)]);
        } else {
            // line 97
            echo "<div class=\"radio\">";
            // line 98
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["widget" => $this->renderParentBlock("radio_widget", $context, $blocks)]);
            // line 99
            echo "</div>";
        }
    }

    // line 104
    public function block_form_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 105
        ob_start(function () { return ''; });
        // line 106
        echo "    ";
        if (( !(($context["label"] ?? null) === false) && ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "options", [], "any", false, false, false, 106)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["form_type"] ?? null) : null) == "horizontal"))) {
            // line 107
            echo "        ";
            $context["label_class"] = "col-sm-3";
            // line 108
            echo "    ";
        }
        // line 109
        echo "
    ";
        // line 110
        $context["label_class"] = ((((isset($context["label_class"]) || array_key_exists("label_class", $context))) ? (_twig_default_filter(($context["label_class"] ?? null), "")) : ("")) . " control-label");
        // line 111
        echo "
    ";
        // line 112
        if ( !(($context["label"] ?? null) === false)) {
            // line 113
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => ((((twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 113)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 113), "")) : ("")) . " ") . ($context["label_class"] ?? null))]);
            // line 114
            echo "
        ";
            // line 115
            if ( !($context["compound"] ?? null)) {
                // line 116
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["for" => ($context["id"] ?? null)]);
                // line 117
                echo "        ";
            }
            // line 118
            echo "        ";
            if (($context["required"] ?? null)) {
                // line 119
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 119)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 119), "")) : ("")) . " required"))]);
                // line 120
                echo "        ";
            }
            // line 121
            echo "
        ";
            // line 122
            if (twig_test_empty(($context["label"] ?? null))) {
                // line 123
                if (((isset($context["label_format"]) || array_key_exists("label_format", $context)) &&  !twig_test_empty(($context["label_format"] ?? null)))) {
                    // line 124
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? null), ["%name%" =>                     // line 125
($context["name"] ?? null), "%id%" =>                     // line 126
($context["id"] ?? null)]);
                } else {
                    // line 129
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
                }
            }
            // line 132
            echo "
        <label";
            // line 133
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
            ";
            // line 134
            if ((($context["translation_domain"] ?? null) === false)) {
                // line 135
                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
            } elseif ( !twig_get_attribute($this->env, $this->source,             // line 136
($context["sonata_admin"] ?? null), "admin", [], "any", false, false, false, 136)) {
                // line 137
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            } else {
                // line 139
                echo "                ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 139), "translationDomain", [], "any", false, false, false, 139)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 139), "translationDomain", [], "any", false, false, false, 139)) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 139)))), "html", null, true);
                echo "
            ";
            }
            // line 141
            echo "        </label>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 146
    public function block_checkbox_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 147
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
    }

    // line 150
    public function block_radio_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 151
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
    }

    // line 154
    public function block_checkbox_radio_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 155
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "admin", [], "any", false, false, false, 155)) {
            // line 156
            echo "        ";
            $context["translation_domain"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 156), "translationDomain", [], "any", false, false, false, 156);
            // line 157
            echo "    ";
        }
        // line 158
        echo "    ";
        // line 159
        echo "    ";
        if ((isset($context["widget"]) || array_key_exists("widget", $context))) {
            // line 160
            echo "        ";
            if (($context["required"] ?? null)) {
                // line 161
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 161)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 161), "")) : ("")) . " required"))]);
                // line 162
                echo "        ";
            }
            // line 163
            echo "        ";
            if ((isset($context["parent_label_class"]) || array_key_exists("parent_label_class", $context))) {
                // line 164
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter(((((twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 164)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 164), "")) : ("")) . " ") . ($context["parent_label_class"] ?? null)))]);
                // line 165
                echo "        ";
            }
            // line 166
            echo "        ";
            if (( !(($context["label"] ?? null) === false) && twig_test_empty(($context["label"] ?? null)))) {
                // line 167
                echo "            ";
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
                // line 168
                echo "        ";
            }
            // line 169
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 170
            echo ($context["widget"] ?? null);
            // line 171
            if ( !(($context["label"] ?? null) === false)) {
                // line 172
                echo "<span class=\"control-label__text\">
                    ";
                // line 173
                if ((($context["translation_domain"] ?? null) === false)) {
                    // line 174
                    echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                } else {
                    // line 176
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
                }
                // line 178
                echo "</span>";
            }
            // line 180
            echo "</label>
    ";
        }
    }

    // line 184
    public function block_choice_widget_expanded($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 185
        ob_start(function () { return ''; });
        // line 186
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 186)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 186), "")) : ("")) . " list-unstyled")]);
        // line 187
        echo "    <ul ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
    ";
        // line 188
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 189
            echo "        <li>
            ";
            // line 190
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', ["horizontal" => false, "horizontal_input_wrapper_class" => "", "translation_domain" =>             // line 193
($context["choice_translation_domain"] ?? null)]);
            // line 194
            echo " ";
            // line 195
            echo "
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 198
        echo "    </ul>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 202
    public function block_choice_widget_collapsed($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 203
        ob_start(function () { return ''; });
        // line 204
        echo "    ";
        if (((($context["required"] ?? null) && (isset($context["placeholder"]) || array_key_exists("placeholder", $context))) && (null === ($context["placeholder"] ?? null)))) {
            // line 205
            echo "        ";
            $context["required"] = false;
            // line 206
            echo "    ";
        } elseif ((((((($context["required"] ?? null) && (isset($context["empty_value"]) || array_key_exists("empty_value", $context))) && (isset($context["empty_value_in_choices"]) || array_key_exists("empty_value_in_choices", $context))) && (null === ($context["empty_value"] ?? null))) &&  !($context["empty_value_in_choices"] ?? null)) &&  !($context["multiple"] ?? null))) {
            // line 207
            echo "        ";
            $context["required"] = false;
            // line 208
            echo "    ";
        }
        // line 209
        echo "
    ";
        // line 210
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 210)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 210), "")) : ("")) . " form-control")]);
        // line 211
        echo "    ";
        if ((((isset($context["sortable"]) || array_key_exists("sortable", $context)) && ($context["sortable"] ?? null)) && ($context["multiple"] ?? null))) {
            // line 212
            echo "        ";
            $this->displayBlock("sonata_type_choice_multiple_sortable", $context, $blocks);
            echo "
    ";
        } else {
            // line 214
            echo "        <select ";
            $this->displayBlock("widget_attributes", $context, $blocks);
            if (($context["multiple"] ?? null)) {
                echo " multiple=\"multiple\"";
            }
            echo " >
            ";
            // line 215
            if (((isset($context["empty_value"]) || array_key_exists("empty_value", $context)) &&  !(null === ($context["empty_value"] ?? null)))) {
                // line 216
                echo "                <option value=\"\"";
                if ((($context["required"] ?? null) && twig_test_empty(($context["value"] ?? null)))) {
                    echo " selected=\"selected\"";
                }
                echo ">
                    ";
                // line 217
                if ((($context["empty_value"] ?? null) != "")) {
                    // line 218
                    echo "                        ";
                    if ( !twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "admin", [], "any", false, false, false, 218)) {
                        // line 219
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["empty_value"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
                    } else {
                        // line 221
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["empty_value"] ?? null), [], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 221), "translationDomain", [], "any", false, false, false, 221)), "html", null, true);
                    }
                    // line 223
                    echo "                    ";
                }
                // line 224
                echo "                </option>
            ";
            } elseif ((            // line 225
(isset($context["placeholder"]) || array_key_exists("placeholder", $context)) &&  !(null === ($context["placeholder"] ?? null)))) {
                // line 226
                echo "                <option value=\"\"";
                if ((($context["required"] ?? null) && twig_test_empty(($context["value"] ?? null)))) {
                    echo " selected=\"selected\"";
                }
                echo ">
                    ";
                // line 227
                if ((($context["placeholder"] ?? null) != "")) {
                    // line 228
                    echo "                        ";
                    if ( !twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "admin", [], "any", false, false, false, 228)) {
                        // line 229
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["placeholder"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
                    } else {
                        // line 231
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["placeholder"] ?? null), [], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 231), "translationDomain", [], "any", false, false, false, 231)), "html", null, true);
                    }
                    // line 233
                    echo "                    ";
                }
                // line 234
                echo "                </option>
            ";
            }
            // line 236
            echo "            ";
            if ((twig_length_filter($this->env, ($context["preferred_choices"] ?? null)) > 0)) {
                // line 237
                echo "                ";
                $context["options"] = ($context["preferred_choices"] ?? null);
                // line 238
                echo "                ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
                ";
                // line 239
                if ((twig_length_filter($this->env, ($context["choices"] ?? null)) > 0)) {
                    // line 240
                    echo "                    <option disabled=\"disabled\">";
                    echo twig_escape_filter($this->env, ($context["separator"] ?? null), "html", null, true);
                    echo "</option>
                ";
                }
                // line 242
                echo "            ";
            }
            // line 243
            echo "            ";
            $context["options"] = ($context["choices"] ?? null);
            // line 244
            echo "            ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
        </select>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 250
    public function block_date_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 251
        ob_start(function () { return ''; });
        // line 252
        echo "    ";
        if ((($context["widget"] ?? null) == "single_text")) {
            // line 253
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 255
            echo "        ";
            if (( !(isset($context["row"]) || array_key_exists("row", $context)) || (($context["row"] ?? null) == true))) {
                // line 256
                echo "            ";
                $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 256)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 256), "")) : ("")) . " row")]);
                // line 257
                echo "        ";
            }
            // line 258
            echo "        ";
            $context["input_wrapper_class"] = (((isset($context["input_wrapper_class"]) || array_key_exists("input_wrapper_class", $context))) ? (_twig_default_filter(($context["input_wrapper_class"] ?? null), "col-sm-4")) : ("col-sm-4"));
            // line 259
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 260
            echo twig_replace_filter(($context["date_pattern"] ?? null), ["{{ year }}" => (((("<div class=\"" .             // line 261
($context["input_wrapper_class"] ?? null)) . "\">") . $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "year", [], "any", false, false, false, 261), 'widget')) . "</div>"), "{{ month }}" => (((("<div class=\"" .             // line 262
($context["input_wrapper_class"] ?? null)) . "\">") . $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "month", [], "any", false, false, false, 262), 'widget')) . "</div>"), "{{ day }}" => (((("<div class=\"" .             // line 263
($context["input_wrapper_class"] ?? null)) . "\">") . $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "day", [], "any", false, false, false, 263), 'widget')) . "</div>")]);
            // line 264
            echo "
        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 270
    public function block_time_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 271
        ob_start(function () { return ''; });
        // line 272
        echo "    ";
        if ((($context["widget"] ?? null) == "single_text")) {
            // line 273
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 275
            echo "        ";
            if (( !(isset($context["row"]) || array_key_exists("row", $context)) || (($context["row"] ?? null) == true))) {
                // line 276
                echo "            ";
                $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 276)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 276), "")) : ("")) . " row")]);
                // line 277
                echo "        ";
            }
            // line 278
            echo "        ";
            $context["input_wrapper_class"] = (((isset($context["input_wrapper_class"]) || array_key_exists("input_wrapper_class", $context))) ? (_twig_default_filter(($context["input_wrapper_class"] ?? null), "col-sm-6")) : ("col-sm-6"));
            // line 279
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            <div class=\"";
            // line 280
            echo twig_escape_filter($this->env, ($context["input_wrapper_class"] ?? null), "html", null, true);
            echo "\">
                ";
            // line 281
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "hour", [], "any", false, false, false, 281), 'widget');
            echo "
            </div>
            ";
            // line 283
            if (($context["with_minutes"] ?? null)) {
                // line 284
                echo "                <div class=\"";
                echo twig_escape_filter($this->env, ($context["input_wrapper_class"] ?? null), "html", null, true);
                echo "\">
                    ";
                // line 285
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "minute", [], "any", false, false, false, 285), 'widget');
                echo "
                </div>
            ";
            }
            // line 288
            echo "            ";
            if (($context["with_seconds"] ?? null)) {
                // line 289
                echo "                <div class=\"";
                echo twig_escape_filter($this->env, ($context["input_wrapper_class"] ?? null), "html", null, true);
                echo "\">
                    ";
                // line 290
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "second", [], "any", false, false, false, 290), 'widget');
                echo "
                </div>
            ";
            }
            // line 293
            echo "        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 298
    public function block_datetime_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 299
        ob_start(function () { return ''; });
        // line 300
        echo "    ";
        if ((($context["widget"] ?? null) == "single_text")) {
            // line 301
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 303
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 303)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 303), "")) : ("")) . " row")]);
            // line 304
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 305
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "date", [], "any", false, false, false, 305), 'errors');
            echo "
            ";
            // line 306
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "time", [], "any", false, false, false, 306), 'errors');
            echo "

            ";
            // line 308
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "date", [], "any", false, false, false, 308), "vars", [], "any", false, false, false, 308), "widget", [], "any", false, false, false, 308) == "single_text")) {
                // line 309
                echo "                <div class=\"col-sm-2\">
                    ";
                // line 310
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "date", [], "any", false, false, false, 310), 'widget');
                echo "
                </div>
            ";
            } else {
                // line 313
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "date", [], "any", false, false, false, 313), 'widget', ["row" => false, "input_wrapper_class" => "col-sm-2"]);
                echo "
            ";
            }
            // line 315
            echo "
            ";
            // line 316
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "time", [], "any", false, false, false, 316), "vars", [], "any", false, false, false, 316), "widget", [], "any", false, false, false, 316) == "single_text")) {
                // line 317
                echo "                <div class=\"col-sm-2\">
                    ";
                // line 318
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "time", [], "any", false, false, false, 318), 'widget');
                echo "
                </div>
            ";
            } else {
                // line 321
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "time", [], "any", false, false, false, 321), 'widget', ["row" => false, "input_wrapper_class" => "col-sm-2"]);
                echo "
            ";
            }
            // line 323
            echo "        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 328
    public function block_form_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 329
        echo "    ";
        $context["show_label"] = (((isset($context["show_label"]) || array_key_exists("show_label", $context))) ? (_twig_default_filter(($context["show_label"] ?? null), true)) : (true));
        // line 330
        echo "    <div class=\"form-group";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            echo " has-error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\">
        ";
        // line 331
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, true, false, 331), "options", [], "any", true, true, false, 331)) {
            // line 332
            echo "            ";
            $context["label"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, true, false, 332), "options", [], "any", false, true, false, 332), "name", [], "any", true, true, false, 332)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, true, false, 332), "options", [], "any", false, true, false, 332), "name", [], "any", false, false, false, 332), ($context["label"] ?? null))) : (($context["label"] ?? null)));
            // line 333
            echo "        ";
        }
        // line 334
        echo "
        ";
        // line 335
        $context["div_class"] = "sonata-ba-field";
        // line 336
        echo "
        ";
        // line 337
        if ((($context["label"] ?? null) === false)) {
            // line 338
            echo "            ";
            $context["div_class"] = (($context["div_class"] ?? null) . " sonata-collection-row-without-label");
            // line 339
            echo "        ";
        }
        // line 340
        echo "
        ";
        // line 341
        if (((isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context)) && ((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "options", [], "any", false, false, false, 341)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["form_type"] ?? null) : null) == "horizontal"))) {
            // line 342
            echo "            ";
            // line 343
            echo "            ";
            if (((($context["label"] ?? null) === false) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 343), "checked", [], "any", true, true, false, 343))) {
                // line 344
                echo "                ";
                if (twig_in_filter("collection", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 344), "vars", [], "any", false, false, false, 344), "block_prefixes", [], "any", false, false, false, 344))) {
                    // line 345
                    echo "                    ";
                    $context["div_class"] = (($context["div_class"] ?? null) . " col-sm-12");
                    // line 346
                    echo "                ";
                } else {
                    // line 347
                    echo "                    ";
                    $context["div_class"] = (($context["div_class"] ?? null) . " col-sm-9 col-sm-offset-3");
                    // line 348
                    echo "                ";
                }
                // line 349
                echo "            ";
            } else {
                // line 350
                echo "                ";
                $context["div_class"] = (($context["div_class"] ?? null) . " col-sm-9");
                // line 351
                echo "            ";
            }
            // line 352
            echo "        ";
        }
        // line 353
        echo "
        ";
        // line 354
        if (($context["show_label"] ?? null)) {
            // line 355
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', (twig_test_empty($_label_ = (((isset($context["label"]) || array_key_exists("label", $context))) ? (_twig_default_filter(($context["label"] ?? null), null)) : (null))) ? [] : ["label" => $_label_]));
            echo "
        ";
        }
        // line 357
        echo "
        ";
        // line 358
        if (((isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context)) && ($context["sonata_admin_enabled"] ?? null))) {
            // line 359
            echo "            ";
            $context["div_class"] = ((((($context["div_class"] ?? null) . " sonata-ba-field-") . twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "edit", [], "any", false, false, false, 359)) . "-") . twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "inline", [], "any", false, false, false, 359));
            // line 360
            echo "        ";
        }
        // line 361
        echo "
        ";
        // line 362
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 363
            echo "            ";
            $context["div_class"] = (($context["div_class"] ?? null) . " sonata-ba-field-error");
            // line 364
            echo "        ";
        }
        // line 365
        echo "
        <div class=\"";
        // line 366
        echo twig_escape_filter($this->env, ($context["div_class"] ?? null), "html", null, true);
        echo "\">
            ";
        // line 367
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["horizontal" => false, "horizontal_input_wrapper_class" => ""]);
        echo " ";
        // line 368
        echo "
            ";
        // line 369
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 370
            echo "                <div class=\"help-block sonata-ba-field-error-messages\">
                    ";
            // line 371
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                </div>
            ";
        }
        // line 374
        echo "
            ";
        // line 375
        if ((((isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context)) && ($context["sonata_admin_enabled"] ?? null)) && ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, true, false, 375), "help", [], "any", true, true, false, 375)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, true, false, 375), "help", [], "any", false, false, false, 375), false)) : (false)))) {
            // line 376
            echo "                <span class=\"help-block sonata-ba-field-help\">";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 376), "help", [], "any", false, false, false, 376), [], ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 376), "translationDomain", [], "any", false, false, false, 376)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "field_description", [], "any", false, false, false, 376), "translationDomain", [], "any", false, false, false, 376)) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 376))));
            echo "</span>
            ";
        }
        // line 378
        echo "        </div>
    </div>
";
    }

    // line 382
    public function block_checkbox_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 383
        $context["show_label"] = false;
        // line 384
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
    }

    // line 387
    public function block_radio_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 388
        $context["show_label"] = false;
        // line 389
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
    }

    // line 392
    public function block_sonata_type_native_collection_widget_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 393
        ob_start(function () { return ''; });
        // line 394
        echo "    <div class=\"sonata-collection-row\">
        ";
        // line 395
        if (($context["allow_delete"] ?? null)) {
            // line 396
            echo "            <div class=\"row\">
                <div class=\"col-xs-1\">
                    <a href=\"#\" class=\"btn btn-link sonata-collection-delete\">
                        <i class=\"fa fa-minus-circle\"></i>
                    </a>
                </div>
                <div class=\"col-xs-11\">
        ";
        }
        // line 404
        echo "            ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["child"] ?? null), 'row', ["label" => false]);
        echo "
        ";
        // line 405
        if (($context["allow_delete"] ?? null)) {
            // line 406
            echo "                </div>
            </div>
        ";
        }
        // line 409
        echo "    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 413
    public function block_sonata_type_native_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 414
        ob_start(function () { return ''; });
        // line 415
        echo "    ";
        if ((isset($context["prototype"]) || array_key_exists("prototype", $context))) {
            // line 416
            echo "        ";
            $context["child"] = ($context["prototype"] ?? null);
            // line 417
            echo "        ";
            $context["allow_delete_backup"] = ($context["allow_delete"] ?? null);
            // line 418
            echo "        ";
            $context["allow_delete"] = true;
            // line 419
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-prototype" =>             $this->renderBlock("sonata_type_native_collection_widget_row", $context, $blocks), "data-prototype-name" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["prototype"] ?? null), "vars", [], "any", false, false, false, 419), "name", [], "any", false, false, false, 419), "class" => ((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 419)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 419), "")) : (""))]);
            // line 420
            echo "        ";
            $context["allow_delete"] = ($context["allow_delete_backup"] ?? null);
            // line 421
            echo "    ";
        }
        // line 422
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        ";
        // line 423
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
        ";
        // line 424
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 425
            echo "            ";
            $this->displayBlock("sonata_type_native_collection_widget_row", $context, $blocks);
            echo "
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 427
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        ";
        // line 428
        if (($context["allow_add"] ?? null)) {
            // line 429
            echo "            <div><a href=\"#\" class=\"btn btn-link sonata-collection-add\"><i class=\"fa fa-plus-circle\"></i></a></div>
        ";
        }
        // line 431
        echo "    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 435
    public function block_sonata_type_immutable_array_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 436
        echo "    ";
        ob_start(function () { return ''; });
        // line 437
        echo "        <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
            ";
        // line 438
        $this->displayBlock("sonata_help", $context, $blocks);
        echo "

            ";
        // line 440
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "

            ";
        // line 442
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["child"]) {
            // line 443
            echo "                ";
            $this->displayBlock("sonata_type_immutable_array_widget_row", $context, $blocks);
            echo "
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 445
        echo "
            ";
        // line 446
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 451
    public function block_sonata_type_immutable_array_widget_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 452
        echo "    ";
        ob_start(function () { return ''; });
        // line 453
        echo "        <div class=\"form-group";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["child"] ?? null), "vars", [], "any", false, false, false, 453), "errors", [], "any", false, false, false, 453)) > 0)) {
            echo " has-error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, ($context["key"] ?? null), "html", null, true);
        echo "\">

            ";
        // line 455
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["child"] ?? null), 'label');
        echo "

            ";
        // line 457
        $context["div_class"] = "";
        // line 458
        echo "            ";
        if (((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "options", [], "any", false, false, false, 458)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["form_type"] ?? null) : null) == "horizontal")) {
            // line 459
            echo "                ";
            $context["div_class"] = "col-sm-9";
            // line 460
            echo "            ";
        }
        // line 461
        echo "
            <div class=\"";
        // line 462
        echo twig_escape_filter($this->env, ($context["div_class"] ?? null), "html", null, true);
        echo " sonata-ba-field sonata-ba-field-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "edit", [], "any", false, false, false, 462), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "inline", [], "any", false, false, false, 462), "html", null, true);
        echo " ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["child"] ?? null), "vars", [], "any", false, false, false, 462), "errors", [], "any", false, false, false, 462)) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">
                ";
        // line 463
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["child"] ?? null), 'widget', ["horizontal" => false, "horizontal_input_wrapper_class" => ""]);
        echo " ";
        // line 464
        echo "                ";
        $context["sonata_help"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["child"] ?? null), "vars", [], "any", false, false, false, 464), "sonata_help", [], "any", false, false, false, 464);
        // line 465
        echo "                ";
        $this->displayBlock("sonata_help", $context, $blocks);
        echo "
            </div>

            ";
        // line 468
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["child"] ?? null), "vars", [], "any", false, false, false, 468), "errors", [], "any", false, false, false, 468)) > 0)) {
            // line 469
            echo "                <div class=\"help-block sonata-ba-field-error-messages\">
                    ";
            // line 470
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["child"] ?? null), 'errors');
            echo "
                </div>
            ";
        }
        // line 473
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 477
    public function block_sonata_type_model_autocomplete_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 478
        echo "    ";
        $this->loadTemplate(($context["template"] ?? null), "@KinulabSonataGentellelaTheme/Form/form_admin_fields.html.twig", 478)->display($context);
    }

    // line 481
    public function block_sonata_type_choice_field_mask_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 482
        echo "    ";
        $this->displayBlock("choice_widget", $context, $blocks);
        echo "
    ";
        // line 484
        echo "    ";
        $context["main_form_name"] = twig_slice($this->env, ($context["id"] ?? null), 0, ((twig_length_filter($this->env, ($context["id"] ?? null)) - twig_length_filter($this->env, ($context["name"] ?? null))) - 1));
        // line 485
        echo "    ";
        if (($context["expanded"] ?? null)) {
            // line 486
            echo "        ";
            $context["js_selector"] = (((("#" . ($context["main_form_name"] ?? null)) . "_") . ($context["name"] ?? null)) . " input");
            // line 487
            echo "        ";
            $context["js_event"] = "ifChecked";
            // line 488
            echo "    ";
        } else {
            // line 489
            echo "        ";
            $context["js_selector"] = ((("#" . ($context["main_form_name"] ?? null)) . "_") . ($context["name"] ?? null));
            // line 490
            echo "        ";
            $context["js_event"] = "change";
            // line 491
            echo "    ";
        }
        // line 492
        echo "    <script>
        jQuery(document).ready(function() {
            var allFields = ";
        // line 494
        echo json_encode(($context["all_fields"] ?? null));
        echo ",
                map = ";
        // line 495
        echo json_encode(($context["map"] ?? null));
        echo ",
                showMaskChoiceEl = jQuery(\"";
        // line 496
        echo twig_escape_filter($this->env, ($context["js_selector"] ?? null), "html", null, true);
        echo "\");

            showMaskChoiceEl.on(\"";
        // line 498
        echo twig_escape_filter($this->env, ($context["js_event"] ?? null), "html", null, true);
        echo "\", function () {
                choice_field_mask_show(jQuery(this).val());
            });

            function choice_field_mask_show(val) {
                var controlGroupIdFunc = function (field) {
                    // Most of fields are named with an underscore
                    var defaultFieldId = '#sonata-ba-field-container-";
        // line 505
        echo twig_escape_filter($this->env, ($context["main_form_name"] ?? null), "html", null, true);
        echo "_' + field;

                    // Some fields may be named with a dash (like keys of immutable array form type)
                    if (jQuery(defaultFieldId).length === 0) {
                        return '#sonata-ba-field-container-";
        // line 509
        echo twig_escape_filter($this->env, ($context["main_form_name"] ?? null), "html", null, true);
        echo "-' + field;
                    }

                    return defaultFieldId;
                };

                jQuery.each(allFields, function (i, field) {
                    jQuery(controlGroupIdFunc(field)).hide();
                });

                if (map[val]) {
                    jQuery.each(map[val], function (i, field) {
                        jQuery(controlGroupIdFunc(field)).show();
                    });
                }
            }

            choice_field_mask_show('";
        // line 526
        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        echo "');
        });

    </script>
";
    }

    // line 532
    public function block_sonata_type_choice_multiple_sortable($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 533
        echo "    <input type=\"hidden\" name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\" id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, twig_join_filter(($context["value"] ?? null), ","), "html", null, true);
        echo "\" />

    <script>
        jQuery(document).ready(function() {
            Admin.setup_sortable_select2(jQuery('#";
        // line 537
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "'), ";
        echo json_encode(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 537), "choices", [], "any", false, false, false, 537));
        echo ");
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "@KinulabSonataGentellelaTheme/Form/form_admin_fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1422 => 537,  1410 => 533,  1406 => 532,  1397 => 526,  1377 => 509,  1370 => 505,  1360 => 498,  1355 => 496,  1351 => 495,  1347 => 494,  1343 => 492,  1340 => 491,  1337 => 490,  1334 => 489,  1331 => 488,  1328 => 487,  1325 => 486,  1322 => 485,  1319 => 484,  1314 => 482,  1310 => 481,  1305 => 478,  1301 => 477,  1295 => 473,  1289 => 470,  1286 => 469,  1284 => 468,  1277 => 465,  1274 => 464,  1271 => 463,  1259 => 462,  1256 => 461,  1253 => 460,  1250 => 459,  1247 => 458,  1245 => 457,  1240 => 455,  1228 => 453,  1225 => 452,  1221 => 451,  1213 => 446,  1210 => 445,  1193 => 443,  1176 => 442,  1171 => 440,  1166 => 438,  1161 => 437,  1158 => 436,  1154 => 435,  1148 => 431,  1144 => 429,  1142 => 428,  1137 => 427,  1120 => 425,  1103 => 424,  1099 => 423,  1094 => 422,  1091 => 421,  1088 => 420,  1085 => 419,  1082 => 418,  1079 => 417,  1076 => 416,  1073 => 415,  1071 => 414,  1067 => 413,  1061 => 409,  1056 => 406,  1054 => 405,  1049 => 404,  1039 => 396,  1037 => 395,  1034 => 394,  1032 => 393,  1028 => 392,  1023 => 389,  1021 => 388,  1017 => 387,  1012 => 384,  1010 => 383,  1006 => 382,  1000 => 378,  994 => 376,  992 => 375,  989 => 374,  983 => 371,  980 => 370,  978 => 369,  975 => 368,  972 => 367,  968 => 366,  965 => 365,  962 => 364,  959 => 363,  957 => 362,  954 => 361,  951 => 360,  948 => 359,  946 => 358,  943 => 357,  937 => 355,  935 => 354,  932 => 353,  929 => 352,  926 => 351,  923 => 350,  920 => 349,  917 => 348,  914 => 347,  911 => 346,  908 => 345,  905 => 344,  902 => 343,  900 => 342,  898 => 341,  895 => 340,  892 => 339,  889 => 338,  887 => 337,  884 => 336,  882 => 335,  879 => 334,  876 => 333,  873 => 332,  871 => 331,  862 => 330,  859 => 329,  855 => 328,  848 => 323,  842 => 321,  836 => 318,  833 => 317,  831 => 316,  828 => 315,  822 => 313,  816 => 310,  813 => 309,  811 => 308,  806 => 306,  802 => 305,  797 => 304,  794 => 303,  788 => 301,  785 => 300,  783 => 299,  779 => 298,  772 => 293,  766 => 290,  761 => 289,  758 => 288,  752 => 285,  747 => 284,  745 => 283,  740 => 281,  736 => 280,  731 => 279,  728 => 278,  725 => 277,  722 => 276,  719 => 275,  713 => 273,  710 => 272,  708 => 271,  704 => 270,  696 => 264,  694 => 263,  693 => 262,  692 => 261,  691 => 260,  686 => 259,  683 => 258,  680 => 257,  677 => 256,  674 => 255,  668 => 253,  665 => 252,  663 => 251,  659 => 250,  649 => 244,  646 => 243,  643 => 242,  637 => 240,  635 => 239,  630 => 238,  627 => 237,  624 => 236,  620 => 234,  617 => 233,  614 => 231,  611 => 229,  608 => 228,  606 => 227,  599 => 226,  597 => 225,  594 => 224,  591 => 223,  588 => 221,  585 => 219,  582 => 218,  580 => 217,  573 => 216,  571 => 215,  563 => 214,  557 => 212,  554 => 211,  552 => 210,  549 => 209,  546 => 208,  543 => 207,  540 => 206,  537 => 205,  534 => 204,  532 => 203,  528 => 202,  522 => 198,  514 => 195,  512 => 194,  510 => 193,  509 => 190,  506 => 189,  502 => 188,  497 => 187,  494 => 186,  492 => 185,  488 => 184,  482 => 180,  479 => 178,  476 => 176,  473 => 174,  471 => 173,  468 => 172,  466 => 171,  464 => 170,  449 => 169,  446 => 168,  443 => 167,  440 => 166,  437 => 165,  434 => 164,  431 => 163,  428 => 162,  425 => 161,  422 => 160,  419 => 159,  417 => 158,  414 => 157,  411 => 156,  408 => 155,  404 => 154,  400 => 151,  396 => 150,  392 => 147,  388 => 146,  381 => 141,  375 => 139,  372 => 137,  370 => 136,  368 => 135,  366 => 134,  351 => 133,  348 => 132,  344 => 129,  341 => 126,  340 => 125,  339 => 124,  337 => 123,  335 => 122,  332 => 121,  329 => 120,  326 => 119,  323 => 118,  320 => 117,  317 => 116,  315 => 115,  312 => 114,  309 => 113,  307 => 112,  304 => 111,  302 => 110,  299 => 109,  296 => 108,  293 => 107,  290 => 106,  288 => 105,  284 => 104,  279 => 99,  277 => 98,  275 => 97,  272 => 95,  270 => 94,  268 => 93,  264 => 92,  259 => 88,  257 => 87,  255 => 86,  252 => 84,  250 => 83,  248 => 82,  244 => 81,  235 => 75,  232 => 74,  229 => 73,  226 => 72,  222 => 71,  217 => 68,  213 => 66,  211 => 65,  208 => 64,  205 => 63,  203 => 62,  197 => 60,  195 => 59,  193 => 58,  190 => 57,  187 => 56,  184 => 54,  182 => 53,  178 => 52,  171 => 49,  168 => 48,  164 => 47,  157 => 44,  154 => 43,  151 => 42,  148 => 41,  145 => 40,  141 => 39,  137 => 36,  133 => 35,  129 => 34,  120 => 29,  118 => 28,  116 => 27,  112 => 26,  107 => 23,  103 => 22,  100 => 21,  91 => 19,  87 => 18,  84 => 17,  79 => 16,  77 => 15,  73 => 14,  62 => 12,);
    }

    public function getSourceContext()
    {
        return new Source("", "@KinulabSonataGentellelaTheme/Form/form_admin_fields.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/kinulab/sonata-gentellela-theme-bundle/Resources/views/Form/form_admin_fields.html.twig");
    }
}
