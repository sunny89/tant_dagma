<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* DataSheet/base_edit_datasheet.html.twig */
class __TwigTemplate_282531e3671c3400dd641a44d54b634627f6953d609b3e7337863eb6242dd5eb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 44
        $_trait_0 = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig", "DataSheet/base_edit_datasheet.html.twig", 44);
        if (!$_trait_0->isTraitable()) {
            throw new RuntimeError('Template "'."@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig".'" cannot be used as a trait.', 44, $this->source);
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        if (!isset($_trait_0_blocks["form"])) {
            throw new RuntimeError('Block "form" is not defined in trait "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig".', 44, $this->source);
        }

        $_trait_0_blocks["parentForm"] = $_trait_0_blocks["form"]; unset($_trait_0_blocks["form"]);

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            [
                'javascripts' => [$this, 'block_javascripts'],
                'title' => [$this, 'block_title'],
                'navbar_title' => [$this, 'block_navbar_title'],
                'actions' => [$this, 'block_actions'],
                'tab_menu' => [$this, 'block_tab_menu'],
                'form' => [$this, 'block_form'],
            ]
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(($context["base_template"] ?? null), "DataSheet/base_edit_datasheet.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/jquery.chained.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/jquery.wizard_for_type.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
";
    }

    // line 20
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        // line 22
        echo "    ";
        if ( !(null === (((isset($context["objectId"]) || array_key_exists("objectId", $context))) ? (_twig_default_filter(($context["objectId"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 22))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 22))))) {
            // line 23
            echo "        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("title_edit", ["%name%" => twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "toString", [0 => ($context["object"] ?? null)], "method", false, false, false, 23), 15)], "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 25
            echo "        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("title_create", [], "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
    }

    // line 29
    public function block_navbar_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
    }

    // line 33
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/action_buttons.html.twig", "DataSheet/base_edit_datasheet.html.twig", 34)->display($context);
    }

    // line 37
    public function block_tab_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "sidemenu", [0 => ($context["action"] ?? null)], "method", false, false, false, 38), ["currentClass" => "active", "template" => $this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("tab_menu_template")], "twig");
    }

    // line 46
    public function block_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        if (twig_in_filter("create", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 47), "uri", [], "any", false, false, false, 47))) {
            // line 48
            echo "    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"x_panel\">
                <div class=\"row\">


                    <div class=\"col-md-6\">
                        <div class=\"box-primary\">
                            <div class=\"x_title\">
                                <h4>
                                    Light-Type-Wizard
                                </h4>
                            </div>
                            <div class=\"x_content\">
                                <div class=\"sonata-ba-collapsed-fields\">
                                    <div class=\"form-group\">
                                        <label class=\"control-label\">
                                            Main-Category
                                        </label>
                                        <select id=\"wizard_maincategory\" name=\"wizard_maincategory\">
                                            <option value=\"\">--</option>
                                            <option value=\"lights\">Lights</option>
                                        </select>
                                        <label class=\"control-label\">
                                            Sub-Category
                                        </label>
                                        <select id=\"wizard_subcategory\" name=\"wizard_subcategory\">
                                            <option value=\"\">--</option>
                                            <option value=\"ceiling\" data-chained=\"lights\">Ceiling</option>
                                            <option value=\"ground\" data-chained=\"lights\">Ground</option>
                                            <option value=\"wall\" data-chained=\"lights\">Wall</option>
                                            <option value=\"linear\" data-chained=\"lights\">Linear</option>
                                            <option value=\"equipment\" data-chained=\"lights\">Equipment</option>
                                            <option value=\"furniture\" data-chained=\"lights\">Furniture</option>
                                            <option value=\"precab\" data-chained=\"lights\">Precab</option>
                                        </select>
                                        <label class=\"control-label\">
                                            Subclass
                                        </label>
                                        <select id=\"wizard_subclass\" name=\"wizard_subclass\">
                                            <option value=\"\">--</option>
                                            <option value=\"technical\" data-chained=\"ceiling\">Technical</option>
                                            <option value=\"decorative\" data-chained=\"ceiling\">Decorative</option>
                                            <option value=\"architectural\" data-chained=\"ceiling\">Architectural</option>
                                            <option value=\"ground\" data-chained=\"ground\">Ground</option>
                                            <option value=\"wall\" data-chained=\"wall\">Wall</option>
                                            <option value=\"linear\" data-chained=\"linea\">Linear</option>
                                            <option value=\"equipment\" data-chained=\"equipment\">Equipment</option>
                                            <option value=\"furniture\" data-chained=\"equipment\">Furniture</option>
                                            <option value=\"precab\" data-chained=\"equipment\">Precab</option>
                                        </select>
                                        <label class=\"control-label\">
                                            Description
                                        </label>
                                        <select id=\"wizard_description\" name=\"wizard_description\">
                                            <option value=\"\">--</option>
                                            <option value=\"ceilingLights\" data-chained=\"technical\">Ceiling lights</option>
                                            <option value=\"engineRoomLights\" data-chained=\"technical\">Engine room lights</option>
                                            <option value=\"galleysRefridgeratedRoomLights\" data-chained=\"technical\">Galleys & Refridgerated room lights</option>
                                            <option value=\"exLights\" data-chained=\"technical\">EX-Lights</option>
                                            <option value=\"exitLights\" data-chained=\"technical\">Exit lights</option>
                                            <option value=\"decorativeCeilingLights\" data-chained=\"decorative\">Decorative ceiling lights</option>
                                            <option value=\"festoonLights\" data-chained=\"decorative\">Festoon lights</option>
                                            <option value=\"compactFluoresent\" data-chained=\"architectural\">Compact fluoresent</option>
                                            <option value=\"lowVoltageHalogen\" data-chained=\"architectural\">low-voltage halogen</option>
                                            <option value=\"mainVoltageIncandescentLEDDownlights\" data-chained=\"architectural\">main voltage incandescent, LED downlights</option>
                                            <option value=\"wallWashersAndAdjustableLED\" data-chained=\"architectural\">Wall-washers and adjustable LED</option>
                                            <option value=\"trackLights\" data-chained=\"architectural\">Track lights</option>
                                            <option value=\"battenLuminaires\" data-chained=\"architectural\">Batten luminaires</option>
                                            <option value=\"outdoorLights\" data-chained=\"architectural\">Outdoor lights</option>
                                        </select>
                                        <label>
                                            BK (old)
                                        </label>
                                        <select id=\"wizard_bk\" name=\"wizard_bk\">
                                            <option value=\"\">--</option>
                                            <option value=\"05\" data-end=\"LCT\" data-chained=\"ceilingLights\">05</option>
                                            <option value=\"13\" data-end=\"LCT\" data-chained=\"engineRoomLights\">13</option>
                                            <option value=\"14\" data-end=\"LCT\" data-chained=\"galleysRefridgeratedRoomLights\">14</option>
                                            <option value=\"16\" data-end=\"LCT\" data-chained=\"exLights\">16</option>
                                            <option value=\"31\" data-end=\"LCT\" data-chained=\"exitLights\">31</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-6\">
                        <div class=\"box-primary\">
                            <div class=\"x_title\">
                                <h4>
                                    Light-Type
                                </h4>
                            </div>
                            <div class=\"x_content\">
                                <div class=\"sonata-ba-collapsed-fields\">
                                    <div class=\"form-group\">
                                        <label class=\"control-label\">
                                            Main Category
                                        </label>
                                        <input type=\"text\" id=\"result_wizard_maincategory\" name=\"result_wizard_maincategory\" required=\"required\" maxlength=\"255\" class=\" form-control\" value=\"\" disabled=\"disabled\">
                                        <label class=\"control-label\">
                                            Sub Category
                                        </label>
                                        <input type=\"text\" id=\"result_wizard_subcategory\" name=\"result_wizard_subcategory\" required=\"required\" maxlength=\"255\" class=\" form-control\" value=\"\" disabled=\"disabled\">
                                        <label class=\"control-label\">
                                            Subclass
                                        </label>
                                        <input type=\"text\" id=\"result_wizard_subclass\" name=\"result_wizard_subclass\" required=\"required\" maxlength=\"255\" class=\" form-control\" value=\"\" disabled=\"disabled\">
                                        <label class=\"control-label\">
                                            Description
                                        </label>
                                        <input type=\"text\" id=\"result_wizard_description\" name=\"result_wizard_description\" required=\"required\" maxlength=\"255\" class=\" form-control\" value=\"\" disabled=\"disabled\">
                                        <label class=\"control-label\">
                                            BK (old)
                                        </label>
                                        <input type=\"text\" id=\"result_wizard_bk\" name=\"result_wizard_bk\" required=\"required\" maxlength=\"255\" class=\" form-control\" value=\"\" disabled=\"disabled\">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        }
        // line 176
        echo "

    ";
        // line 178
        $this->displayBlock("parentForm", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "DataSheet/base_edit_datasheet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 178,  273 => 176,  143 => 48,  141 => 47,  137 => 46,  133 => 38,  129 => 37,  125 => 34,  121 => 33,  114 => 30,  110 => 29,  102 => 25,  96 => 23,  93 => 22,  91 => 21,  87 => 20,  81 => 17,  77 => 16,  72 => 15,  68 => 14,  58 => 12,  28 => 44,);
    }

    public function getSourceContext()
    {
        return new Source("", "DataSheet/base_edit_datasheet.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/templates/DataSheet/base_edit_datasheet.html.twig");
    }
}
