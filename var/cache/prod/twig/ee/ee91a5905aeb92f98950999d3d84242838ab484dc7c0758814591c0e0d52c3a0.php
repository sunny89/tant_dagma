<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @KinulabSonataGentellelaTheme/standard_layout.html.twig */
class __TwigTemplate_3f60c5f165075641fd8f0ca20aa5492b30d389f50a29cbb773b9b7be15b94bc6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'html_attributes' => [$this, 'block_html_attributes'],
            'meta_tags' => [$this, 'block_meta_tags'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'sonata_head_title' => [$this, 'block_sonata_head_title'],
            'body_attributes' => [$this, 'block_body_attributes'],
            'sonata_header' => [$this, 'block_sonata_header'],
            'sonata_nav' => [$this, 'block_sonata_nav'],
            'logo' => [$this, 'block_logo'],
            'sonata_left_side' => [$this, 'block_sonata_left_side'],
            'sonata_side_nav' => [$this, 'block_sonata_side_nav'],
            'side_bar_before_nav' => [$this, 'block_side_bar_before_nav'],
            'side_bar_nav' => [$this, 'block_side_bar_nav'],
            'side_bar_after_nav' => [$this, 'block_side_bar_after_nav'],
            'side_bar_after_nav_content' => [$this, 'block_side_bar_after_nav_content'],
            'sonata_breadcrumb' => [$this, 'block_sonata_breadcrumb'],
            'sonata_header_noscript_warning' => [$this, 'block_sonata_header_noscript_warning'],
            'sonata_top_nav_menu' => [$this, 'block_sonata_top_nav_menu'],
            'sonata_top_nav_menu_user_block' => [$this, 'block_sonata_top_nav_menu_user_block'],
            'sonata_top_nav_menu_add_block' => [$this, 'block_sonata_top_nav_menu_add_block'],
            'sonata_sidebar_search' => [$this, 'block_sonata_sidebar_search'],
            'sonata_page_content' => [$this, 'block_sonata_page_content'],
            'sonata_page_content_header' => [$this, 'block_sonata_page_content_header'],
            'sonata_page_content_nav' => [$this, 'block_sonata_page_content_nav'],
            'tab_menu_navbar_header' => [$this, 'block_tab_menu_navbar_header'],
            'sonata_admin_content_actions_wrappers' => [$this, 'block_sonata_admin_content_actions_wrappers'],
            'sonata_admin_content' => [$this, 'block_sonata_admin_content'],
            'notice' => [$this, 'block_notice'],
            'bootlint' => [$this, 'block_bootlint'],
            'footer_javascripts' => [$this, 'block_footer_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        $context["_preview"] = ((        $this->hasBlock("preview", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("preview", $context, $blocks))) : (null));
        // line 12
        $context["_form"] = ((        $this->hasBlock("form", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("form", $context, $blocks))) : (null));
        // line 13
        $context["_show"] = ((        $this->hasBlock("show", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("show", $context, $blocks))) : (null));
        // line 14
        $context["_list_table"] = ((        $this->hasBlock("list_table", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_table", $context, $blocks))) : (null));
        // line 15
        $context["_list_filters"] = ((        $this->hasBlock("list_filters", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters", $context, $blocks))) : (null));
        // line 16
        $context["_tab_menu"] = ((        $this->hasBlock("tab_menu", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("tab_menu", $context, $blocks))) : (null));
        // line 17
        $context["_content"] = ((        $this->hasBlock("content", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("content", $context, $blocks))) : (null));
        // line 18
        $context["_title"] = ((        $this->hasBlock("title", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("title", $context, $blocks))) : (null));
        // line 19
        $context["_breadcrumb"] = ((        $this->hasBlock("breadcrumb", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("breadcrumb", $context, $blocks))) : (null));
        // line 20
        $context["_actions"] = ((        $this->hasBlock("actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("actions", $context, $blocks))) : (null));
        // line 21
        $context["_navbar_title"] = ((        $this->hasBlock("navbar_title", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("navbar_title", $context, $blocks))) : (null));
        // line 22
        $context["_list_filters_actions"] = ((        $this->hasBlock("list_filters_actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters_actions", $context, $blocks))) : (null));
        // line 24
        echo "<!DOCTYPE html>
<html ";
        // line 25
        $this->displayBlock('html_attributes', $context, $blocks);
        echo " lang=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 25), "getLocale", [], "method", false, false, false, 25), "html", null, true);
        echo "\">
    <head>
        ";
        // line 27
        $this->displayBlock('meta_tags', $context, $blocks);
        // line 32
        echo "
        ";
        // line 33
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 38
        echo "
        ";
        // line 39
        $this->displayBlock('javascripts', $context, $blocks);
        // line 60
        echo "
        <title>
        ";
        // line 62
        $this->displayBlock('sonata_head_title', $context, $blocks);
        // line 88
        echo "        </title>
    </head>
    <body
            ";
        // line 91
        $this->displayBlock('body_attributes', $context, $blocks);
        // line 97
        echo ">
        <div class=\"container body\">
            <div class=\"main_container\">
                ";
        // line 100
        $this->displayBlock('sonata_header', $context, $blocks);
        // line 254
        echo "
                <div class=\"right_col\" role=\"main\">
                    ";
        // line 256
        $this->displayBlock('sonata_page_content', $context, $blocks);
        // line 360
        echo "                </div>
            </div>
        </div>


    ";
        // line 365
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 365), "getOption", [0 => "use_bootlint"], "method", false, false, false, 365)) {
            // line 366
            echo "        ";
            $this->displayBlock('bootlint', $context, $blocks);
            // line 372
            echo "    ";
        }
        // line 373
        echo "
    ";
        // line 374
        $this->displayBlock('footer_javascripts', $context, $blocks);
        // line 397
        echo "
    </body>
</html>
";
    }

    // line 25
    public function block_html_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "class=\"no-js\"";
    }

    // line 27
    public function block_meta_tags($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        ";
    }

    // line 33
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 34), "getOption", [0 => "stylesheets", 1 => []], "method", false, false, false, 34));
        foreach ($context['_seq'] as $context["_key"] => $context["stylesheet"]) {
            // line 35
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($context["stylesheet"]), "html", null, true);
            echo "\">
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stylesheet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        ";
    }

    // line 39
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "            <script>
                window.SONATA_CONFIG = {
                    CONFIRM_EXIT: ";
        // line 42
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 42), "getOption", [0 => "confirm_exit"], "method", false, false, false, 42)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                    USE_SELECT2: ";
        // line 43
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 43), "getOption", [0 => "use_select2"], "method", false, false, false, 43)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                    USE_ICHECK: ";
        // line 44
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 44), "getOption", [0 => "use_icheck"], "method", false, false, false, 44)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                    USE_STICKYFORMS: ";
        // line 45
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 45), "getOption", [0 => "use_stickyforms"], "method", false, false, false, 45)) {
            echo "true";
        } else {
            echo "false";
        }
        // line 46
        echo "                };
                window.SONATA_TRANSLATIONS = {
                    CONFIRM_EXIT: '";
        // line 48
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("confirm_exit", [], "SonataAdminBundle"), "js"), "html", null, true);
        echo "'
                };

                // http://getbootstrap.com/getting-started/#support-ie10-width
                if (navigator.userAgent.match(/IEMobile\\/10\\.0/)) {
                    var msViewportStyle = document.createElement('style');
                    msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
                    document.querySelector('head').appendChild(msViewportStyle);
                }
            </script>
            <script src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/kinulabsonatagentellelatheme/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    // line 62
    public function block_sonata_head_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "            ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Admin", [], "SonataAdminBundle"), "html", null, true);
        echo "

            ";
        // line 65
        if ( !twig_test_empty(($context["_title"] ?? null))) {
            // line 66
            echo "                ";
            echo strip_tags(($context["_title"] ?? null));
            echo "
            ";
        } else {
            // line 68
            echo "                ";
            if ((isset($context["action"]) || array_key_exists("action", $context))) {
                // line 69
                echo "                    -
                    ";
                // line 70
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["breadcrumbs_builder"] ?? null), "breadcrumbs", [0 => ($context["admin"] ?? null), 1 => ($context["action"] ?? null)], "method", false, false, false, 70));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                    // line 71
                    echo "                        ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 71)) {
                        // line 72
                        echo "                            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 72) != 2)) {
                            // line 73
                            echo "                                &gt;
                            ";
                        }
                        // line 76
                        $context["translation_domain"] = twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_domain", 1 => "messages"], "method", false, false, false, 76);
                        // line 77
                        $context["label"] = twig_get_attribute($this->env, $this->source, $context["menu"], "label", [], "any", false, false, false, 77);
                        // line 78
                        if ( !(($context["translation_domain"] ?? null) === false)) {
                            // line 79
                            $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_params", 1 => []], "method", false, false, false, 79), ($context["translation_domain"] ?? null));
                        }
                        // line 82
                        echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                        echo "
                        ";
                    }
                    // line 84
                    echo "                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 85
                echo "                ";
            }
            // line 86
            echo "            ";
        }
        // line 87
        echo "        ";
    }

    // line 91
    public function block_body_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "class=\"sonata-bc nav-md
                ";
        // line 93
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 93), "cookies", [], "any", false, false, false, 93), "get", [0 => "sonata_sidebar_hide"], "method", false, false, false, 93)) {
            // line 94
            echo "sidebar-collapse";
        }
        // line 95
        echo "\"";
    }

    // line 100
    public function block_sonata_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 101
        echo "                    ";
        $this->displayBlock('sonata_nav', $context, $blocks);
        // line 253
        echo "                ";
    }

    // line 101
    public function block_sonata_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 102
        echo "                <div class=\"col-md-3 left_col\">
                    <div class=\"left_col scroll-view\">
                        <div class=\"navbar nav_title\" style=\"border: 0;\">
                            ";
        // line 105
        $this->displayBlock('logo', $context, $blocks);
        // line 117
        echo "                        </div>

                        <div class=\"clearfix\"></div>

                        ";
        // line 121
        $this->displayBlock('sonata_left_side', $context, $blocks);
        // line 140
        echo "                    </div>
                </div>

                <div class=\"top_nav\">
                    <div class=\"nav_menu\">
                        <div role=\"navigation\">
                            <div class=\"nav toggle\">
                                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
                            </div>

                            ";
        // line 150
        $this->displayBlock('sonata_breadcrumb', $context, $blocks);
        // line 191
        echo "
                            ";
        // line 192
        $this->displayBlock('sonata_header_noscript_warning', $context, $blocks);
        // line 199
        echo "


                            ";
        // line 202
        $this->displayBlock('sonata_top_nav_menu', $context, $blocks);
        // line 248
        echo "
                        </div>
                    </div>
                </div>
                ";
    }

    // line 105
    public function block_logo($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 106
        echo "                            ";
        ob_start(function () { return ''; });
        // line 107
        echo "                                <a class=\"site_title\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sonata_admin_dashboard");
        echo "\">
                                ";
        // line 108
        if ((("single_image" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 108), "getOption", [0 => "title_mode"], "method", false, false, false, 108)) || ("both" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 108), "getOption", [0 => "title_mode"], "method", false, false, false, 108)))) {
            // line 109
            echo "                                    <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 109), "titlelogo", [], "any", false, false, false, 109)), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 109), "title", [], "any", false, false, false, 109), "html", null, true);
            echo "\">
                                ";
        }
        // line 111
        echo "                                ";
        if ((("single_text" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 111), "getOption", [0 => "title_mode"], "method", false, false, false, 111)) || ("both" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 111), "getOption", [0 => "title_mode"], "method", false, false, false, 111)))) {
            // line 112
            echo "                                    <span>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 112), "title", [], "any", false, false, false, 112), "html", null, true);
            echo "</span>
                                ";
        }
        // line 114
        echo "                                </a>
                            ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 116
        echo "                            ";
    }

    // line 121
    public function block_sonata_left_side($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 122
        echo "                            <div class=\"main_menu_side hidden-print main_menu\" id=\"sidebar-menu\">
                                <div class=\"menu_section active\">
                            ";
        // line 124
        $this->displayBlock('sonata_side_nav', $context, $blocks);
        // line 137
        echo "                                </div>
                            </div>
                        ";
    }

    // line 124
    public function block_sonata_side_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 125
        echo "
                                ";
        // line 126
        $this->displayBlock('side_bar_before_nav', $context, $blocks);
        // line 127
        echo "                                ";
        $this->displayBlock('side_bar_nav', $context, $blocks);
        // line 130
        echo "                                ";
        $this->displayBlock('side_bar_after_nav', $context, $blocks);
        // line 136
        echo "                            ";
    }

    // line 126
    public function block_side_bar_before_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 127
    public function block_side_bar_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 128
        echo "                                    ";
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render("sonata_admin_sidebar", ["template" => $this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("knp_menu_template")]);
        echo "
                                ";
    }

    // line 130
    public function block_side_bar_after_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 131
        echo "                                    <p class=\"text-center small project-link\" style=\"border-top: 1px solid #444444; padding-top: 10px\">
                                        ";
        // line 132
        $this->displayBlock('side_bar_after_nav_content', $context, $blocks);
        // line 134
        echo "                                    </p>
                                ";
    }

    // line 132
    public function block_side_bar_after_nav_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 133
        echo "                                        ";
    }

    // line 150
    public function block_sonata_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 151
        echo "                                <div class=\"hidden-xs nav navbar-nav navbar-left\">
                                    ";
        // line 152
        if (( !twig_test_empty(($context["_breadcrumb"] ?? null)) || (isset($context["action"]) || array_key_exists("action", $context)))) {
            // line 153
            echo "                                        <ol class=\"breadcrumb\">
                                            ";
            // line 154
            if (twig_test_empty(($context["_breadcrumb"] ?? null))) {
                // line 155
                echo "                                                ";
                if ((isset($context["action"]) || array_key_exists("action", $context))) {
                    // line 156
                    echo "                                                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["breadcrumbs_builder"] ?? null), "breadcrumbs", [0 => ($context["admin"] ?? null), 1 => ($context["action"] ?? null)], "method", false, false, false, 156));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                        // line 157
                        $context["translation_domain"] = twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_domain", 1 => "messages"], "method", false, false, false, 157);
                        // line 158
                        $context["label"] = twig_get_attribute($this->env, $this->source, $context["menu"], "label", [], "any", false, false, false, 158);
                        // line 159
                        if ( !(($context["translation_domain"] ?? null) === false)) {
                            // line 160
                            $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_params", 1 => []], "method", false, false, false, 160), ($context["translation_domain"] ?? null));
                        }
                        // line 163
                        if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 163)) {
                            // line 164
                            echo "                                                            <li>
                                                                ";
                            // line 165
                            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["menu"], "uri", [], "any", false, false, false, 165))) {
                                // line 166
                                echo "                                                                    <a href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["menu"], "uri", [], "any", false, false, false, 166), "html", null, true);
                                echo "\">
                                                                        ";
                                // line 167
                                if (twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "safe_label", 1 => true], "method", false, false, false, 167)) {
                                    // line 168
                                    echo ($context["label"] ?? null);
                                } else {
                                    // line 170
                                    echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                                }
                                // line 172
                                echo "                                                                    </a>
                                                                ";
                            } else {
                                // line 174
                                echo "                                                                    <span>";
                                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                                echo "</span>
                                                                ";
                            }
                            // line 176
                            echo "                                                            </li>
                                                        ";
                        } else {
                            // line 178
                            echo "                                                            <li class=\"active\">
                                                                <span>";
                            // line 179
                            echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                            echo "</span>
                                                            </li>
                                                        ";
                        }
                        // line 182
                        echo "                                                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 183
                    echo "                                                ";
                }
                // line 184
                echo "                                            ";
            } else {
                // line 185
                echo "                                                ";
                echo ($context["_breadcrumb"] ?? null);
                echo "
                                            ";
            }
            // line 187
            echo "                                        </ol>
                                    ";
        }
        // line 189
        echo "                                </div>
                            ";
    }

    // line 192
    public function block_sonata_header_noscript_warning($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 193
        echo "                                <noscript>
                                    <div class=\"noscript-warning\">
                                        ";
        // line 195
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("noscript_warning", [], "SonataAdminBundle"), "html", null, true);
        echo "
                                    </div>
                                </noscript>
                            ";
    }

    // line 202
    public function block_sonata_top_nav_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 203
        echo "                                ";
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 203) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 203), "getOption", [0 => "role_admin"], "method", false, false, false, 203)))) {
            // line 204
            echo "                                        <ul class=\"nav navbar-nav navbar-right\">
                                            ";
            // line 205
            $this->displayBlock('sonata_top_nav_menu_user_block', $context, $blocks);
            // line 216
            echo "                                            ";
            $this->displayBlock('sonata_top_nav_menu_add_block', $context, $blocks);
            // line 224
            echo "                                            ";
            $this->displayBlock('sonata_sidebar_search', $context, $blocks);
            // line 245
            echo "                                        </ul>
                                ";
        }
        // line 247
        echo "                            ";
    }

    // line 205
    public function block_sonata_top_nav_menu_user_block($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 206
        echo "                                                <li class=\"dropdown user-menu\">
                                                    <a class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                        <img src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/kinulabsonatagentellelatheme/images/user.png"), "html", null, true);
        echo "\" alt=\"\" /> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 208), "html", null, true);
        echo "
                                                        <span class=\"fa fa-angle-down\"></span>
                                                    </a>
                                                    <ul class=\"dropdown-menu dropdown-user\">
                                                        ";
        // line 212
        $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("user_block"), "@KinulabSonataGentellelaTheme/standard_layout.html.twig", 212)->display($context);
        // line 213
        echo "                                                    </ul>
                                                </li>
                                            ";
    }

    // line 216
    public function block_sonata_top_nav_menu_add_block($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 217
        echo "                                                <li class=\"dropdown\">
                                                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                        <i class=\"fa fa-plus-circle fa-fw\"></i> <span class=\"fa fa-angle-down\"></span>
                                                    </a>
                                                    ";
        // line 221
        $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("add_block"), "@KinulabSonataGentellelaTheme/standard_layout.html.twig", 221)->display($context);
        // line 222
        echo "                                                </li>
                                            ";
    }

    // line 224
    public function block_sonata_sidebar_search($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 225
        echo "                                                ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 225), "getOption", [0 => "search"], "method", false, false, false, 225)) {
            // line 226
            echo "                                                    <li class=\"dropdown\">
                                                        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                            <i class=\"fa fa-search fa-fw\"></i> <span class=\"fa fa-angle-down\"></span>
                                                        </a>
                                                        <div class=\"dropdown-menu dropdown-search\">
                                                            <div class=\"top_search form-group\"
                                                                <form action=\"";
            // line 232
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sonata_admin_search");
            echo "\" method=\"GET\" role=\"search\">
                                                                    <div class=\"input-group\">
                                                                        <input type=\"text\" name=\"q\" value=\"";
            // line 234
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 234), "get", [0 => "q"], "method", false, false, false, 234), "html", null, true);
            echo "\" class=\"form-control\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("search_placeholder", [], "SonataAdminBundle"), "html", null, true);
            echo "\">
                                                                        <span class=\"input-group-btn\">
                                                                            <button type=\"submit\" class=\"btn btn-default\">Go!</button>
                                                                        </span>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </li>
                                                ";
        }
        // line 244
        echo "                                            ";
    }

    // line 256
    public function block_sonata_page_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 257
        echo "                    <section class=\"content-header\">

                        ";
        // line 259
        $this->displayBlock('sonata_page_content_header', $context, $blocks);
        // line 321
        echo "                    </section>

                    <section class=\"content\">
                        ";
        // line 324
        $this->displayBlock('sonata_admin_content', $context, $blocks);
        // line 358
        echo "                    </section>
                ";
    }

    // line 259
    public function block_sonata_page_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 260
        echo "                            ";
        $this->displayBlock('sonata_page_content_nav', $context, $blocks);
        // line 320
        echo "                        ";
    }

    // line 260
    public function block_sonata_page_content_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 261
        echo "                                ";
        if (((( !twig_test_empty(($context["_navbar_title"] ?? null)) ||  !twig_test_empty(        // line 262
($context["_tab_menu"] ?? null))) ||  !twig_test_empty(        // line 263
($context["_actions"] ?? null))) ||  !twig_test_empty(        // line 264
($context["_list_filters_actions"] ?? null)))) {
            // line 266
            echo "                                    <nav class=\"nav\" role=\"navigation\">
                                        <div class=\"container\">
                                            ";
            // line 268
            $this->displayBlock('tab_menu_navbar_header', $context, $blocks);
            // line 277
            echo "
                                            ";
            // line 278
            if ((( !twig_test_empty(($context["_tab_menu"] ?? null)) ||  !twig_test_empty(($context["_actions"] ?? null))) ||  !twig_test_empty(($context["_list_filters_actions"] ?? null)))) {
                // line 279
                echo "                                            <div class=\"navbar-collapse x_panel tab-menu-header\">
                                                ";
                // line 280
                if ( !twig_test_empty(($context["_tab_menu"] ?? null))) {
                    // line 281
                    echo "                                                    <div class=\"navbar-left\">
                                                        ";
                    // line 282
                    echo ($context["_tab_menu"] ?? null);
                    echo "
                                                    </div>
                                                ";
                }
                // line 285
                echo "
                                                ";
                // line 286
                if (((((isset($context["admin"]) || array_key_exists("admin", $context)) && (isset($context["action"]) || array_key_exists("action", $context))) && (($context["action"] ?? null) == "list")) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "listModes", [], "any", false, false, false, 286)) > 1))) {
                    // line 287
                    echo "                                                    <div class=\"nav navbar-right btn-group\">
                                                        ";
                    // line 288
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "listModes", [], "any", false, false, false, 288));
                    foreach ($context['_seq'] as $context["mode"] => $context["settings"]) {
                        // line 289
                        echo "                                                            <a href=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "list", 1 => twig_array_merge(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 289), "query", [], "any", false, false, false, 289), "all", [], "any", false, false, false, 289), ["_list_mode" => $context["mode"]])], "method", false, false, false, 289), "html", null, true);
                        echo "\" class=\"btn btn-default navbar-btn btn-sm";
                        if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "getListMode", [], "method", false, false, false, 289) == $context["mode"])) {
                            echo " active";
                        }
                        echo "\"><i class=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["settings"], "class", [], "any", false, false, false, 289), "html", null, true);
                        echo "\"></i></a>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['mode'], $context['settings'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 291
                    echo "                                                    </div>
                                                ";
                }
                // line 293
                echo "
                                                ";
                // line 294
                $this->displayBlock('sonata_admin_content_actions_wrappers', $context, $blocks);
                // line 310
                echo "
                                                ";
                // line 311
                if ( !twig_test_empty(($context["_list_filters_actions"] ?? null))) {
                    // line 312
                    echo "                                                    ";
                    echo ($context["_list_filters_actions"] ?? null);
                    echo "
                                                ";
                }
                // line 314
                echo "                                            </div>
                                            ";
            }
            // line 316
            echo "                                        </div>
                                    </nav>
                                ";
        }
        // line 319
        echo "                            ";
    }

    // line 268
    public function block_tab_menu_navbar_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 269
        echo "                                                ";
        if ( !twig_test_empty(($context["_navbar_title"] ?? null))) {
            // line 270
            echo "                                                    <div class=\"page-title\">
                                                        <div class=\"title_left\">
                                                            <h3>";
            // line 272
            echo ($context["_navbar_title"] ?? null);
            echo "</h3>
                                                        </div>
                                                    </div>
                                                ";
        }
        // line 276
        echo "                                            ";
    }

    // line 294
    public function block_sonata_admin_content_actions_wrappers($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 295
        echo "                                                    ";
        if ( !twig_test_empty(twig_trim_filter(twig_replace_filter(($context["_actions"] ?? null), ["<li>" => "", "</li>" => ""])))) {
            // line 296
            echo "                                                        <ul class=\"nav navbar-nav navbar-right\">
                                                        ";
            // line 297
            if ((twig_length_filter($this->env, twig_split_filter($this->env, ($context["_actions"] ?? null), "</a>")) > 2)) {
                // line 298
                echo "                                                            <li class=\"dropdown sonata-actions\">
                                                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 299
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_actions", [], "SonataAdminBundle"), "html", null, true);
                echo " <b class=\"caret\"></b></a>
                                                                <ul class=\"dropdown-menu\" role=\"menu\">
                                                                    ";
                // line 301
                echo ($context["_actions"] ?? null);
                echo "
                                                                </ul>
                                                            </li>
                                                        ";
            } else {
                // line 305
                echo "                                                            ";
                echo ($context["_actions"] ?? null);
                echo "
                                                        ";
            }
            // line 307
            echo "                                                        </ul>
                                                    ";
        }
        // line 309
        echo "                                                ";
    }

    // line 324
    public function block_sonata_admin_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 325
        echo "
                            ";
        // line 326
        $this->displayBlock('notice', $context, $blocks);
        // line 329
        echo "
                            ";
        // line 330
        if ( !twig_test_empty(($context["_preview"] ?? null))) {
            // line 331
            echo "                                <div class=\"sonata-ba-preview\">";
            echo ($context["_preview"] ?? null);
            echo "</div>
                            ";
        }
        // line 333
        echo "
                            ";
        // line 334
        if ( !twig_test_empty(($context["_content"] ?? null))) {
            // line 335
            echo "                                <div class=\"sonata-ba-content\">";
            echo ($context["_content"] ?? null);
            echo "</div>
                            ";
        }
        // line 337
        echo "
                            ";
        // line 338
        if ( !twig_test_empty(($context["_show"] ?? null))) {
            // line 339
            echo "                                <div class=\"sonata-ba-show\">";
            echo ($context["_show"] ?? null);
            echo "</div>
                            ";
        }
        // line 341
        echo "
                            ";
        // line 342
        if ( !twig_test_empty(($context["_form"] ?? null))) {
            // line 343
            echo "                                <div class=\"sonata-ba-form\">";
            echo ($context["_form"] ?? null);
            echo "</div>
                            ";
        }
        // line 345
        echo "
                            ";
        // line 346
        if ( !twig_test_empty(($context["_list_filters"] ?? null))) {
            // line 347
            echo "                                <div class=\"row\">
                                    ";
            // line 348
            echo ($context["_list_filters"] ?? null);
            echo "
                                </div>
                            ";
        }
        // line 351
        echo "
                            ";
        // line 352
        if ( !twig_test_empty(($context["_list_table"] ?? null))) {
            // line 353
            echo "                                <div class=\"row\">
                                    ";
            // line 354
            echo ($context["_list_table"] ?? null);
            echo "
                                </div>
                            ";
        }
        // line 357
        echo "                        ";
    }

    // line 326
    public function block_notice($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 327
        echo "                                ";
        $this->loadTemplate("@SonataCore/FlashMessage/render.html.twig", "@KinulabSonataGentellelaTheme/standard_layout.html.twig", 327)->display($context);
        // line 328
        echo "                            ";
    }

    // line 366
    public function block_bootlint($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 367
        echo "            ";
        // line 368
        echo "            <script type=\"text/javascript\">
                javascript:(function(){var s=document.createElement(\"script\");s.onload=function(){bootlint.showLintReportForCurrentDocument([], {hasProblems: false, problemFree: false});};s.src=\"https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js\";document.body.appendChild(s)})();
            </script>
        ";
    }

    // line 374
    public function block_footer_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 375
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 375), "getOption", [0 => "javascripts", 1 => []], "method", false, false, false, 375));
        foreach ($context['_seq'] as $context["_key"] => $context["javascript"]) {
            // line 376
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($context["javascript"]), "html", null, true);
            echo "\"></script>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['javascript'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 378
        echo "
        ";
        // line 380
        echo "            ";
        $context["localeForMoment"] = $this->extensions['Sonata\AdminBundle\Twig\Extension\SonataAdminExtension']->getCanonicalizedLocaleForMoment($context);
        // line 381
        echo "            ";
        if (($context["localeForMoment"] ?? null)) {
            // line 382
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("bundles/sonatacore/vendor/moment/locale/" .             // line 384
($context["localeForMoment"] ?? null)) . ".js")), "html", null, true);
            // line 386
            echo "\"></script>
        ";
        }
        // line 388
        echo "
        ";
        // line 390
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 390), "getOption", [0 => "use_select2"], "method", false, false, false, 390)) {
            // line 391
            echo "            ";
            $context["localeForSelect2"] = $this->extensions['Sonata\AdminBundle\Twig\Extension\SonataAdminExtension']->getCanonicalizedLocaleForSelect2($context);
            // line 392
            echo "            ";
            if (($context["localeForSelect2"] ?? null)) {
                // line 393
                echo "                <script src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("bundles/sonatacore/vendor/select2/select2_locale_" . ($context["localeForSelect2"] ?? null)) . ".js")), "html", null, true);
                echo "\"></script>
            ";
            }
            // line 395
            echo "        ";
        }
        // line 396
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@KinulabSonataGentellelaTheme/standard_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1151 => 396,  1148 => 395,  1142 => 393,  1139 => 392,  1136 => 391,  1133 => 390,  1130 => 388,  1126 => 386,  1124 => 384,  1122 => 382,  1119 => 381,  1116 => 380,  1113 => 378,  1104 => 376,  1099 => 375,  1095 => 374,  1088 => 368,  1086 => 367,  1082 => 366,  1078 => 328,  1075 => 327,  1071 => 326,  1067 => 357,  1061 => 354,  1058 => 353,  1056 => 352,  1053 => 351,  1047 => 348,  1044 => 347,  1042 => 346,  1039 => 345,  1033 => 343,  1031 => 342,  1028 => 341,  1022 => 339,  1020 => 338,  1017 => 337,  1011 => 335,  1009 => 334,  1006 => 333,  1000 => 331,  998 => 330,  995 => 329,  993 => 326,  990 => 325,  986 => 324,  982 => 309,  978 => 307,  972 => 305,  965 => 301,  960 => 299,  957 => 298,  955 => 297,  952 => 296,  949 => 295,  945 => 294,  941 => 276,  934 => 272,  930 => 270,  927 => 269,  923 => 268,  919 => 319,  914 => 316,  910 => 314,  904 => 312,  902 => 311,  899 => 310,  897 => 294,  894 => 293,  890 => 291,  875 => 289,  871 => 288,  868 => 287,  866 => 286,  863 => 285,  857 => 282,  854 => 281,  852 => 280,  849 => 279,  847 => 278,  844 => 277,  842 => 268,  838 => 266,  836 => 264,  835 => 263,  834 => 262,  832 => 261,  828 => 260,  824 => 320,  821 => 260,  817 => 259,  812 => 358,  810 => 324,  805 => 321,  803 => 259,  799 => 257,  795 => 256,  791 => 244,  776 => 234,  771 => 232,  763 => 226,  760 => 225,  756 => 224,  751 => 222,  749 => 221,  743 => 217,  739 => 216,  733 => 213,  731 => 212,  722 => 208,  718 => 206,  714 => 205,  710 => 247,  706 => 245,  703 => 224,  700 => 216,  698 => 205,  695 => 204,  692 => 203,  688 => 202,  680 => 195,  676 => 193,  672 => 192,  667 => 189,  663 => 187,  657 => 185,  654 => 184,  651 => 183,  637 => 182,  631 => 179,  628 => 178,  624 => 176,  618 => 174,  614 => 172,  611 => 170,  608 => 168,  606 => 167,  601 => 166,  599 => 165,  596 => 164,  594 => 163,  591 => 160,  589 => 159,  587 => 158,  585 => 157,  567 => 156,  564 => 155,  562 => 154,  559 => 153,  557 => 152,  554 => 151,  550 => 150,  546 => 133,  542 => 132,  537 => 134,  535 => 132,  532 => 131,  528 => 130,  521 => 128,  517 => 127,  510 => 126,  506 => 136,  503 => 130,  500 => 127,  498 => 126,  495 => 125,  491 => 124,  485 => 137,  483 => 124,  479 => 122,  475 => 121,  471 => 116,  467 => 114,  461 => 112,  458 => 111,  450 => 109,  448 => 108,  443 => 107,  440 => 106,  436 => 105,  428 => 248,  426 => 202,  421 => 199,  419 => 192,  416 => 191,  414 => 150,  402 => 140,  400 => 121,  394 => 117,  392 => 105,  387 => 102,  383 => 101,  379 => 253,  376 => 101,  372 => 100,  368 => 95,  365 => 94,  363 => 93,  360 => 92,  356 => 91,  352 => 87,  349 => 86,  346 => 85,  332 => 84,  327 => 82,  324 => 79,  322 => 78,  320 => 77,  318 => 76,  314 => 73,  311 => 72,  308 => 71,  291 => 70,  288 => 69,  285 => 68,  279 => 66,  277 => 65,  271 => 63,  267 => 62,  261 => 58,  248 => 48,  244 => 46,  238 => 45,  230 => 44,  222 => 43,  214 => 42,  210 => 40,  206 => 39,  202 => 37,  193 => 35,  188 => 34,  184 => 33,  177 => 28,  173 => 27,  166 => 25,  159 => 397,  157 => 374,  154 => 373,  151 => 372,  148 => 366,  146 => 365,  139 => 360,  137 => 256,  133 => 254,  131 => 100,  126 => 97,  124 => 91,  119 => 88,  117 => 62,  113 => 60,  111 => 39,  108 => 38,  106 => 33,  103 => 32,  101 => 27,  94 => 25,  91 => 24,  89 => 22,  87 => 21,  85 => 20,  83 => 19,  81 => 18,  79 => 17,  77 => 16,  75 => 15,  73 => 14,  71 => 13,  69 => 12,  67 => 11,);
    }

    public function getSourceContext()
    {
        return new Source("", "@KinulabSonataGentellelaTheme/standard_layout.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/kinulab/sonata-gentellela-theme-bundle/Resources/views/standard_layout.html.twig");
    }
}
