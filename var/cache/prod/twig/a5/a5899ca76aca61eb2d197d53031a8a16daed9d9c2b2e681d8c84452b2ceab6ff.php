<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @KinulabSonataGentellelaTheme/CRUD/base_edit.html.twig */
class __TwigTemplate_fac9d408bb54f9970ba70e0a91d4e79e733ead79e56b18c54dab1d4b91cb656e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 38
        $_trait_0 = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig", "@KinulabSonataGentellelaTheme/CRUD/base_edit.html.twig", 38);
        if (!$_trait_0->isTraitable()) {
            throw new RuntimeError('Template "'."@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig".'" cannot be used as a trait.', 38, $this->source);
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        if (!isset($_trait_0_blocks["form"])) {
            throw new RuntimeError('Block "form" is not defined in trait "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig".', 38, $this->source);
        }

        $_trait_0_blocks["parentForm"] = $_trait_0_blocks["form"]; unset($_trait_0_blocks["form"]);

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            [
                'title' => [$this, 'block_title'],
                'navbar_title' => [$this, 'block_navbar_title'],
                'actions' => [$this, 'block_actions'],
                'tab_menu' => [$this, 'block_tab_menu'],
                'form' => [$this, 'block_form'],
            ]
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(($context["base_template"] ?? null), "@KinulabSonataGentellelaTheme/CRUD/base_edit.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    ";
        // line 16
        echo "    ";
        if ( !(null === (((isset($context["objectId"]) || array_key_exists("objectId", $context))) ? (_twig_default_filter(($context["objectId"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 16))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 16))))) {
            // line 17
            echo "        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("title_edit", ["%name%" => twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "toString", [0 => ($context["object"] ?? null)], "method", false, false, false, 17), 15)], "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 19
            echo "        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("title_create", [], "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
    }

    // line 23
    public function block_navbar_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
    }

    // line 27
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/action_buttons.html.twig", "@KinulabSonataGentellelaTheme/CRUD/base_edit.html.twig", 28)->display($context);
    }

    // line 31
    public function block_tab_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "sidemenu", [0 => ($context["action"] ?? null)], "method", false, false, false, 32), ["currentClass" => "active", "template" => $this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("tab_menu_template")], "twig");
    }

    // line 40
    public function block_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    ";
        $this->displayBlock("parentForm", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@KinulabSonataGentellelaTheme/CRUD/base_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 41,  117 => 40,  113 => 32,  109 => 31,  105 => 28,  101 => 27,  94 => 24,  90 => 23,  82 => 19,  76 => 17,  73 => 16,  71 => 15,  67 => 14,  57 => 12,  28 => 38,);
    }

    public function getSourceContext()
    {
        return new Source("", "@KinulabSonataGentellelaTheme/CRUD/base_edit.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/kinulab/sonata-gentellela-theme-bundle/Resources/views/CRUD/base_edit.html.twig");
    }
}
