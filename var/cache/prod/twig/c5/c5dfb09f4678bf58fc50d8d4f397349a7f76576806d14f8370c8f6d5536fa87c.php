<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig */
class __TwigTemplate_caa84bfa143a69d254fd76a09356836a66da2623a579d8667426bada821a90d8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'form' => [$this, 'block_form'],
            'sonata_form_action_url' => [$this, 'block_sonata_form_action_url'],
            'sonata_form_attributes' => [$this, 'block_sonata_form_attributes'],
            'sonata_pre_fieldsets' => [$this, 'block_sonata_pre_fieldsets'],
            'sonata_tab_content' => [$this, 'block_sonata_tab_content'],
            'formactions' => [$this, 'block_formactions'],
            'sonata_form_actions' => [$this, 'block_sonata_form_actions'],
            'sonata_post_fieldsets' => [$this, 'block_sonata_post_fieldsets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('form', $context, $blocks);
    }

    public function block_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $macros["form_helper"] = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_edit_form_macro.html.twig", "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig", 2)->unwrap();
        // line 3
        echo "    ";
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sonata.admin.edit.form.top", ["admin" => ($context["admin"] ?? null), "object" => ($context["object"] ?? null)]]);
        echo "
    ";
        // line 5
        echo "    ";
        $context["url"] = (( !(null === (((isset($context["objectId"]) || array_key_exists("objectId", $context))) ? (_twig_default_filter(($context["objectId"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 5))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 5))))) ? ("edit") : ("create"));
        // line 6
        echo "    ";
        if ( !twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => ($context["url"] ?? null)], "method", false, false, false, 6)) {
            // line 7
            echo "        <div>
            ";
            // line 8
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("form_not_available", [], "SonataAdminBundle"), "html", null, true);
            echo "
        </div>
    ";
        } else {
            // line 11
            echo "        <form
                ";
            // line 12
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 12), "getOption", [0 => "form_type"], "method", false, false, false, 12) == "horizontal")) {
                echo "class=\"form-horizontal\"";
            }
            // line 13
            echo "                role=\"form\"
                ";
            // line 15
            echo "                action=\"";
            $this->displayBlock('sonata_form_action_url', $context, $blocks);
            echo "\"
                ";
            // line 16
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "multipart", [], "any", false, false, false, 16)) {
                echo " enctype=\"multipart/form-data\"";
            }
            // line 17
            echo "                method=\"POST\"
                ";
            // line 18
            if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 18), "getOption", [0 => "html5_validate"], "method", false, false, false, 18)) {
                echo "novalidate=\"novalidate\"";
            }
            // line 19
            echo "                ";
            $this->displayBlock('sonata_form_attributes', $context, $blocks);
            // line 20
            echo "        >
            ";
            // line 21
            echo twig_include($this->env, $context, "@KinulabSonataGentellelaTheme/Helper/render_form_dismissable_errors.html.twig");
            echo "
            ";
            // line 22
            $this->displayBlock('sonata_pre_fieldsets', $context, $blocks);
            // line 25
            echo "                ";
            $this->displayBlock('sonata_tab_content', $context, $blocks);
            // line 105
            echo "                ";
            $this->displayBlock('sonata_post_fieldsets', $context, $blocks);
            // line 108
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
        </form>
    ";
        }
        // line 111
        echo "    ";
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sonata.admin.edit.form.bottom", ["admin" => ($context["admin"] ?? null), "object" => ($context["object"] ?? null)]]);
        echo "
";
    }

    // line 15
    public function block_sonata_form_action_url($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => ($context["url"] ?? null), 1 => ["id" => (((isset($context["objectId"]) || array_key_exists("objectId", $context))) ? (_twig_default_filter(($context["objectId"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 15))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 15))), "uniqid" => twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 15), "subclass" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 15), "get", [0 => "subclass"], "method", false, false, false, 15)]], "method", false, false, false, 15), "html", null, true);
    }

    // line 19
    public function block_sonata_form_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 22
    public function block_sonata_pre_fieldsets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "            <div class=\"row\">
                ";
    }

    // line 25
    public function block_sonata_tab_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "                    ";
        $context["has_tab"] = (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "formtabs", [], "any", false, false, false, 26)) == 1) && ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_array_keys_filter(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "formtabs", [], "any", false, false, false, 26))) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null) != "default")) || (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "formtabs", [], "any", false, false, false, 26)) > 1));
        // line 27
        echo "                    <div class=\"col-md-12\">
                        <div class=\"x_panel\">
                            ";
        // line 29
        if (($context["has_tab"] ?? null)) {
            // line 30
            echo "                                <div class=\"nav-tabs-custom\">
                                    <ul class=\"nav nav-tabs\" role=\"tablist\">
                                        ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "formtabs", [], "any", false, false, false, 32));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["name"] => $context["form_tab"]) {
                // line 33
                echo "                                            <li";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 33) == 1)) {
                    echo " class=\"active\"";
                }
                echo "><a href=\"#tab_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 33), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 33), "html", null, true);
                echo "\" data-toggle=\"tab\"><i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i> ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["form_tab"], "label", [], "any", false, false, false, 33), [], ((twig_get_attribute($this->env, $this->source, $context["form_tab"], "translation_domain", [], "any", false, false, false, 33)) ? (twig_get_attribute($this->env, $this->source, $context["form_tab"], "translation_domain", [], "any", false, false, false, 33)) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 33)))), "html", null, true);
                echo "</a></li>
                                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['form_tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "                                    </ul>
                                    <div class=\"tab-content\">
                                        ";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "formtabs", [], "any", false, false, false, 37));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["code"] => $context["form_tab"]) {
                // line 38
                echo "                                            <div class=\"tab-pane fade";
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 38)) {
                    echo " in active";
                }
                echo "\" id=\"tab_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "uniqid", [], "any", false, false, false, 38), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 38), "html", null, true);
                echo "\">
                                                <div class=\"box-body  container-fluid\">
                                                    <div class=\"sonata-ba-collapsed-fields\">
                                                        ";
                // line 41
                if ((twig_get_attribute($this->env, $this->source, $context["form_tab"], "description", [], "any", false, false, false, 41) != false)) {
                    // line 42
                    echo "                                                            <p>
                                                                ";
                    // line 43
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["form_tab"], "description", [], "any", false, false, false, 43), [], ((twig_get_attribute($this->env, $this->source, $context["form_tab"], "translation_domain", [], "any", false, false, false, 43)) ? (twig_get_attribute($this->env, $this->source, $context["form_tab"], "translation_domain", [], "any", false, false, false, 43)) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "translationDomain", [], "any", false, false, false, 43)))), "html", null, true);
                    echo "
                                                            </p>
                                                        ";
                }
                // line 46
                echo "                                                        ";
                $macros["form_helper"] = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_edit_form_macro.html.twig", "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig", 46)->unwrap();
                // line 47
                echo "                                                        ";
                echo twig_call_macro($macros["form_helper"], "macro_render_groups", [($context["admin"] ?? null), ($context["form"] ?? null), (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["form_tab"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["groups"] ?? null) : null), ($context["has_tab"] ?? null)], 47, $context, $this->getSourceContext());
                echo "
                                                    </div>
                                                </div>
                                            </div>
                                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['code'], $context['form_tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "                                    </div>
                                </div>
                            ";
        } else {
            // line 55
            echo "                                ";
            $macros["form_helper"] = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_edit_form_macro.html.twig", "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig", 55)->unwrap();
            // line 56
            echo "                                ";
            echo twig_call_macro($macros["form_helper"], "macro_render_groups", [($context["admin"] ?? null), ($context["form"] ?? null), twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "formtabs", [], "any", false, false, false, 56)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["default"] ?? null) : null), "groups", [], "any", false, false, false, 56), ($context["has_tab"] ?? null)], 56, $context, $this->getSourceContext());
            echo "
                            ";
        }
        // line 58
        echo "                            ";
        $this->displayBlock('formactions', $context, $blocks);
        // line 102
        echo "                        </div>
                    </div>
                ";
    }

    // line 58
    public function block_formactions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "                                <div class=\"ln_solid\"></div>
                                <div class=\"sonata-ba-form-actions form-actions\">
                                    ";
        // line 61
        $this->displayBlock('sonata_form_actions', $context, $blocks);
        // line 100
        echo "                                </div>
                            ";
    }

    // line 61
    public function block_sonata_form_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        echo "                                        ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 62), "isxmlhttprequest", [], "any", false, false, false, 62)) {
            // line 63
            echo "                                            ";
            // line 64
            echo "                                            ";
            if ( !(null === (((isset($context["objectId"]) || array_key_exists("objectId", $context))) ? (_twig_default_filter(($context["objectId"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 64))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 64))))) {
                // line 65
                echo "                                                <button type=\"submit\" class=\"btn btn-success\" name=\"btn_update\"><i class=\"fa fa-save\"></i> ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_update", [], "SonataAdminBundle"), "html", null, true);
                echo "</button>
                                            ";
            } else {
                // line 67
                echo "                                                <button type=\"submit\" class=\"btn btn-success\" name=\"btn_create\"><i class=\"fa fa-plus-circle\"></i> ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_create", [], "SonataAdminBundle"), "html", null, true);
                echo "</button>
                                            ";
            }
            // line 69
            echo "                                        ";
        } else {
            // line 70
            echo "                                            ";
            if (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "supportsPreviewMode", [], "any", false, false, false, 70)) {
                // line 71
                echo "                                                <button class=\"btn btn-info persist-preview\" name=\"btn_preview\" type=\"submit\">
                                                    <i class=\"fa fa-eye\"></i>
                                                    ";
                // line 73
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_preview", [], "SonataAdminBundle"), "html", null, true);
                echo "
                                                </button>
                                            ";
            }
            // line 76
            echo "                                            ";
            // line 77
            echo "                                            ";
            if ( !(null === (((isset($context["objectId"]) || array_key_exists("objectId", $context))) ? (_twig_default_filter(($context["objectId"] ?? null), twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 77))) : (twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "id", [0 => ($context["object"] ?? null)], "method", false, false, false, 77))))) {
                // line 78
                echo "                                                <button type=\"submit\" class=\"btn btn-success\" name=\"btn_update_and_edit\"><i class=\"fa fa-save\"></i> ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_update_and_edit_again", [], "SonataAdminBundle"), "html", null, true);
                echo "</button>
                                                ";
                // line 79
                if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "list"], "method", false, false, false, 79) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "list"], "method", false, false, false, 79))) {
                    // line 80
                    echo "                                                    <button type=\"submit\" class=\"btn btn-success\" name=\"btn_update_and_list\"><i class=\"fa fa-save\"></i> <i class=\"fa fa-list\"></i> ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_update_and_return_to_list", [], "SonataAdminBundle"), "html", null, true);
                    echo "</button>
                                                ";
                }
                // line 82
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "delete"], "method", false, false, false, 82) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "delete", 1 => ($context["object"] ?? null)], "method", false, false, false, 82))) {
                    // line 83
                    echo "                                                    ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("delete_or", [], "SonataAdminBundle"), "html", null, true);
                    echo "
                                                    <a class=\"btn btn-danger\" href=\"";
                    // line 84
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateObjectUrl", [0 => "delete", 1 => ($context["object"] ?? null)], "method", false, false, false, 84), "html", null, true);
                    echo "\"><i class=\"fa fa-minus-circle\"></i> ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_delete", [], "SonataAdminBundle"), "html", null, true);
                    echo "</a>
                                                ";
                }
                // line 86
                echo "                                                ";
                if (((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "isAclEnabled", [], "method", false, false, false, 86) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "acl"], "method", false, false, false, 86)) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "acl", 1 => ($context["object"] ?? null)], "method", false, false, false, 86))) {
                    // line 87
                    echo "                                                    <a class=\"btn btn-info\" href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateObjectUrl", [0 => "acl", 1 => ($context["object"] ?? null)], "method", false, false, false, 87), "html", null, true);
                    echo "\"><i class=\"fa fa-users\"></i> ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_edit_acl", [], "SonataAdminBundle"), "html", null, true);
                    echo "</a>
                                                ";
                }
                // line 89
                echo "                                            ";
            } else {
                // line 90
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "edit"], "method", false, false, false, 90) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "edit"], "method", false, false, false, 90))) {
                    // line 91
                    echo "                                                    <button class=\"btn btn-success\" type=\"submit\" name=\"btn_create_and_edit\"><i class=\"fa fa-save\"></i> ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_create_and_edit_again", [], "SonataAdminBundle"), "html", null, true);
                    echo "</button>
                                                ";
                }
                // line 93
                echo "                                                ";
                if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasRoute", [0 => "list"], "method", false, false, false, 93) && twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "hasAccess", [0 => "list"], "method", false, false, false, 93))) {
                    // line 94
                    echo "                                                    <button type=\"submit\" class=\"btn btn-success\" name=\"btn_create_and_list\"><i class=\"fa fa-save\"></i> <i class=\"fa fa-list\"></i> ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_create_and_return_to_list", [], "SonataAdminBundle"), "html", null, true);
                    echo "</button>
                                                ";
                }
                // line 96
                echo "                                                <button class=\"btn btn-success\" type=\"submit\" name=\"btn_create_and_create\"><i class=\"fa fa-plus-circle\"></i> ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("btn_create_and_create_a_new_one", [], "SonataAdminBundle"), "html", null, true);
                echo "</button>
                                            ";
            }
            // line 98
            echo "                                        ";
        }
        // line 99
        echo "                                    ";
    }

    // line 105
    public function block_sonata_post_fieldsets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 106
        echo "            </div>
            ";
    }

    public function getTemplateName()
    {
        return "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  442 => 106,  438 => 105,  434 => 99,  431 => 98,  425 => 96,  419 => 94,  416 => 93,  410 => 91,  407 => 90,  404 => 89,  396 => 87,  393 => 86,  386 => 84,  381 => 83,  378 => 82,  372 => 80,  370 => 79,  365 => 78,  362 => 77,  360 => 76,  354 => 73,  350 => 71,  347 => 70,  344 => 69,  338 => 67,  332 => 65,  329 => 64,  327 => 63,  324 => 62,  320 => 61,  315 => 100,  313 => 61,  309 => 59,  305 => 58,  299 => 102,  296 => 58,  290 => 56,  287 => 55,  282 => 52,  262 => 47,  259 => 46,  253 => 43,  250 => 42,  248 => 41,  235 => 38,  218 => 37,  214 => 35,  189 => 33,  172 => 32,  168 => 30,  166 => 29,  162 => 27,  159 => 26,  155 => 25,  150 => 23,  146 => 22,  140 => 19,  133 => 15,  126 => 111,  119 => 108,  116 => 105,  113 => 25,  111 => 22,  107 => 21,  104 => 20,  101 => 19,  97 => 18,  94 => 17,  90 => 16,  85 => 15,  82 => 13,  78 => 12,  75 => 11,  69 => 8,  66 => 7,  63 => 6,  60 => 5,  55 => 3,  52 => 2,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@KinulabSonataGentellelaTheme/CRUD/base_edit_form.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/templates/bundles/KinulabSonataGentellelaThemeBundle/CRUD/base_edit_form.html.twig");
    }
}
