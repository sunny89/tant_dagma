<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SonataAdmin/standard_layout.html.twig */
class __TwigTemplate_951f857bb70571402d42d38ee3bcdfb94f162d0a997eb055166f7c0e388de312 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'html_attributes' => [$this, 'block_html_attributes'],
            'meta_tags' => [$this, 'block_meta_tags'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'sonata_javascript_config' => [$this, 'block_sonata_javascript_config'],
            'sonata_javascript_pool' => [$this, 'block_sonata_javascript_pool'],
            'sonata_head_title' => [$this, 'block_sonata_head_title'],
            'body_attributes' => [$this, 'block_body_attributes'],
            'sonata_header' => [$this, 'block_sonata_header'],
            'sonata_header_noscript_warning' => [$this, 'block_sonata_header_noscript_warning'],
            'logo' => [$this, 'block_logo'],
            'sonata_nav' => [$this, 'block_sonata_nav'],
            'sonata_breadcrumb' => [$this, 'block_sonata_breadcrumb'],
            'sonata_top_nav_menu' => [$this, 'block_sonata_top_nav_menu'],
            'sonata_top_nav_menu_add_block' => [$this, 'block_sonata_top_nav_menu_add_block'],
            'sonata_top_nav_menu_user_block' => [$this, 'block_sonata_top_nav_menu_user_block'],
            'sonata_wrapper' => [$this, 'block_sonata_wrapper'],
            'sonata_left_side' => [$this, 'block_sonata_left_side'],
            'sonata_side_nav' => [$this, 'block_sonata_side_nav'],
            'sonata_sidebar_search' => [$this, 'block_sonata_sidebar_search'],
            'side_bar_before_nav' => [$this, 'block_side_bar_before_nav'],
            'side_bar_nav' => [$this, 'block_side_bar_nav'],
            'side_bar_after_nav' => [$this, 'block_side_bar_after_nav'],
            'side_bar_after_nav_content' => [$this, 'block_side_bar_after_nav_content'],
            'sonata_page_content' => [$this, 'block_sonata_page_content'],
            'sonata_page_content_header' => [$this, 'block_sonata_page_content_header'],
            'sonata_page_content_nav' => [$this, 'block_sonata_page_content_nav'],
            'tab_menu_navbar_header' => [$this, 'block_tab_menu_navbar_header'],
            'sonata_admin_content_actions_wrappers' => [$this, 'block_sonata_admin_content_actions_wrappers'],
            'sonata_admin_content' => [$this, 'block_sonata_admin_content'],
            'notice' => [$this, 'block_notice'],
            'bootlint' => [$this, 'block_bootlint'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        $context["_preview"] = ((        $this->hasBlock("preview", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("preview", $context, $blocks))) : (null));
        // line 13
        $context["_form"] = ((        $this->hasBlock("form", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("form", $context, $blocks))) : (null));
        // line 14
        $context["_show"] = ((        $this->hasBlock("show", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("show", $context, $blocks))) : (null));
        // line 15
        $context["_list_table"] = ((        $this->hasBlock("list_table", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_table", $context, $blocks))) : (null));
        // line 16
        $context["_list_filters"] = ((        $this->hasBlock("list_filters", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters", $context, $blocks))) : (null));
        // line 17
        $context["_tab_menu"] = ((        $this->hasBlock("tab_menu", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("tab_menu", $context, $blocks))) : (null));
        // line 18
        $context["_content"] = ((        $this->hasBlock("content", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("content", $context, $blocks))) : (null));
        // line 19
        $context["_title"] = ((        $this->hasBlock("title", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("title", $context, $blocks))) : (null));
        // line 20
        $context["_breadcrumb"] = ((        $this->hasBlock("breadcrumb", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("breadcrumb", $context, $blocks))) : (null));
        // line 21
        $context["_actions"] = ((        $this->hasBlock("actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("actions", $context, $blocks))) : (null));
        // line 22
        $context["_navbar_title"] = ((        $this->hasBlock("navbar_title", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("navbar_title", $context, $blocks))) : (null));
        // line 23
        $context["_list_filters_actions"] = ((        $this->hasBlock("list_filters_actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters_actions", $context, $blocks))) : (null));
        // line 25
        echo "<!DOCTYPE html>
<html ";
        // line 26
        $this->displayBlock('html_attributes', $context, $blocks);
        echo ">
    <head>
        ";
        // line 28
        $this->displayBlock('meta_tags', $context, $blocks);
        // line 33
        echo "
        ";
        // line 34
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 39
        echo "
        ";
        // line 40
        $this->displayBlock('javascripts', $context, $blocks);
        // line 86
        echo "
        <title>
        ";
        // line 88
        $this->displayBlock('sonata_head_title', $context, $blocks);
        // line 114
        echo "        </title>
    </head>
    <body
            ";
        // line 117
        $this->displayBlock('body_attributes', $context, $blocks);
        // line 123
        echo ">

    <div class=\"wrapper\">

        ";
        // line 127
        $this->displayBlock('sonata_header', $context, $blocks);
        // line 226
        echo "
        ";
        // line 227
        $this->displayBlock('sonata_wrapper', $context, $blocks);
        // line 365
        echo "    </div>

    ";
        // line 367
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 367), "getOption", [0 => "use_bootlint"], "method", false, false, false, 367)) {
            // line 368
            echo "        ";
            $this->displayBlock('bootlint', $context, $blocks);
            // line 374
            echo "    ";
        }
        // line 375
        echo "
    </body>
</html>
";
    }

    // line 26
    public function block_html_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "class=\"no-js\"";
    }

    // line 28
    public function block_meta_tags($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta charset=\"UTF-8\">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        ";
    }

    // line 34
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 35), "getOption", [0 => "stylesheets", 1 => []], "method", false, false, false, 35));
        foreach ($context['_seq'] as $context["_key"] => $context["stylesheet"]) {
            // line 36
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($context["stylesheet"]), "html", null, true);
            echo "\">
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stylesheet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        ";
    }

    // line 40
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "            ";
        $this->displayBlock('sonata_javascript_config', $context, $blocks);
        // line 61
        echo "
            ";
        // line 62
        $this->displayBlock('sonata_javascript_pool', $context, $blocks);
        // line 67
        echo "
            ";
        // line 69
        echo "            ";
        $context["localeForMoment"] = $this->extensions['Sonata\AdminBundle\Twig\Extension\SonataAdminExtension']->getCanonicalizedLocaleForMoment($context);
        // line 70
        echo "            ";
        if (($context["localeForMoment"] ?? null)) {
            // line 71
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("bundles/sonatacore/vendor/moment/locale/" .             // line 73
($context["localeForMoment"] ?? null)) . ".js")), "html", null, true);
            // line 75
            echo "\"></script>
            ";
        }
        // line 77
        echo "
            ";
        // line 79
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 79), "getOption", [0 => "use_select2"], "method", false, false, false, 79)) {
            // line 80
            echo "                ";
            $context["localeForSelect2"] = $this->extensions['Sonata\AdminBundle\Twig\Extension\SonataAdminExtension']->getCanonicalizedLocaleForSelect2($context);
            // line 81
            echo "                ";
            if (($context["localeForSelect2"] ?? null)) {
                // line 82
                echo "                    <script src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("bundles/sonatacore/vendor/select2/select2_locale_" . ($context["localeForSelect2"] ?? null)) . ".js")), "html", null, true);
                echo "\"></script>
                ";
            }
            // line 84
            echo "            ";
        }
        // line 85
        echo "        ";
    }

    // line 41
    public function block_sonata_javascript_config($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "                <script>
                    window.SONATA_CONFIG = {
                        CONFIRM_EXIT: ";
        // line 44
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 44), "getOption", [0 => "confirm_exit"], "method", false, false, false, 44)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USE_SELECT2: ";
        // line 45
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 45), "getOption", [0 => "use_select2"], "method", false, false, false, 45)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USE_ICHECK: ";
        // line 46
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 46), "getOption", [0 => "use_icheck"], "method", false, false, false, 46)) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USE_STICKYFORMS: ";
        // line 47
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 47), "getOption", [0 => "use_stickyforms"], "method", false, false, false, 47)) {
            echo "true";
        } else {
            echo "false";
        }
        // line 48
        echo "                    };
                    window.SONATA_TRANSLATIONS = {
                        CONFIRM_EXIT: '";
        // line 50
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("confirm_exit", [], "SonataAdminBundle"), "js"), "html", null, true);
        echo "'
                    };

                    // http://getbootstrap.com/getting-started/#support-ie10-width
                    if (navigator.userAgent.match(/IEMobile\\/10\\.0/)) {
                        var msViewportStyle = document.createElement('style');
                        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
                        document.querySelector('head').appendChild(msViewportStyle);
                    }
                </script>
            ";
    }

    // line 62
    public function block_sonata_javascript_pool($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 63), "getOption", [0 => "javascripts", 1 => []], "method", false, false, false, 63));
        foreach ($context['_seq'] as $context["_key"] => $context["javascript"]) {
            // line 64
            echo "                    <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($context["javascript"]), "html", null, true);
            echo "\"></script>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['javascript'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "            ";
    }

    // line 88
    public function block_sonata_head_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "            ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Admin", [], "SonataAdminBundle"), "html", null, true);
        echo "

            ";
        // line 91
        if ( !twig_test_empty(($context["_title"] ?? null))) {
            // line 92
            echo "                ";
            echo strip_tags(($context["_title"] ?? null));
            echo "
            ";
        } else {
            // line 94
            echo "                ";
            if ((isset($context["action"]) || array_key_exists("action", $context))) {
                // line 95
                echo "                    -
                    ";
                // line 96
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["breadcrumbs_builder"] ?? null), "breadcrumbs", [0 => ($context["admin"] ?? null), 1 => ($context["action"] ?? null)], "method", false, false, false, 96));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                    // line 97
                    echo "                        ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 97)) {
                        // line 98
                        echo "                            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 98) != 2)) {
                            // line 99
                            echo "                                &gt;
                            ";
                        }
                        // line 102
                        $context["translation_domain"] = twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_domain", 1 => "messages"], "method", false, false, false, 102);
                        // line 103
                        $context["label"] = twig_get_attribute($this->env, $this->source, $context["menu"], "label", [], "any", false, false, false, 103);
                        // line 104
                        if ( !(($context["translation_domain"] ?? null) === false)) {
                            // line 105
                            $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_params", 1 => []], "method", false, false, false, 105), ($context["translation_domain"] ?? null));
                        }
                        // line 108
                        echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                        echo "
                        ";
                    }
                    // line 110
                    echo "                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 111
                echo "                ";
            }
            // line 112
            echo "            ";
        }
        // line 113
        echo "        ";
    }

    // line 117
    public function block_body_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 118
        echo "class=\"sonata-bc skin-black fixed
                ";
        // line 119
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 119), "cookies", [], "any", false, false, false, 119), "get", [0 => "sonata_sidebar_hide"], "method", false, false, false, 119)) {
            // line 120
            echo "sidebar-collapse";
        }
        // line 121
        echo "\"";
    }

    // line 127
    public function block_sonata_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 128
        echo "            <header class=\"main-header\">
                ";
        // line 129
        $this->displayBlock('sonata_header_noscript_warning', $context, $blocks);
        // line 136
        echo "                ";
        $this->displayBlock('logo', $context, $blocks);
        // line 148
        echo "                ";
        $this->displayBlock('sonata_nav', $context, $blocks);
        // line 224
        echo "            </header>
        ";
    }

    // line 129
    public function block_sonata_header_noscript_warning($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 130
        echo "                    <noscript>
                        <div class=\"noscript-warning\">
                            ";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("noscript_warning", [], "SonataAdminBundle"), "html", null, true);
        echo "
                        </div>
                    </noscript>
                ";
    }

    // line 136
    public function block_logo($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 137
        echo "                    ";
        ob_start(function () { return ''; });
        // line 138
        echo "                        <a class=\"logo\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sonata_admin_dashboard");
        echo "\">
                            ";
        // line 139
        if ((("single_image" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 139), "getOption", [0 => "title_mode"], "method", false, false, false, 139)) || ("both" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 139), "getOption", [0 => "title_mode"], "method", false, false, false, 139)))) {
            // line 140
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 140), "titlelogo", [], "any", false, false, false, 140)), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 140), "title", [], "any", false, false, false, 140), "html", null, true);
            echo "\">
                            ";
        }
        // line 142
        echo "                            ";
        if ((("single_text" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 142), "getOption", [0 => "title_mode"], "method", false, false, false, 142)) || ("both" == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 142), "getOption", [0 => "title_mode"], "method", false, false, false, 142)))) {
            // line 143
            echo "                                <span>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 143), "title", [], "any", false, false, false, 143), "html", null, true);
            echo "</span>
                            ";
        }
        // line 145
        echo "                        </a>
                    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 147
        echo "                ";
    }

    // line 148
    public function block_sonata_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 149
        echo "                    <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                            <span class=\"sr-only\">";
        // line 151
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("toggle_navigation", [], "SonataAdminBundle"), "html", null, true);
        echo "</span>
                        </a>

                        <div class=\"navbar-left\">
                            ";
        // line 155
        $this->displayBlock('sonata_breadcrumb', $context, $blocks);
        // line 194
        echo "                        </div>

                        ";
        // line 196
        $this->displayBlock('sonata_top_nav_menu', $context, $blocks);
        // line 222
        echo "                    </nav>
                ";
    }

    // line 155
    public function block_sonata_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 156
        echo "                                <div class=\"hidden-xs\">
                                    ";
        // line 157
        if (( !twig_test_empty(($context["_breadcrumb"] ?? null)) || (isset($context["action"]) || array_key_exists("action", $context)))) {
            // line 158
            echo "                                        <ol class=\"nav navbar-top-links breadcrumb\">
                                            ";
            // line 159
            if (twig_test_empty(($context["_breadcrumb"] ?? null))) {
                // line 160
                echo "                                                ";
                if ((isset($context["action"]) || array_key_exists("action", $context))) {
                    // line 161
                    echo "                                                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["breadcrumbs_builder"] ?? null), "breadcrumbs", [0 => ($context["admin"] ?? null), 1 => ($context["action"] ?? null)], "method", false, false, false, 161));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                        // line 162
                        $context["translation_domain"] = twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_domain", 1 => "messages"], "method", false, false, false, 162);
                        // line 163
                        $context["label"] = twig_get_attribute($this->env, $this->source, $context["menu"], "label", [], "any", false, false, false, 163);
                        // line 164
                        if ( !(($context["translation_domain"] ?? null) === false)) {
                            // line 165
                            $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "translation_params", 1 => []], "method", false, false, false, 165), ($context["translation_domain"] ?? null));
                        }
                        // line 168
                        if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 168)) {
                            // line 169
                            echo "                                                            <li>
                                                                ";
                            // line 170
                            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["menu"], "uri", [], "any", false, false, false, 170))) {
                                // line 171
                                echo "                                                                    <a href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["menu"], "uri", [], "any", false, false, false, 171), "html", null, true);
                                echo "\">
                                                                        ";
                                // line 172
                                if (twig_get_attribute($this->env, $this->source, $context["menu"], "extra", [0 => "safe_label", 1 => true], "method", false, false, false, 172)) {
                                    // line 173
                                    echo ($context["label"] ?? null);
                                } else {
                                    // line 175
                                    echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                                }
                                // line 177
                                echo "                                                                    </a>
                                                                ";
                            } else {
                                // line 179
                                echo "                                                                    <span>";
                                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                                echo "</span>
                                                                ";
                            }
                            // line 181
                            echo "                                                            </li>
                                                        ";
                        } else {
                            // line 183
                            echo "                                                            <li class=\"active\"><span>";
                            echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                            echo "</span></li>
                                                        ";
                        }
                        // line 185
                        echo "                                                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 186
                    echo "                                                ";
                }
                // line 187
                echo "                                            ";
            } else {
                // line 188
                echo "                                                ";
                echo ($context["_breadcrumb"] ?? null);
                echo "
                                            ";
            }
            // line 190
            echo "                                        </ol>
                                    ";
        }
        // line 192
        echo "                                </div>
                            ";
    }

    // line 196
    public function block_sonata_top_nav_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 197
        echo "                            ";
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 197) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 197), "getOption", [0 => "role_admin"], "method", false, false, false, 197)))) {
            // line 198
            echo "                                <div class=\"navbar-custom-menu\">
                                    <ul class=\"nav navbar-nav\">
                                        ";
            // line 200
            $this->displayBlock('sonata_top_nav_menu_add_block', $context, $blocks);
            // line 208
            echo "                                        ";
            $this->displayBlock('sonata_top_nav_menu_user_block', $context, $blocks);
            // line 218
            echo "                                    </ul>
                                </div>
                            ";
        }
        // line 221
        echo "                        ";
    }

    // line 200
    public function block_sonata_top_nav_menu_add_block($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 201
        echo "                                            <li class=\"dropdown\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-plus-square fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                ";
        // line 205
        $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("add_block"), "@SonataAdmin/standard_layout.html.twig", 205)->display($context);
        // line 206
        echo "                                            </li>
                                        ";
    }

    // line 208
    public function block_sonata_top_nav_menu_user_block($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 209
        echo "                                            <li class=\"dropdown user-menu\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-user fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                <ul class=\"dropdown-menu dropdown-user\">
                                                    ";
        // line 214
        $this->loadTemplate($this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("user_block"), "@SonataAdmin/standard_layout.html.twig", 214)->display($context);
        // line 215
        echo "                                                </ul>
                                            </li>
                                        ";
    }

    // line 227
    public function block_sonata_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 228
        echo "            ";
        $this->displayBlock('sonata_left_side', $context, $blocks);
        // line 261
        echo "
            <div class=\"content-wrapper\">
                ";
        // line 263
        $this->displayBlock('sonata_page_content', $context, $blocks);
        // line 363
        echo "            </div>
        ";
    }

    // line 228
    public function block_sonata_left_side($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 229
        echo "                <aside class=\"main-sidebar\">
                    <section class=\"sidebar\">
                        ";
        // line 231
        $this->displayBlock('sonata_side_nav', $context, $blocks);
        // line 258
        echo "                    </section>
                </aside>
            ";
    }

    // line 231
    public function block_sonata_side_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 232
        echo "                            ";
        $this->displayBlock('sonata_sidebar_search', $context, $blocks);
        // line 246
        echo "
                            ";
        // line 247
        $this->displayBlock('side_bar_before_nav', $context, $blocks);
        // line 248
        echo "                            ";
        $this->displayBlock('side_bar_nav', $context, $blocks);
        // line 251
        echo "                            ";
        $this->displayBlock('side_bar_after_nav', $context, $blocks);
        // line 257
        echo "                        ";
    }

    // line 232
    public function block_sonata_sidebar_search($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 233
        echo "                                ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["sonata_admin"] ?? null), "adminPool", [], "any", false, false, false, 233), "getOption", [0 => "search"], "method", false, false, false, 233)) {
            // line 234
            echo "                                    <form action=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sonata_admin_search");
            echo "\" method=\"GET\" class=\"sidebar-form\" role=\"search\">
                                        <div class=\"input-group custom-search-form\">
                                            <input type=\"text\" name=\"q\" value=\"";
            // line 236
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 236), "get", [0 => "q"], "method", false, false, false, 236), "html", null, true);
            echo "\" class=\"form-control\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("search_placeholder", [], "SonataAdminBundle"), "html", null, true);
            echo "\">
                                            <span class=\"input-group-btn\">
                                                <button class=\"btn btn-flat\" type=\"submit\">
                                                    <i class=\"fa fa-search\" aria-hidden=\"true\"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                ";
        }
        // line 245
        echo "                            ";
    }

    // line 247
    public function block_side_bar_before_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 248
    public function block_side_bar_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 249
        echo "                                ";
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render("sonata_admin_sidebar", ["template" => $this->extensions['Sonata\AdminBundle\Twig\Extension\TemplateRegistryExtension']->getGlobalTemplate("knp_menu_template")]);
        echo "
                            ";
    }

    // line 251
    public function block_side_bar_after_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 252
        echo "                                <p class=\"text-center small\" style=\"border-top: 1px solid #444444; padding-top: 10px\">
                                    ";
        // line 253
        $this->displayBlock('side_bar_after_nav_content', $context, $blocks);
        // line 255
        echo "                                </p>
                            ";
    }

    // line 253
    public function block_side_bar_after_nav_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 254
        echo "                                    ";
    }

    // line 263
    public function block_sonata_page_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 264
        echo "                    <section class=\"content-header\">

                        ";
        // line 266
        $this->displayBlock('sonata_page_content_header', $context, $blocks);
        // line 324
        echo "                    </section>

                    <section class=\"content\">
                        ";
        // line 327
        $this->displayBlock('sonata_admin_content', $context, $blocks);
        // line 361
        echo "                    </section>
                ";
    }

    // line 266
    public function block_sonata_page_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 267
        echo "                            ";
        $this->displayBlock('sonata_page_content_nav', $context, $blocks);
        // line 323
        echo "                        ";
    }

    // line 267
    public function block_sonata_page_content_nav($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 268
        echo "                                ";
        if (((( !twig_test_empty(($context["_navbar_title"] ?? null)) ||  !twig_test_empty(        // line 269
($context["_tab_menu"] ?? null))) ||  !twig_test_empty(        // line 270
($context["_actions"] ?? null))) ||  !twig_test_empty(        // line 271
($context["_list_filters_actions"] ?? null)))) {
            // line 273
            echo "                                    <nav class=\"navbar navbar-default\" role=\"navigation\">
                                        <div class=\"container-fluid\">
                                            ";
            // line 275
            $this->displayBlock('tab_menu_navbar_header', $context, $blocks);
            // line 282
            echo "
                                            <div class=\"navbar-collapse\">
                                                ";
            // line 284
            if ( !twig_test_empty(($context["_tab_menu"] ?? null))) {
                // line 285
                echo "                                                    <div class=\"navbar-left\">
                                                        ";
                // line 286
                echo ($context["_tab_menu"] ?? null);
                echo "
                                                    </div>
                                                ";
            }
            // line 289
            echo "
                                                ";
            // line 290
            if (((((isset($context["admin"]) || array_key_exists("admin", $context)) && (isset($context["action"]) || array_key_exists("action", $context))) && (($context["action"] ?? null) == "list")) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "listModes", [], "any", false, false, false, 290)) > 1))) {
                // line 291
                echo "                                                    <div class=\"nav navbar-right btn-group\">
                                                        ";
                // line 292
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "listModes", [], "any", false, false, false, 292));
                foreach ($context['_seq'] as $context["mode"] => $context["settings"]) {
                    // line 293
                    echo "                                                            <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "generateUrl", [0 => "list", 1 => twig_array_merge(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 293), "query", [], "any", false, false, false, 293), "all", [], "any", false, false, false, 293), ["_list_mode" => $context["mode"]])], "method", false, false, false, 293), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if ((twig_get_attribute($this->env, $this->source, ($context["admin"] ?? null), "getListMode", [], "method", false, false, false, 293) == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["settings"], "class", [], "any", false, false, false, 293), "html", null, true);
                    echo "\"></i></a>
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['mode'], $context['settings'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 295
                echo "                                                    </div>
                                                ";
            }
            // line 297
            echo "
                                                ";
            // line 298
            $this->displayBlock('sonata_admin_content_actions_wrappers', $context, $blocks);
            // line 314
            echo "
                                                ";
            // line 315
            if ( !twig_test_empty(($context["_list_filters_actions"] ?? null))) {
                // line 316
                echo "                                                    ";
                echo ($context["_list_filters_actions"] ?? null);
                echo "
                                                ";
            }
            // line 318
            echo "                                            </div>
                                        </div>
                                    </nav>
                                ";
        }
        // line 322
        echo "                            ";
    }

    // line 275
    public function block_tab_menu_navbar_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 276
        echo "                                                ";
        if ( !twig_test_empty(($context["_navbar_title"] ?? null))) {
            // line 277
            echo "                                                    <div class=\"navbar-header\">
                                                        <a class=\"navbar-brand\" href=\"#\">";
            // line 278
            echo ($context["_navbar_title"] ?? null);
            echo "</a>
                                                    </div>
                                                ";
        }
        // line 281
        echo "                                            ";
    }

    // line 298
    public function block_sonata_admin_content_actions_wrappers($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 299
        echo "                                                    ";
        if ( !twig_test_empty(twig_trim_filter(twig_replace_filter(($context["_actions"] ?? null), ["<li>" => "", "</li>" => ""])))) {
            // line 300
            echo "                                                        <ul class=\"nav navbar-nav navbar-right\">
                                                        ";
            // line 301
            if ((twig_length_filter($this->env, twig_split_filter($this->env, ($context["_actions"] ?? null), "</a>")) > 2)) {
                // line 302
                echo "                                                            <li class=\"dropdown sonata-actions\">
                                                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 303
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_actions", [], "SonataAdminBundle"), "html", null, true);
                echo " <b class=\"caret\"></b></a>
                                                                <ul class=\"dropdown-menu\" role=\"menu\">
                                                                    ";
                // line 305
                echo ($context["_actions"] ?? null);
                echo "
                                                                </ul>
                                                            </li>
                                                        ";
            } else {
                // line 309
                echo "                                                            ";
                echo ($context["_actions"] ?? null);
                echo "
                                                        ";
            }
            // line 311
            echo "                                                        </ul>
                                                    ";
        }
        // line 313
        echo "                                                ";
    }

    // line 327
    public function block_sonata_admin_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 328
        echo "
                            ";
        // line 329
        $this->displayBlock('notice', $context, $blocks);
        // line 332
        echo "
                            ";
        // line 333
        if ( !twig_test_empty(($context["_preview"] ?? null))) {
            // line 334
            echo "                                <div class=\"sonata-ba-preview\">";
            echo ($context["_preview"] ?? null);
            echo "</div>
                            ";
        }
        // line 336
        echo "
                            ";
        // line 337
        if ( !twig_test_empty(($context["_content"] ?? null))) {
            // line 338
            echo "                                <div class=\"sonata-ba-content\">";
            echo ($context["_content"] ?? null);
            echo "</div>
                            ";
        }
        // line 340
        echo "
                            ";
        // line 341
        if ( !twig_test_empty(($context["_show"] ?? null))) {
            // line 342
            echo "                                <div class=\"sonata-ba-show\">";
            echo ($context["_show"] ?? null);
            echo "</div>
                            ";
        }
        // line 344
        echo "
                            ";
        // line 345
        if ( !twig_test_empty(($context["_form"] ?? null))) {
            // line 346
            echo "                                <div class=\"sonata-ba-form\">";
            echo ($context["_form"] ?? null);
            echo "</div>
                            ";
        }
        // line 348
        echo "
                            ";
        // line 349
        if ( !twig_test_empty(($context["_list_filters"] ?? null))) {
            // line 350
            echo "                                <div class=\"row\">
                                    ";
            // line 351
            echo ($context["_list_filters"] ?? null);
            echo "
                                </div>
                            ";
        }
        // line 354
        echo "
                            ";
        // line 355
        if ( !twig_test_empty(($context["_list_table"] ?? null))) {
            // line 356
            echo "                                <div class=\"row\">
                                    ";
            // line 357
            echo ($context["_list_table"] ?? null);
            echo "
                                </div>
                            ";
        }
        // line 360
        echo "                        ";
    }

    // line 329
    public function block_notice($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 330
        echo "                                ";
        $this->loadTemplate("@SonataCore/FlashMessage/render.html.twig", "@SonataAdmin/standard_layout.html.twig", 330)->display($context);
        // line 331
        echo "                            ";
    }

    // line 368
    public function block_bootlint($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 369
        echo "            ";
        // line 370
        echo "            <script type=\"text/javascript\">
                javascript:(function(){var s=document.createElement(\"script\");s.onload=function(){bootlint.showLintReportForCurrentDocument([], {hasProblems: false, problemFree: false});};s.src=\"https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js\";document.body.appendChild(s)})();
            </script>
        ";
    }

    public function getTemplateName()
    {
        return "@SonataAdmin/standard_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1130 => 370,  1128 => 369,  1124 => 368,  1120 => 331,  1117 => 330,  1113 => 329,  1109 => 360,  1103 => 357,  1100 => 356,  1098 => 355,  1095 => 354,  1089 => 351,  1086 => 350,  1084 => 349,  1081 => 348,  1075 => 346,  1073 => 345,  1070 => 344,  1064 => 342,  1062 => 341,  1059 => 340,  1053 => 338,  1051 => 337,  1048 => 336,  1042 => 334,  1040 => 333,  1037 => 332,  1035 => 329,  1032 => 328,  1028 => 327,  1024 => 313,  1020 => 311,  1014 => 309,  1007 => 305,  1002 => 303,  999 => 302,  997 => 301,  994 => 300,  991 => 299,  987 => 298,  983 => 281,  977 => 278,  974 => 277,  971 => 276,  967 => 275,  963 => 322,  957 => 318,  951 => 316,  949 => 315,  946 => 314,  944 => 298,  941 => 297,  937 => 295,  922 => 293,  918 => 292,  915 => 291,  913 => 290,  910 => 289,  904 => 286,  901 => 285,  899 => 284,  895 => 282,  893 => 275,  889 => 273,  887 => 271,  886 => 270,  885 => 269,  883 => 268,  879 => 267,  875 => 323,  872 => 267,  868 => 266,  863 => 361,  861 => 327,  856 => 324,  854 => 266,  850 => 264,  846 => 263,  842 => 254,  838 => 253,  833 => 255,  831 => 253,  828 => 252,  824 => 251,  817 => 249,  813 => 248,  806 => 247,  802 => 245,  788 => 236,  782 => 234,  779 => 233,  775 => 232,  771 => 257,  768 => 251,  765 => 248,  763 => 247,  760 => 246,  757 => 232,  753 => 231,  747 => 258,  745 => 231,  741 => 229,  737 => 228,  732 => 363,  730 => 263,  726 => 261,  723 => 228,  719 => 227,  713 => 215,  711 => 214,  704 => 209,  700 => 208,  695 => 206,  693 => 205,  687 => 201,  683 => 200,  679 => 221,  674 => 218,  671 => 208,  669 => 200,  665 => 198,  662 => 197,  658 => 196,  653 => 192,  649 => 190,  643 => 188,  640 => 187,  637 => 186,  623 => 185,  617 => 183,  613 => 181,  607 => 179,  603 => 177,  600 => 175,  597 => 173,  595 => 172,  590 => 171,  588 => 170,  585 => 169,  583 => 168,  580 => 165,  578 => 164,  576 => 163,  574 => 162,  556 => 161,  553 => 160,  551 => 159,  548 => 158,  546 => 157,  543 => 156,  539 => 155,  534 => 222,  532 => 196,  528 => 194,  526 => 155,  519 => 151,  515 => 149,  511 => 148,  507 => 147,  503 => 145,  497 => 143,  494 => 142,  486 => 140,  484 => 139,  479 => 138,  476 => 137,  472 => 136,  464 => 132,  460 => 130,  456 => 129,  451 => 224,  448 => 148,  445 => 136,  443 => 129,  440 => 128,  436 => 127,  432 => 121,  429 => 120,  427 => 119,  424 => 118,  420 => 117,  416 => 113,  413 => 112,  410 => 111,  396 => 110,  391 => 108,  388 => 105,  386 => 104,  384 => 103,  382 => 102,  378 => 99,  375 => 98,  372 => 97,  355 => 96,  352 => 95,  349 => 94,  343 => 92,  341 => 91,  335 => 89,  331 => 88,  327 => 66,  318 => 64,  313 => 63,  309 => 62,  294 => 50,  290 => 48,  284 => 47,  276 => 46,  268 => 45,  260 => 44,  256 => 42,  252 => 41,  248 => 85,  245 => 84,  239 => 82,  236 => 81,  233 => 80,  230 => 79,  227 => 77,  223 => 75,  221 => 73,  219 => 71,  216 => 70,  213 => 69,  210 => 67,  208 => 62,  205 => 61,  202 => 41,  198 => 40,  194 => 38,  185 => 36,  180 => 35,  176 => 34,  169 => 29,  165 => 28,  158 => 26,  151 => 375,  148 => 374,  145 => 368,  143 => 367,  139 => 365,  137 => 227,  134 => 226,  132 => 127,  126 => 123,  124 => 117,  119 => 114,  117 => 88,  113 => 86,  111 => 40,  108 => 39,  106 => 34,  103 => 33,  101 => 28,  96 => 26,  93 => 25,  91 => 23,  89 => 22,  87 => 21,  85 => 20,  83 => 19,  81 => 18,  79 => 17,  77 => 16,  75 => 15,  73 => 14,  71 => 13,  69 => 12,);
    }

    public function getSourceContext()
    {
        return new Source("", "@SonataAdmin/standard_layout.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/sonata-project/admin-bundle/src/Resources/views/standard_layout.html.twig");
    }
}
