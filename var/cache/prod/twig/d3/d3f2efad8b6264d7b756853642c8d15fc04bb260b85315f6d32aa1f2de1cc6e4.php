<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @KinulabSonataGentellelaTheme/CRUD/show_array.html.twig */
class __TwigTemplate_313ea72302786a77122afc737c2a69da86414fe9a4d43781097f5cfd631f35b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'field' => [$this, 'block_field'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return "@KinulabSonataGentellelaTheme/CRUD/base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        $macros["show"] = $this->macros["show"] = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_array_macro.html.twig", "@KinulabSonataGentellelaTheme/CRUD/show_array.html.twig", 11)->unwrap();
        // line 13
        $this->parent = $this->loadTemplate("@KinulabSonataGentellelaTheme/CRUD/base_show_field.html.twig", "@KinulabSonataGentellelaTheme/CRUD/show_array.html.twig", 13);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 15
    public function block_field($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        echo twig_call_macro($macros["show"], "macro_render_array", [($context["value"] ?? null), ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field_description"] ?? null), "options", [], "any", false, true, false, 16), "inline", [], "any", true, true, false, 16)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["field_description"] ?? null), "options", [], "any", false, true, false, 16), "inline", [], "any", false, false, false, 16), false)) : (false))], 16, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@KinulabSonataGentellelaTheme/CRUD/show_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 16,  49 => 15,  44 => 13,  42 => 11,  35 => 13,);
    }

    public function getSourceContext()
    {
        return new Source("", "@KinulabSonataGentellelaTheme/CRUD/show_array.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/kinulab/sonata-gentellela-theme-bundle/Resources/views/CRUD/show_array.html.twig");
    }
}
