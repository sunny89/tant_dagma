<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @KinulabSonataGentellelaTheme/CRUD/base_array_macro.html.twig */
class __TwigTemplate_327d50d7c835e223b00628eeffe4fe0c957439acba6125a973b38be92b156079 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 11
    public function macro_render_array($__value__ = null, $__inline__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "inline" => $__inline__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 12
            echo "    ";
            $macros["__internal_69d423c37754d8217349ac8318ebce270f4cc82f42bbbca78c5aeb9e7ea9e451"] = $this;
            // line 13
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["val"]) {
                // line 14
                echo "        ";
                if (twig_test_iterable($context["val"])) {
                    // line 15
                    echo "            [";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo " => ";
                    echo twig_call_macro($macros["__internal_69d423c37754d8217349ac8318ebce270f4cc82f42bbbca78c5aeb9e7ea9e451"], "macro_render_array", [$context["val"], ($context["inline"] ?? null)], 15, $context, $this->getSourceContext());
                    echo "]
        ";
                } else {
                    // line 17
                    echo "            [";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo " => ";
                    echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                    echo "]
        ";
                }
                // line 19
                echo "
        ";
                // line 20
                if (( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 20) &&  !($context["inline"] ?? null))) {
                    // line 21
                    echo "            <br>
        ";
                }
                // line 23
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@KinulabSonataGentellelaTheme/CRUD/base_array_macro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 23,  98 => 21,  96 => 20,  93 => 19,  85 => 17,  77 => 15,  74 => 14,  56 => 13,  53 => 12,  39 => 11,);
    }

    public function getSourceContext()
    {
        return new Source("", "@KinulabSonataGentellelaTheme/CRUD/base_array_macro.html.twig", "/Users/tinahenkensiefken/Documents/privat/Symfony Projekte/Git/tant_dagma/vendor/kinulab/sonata-gentellela-theme-bundle/Resources/views/CRUD/base_array_macro.html.twig");
    }
}
