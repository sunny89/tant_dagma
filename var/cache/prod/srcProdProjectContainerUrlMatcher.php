<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // create_pdf
        if (0 === strpos($pathinfo, '/createPDF') && preg_match('#^/createPDF/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'create_pdf']), array (  '_controller' => 'App\\Controller\\DatasheetPDFController::createPDF',));
        }

        if (0 === strpos($pathinfo, '/core')) {
            if (0 === strpos($pathinfo, '/core/get-')) {
                // sonata_admin_retrieve_form_element
                if ('/core/get-form-field-element' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                }

                // sonata_admin_short_object_information
                if (0 === strpos($pathinfo, '/core/get-short-object-description') && preg_match('#^/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'sonata_admin_short_object_information']), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                }

                // sonata_admin_retrieve_autocomplete_items
                if ('/core/get-autocomplete-items' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  '_route' => 'sonata_admin_retrieve_autocomplete_items',);
                }

            }

            // sonata_admin_append_form_element
            if ('/core/append-form-field-element' === $pathinfo) {
                return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
            }

            // sonata_admin_set_object_field_value
            if ('/core/set-object-field-value' === $pathinfo) {
                return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
            }

        }

        // sonata_admin_redirect
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_sonata_admin_redirect;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'sonata_admin_redirect'));
            }

            return $ret;
        }
        not_sonata_admin_redirect:

        // sonata_admin_dashboard
        if ('/dashboard' === $pathinfo) {
            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
        }

        // sonata_admin_search
        if ('/search' === $pathinfo) {
            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
        }

        if (0 === strpos($pathinfo, '/app')) {
            if (0 === strpos($pathinfo, '/app/datasheet')) {
                // admin_app_datasheet_list
                if ('/app/datasheet/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_list',  '_route' => 'admin_app_datasheet_list',);
                }

                // admin_app_datasheet_create
                if ('/app/datasheet/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_create',  '_route' => 'admin_app_datasheet_create',);
                }

                // admin_app_datasheet_batch
                if ('/app/datasheet/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_batch',  '_route' => 'admin_app_datasheet_batch',);
                }

                // admin_app_datasheet_edit
                if (preg_match('#^/app/datasheet/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_edit',));
                }

                // admin_app_datasheet_delete
                if (preg_match('#^/app/datasheet/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_delete',));
                }

                // admin_app_datasheet_show
                if (preg_match('#^/app/datasheet/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_show',));
                }

                // admin_app_datasheet_export
                if ('/app/datasheet/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_export',  '_route' => 'admin_app_datasheet_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/dimmertype')) {
                // admin_app_dimmertype_list
                if ('/app/dimmertype/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_list',  '_route' => 'admin_app_dimmertype_list',);
                }

                // admin_app_dimmertype_create
                if ('/app/dimmertype/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_create',  '_route' => 'admin_app_dimmertype_create',);
                }

                // admin_app_dimmertype_batch
                if ('/app/dimmertype/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_batch',  '_route' => 'admin_app_dimmertype_batch',);
                }

                // admin_app_dimmertype_edit
                if (preg_match('#^/app/dimmertype/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_dimmertype_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_edit',));
                }

                // admin_app_dimmertype_delete
                if (preg_match('#^/app/dimmertype/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_dimmertype_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_delete',));
                }

                // admin_app_dimmertype_show
                if (preg_match('#^/app/dimmertype/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_dimmertype_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_show',));
                }

                // admin_app_dimmertype_export
                if ('/app/dimmertype/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_export',  '_route' => 'admin_app_dimmertype_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/adjustingangle')) {
                // admin_app_adjustingangle_list
                if ('/app/adjustingangle/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_list',  '_route' => 'admin_app_adjustingangle_list',);
                }

                // admin_app_adjustingangle_create
                if ('/app/adjustingangle/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_create',  '_route' => 'admin_app_adjustingangle_create',);
                }

                // admin_app_adjustingangle_batch
                if ('/app/adjustingangle/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_batch',  '_route' => 'admin_app_adjustingangle_batch',);
                }

                // admin_app_adjustingangle_edit
                if (preg_match('#^/app/adjustingangle/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_adjustingangle_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_edit',));
                }

                // admin_app_adjustingangle_delete
                if (preg_match('#^/app/adjustingangle/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_adjustingangle_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_delete',));
                }

                // admin_app_adjustingangle_show
                if (preg_match('#^/app/adjustingangle/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_adjustingangle_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_show',));
                }

                // admin_app_adjustingangle_export
                if ('/app/adjustingangle/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_export',  '_route' => 'admin_app_adjustingangle_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/beamangle')) {
                // admin_app_beamangle_list
                if ('/app/beamangle/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_list',  '_route' => 'admin_app_beamangle_list',);
                }

                // admin_app_beamangle_create
                if ('/app/beamangle/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_create',  '_route' => 'admin_app_beamangle_create',);
                }

                // admin_app_beamangle_batch
                if ('/app/beamangle/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_batch',  '_route' => 'admin_app_beamangle_batch',);
                }

                // admin_app_beamangle_edit
                if (preg_match('#^/app/beamangle/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_beamangle_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_edit',));
                }

                // admin_app_beamangle_delete
                if (preg_match('#^/app/beamangle/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_beamangle_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_delete',));
                }

                // admin_app_beamangle_show
                if (preg_match('#^/app/beamangle/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_beamangle_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_show',));
                }

                // admin_app_beamangle_export
                if ('/app/beamangle/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_export',  '_route' => 'admin_app_beamangle_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/co')) {
                if (0 === strpos($pathinfo, '/app/colourofhousing')) {
                    // admin_app_colourofhousing_list
                    if ('/app/colourofhousing/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_list',  '_route' => 'admin_app_colourofhousing_list',);
                    }

                    // admin_app_colourofhousing_create
                    if ('/app/colourofhousing/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_create',  '_route' => 'admin_app_colourofhousing_create',);
                    }

                    // admin_app_colourofhousing_batch
                    if ('/app/colourofhousing/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_batch',  '_route' => 'admin_app_colourofhousing_batch',);
                    }

                    // admin_app_colourofhousing_edit
                    if (preg_match('#^/app/colourofhousing/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofhousing_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_edit',));
                    }

                    // admin_app_colourofhousing_delete
                    if (preg_match('#^/app/colourofhousing/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofhousing_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_delete',));
                    }

                    // admin_app_colourofhousing_show
                    if (preg_match('#^/app/colourofhousing/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofhousing_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_show',));
                    }

                    // admin_app_colourofhousing_export
                    if ('/app/colourofhousing/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_export',  '_route' => 'admin_app_colourofhousing_export',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/app/colourofshade')) {
                    // admin_app_colourofshade_list
                    if ('/app/colourofshade/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_list',  '_route' => 'admin_app_colourofshade_list',);
                    }

                    // admin_app_colourofshade_create
                    if ('/app/colourofshade/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_create',  '_route' => 'admin_app_colourofshade_create',);
                    }

                    // admin_app_colourofshade_batch
                    if ('/app/colourofshade/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_batch',  '_route' => 'admin_app_colourofshade_batch',);
                    }

                    // admin_app_colourofshade_edit
                    if (preg_match('#^/app/colourofshade/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofshade_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_edit',));
                    }

                    // admin_app_colourofshade_delete
                    if (preg_match('#^/app/colourofshade/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofshade_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_delete',));
                    }

                    // admin_app_colourofshade_show
                    if (preg_match('#^/app/colourofshade/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofshade_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_show',));
                    }

                    // admin_app_colourofshade_export
                    if ('/app/colourofshade/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_export',  '_route' => 'admin_app_colourofshade_export',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/app/cover')) {
                    // admin_app_cover_list
                    if ('/app/cover/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_list',  '_route' => 'admin_app_cover_list',);
                    }

                    // admin_app_cover_create
                    if ('/app/cover/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_create',  '_route' => 'admin_app_cover_create',);
                    }

                    // admin_app_cover_batch
                    if ('/app/cover/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_batch',  '_route' => 'admin_app_cover_batch',);
                    }

                    // admin_app_cover_edit
                    if (preg_match('#^/app/cover/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cover_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_edit',));
                    }

                    // admin_app_cover_delete
                    if (preg_match('#^/app/cover/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cover_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_delete',));
                    }

                    // admin_app_cover_show
                    if (preg_match('#^/app/cover/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cover_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_show',));
                    }

                    // admin_app_cover_export
                    if ('/app/cover/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_export',  '_route' => 'admin_app_cover_export',);
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/app/cri')) {
                // admin_app_cri_list
                if ('/app/cri/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_list',  '_route' => 'admin_app_cri_list',);
                }

                // admin_app_cri_create
                if ('/app/cri/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_create',  '_route' => 'admin_app_cri_create',);
                }

                // admin_app_cri_batch
                if ('/app/cri/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_batch',  '_route' => 'admin_app_cri_batch',);
                }

                // admin_app_cri_edit
                if (preg_match('#^/app/cri/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cri_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_edit',));
                }

                // admin_app_cri_delete
                if (preg_match('#^/app/cri/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cri_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_delete',));
                }

                // admin_app_cri_show
                if (preg_match('#^/app/cri/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cri_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_show',));
                }

                // admin_app_cri_export
                if ('/app/cri/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_export',  '_route' => 'admin_app_cri_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/fixturelightoutput')) {
                // admin_app_fixturelightoutput_list
                if ('/app/fixturelightoutput/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_list',  '_route' => 'admin_app_fixturelightoutput_list',);
                }

                // admin_app_fixturelightoutput_create
                if ('/app/fixturelightoutput/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_create',  '_route' => 'admin_app_fixturelightoutput_create',);
                }

                // admin_app_fixturelightoutput_batch
                if ('/app/fixturelightoutput/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_batch',  '_route' => 'admin_app_fixturelightoutput_batch',);
                }

                // admin_app_fixturelightoutput_edit
                if (preg_match('#^/app/fixturelightoutput/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_fixturelightoutput_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_edit',));
                }

                // admin_app_fixturelightoutput_delete
                if (preg_match('#^/app/fixturelightoutput/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_fixturelightoutput_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_delete',));
                }

                // admin_app_fixturelightoutput_show
                if (preg_match('#^/app/fixturelightoutput/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_fixturelightoutput_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_show',));
                }

                // admin_app_fixturelightoutput_export
                if ('/app/fixturelightoutput/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_export',  '_route' => 'admin_app_fixturelightoutput_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/frequency')) {
                // admin_app_frequency_list
                if ('/app/frequency/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_list',  '_route' => 'admin_app_frequency_list',);
                }

                // admin_app_frequency_create
                if ('/app/frequency/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_create',  '_route' => 'admin_app_frequency_create',);
                }

                // admin_app_frequency_batch
                if ('/app/frequency/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_batch',  '_route' => 'admin_app_frequency_batch',);
                }

                // admin_app_frequency_edit
                if (preg_match('#^/app/frequency/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_frequency_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_edit',));
                }

                // admin_app_frequency_delete
                if (preg_match('#^/app/frequency/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_frequency_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_delete',));
                }

                // admin_app_frequency_show
                if (preg_match('#^/app/frequency/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_frequency_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_show',));
                }

                // admin_app_frequency_export
                if ('/app/frequency/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_export',  '_route' => 'admin_app_frequency_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/lightcolour')) {
                // admin_app_lightcolour_list
                if ('/app/lightcolour/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_list',  '_route' => 'admin_app_lightcolour_list',);
                }

                // admin_app_lightcolour_create
                if ('/app/lightcolour/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_create',  '_route' => 'admin_app_lightcolour_create',);
                }

                // admin_app_lightcolour_batch
                if ('/app/lightcolour/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_batch',  '_route' => 'admin_app_lightcolour_batch',);
                }

                // admin_app_lightcolour_edit
                if (preg_match('#^/app/lightcolour/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightcolour_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_edit',));
                }

                // admin_app_lightcolour_delete
                if (preg_match('#^/app/lightcolour/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightcolour_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_delete',));
                }

                // admin_app_lightcolour_show
                if (preg_match('#^/app/lightcolour/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightcolour_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_show',));
                }

                // admin_app_lightcolour_export
                if ('/app/lightcolour/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_export',  '_route' => 'admin_app_lightcolour_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/lightpoints')) {
                // admin_app_lightpoints_list
                if ('/app/lightpoints/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_list',  '_route' => 'admin_app_lightpoints_list',);
                }

                // admin_app_lightpoints_create
                if ('/app/lightpoints/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_create',  '_route' => 'admin_app_lightpoints_create',);
                }

                // admin_app_lightpoints_batch
                if ('/app/lightpoints/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_batch',  '_route' => 'admin_app_lightpoints_batch',);
                }

                // admin_app_lightpoints_edit
                if (preg_match('#^/app/lightpoints/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightpoints_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_edit',));
                }

                // admin_app_lightpoints_delete
                if (preg_match('#^/app/lightpoints/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightpoints_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_delete',));
                }

                // admin_app_lightpoints_show
                if (preg_match('#^/app/lightpoints/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightpoints_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_show',));
                }

                // admin_app_lightpoints_export
                if ('/app/lightpoints/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_export',  '_route' => 'admin_app_lightpoints_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/materialofhousing')) {
                // admin_app_materialofhousing_list
                if ('/app/materialofhousing/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_list',  '_route' => 'admin_app_materialofhousing_list',);
                }

                // admin_app_materialofhousing_create
                if ('/app/materialofhousing/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_create',  '_route' => 'admin_app_materialofhousing_create',);
                }

                // admin_app_materialofhousing_batch
                if ('/app/materialofhousing/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_batch',  '_route' => 'admin_app_materialofhousing_batch',);
                }

                // admin_app_materialofhousing_edit
                if (preg_match('#^/app/materialofhousing/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofhousing_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_edit',));
                }

                // admin_app_materialofhousing_delete
                if (preg_match('#^/app/materialofhousing/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofhousing_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_delete',));
                }

                // admin_app_materialofhousing_show
                if (preg_match('#^/app/materialofhousing/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofhousing_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_show',));
                }

                // admin_app_materialofhousing_export
                if ('/app/materialofhousing/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_export',  '_route' => 'admin_app_materialofhousing_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/materialofshade')) {
                // admin_app_materialofshade_list
                if ('/app/materialofshade/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_list',  '_route' => 'admin_app_materialofshade_list',);
                }

                // admin_app_materialofshade_create
                if ('/app/materialofshade/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_create',  '_route' => 'admin_app_materialofshade_create',);
                }

                // admin_app_materialofshade_batch
                if ('/app/materialofshade/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_batch',  '_route' => 'admin_app_materialofshade_batch',);
                }

                // admin_app_materialofshade_edit
                if (preg_match('#^/app/materialofshade/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofshade_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_edit',));
                }

                // admin_app_materialofshade_delete
                if (preg_match('#^/app/materialofshade/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofshade_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_delete',));
                }

                // admin_app_materialofshade_show
                if (preg_match('#^/app/materialofshade/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofshade_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_show',));
                }

                // admin_app_materialofshade_export
                if ('/app/materialofshade/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_export',  '_route' => 'admin_app_materialofshade_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/numberoflamps')) {
                // admin_app_numberoflamps_list
                if ('/app/numberoflamps/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_list',  '_route' => 'admin_app_numberoflamps_list',);
                }

                // admin_app_numberoflamps_create
                if ('/app/numberoflamps/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_create',  '_route' => 'admin_app_numberoflamps_create',);
                }

                // admin_app_numberoflamps_batch
                if ('/app/numberoflamps/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_batch',  '_route' => 'admin_app_numberoflamps_batch',);
                }

                // admin_app_numberoflamps_edit
                if (preg_match('#^/app/numberoflamps/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_numberoflamps_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_edit',));
                }

                // admin_app_numberoflamps_delete
                if (preg_match('#^/app/numberoflamps/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_numberoflamps_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_delete',));
                }

                // admin_app_numberoflamps_show
                if (preg_match('#^/app/numberoflamps/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_numberoflamps_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_show',));
                }

                // admin_app_numberoflamps_export
                if ('/app/numberoflamps/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_export',  '_route' => 'admin_app_numberoflamps_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/p')) {
                if (0 === strpos($pathinfo, '/app/plug')) {
                    // admin_app_plug_list
                    if ('/app/plug/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_list',  '_route' => 'admin_app_plug_list',);
                    }

                    // admin_app_plug_create
                    if ('/app/plug/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_create',  '_route' => 'admin_app_plug_create',);
                    }

                    // admin_app_plug_batch
                    if ('/app/plug/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_batch',  '_route' => 'admin_app_plug_batch',);
                    }

                    // admin_app_plug_edit
                    if (preg_match('#^/app/plug/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_plug_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_edit',));
                    }

                    // admin_app_plug_delete
                    if (preg_match('#^/app/plug/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_plug_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_delete',));
                    }

                    // admin_app_plug_show
                    if (preg_match('#^/app/plug/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_plug_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_show',));
                    }

                    // admin_app_plug_export
                    if ('/app/plug/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_export',  '_route' => 'admin_app_plug_export',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/app/poweroffitting')) {
                    // admin_app_poweroffitting_list
                    if ('/app/poweroffitting/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_list',  '_route' => 'admin_app_poweroffitting_list',);
                    }

                    // admin_app_poweroffitting_create
                    if ('/app/poweroffitting/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_create',  '_route' => 'admin_app_poweroffitting_create',);
                    }

                    // admin_app_poweroffitting_batch
                    if ('/app/poweroffitting/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_batch',  '_route' => 'admin_app_poweroffitting_batch',);
                    }

                    // admin_app_poweroffitting_edit
                    if (preg_match('#^/app/poweroffitting/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroffitting_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_edit',));
                    }

                    // admin_app_poweroffitting_delete
                    if (preg_match('#^/app/poweroffitting/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroffitting_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_delete',));
                    }

                    // admin_app_poweroffitting_show
                    if (preg_match('#^/app/poweroffitting/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroffitting_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_show',));
                    }

                    // admin_app_poweroffitting_export
                    if ('/app/poweroffitting/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_export',  '_route' => 'admin_app_poweroffitting_export',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/app/poweroflamp')) {
                    // admin_app_poweroflamp_list
                    if ('/app/poweroflamp/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_list',  '_route' => 'admin_app_poweroflamp_list',);
                    }

                    // admin_app_poweroflamp_create
                    if ('/app/poweroflamp/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_create',  '_route' => 'admin_app_poweroflamp_create',);
                    }

                    // admin_app_poweroflamp_batch
                    if ('/app/poweroflamp/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_batch',  '_route' => 'admin_app_poweroflamp_batch',);
                    }

                    // admin_app_poweroflamp_edit
                    if (preg_match('#^/app/poweroflamp/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroflamp_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_edit',));
                    }

                    // admin_app_poweroflamp_delete
                    if (preg_match('#^/app/poweroflamp/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroflamp_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_delete',));
                    }

                    // admin_app_poweroflamp_show
                    if (preg_match('#^/app/poweroflamp/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroflamp_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_show',));
                    }

                    // admin_app_poweroflamp_export
                    if ('/app/poweroflamp/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_export',  '_route' => 'admin_app_poweroflamp_export',);
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/app/supplier')) {
                // admin_app_supplier_list
                if ('/app/supplier/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_list',  '_route' => 'admin_app_supplier_list',);
                }

                // admin_app_supplier_create
                if ('/app/supplier/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_create',  '_route' => 'admin_app_supplier_create',);
                }

                // admin_app_supplier_batch
                if ('/app/supplier/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_batch',  '_route' => 'admin_app_supplier_batch',);
                }

                // admin_app_supplier_edit
                if (preg_match('#^/app/supplier/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_supplier_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_edit',));
                }

                // admin_app_supplier_delete
                if (preg_match('#^/app/supplier/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_supplier_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_delete',));
                }

                // admin_app_supplier_show
                if (preg_match('#^/app/supplier/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_supplier_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_show',));
                }

                // admin_app_supplier_export
                if ('/app/supplier/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_export',  '_route' => 'admin_app_supplier_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/socket')) {
                // admin_app_socket_list
                if ('/app/socket/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_list',  '_route' => 'admin_app_socket_list',);
                }

                // admin_app_socket_create
                if ('/app/socket/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_create',  '_route' => 'admin_app_socket_create',);
                }

                // admin_app_socket_batch
                if ('/app/socket/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_batch',  '_route' => 'admin_app_socket_batch',);
                }

                // admin_app_socket_edit
                if (preg_match('#^/app/socket/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_socket_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_edit',));
                }

                // admin_app_socket_delete
                if (preg_match('#^/app/socket/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_socket_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_delete',));
                }

                // admin_app_socket_show
                if (preg_match('#^/app/socket/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_socket_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_show',));
                }

                // admin_app_socket_export
                if ('/app/socket/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_export',  '_route' => 'admin_app_socket_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/typeoflamp')) {
                // admin_app_typeoflamp_list
                if ('/app/typeoflamp/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_list',  '_route' => 'admin_app_typeoflamp_list',);
                }

                // admin_app_typeoflamp_create
                if ('/app/typeoflamp/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_create',  '_route' => 'admin_app_typeoflamp_create',);
                }

                // admin_app_typeoflamp_batch
                if ('/app/typeoflamp/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_batch',  '_route' => 'admin_app_typeoflamp_batch',);
                }

                // admin_app_typeoflamp_edit
                if (preg_match('#^/app/typeoflamp/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeoflamp_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_edit',));
                }

                // admin_app_typeoflamp_delete
                if (preg_match('#^/app/typeoflamp/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeoflamp_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_delete',));
                }

                // admin_app_typeoflamp_show
                if (preg_match('#^/app/typeoflamp/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeoflamp_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_show',));
                }

                // admin_app_typeoflamp_export
                if ('/app/typeoflamp/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_export',  '_route' => 'admin_app_typeoflamp_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/typeofbulb')) {
                // admin_app_typeofbulb_list
                if ('/app/typeofbulb/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_list',  '_route' => 'admin_app_typeofbulb_list',);
                }

                // admin_app_typeofbulb_create
                if ('/app/typeofbulb/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_create',  '_route' => 'admin_app_typeofbulb_create',);
                }

                // admin_app_typeofbulb_batch
                if ('/app/typeofbulb/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_batch',  '_route' => 'admin_app_typeofbulb_batch',);
                }

                // admin_app_typeofbulb_edit
                if (preg_match('#^/app/typeofbulb/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeofbulb_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_edit',));
                }

                // admin_app_typeofbulb_delete
                if (preg_match('#^/app/typeofbulb/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeofbulb_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_delete',));
                }

                // admin_app_typeofbulb_show
                if (preg_match('#^/app/typeofbulb/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeofbulb_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_show',));
                }

                // admin_app_typeofbulb_export
                if ('/app/typeofbulb/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_export',  '_route' => 'admin_app_typeofbulb_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/voltage')) {
                // admin_app_voltage_list
                if ('/app/voltage/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_list',  '_route' => 'admin_app_voltage_list',);
                }

                // admin_app_voltage_create
                if ('/app/voltage/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_create',  '_route' => 'admin_app_voltage_create',);
                }

                // admin_app_voltage_batch
                if ('/app/voltage/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_batch',  '_route' => 'admin_app_voltage_batch',);
                }

                // admin_app_voltage_edit
                if (preg_match('#^/app/voltage/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_voltage_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_edit',));
                }

                // admin_app_voltage_delete
                if (preg_match('#^/app/voltage/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_voltage_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_delete',));
                }

                // admin_app_voltage_show
                if (preg_match('#^/app/voltage/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_voltage_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_show',));
                }

                // admin_app_voltage_export
                if ('/app/voltage/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_export',  '_route' => 'admin_app_voltage_export',);
                }

            }

            elseif (0 === strpos($pathinfo, '/app/ipclass')) {
                // admin_app_ipclass_list
                if ('/app/ipclass/list' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_list',  '_route' => 'admin_app_ipclass_list',);
                }

                // admin_app_ipclass_create
                if ('/app/ipclass/create' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_create',  '_route' => 'admin_app_ipclass_create',);
                }

                // admin_app_ipclass_batch
                if ('/app/ipclass/batch' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_batch',  '_route' => 'admin_app_ipclass_batch',);
                }

                // admin_app_ipclass_edit
                if (preg_match('#^/app/ipclass/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_ipclass_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_edit',));
                }

                // admin_app_ipclass_delete
                if (preg_match('#^/app/ipclass/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_ipclass_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_delete',));
                }

                // admin_app_ipclass_show
                if (preg_match('#^/app/ipclass/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_ipclass_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_show',));
                }

                // admin_app_ipclass_export
                if ('/app/ipclass/export' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.ipClass',  '_sonata_name' => 'admin_app_ipclass_export',  '_route' => 'admin_app_ipclass_export',);
                }

            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
