<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'knp_menu.menu_provider' shared service.

include_once $this->targetDirs[3].'/vendor/knplabs/knp-menu/src/Knp/Menu/Provider/MenuProviderInterface.php';
include_once $this->targetDirs[3].'/vendor/knplabs/knp-menu/src/Knp/Menu/Provider/ChainProvider.php';

return $this->services['knp_menu.menu_provider'] = new \Knp\Menu\Provider\ChainProvider(new RewindableGenerator(function () {
    yield 0 => ${($_ = isset($this->services['knp_menu.menu_provider.lazy']) ? $this->services['knp_menu.menu_provider.lazy'] : $this->load('getKnpMenu_MenuProvider_LazyService.php')) && false ?: '_'};
    yield 1 => ${($_ = isset($this->services['knp_menu.menu_provider.builder_alias']) ? $this->services['knp_menu.menu_provider.builder_alias'] : $this->load('getKnpMenu_MenuProvider_BuilderAliasService.php')) && false ?: '_'};
    yield 2 => ${($_ = isset($this->services['sonata.admin.menu.group_provider']) ? $this->services['sonata.admin.menu.group_provider'] : $this->load('getSonata_Admin_Menu_GroupProviderService.php')) && false ?: '_'};
}, 3));
