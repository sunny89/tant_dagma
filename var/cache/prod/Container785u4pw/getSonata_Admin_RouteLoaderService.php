<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sonata.admin.route_loader' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/config/Loader/LoaderInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/config/Loader/Loader.php';
include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Route/AdminPoolLoader.php';

return $this->services['sonata.admin.route_loader'] = new \Sonata\AdminBundle\Route\AdminPoolLoader(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'}, [0 => 'admin.datasheet', 1 => 'admin.adjustingAngle', 2 => 'admin.beamAngle', 3 => 'admin.colourOfHousing', 4 => 'admin.colourOfShade', 5 => 'admin.cover', 6 => 'admin.fixtureLightOutput', 7 => 'admin.lightColour', 8 => 'admin.lightPoints', 9 => 'admin.materialOfHousing', 10 => 'admin.materialOfShade', 11 => 'admin.numberOfLamps', 12 => 'admin.plug', 13 => 'admin.powerOfFitting', 14 => 'admin.powerOfLamp', 15 => 'admin.supplier', 16 => 'admin.socket', 17 => 'admin.typeOfLamp', 18 => 'admin.voltage', 19 => 'admin.typeOfBulb', 20 => 'admin.dimmerType', 21 => 'admin.frequency', 22 => 'admin.ipClass', 23 => 'admin.cri'], $this);
