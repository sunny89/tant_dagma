<?php

namespace Container785u4pw;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/*
 * This class has been auto-generated
 * by the Symfony Dependency Injection Component.
 *
 * @final since Symfony 3.3
 */
class srcProdProjectContainer extends Container
{
    private $buildParameters;
    private $containerDir;
    private $parameters = [];
    private $targetDirs = [];

    public function __construct(array $buildParameters = [], $containerDir = __DIR__)
    {
        $dir = $this->targetDirs[0] = \dirname($containerDir);
        for ($i = 1; $i <= 5; ++$i) {
            $this->targetDirs[$i] = $dir = \dirname($dir);
        }
        $this->buildParameters = $buildParameters;
        $this->containerDir = $containerDir;
        $this->parameters = $this->getDefaultParameters();

        $this->services = [];
        $this->normalizedIds = [
            'admin.adjustingangle' => 'admin.adjustingAngle',
            'admin.adjustingangle.template_registry' => 'admin.adjustingAngle.template_registry',
            'admin.beamangle' => 'admin.beamAngle',
            'admin.beamangle.template_registry' => 'admin.beamAngle.template_registry',
            'admin.colourofhousing' => 'admin.colourOfHousing',
            'admin.colourofhousing.template_registry' => 'admin.colourOfHousing.template_registry',
            'admin.colourofshade' => 'admin.colourOfShade',
            'admin.colourofshade.template_registry' => 'admin.colourOfShade.template_registry',
            'admin.dimmertype' => 'admin.dimmerType',
            'admin.dimmertype.template_registry' => 'admin.dimmerType.template_registry',
            'admin.fixturelightoutput' => 'admin.fixtureLightOutput',
            'admin.fixturelightoutput.template_registry' => 'admin.fixtureLightOutput.template_registry',
            'admin.ipclass' => 'admin.ipClass',
            'admin.ipclass.template_registry' => 'admin.ipClass.template_registry',
            'admin.lightcolour' => 'admin.lightColour',
            'admin.lightcolour.template_registry' => 'admin.lightColour.template_registry',
            'admin.lightpoints' => 'admin.lightPoints',
            'admin.lightpoints.template_registry' => 'admin.lightPoints.template_registry',
            'admin.materialofhousing' => 'admin.materialOfHousing',
            'admin.materialofhousing.template_registry' => 'admin.materialOfHousing.template_registry',
            'admin.materialofshade' => 'admin.materialOfShade',
            'admin.materialofshade.template_registry' => 'admin.materialOfShade.template_registry',
            'admin.numberoflamps' => 'admin.numberOfLamps',
            'admin.numberoflamps.template_registry' => 'admin.numberOfLamps.template_registry',
            'admin.poweroffitting' => 'admin.powerOfFitting',
            'admin.poweroffitting.template_registry' => 'admin.powerOfFitting.template_registry',
            'admin.poweroflamp' => 'admin.powerOfLamp',
            'admin.poweroflamp.template_registry' => 'admin.powerOfLamp.template_registry',
            'admin.typeofbulb' => 'admin.typeOfBulb',
            'admin.typeofbulb.template_registry' => 'admin.typeOfBulb.template_registry',
            'admin.typeoflamp' => 'admin.typeOfLamp',
            'admin.typeoflamp.template_registry' => 'admin.typeOfLamp.template_registry',
            'app\\controller\\datasheetpdfcontroller' => 'App\\Controller\\DatasheetPDFController',
            'app\\repository\\adjustinganglerepository' => 'App\\Repository\\AdjustingAngleRepository',
            'app\\repository\\beamanglerepository' => 'App\\Repository\\BeamAngleRepository',
            'app\\repository\\buildgrouprepository' => 'App\\Repository\\BuildGroupRepository',
            'app\\repository\\colourofhousingrepository' => 'App\\Repository\\ColourOfHousingRepository',
            'app\\repository\\colourofshaderepository' => 'App\\Repository\\ColourOfShadeRepository',
            'app\\repository\\colourtemperaturerepository' => 'App\\Repository\\ColourTemperatureRepository',
            'app\\repository\\coverrepository' => 'App\\Repository\\CoverRepository',
            'app\\repository\\crirepository' => 'App\\Repository\\CriRepository',
            'app\\repository\\datasheetrepository' => 'App\\Repository\\DataSheetRepository',
            'app\\repository\\dimmertyperepository' => 'App\\Repository\\DimmerTypeRepository',
            'app\\repository\\fixturelightoutputrepository' => 'App\\Repository\\FixtureLightOutputRepository',
            'app\\repository\\frequencyrepository' => 'App\\Repository\\FrequencyRepository',
            'app\\repository\\ipclassrepository' => 'App\\Repository\\IPClassRepository',
            'app\\repository\\lightcolourrepository' => 'App\\Repository\\LightColourRepository',
            'app\\repository\\lightpointsrepository' => 'App\\Repository\\LightPointsRepository',
            'app\\repository\\materialofhousingrepository' => 'App\\Repository\\MaterialOfHousingRepository',
            'app\\repository\\materialofshaderepository' => 'App\\Repository\\MaterialOfShadeRepository',
            'app\\repository\\numberoflampsrepository' => 'App\\Repository\\NumberOfLampsRepository',
            'app\\repository\\plugrepository' => 'App\\Repository\\PlugRepository',
            'app\\repository\\poweroffittingrepository' => 'App\\Repository\\PowerOfFittingRepository',
            'app\\repository\\poweroflamprepository' => 'App\\Repository\\PowerOfLampRepository',
            'app\\repository\\socketrepository' => 'App\\Repository\\SocketRepository',
            'app\\repository\\supplierrepository' => 'App\\Repository\\SupplierRepository',
            'app\\repository\\typeofbulbrepository' => 'App\\Repository\\TypeOfBulbRepository',
            'app\\repository\\typeoflamprepository' => 'App\\Repository\\TypeOfLampRepository',
            'app\\repository\\voltagerepository' => 'App\\Repository\\VoltageRepository',
            'sonata\\adminbundle\\admin\\pool' => 'Sonata\\AdminBundle\\Admin\\Pool',
            'sonata\\adminbundle\\command\\createclasscachecommand' => 'Sonata\\AdminBundle\\Command\\CreateClassCacheCommand',
            'sonata\\adminbundle\\command\\explainadmincommand' => 'Sonata\\AdminBundle\\Command\\ExplainAdminCommand',
            'sonata\\adminbundle\\command\\generateadmincommand' => 'Sonata\\AdminBundle\\Command\\GenerateAdminCommand',
            'sonata\\adminbundle\\command\\generateobjectaclcommand' => 'Sonata\\AdminBundle\\Command\\GenerateObjectAclCommand',
            'sonata\\adminbundle\\command\\listadmincommand' => 'Sonata\\AdminBundle\\Command\\ListAdminCommand',
            'sonata\\adminbundle\\command\\setupaclcommand' => 'Sonata\\AdminBundle\\Command\\SetupAclCommand',
            'sonata\\blockbundle\\command\\debugblockscommand' => 'Sonata\\BlockBundle\\Command\\DebugBlocksCommand',
            'sonata\\corebundle\\command\\sonatadumpdoctrinemetacommand' => 'Sonata\\CoreBundle\\Command\\SonataDumpDoctrineMetaCommand',
            'sonata\\corebundle\\command\\sonatalistformmappingcommand' => 'Sonata\\CoreBundle\\Command\\SonataListFormMappingCommand',
            'symfony\\bundle\\frameworkbundle\\controller\\redirectcontroller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController',
            'symfony\\bundle\\frameworkbundle\\controller\\templatecontroller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController',
        ];
        $this->syntheticIds = [
            'kernel' => true,
        ];
        $this->methodMap = [
            'admin.adjustingAngle' => 'getAdmin_AdjustingAngleService',
            'admin.beamAngle' => 'getAdmin_BeamAngleService',
            'admin.colourOfHousing' => 'getAdmin_ColourOfHousingService',
            'admin.colourOfShade' => 'getAdmin_ColourOfShadeService',
            'admin.cover' => 'getAdmin_CoverService',
            'admin.cri' => 'getAdmin_CriService',
            'admin.datasheet' => 'getAdmin_DatasheetService',
            'admin.dimmerType' => 'getAdmin_DimmerTypeService',
            'admin.fixtureLightOutput' => 'getAdmin_FixtureLightOutputService',
            'admin.frequency' => 'getAdmin_FrequencyService',
            'admin.ipClass' => 'getAdmin_IpClassService',
            'admin.lightColour' => 'getAdmin_LightColourService',
            'admin.lightPoints' => 'getAdmin_LightPointsService',
            'admin.materialOfHousing' => 'getAdmin_MaterialOfHousingService',
            'admin.materialOfShade' => 'getAdmin_MaterialOfShadeService',
            'admin.numberOfLamps' => 'getAdmin_NumberOfLampsService',
            'admin.plug' => 'getAdmin_PlugService',
            'admin.powerOfFitting' => 'getAdmin_PowerOfFittingService',
            'admin.powerOfLamp' => 'getAdmin_PowerOfLampService',
            'admin.socket' => 'getAdmin_SocketService',
            'admin.supplier' => 'getAdmin_SupplierService',
            'admin.typeOfBulb' => 'getAdmin_TypeOfBulbService',
            'admin.typeOfLamp' => 'getAdmin_TypeOfLampService',
            'admin.voltage' => 'getAdmin_VoltageService',
            'config_cache_factory' => 'getConfigCacheFactoryService',
            'controller_name_converter' => 'getControllerNameConverterService',
            'debug.debug_handlers_listener' => 'getDebug_DebugHandlersListenerService',
            'debug.file_link_formatter' => 'getDebug_FileLinkFormatterService',
            'event_dispatcher' => 'getEventDispatcherService',
            'http_kernel' => 'getHttpKernelService',
            'locale_listener' => 'getLocaleListenerService',
            'logger' => 'getLoggerService',
            'request_stack' => 'getRequestStackService',
            'resolve_controller_name_subscriber' => 'getResolveControllerNameSubscriberService',
            'response_listener' => 'getResponseListenerService',
            'router' => 'getRouterService',
            'router.request_context' => 'getRouter_RequestContextService',
            'router_listener' => 'getRouterListenerService',
            'security.firewall' => 'getSecurity_FirewallService',
            'security.logout_url_generator' => 'getSecurity_LogoutUrlGeneratorService',
            'security.rememberme.response_listener' => 'getSecurity_Rememberme_ResponseListenerService',
            'security.token_storage' => 'getSecurity_TokenStorageService',
            'session.save_listener' => 'getSession_SaveListenerService',
            'session_listener' => 'getSessionListenerService',
            'sonata.admin.orm.filter.type.boolean' => 'getSonata_Admin_Orm_Filter_Type_BooleanService',
            'sonata.admin.orm.filter.type.callback' => 'getSonata_Admin_Orm_Filter_Type_CallbackService',
            'sonata.admin.orm.filter.type.choice' => 'getSonata_Admin_Orm_Filter_Type_ChoiceService',
            'sonata.admin.orm.filter.type.class' => 'getSonata_Admin_Orm_Filter_Type_ClassService',
            'sonata.admin.orm.filter.type.date' => 'getSonata_Admin_Orm_Filter_Type_DateService',
            'sonata.admin.orm.filter.type.date_range' => 'getSonata_Admin_Orm_Filter_Type_DateRangeService',
            'sonata.admin.orm.filter.type.datetime' => 'getSonata_Admin_Orm_Filter_Type_DatetimeService',
            'sonata.admin.orm.filter.type.datetime_range' => 'getSonata_Admin_Orm_Filter_Type_DatetimeRangeService',
            'sonata.admin.orm.filter.type.model' => 'getSonata_Admin_Orm_Filter_Type_ModelService',
            'sonata.admin.orm.filter.type.model_autocomplete' => 'getSonata_Admin_Orm_Filter_Type_ModelAutocompleteService',
            'sonata.admin.orm.filter.type.number' => 'getSonata_Admin_Orm_Filter_Type_NumberService',
            'sonata.admin.orm.filter.type.string' => 'getSonata_Admin_Orm_Filter_Type_StringService',
            'sonata.admin.orm.filter.type.time' => 'getSonata_Admin_Orm_Filter_Type_TimeService',
            'sonata.block.cache.handler.default' => 'getSonata_Block_Cache_Handler_DefaultService',
            'streamed_response_listener' => 'getStreamedResponseListenerService',
            'translator.default' => 'getTranslator_DefaultService',
            'translator_listener' => 'getTranslatorListenerService',
            'validate_request_listener' => 'getValidateRequestListenerService',
        ];
        $this->fileMap = [
            'App\\Controller\\DatasheetPDFController' => 'getDatasheetPDFControllerService.php',
            'App\\Repository\\AdjustingAngleRepository' => 'getAdjustingAngleRepositoryService.php',
            'App\\Repository\\BeamAngleRepository' => 'getBeamAngleRepositoryService.php',
            'App\\Repository\\BuildGroupRepository' => 'getBuildGroupRepositoryService.php',
            'App\\Repository\\ColourOfHousingRepository' => 'getColourOfHousingRepositoryService.php',
            'App\\Repository\\ColourOfShadeRepository' => 'getColourOfShadeRepositoryService.php',
            'App\\Repository\\ColourTemperatureRepository' => 'getColourTemperatureRepositoryService.php',
            'App\\Repository\\CoverRepository' => 'getCoverRepositoryService.php',
            'App\\Repository\\CriRepository' => 'getCriRepositoryService.php',
            'App\\Repository\\DataSheetRepository' => 'getDataSheetRepositoryService.php',
            'App\\Repository\\DimmerTypeRepository' => 'getDimmerTypeRepositoryService.php',
            'App\\Repository\\FixtureLightOutputRepository' => 'getFixtureLightOutputRepositoryService.php',
            'App\\Repository\\FrequencyRepository' => 'getFrequencyRepositoryService.php',
            'App\\Repository\\IPClassRepository' => 'getIPClassRepositoryService.php',
            'App\\Repository\\LightColourRepository' => 'getLightColourRepositoryService.php',
            'App\\Repository\\LightPointsRepository' => 'getLightPointsRepositoryService.php',
            'App\\Repository\\MaterialOfHousingRepository' => 'getMaterialOfHousingRepositoryService.php',
            'App\\Repository\\MaterialOfShadeRepository' => 'getMaterialOfShadeRepositoryService.php',
            'App\\Repository\\NumberOfLampsRepository' => 'getNumberOfLampsRepositoryService.php',
            'App\\Repository\\PlugRepository' => 'getPlugRepositoryService.php',
            'App\\Repository\\PowerOfFittingRepository' => 'getPowerOfFittingRepositoryService.php',
            'App\\Repository\\PowerOfLampRepository' => 'getPowerOfLampRepositoryService.php',
            'App\\Repository\\SocketRepository' => 'getSocketRepositoryService.php',
            'App\\Repository\\SupplierRepository' => 'getSupplierRepositoryService.php',
            'App\\Repository\\TypeOfBulbRepository' => 'getTypeOfBulbRepositoryService.php',
            'App\\Repository\\TypeOfLampRepository' => 'getTypeOfLampRepositoryService.php',
            'App\\Repository\\VoltageRepository' => 'getVoltageRepositoryService.php',
            'Sonata\\AdminBundle\\Command\\CreateClassCacheCommand' => 'getCreateClassCacheCommandService.php',
            'Sonata\\AdminBundle\\Command\\ExplainAdminCommand' => 'getExplainAdminCommandService.php',
            'Sonata\\AdminBundle\\Command\\GenerateAdminCommand' => 'getGenerateAdminCommandService.php',
            'Sonata\\AdminBundle\\Command\\GenerateObjectAclCommand' => 'getGenerateObjectAclCommandService.php',
            'Sonata\\AdminBundle\\Command\\ListAdminCommand' => 'getListAdminCommandService.php',
            'Sonata\\AdminBundle\\Command\\SetupAclCommand' => 'getSetupAclCommandService.php',
            'Sonata\\BlockBundle\\Command\\DebugBlocksCommand' => 'getDebugBlocksCommandService.php',
            'Sonata\\CoreBundle\\Command\\SonataDumpDoctrineMetaCommand' => 'getSonataDumpDoctrineMetaCommandService.php',
            'Sonata\\CoreBundle\\Command\\SonataListFormMappingCommand' => 'getSonataListFormMappingCommandService.php',
            'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController' => 'getRedirectControllerService.php',
            'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController' => 'getTemplateControllerService.php',
            'admin.adjustingAngle.template_registry' => 'getAdmin_AdjustingAngle_TemplateRegistryService.php',
            'admin.beamAngle.template_registry' => 'getAdmin_BeamAngle_TemplateRegistryService.php',
            'admin.colourOfHousing.template_registry' => 'getAdmin_ColourOfHousing_TemplateRegistryService.php',
            'admin.colourOfShade.template_registry' => 'getAdmin_ColourOfShade_TemplateRegistryService.php',
            'admin.cover.template_registry' => 'getAdmin_Cover_TemplateRegistryService.php',
            'admin.cri.template_registry' => 'getAdmin_Cri_TemplateRegistryService.php',
            'admin.datasheet.template_registry' => 'getAdmin_Datasheet_TemplateRegistryService.php',
            'admin.dimmerType.template_registry' => 'getAdmin_DimmerType_TemplateRegistryService.php',
            'admin.fixtureLightOutput.template_registry' => 'getAdmin_FixtureLightOutput_TemplateRegistryService.php',
            'admin.frequency.template_registry' => 'getAdmin_Frequency_TemplateRegistryService.php',
            'admin.ipClass.template_registry' => 'getAdmin_IpClass_TemplateRegistryService.php',
            'admin.lightColour.template_registry' => 'getAdmin_LightColour_TemplateRegistryService.php',
            'admin.lightPoints.template_registry' => 'getAdmin_LightPoints_TemplateRegistryService.php',
            'admin.materialOfHousing.template_registry' => 'getAdmin_MaterialOfHousing_TemplateRegistryService.php',
            'admin.materialOfShade.template_registry' => 'getAdmin_MaterialOfShade_TemplateRegistryService.php',
            'admin.numberOfLamps.template_registry' => 'getAdmin_NumberOfLamps_TemplateRegistryService.php',
            'admin.plug.template_registry' => 'getAdmin_Plug_TemplateRegistryService.php',
            'admin.powerOfFitting.template_registry' => 'getAdmin_PowerOfFitting_TemplateRegistryService.php',
            'admin.powerOfLamp.template_registry' => 'getAdmin_PowerOfLamp_TemplateRegistryService.php',
            'admin.socket.template_registry' => 'getAdmin_Socket_TemplateRegistryService.php',
            'admin.supplier.template_registry' => 'getAdmin_Supplier_TemplateRegistryService.php',
            'admin.typeOfBulb.template_registry' => 'getAdmin_TypeOfBulb_TemplateRegistryService.php',
            'admin.typeOfLamp.template_registry' => 'getAdmin_TypeOfLamp_TemplateRegistryService.php',
            'admin.voltage.template_registry' => 'getAdmin_Voltage_TemplateRegistryService.php',
            'annotation_reader' => 'getAnnotationReaderService.php',
            'annotations.cache' => 'getAnnotations_CacheService.php',
            'annotations.cache_warmer' => 'getAnnotations_CacheWarmerService.php',
            'annotations.reader' => 'getAnnotations_ReaderService.php',
            'argument_resolver.default' => 'getArgumentResolver_DefaultService.php',
            'argument_resolver.request' => 'getArgumentResolver_RequestService.php',
            'argument_resolver.request_attribute' => 'getArgumentResolver_RequestAttributeService.php',
            'argument_resolver.service' => 'getArgumentResolver_ServiceService.php',
            'argument_resolver.session' => 'getArgumentResolver_SessionService.php',
            'argument_resolver.variadic' => 'getArgumentResolver_VariadicService.php',
            'assets.context' => 'getAssets_ContextService.php',
            'assets.packages' => 'getAssets_PackagesService.php',
            'cache.annotations' => 'getCache_AnnotationsService.php',
            'cache.app' => 'getCache_AppService.php',
            'cache.default_clearer' => 'getCache_DefaultClearerService.php',
            'cache.global_clearer' => 'getCache_GlobalClearerService.php',
            'cache.property_access' => 'getCache_PropertyAccessService.php',
            'cache.system' => 'getCache_SystemService.php',
            'cache.system_clearer' => 'getCache_SystemClearerService.php',
            'cache.validator' => 'getCache_ValidatorService.php',
            'cache_clearer' => 'getCacheClearerService.php',
            'cache_warmer' => 'getCacheWarmerService.php',
            'console.command.about' => 'getConsole_Command_AboutService.php',
            'console.command.assets_install' => 'getConsole_Command_AssetsInstallService.php',
            'console.command.cache_clear' => 'getConsole_Command_CacheClearService.php',
            'console.command.cache_pool_clear' => 'getConsole_Command_CachePoolClearService.php',
            'console.command.cache_pool_prune' => 'getConsole_Command_CachePoolPruneService.php',
            'console.command.cache_warmup' => 'getConsole_Command_CacheWarmupService.php',
            'console.command.config_debug' => 'getConsole_Command_ConfigDebugService.php',
            'console.command.config_dump_reference' => 'getConsole_Command_ConfigDumpReferenceService.php',
            'console.command.container_debug' => 'getConsole_Command_ContainerDebugService.php',
            'console.command.debug_autowiring' => 'getConsole_Command_DebugAutowiringService.php',
            'console.command.event_dispatcher_debug' => 'getConsole_Command_EventDispatcherDebugService.php',
            'console.command.form_debug' => 'getConsole_Command_FormDebugService.php',
            'console.command.router_debug' => 'getConsole_Command_RouterDebugService.php',
            'console.command.router_match' => 'getConsole_Command_RouterMatchService.php',
            'console.command.translation_debug' => 'getConsole_Command_TranslationDebugService.php',
            'console.command.translation_update' => 'getConsole_Command_TranslationUpdateService.php',
            'console.command.xliff_lint' => 'getConsole_Command_XliffLintService.php',
            'console.command.yaml_lint' => 'getConsole_Command_YamlLintService.php',
            'console.command_loader' => 'getConsole_CommandLoaderService.php',
            'console.error_listener' => 'getConsole_ErrorListenerService.php',
            'debug.stopwatch' => 'getDebug_StopwatchService.php',
            'deprecated.form.registry' => 'getDeprecated_Form_RegistryService.php',
            'deprecated.form.registry.csrf' => 'getDeprecated_Form_Registry_CsrfService.php',
            'doctrine' => 'getDoctrineService.php',
            'doctrine.cache_clear_metadata_command' => 'getDoctrine_CacheClearMetadataCommandService.php',
            'doctrine.cache_clear_query_cache_command' => 'getDoctrine_CacheClearQueryCacheCommandService.php',
            'doctrine.cache_clear_result_command' => 'getDoctrine_CacheClearResultCommandService.php',
            'doctrine.cache_collection_region_command' => 'getDoctrine_CacheCollectionRegionCommandService.php',
            'doctrine.clear_entity_region_command' => 'getDoctrine_ClearEntityRegionCommandService.php',
            'doctrine.clear_query_region_command' => 'getDoctrine_ClearQueryRegionCommandService.php',
            'doctrine.database_create_command' => 'getDoctrine_DatabaseCreateCommandService.php',
            'doctrine.database_drop_command' => 'getDoctrine_DatabaseDropCommandService.php',
            'doctrine.database_import_command' => 'getDoctrine_DatabaseImportCommandService.php',
            'doctrine.dbal.connection_factory' => 'getDoctrine_Dbal_ConnectionFactoryService.php',
            'doctrine.dbal.default_connection' => 'getDoctrine_Dbal_DefaultConnectionService.php',
            'doctrine.ensure_production_settings_command' => 'getDoctrine_EnsureProductionSettingsCommandService.php',
            'doctrine.generate_entities_command' => 'getDoctrine_GenerateEntitiesCommandService.php',
            'doctrine.mapping_convert_command' => 'getDoctrine_MappingConvertCommandService.php',
            'doctrine.mapping_import_command' => 'getDoctrine_MappingImportCommandService.php',
            'doctrine.mapping_info_command' => 'getDoctrine_MappingInfoCommandService.php',
            'doctrine.orm.default_entity_listener_resolver' => 'getDoctrine_Orm_DefaultEntityListenerResolverService.php',
            'doctrine.orm.default_entity_manager' => 'getDoctrine_Orm_DefaultEntityManagerService.php',
            'doctrine.orm.default_listeners.attach_entity_listeners' => 'getDoctrine_Orm_DefaultListeners_AttachEntityListenersService.php',
            'doctrine.orm.default_manager_configurator' => 'getDoctrine_Orm_DefaultManagerConfiguratorService.php',
            'doctrine.orm.proxy_cache_warmer' => 'getDoctrine_Orm_ProxyCacheWarmerService.php',
            'doctrine.orm.validator.unique' => 'getDoctrine_Orm_Validator_UniqueService.php',
            'doctrine.orm.validator_initializer' => 'getDoctrine_Orm_ValidatorInitializerService.php',
            'doctrine.query_dql_command' => 'getDoctrine_QueryDqlCommandService.php',
            'doctrine.query_sql_command' => 'getDoctrine_QuerySqlCommandService.php',
            'doctrine.result_cache_pool' => 'getDoctrine_ResultCachePoolService.php',
            'doctrine.schema_create_command' => 'getDoctrine_SchemaCreateCommandService.php',
            'doctrine.schema_drop_command' => 'getDoctrine_SchemaDropCommandService.php',
            'doctrine.schema_update_command' => 'getDoctrine_SchemaUpdateCommandService.php',
            'doctrine.schema_validate_command' => 'getDoctrine_SchemaValidateCommandService.php',
            'doctrine.system_cache_pool' => 'getDoctrine_SystemCachePoolService.php',
            'doctrine_cache.contains_command' => 'getDoctrineCache_ContainsCommandService.php',
            'doctrine_cache.delete_command' => 'getDoctrineCache_DeleteCommandService.php',
            'doctrine_cache.flush_command' => 'getDoctrineCache_FlushCommandService.php',
            'doctrine_cache.stats_command' => 'getDoctrineCache_StatsCommandService.php',
            'doctrine_migrations.diff_command' => 'getDoctrineMigrations_DiffCommandService.php',
            'doctrine_migrations.dump_schema_command' => 'getDoctrineMigrations_DumpSchemaCommandService.php',
            'doctrine_migrations.execute_command' => 'getDoctrineMigrations_ExecuteCommandService.php',
            'doctrine_migrations.generate_command' => 'getDoctrineMigrations_GenerateCommandService.php',
            'doctrine_migrations.latest_command' => 'getDoctrineMigrations_LatestCommandService.php',
            'doctrine_migrations.migrate_command' => 'getDoctrineMigrations_MigrateCommandService.php',
            'doctrine_migrations.rollup_command' => 'getDoctrineMigrations_RollupCommandService.php',
            'doctrine_migrations.status_command' => 'getDoctrineMigrations_StatusCommandService.php',
            'doctrine_migrations.up_to_date_command' => 'getDoctrineMigrations_UpToDateCommandService.php',
            'doctrine_migrations.version_command' => 'getDoctrineMigrations_VersionCommandService.php',
            'file_locator' => 'getFileLocatorService.php',
            'filesystem' => 'getFilesystemService.php',
            'form.factory' => 'getForm_FactoryService.php',
            'form.registry' => 'getForm_RegistryService.php',
            'form.resolved_type_factory' => 'getForm_ResolvedTypeFactoryService.php',
            'form.server_params' => 'getForm_ServerParamsService.php',
            'form.type.birthday' => 'getForm_Type_BirthdayService.php',
            'form.type.button' => 'getForm_Type_ButtonService.php',
            'form.type.checkbox' => 'getForm_Type_CheckboxService.php',
            'form.type.choice' => 'getForm_Type_ChoiceService.php',
            'form.type.collection' => 'getForm_Type_CollectionService.php',
            'form.type.country' => 'getForm_Type_CountryService.php',
            'form.type.currency' => 'getForm_Type_CurrencyService.php',
            'form.type.date' => 'getForm_Type_DateService.php',
            'form.type.datetime' => 'getForm_Type_DatetimeService.php',
            'form.type.email' => 'getForm_Type_EmailService.php',
            'form.type.entity' => 'getForm_Type_EntityService.php',
            'form.type.file' => 'getForm_Type_FileService.php',
            'form.type.form' => 'getForm_Type_FormService.php',
            'form.type.hidden' => 'getForm_Type_HiddenService.php',
            'form.type.integer' => 'getForm_Type_IntegerService.php',
            'form.type.language' => 'getForm_Type_LanguageService.php',
            'form.type.locale' => 'getForm_Type_LocaleService.php',
            'form.type.money' => 'getForm_Type_MoneyService.php',
            'form.type.number' => 'getForm_Type_NumberService.php',
            'form.type.password' => 'getForm_Type_PasswordService.php',
            'form.type.percent' => 'getForm_Type_PercentService.php',
            'form.type.radio' => 'getForm_Type_RadioService.php',
            'form.type.range' => 'getForm_Type_RangeService.php',
            'form.type.repeated' => 'getForm_Type_RepeatedService.php',
            'form.type.reset' => 'getForm_Type_ResetService.php',
            'form.type.search' => 'getForm_Type_SearchService.php',
            'form.type.submit' => 'getForm_Type_SubmitService.php',
            'form.type.text' => 'getForm_Type_TextService.php',
            'form.type.textarea' => 'getForm_Type_TextareaService.php',
            'form.type.time' => 'getForm_Type_TimeService.php',
            'form.type.timezone' => 'getForm_Type_TimezoneService.php',
            'form.type.url' => 'getForm_Type_UrlService.php',
            'form.type_extension.csrf' => 'getForm_TypeExtension_CsrfService.php',
            'form.type_extension.form.http_foundation' => 'getForm_TypeExtension_Form_HttpFoundationService.php',
            'form.type_extension.form.transformation_failure_handling' => 'getForm_TypeExtension_Form_TransformationFailureHandlingService.php',
            'form.type_extension.form.validator' => 'getForm_TypeExtension_Form_ValidatorService.php',
            'form.type_extension.repeated.validator' => 'getForm_TypeExtension_Repeated_ValidatorService.php',
            'form.type_extension.submit.validator' => 'getForm_TypeExtension_Submit_ValidatorService.php',
            'form.type_extension.upload.validator' => 'getForm_TypeExtension_Upload_ValidatorService.php',
            'form.type_guesser.doctrine' => 'getForm_TypeGuesser_DoctrineService.php',
            'form.type_guesser.validator' => 'getForm_TypeGuesser_ValidatorService.php',
            'fragment.handler' => 'getFragment_HandlerService.php',
            'fragment.renderer.inline' => 'getFragment_Renderer_InlineService.php',
            'kernel.class_cache.cache_warmer' => 'getKernel_ClassCache_CacheWarmerService.php',
            'knp_menu.factory' => 'getKnpMenu_FactoryService.php',
            'knp_menu.matcher' => 'getKnpMenu_MatcherService.php',
            'knp_menu.menu_provider' => 'getKnpMenu_MenuProviderService.php',
            'knp_menu.menu_provider.builder_alias' => 'getKnpMenu_MenuProvider_BuilderAliasService.php',
            'knp_menu.menu_provider.lazy' => 'getKnpMenu_MenuProvider_LazyService.php',
            'knp_menu.renderer.list' => 'getKnpMenu_Renderer_ListService.php',
            'knp_menu.renderer.twig' => 'getKnpMenu_Renderer_TwigService.php',
            'knp_menu.renderer_provider' => 'getKnpMenu_RendererProviderService.php',
            'knp_menu.voter.router' => 'getKnpMenu_Voter_RouterService.php',
            'property_accessor' => 'getPropertyAccessorService.php',
            'router.cache_warmer' => 'getRouter_CacheWarmerService.php',
            'routing.loader' => 'getRouting_LoaderService.php',
            'security.access.authenticated_voter' => 'getSecurity_Access_AuthenticatedVoterService.php',
            'security.access.decision_manager' => 'getSecurity_Access_DecisionManagerService.php',
            'security.access.expression_voter' => 'getSecurity_Access_ExpressionVoterService.php',
            'security.access.simple_role_voter' => 'getSecurity_Access_SimpleRoleVoterService.php',
            'security.access_listener' => 'getSecurity_AccessListenerService.php',
            'security.access_map' => 'getSecurity_AccessMapService.php',
            'security.authentication.guard_handler' => 'getSecurity_Authentication_GuardHandlerService.php',
            'security.authentication.listener.anonymous.main' => 'getSecurity_Authentication_Listener_Anonymous_MainService.php',
            'security.authentication.manager' => 'getSecurity_Authentication_ManagerService.php',
            'security.authentication.provider.anonymous.main' => 'getSecurity_Authentication_Provider_Anonymous_MainService.php',
            'security.authentication.session_strategy.main' => 'getSecurity_Authentication_SessionStrategy_MainService.php',
            'security.authentication.trust_resolver' => 'getSecurity_Authentication_TrustResolverService.php',
            'security.authentication_utils' => 'getSecurity_AuthenticationUtilsService.php',
            'security.authorization_checker' => 'getSecurity_AuthorizationCheckerService.php',
            'security.channel_listener' => 'getSecurity_ChannelListenerService.php',
            'security.command.user_password_encoder' => 'getSecurity_Command_UserPasswordEncoderService.php',
            'security.context_listener.0' => 'getSecurity_ContextListener_0Service.php',
            'security.csrf.token_manager' => 'getSecurity_Csrf_TokenManagerService.php',
            'security.encoder_factory' => 'getSecurity_EncoderFactoryService.php',
            'security.firewall.map.context.dev' => 'getSecurity_Firewall_Map_Context_DevService.php',
            'security.firewall.map.context.main' => 'getSecurity_Firewall_Map_Context_MainService.php',
            'security.password_encoder' => 'getSecurity_PasswordEncoderService.php',
            'security.request_matcher.zfhj2lw' => 'getSecurity_RequestMatcher_Zfhj2lwService.php',
            'security.user.provider.concrete.in_memory' => 'getSecurity_User_Provider_Concrete_InMemoryService.php',
            'security.user_value_resolver' => 'getSecurity_UserValueResolverService.php',
            'security.validator.user_password' => 'getSecurity_Validator_UserPasswordService.php',
            'services_resetter' => 'getServicesResetterService.php',
            'session' => 'getSessionService.php',
            'session.storage.filesystem' => 'getSession_Storage_FilesystemService.php',
            'session.storage.metadata_bag' => 'getSession_Storage_MetadataBagService.php',
            'session.storage.native' => 'getSession_Storage_NativeService.php',
            'session.storage.php_bridge' => 'getSession_Storage_PhpBridgeService.php',
            'sonata.admin.audit.manager' => 'getSonata_Admin_Audit_ManagerService.php',
            'sonata.admin.block.admin_list' => 'getSonata_Admin_Block_AdminListService.php',
            'sonata.admin.block.search_result' => 'getSonata_Admin_Block_SearchResultService.php',
            'sonata.admin.block.stats' => 'getSonata_Admin_Block_StatsService.php',
            'sonata.admin.breadcrumbs_builder' => 'getSonata_Admin_BreadcrumbsBuilderService.php',
            'sonata.admin.builder.filter.factory' => 'getSonata_Admin_Builder_Filter_FactoryService.php',
            'sonata.admin.builder.orm_datagrid' => 'getSonata_Admin_Builder_OrmDatagridService.php',
            'sonata.admin.builder.orm_form' => 'getSonata_Admin_Builder_OrmFormService.php',
            'sonata.admin.builder.orm_list' => 'getSonata_Admin_Builder_OrmListService.php',
            'sonata.admin.builder.orm_show' => 'getSonata_Admin_Builder_OrmShowService.php',
            'sonata.admin.controller.admin' => 'getSonata_Admin_Controller_AdminService.php',
            'sonata.admin.doctrine_orm.form.type.choice_field_mask' => 'getSonata_Admin_DoctrineOrm_Form_Type_ChoiceFieldMaskService.php',
            'sonata.admin.event.extension' => 'getSonata_Admin_Event_ExtensionService.php',
            'sonata.admin.exporter' => 'getSonata_Admin_ExporterService.php',
            'sonata.admin.filter_persister.session' => 'getSonata_Admin_FilterPersister_SessionService.php',
            'sonata.admin.form.extension.choice' => 'getSonata_Admin_Form_Extension_ChoiceService.php',
            'sonata.admin.form.extension.field' => 'getSonata_Admin_Form_Extension_FieldService.php',
            'sonata.admin.form.extension.field.mopa' => 'getSonata_Admin_Form_Extension_Field_MopaService.php',
            'sonata.admin.form.filter.type.choice' => 'getSonata_Admin_Form_Filter_Type_ChoiceService.php',
            'sonata.admin.form.filter.type.date' => 'getSonata_Admin_Form_Filter_Type_DateService.php',
            'sonata.admin.form.filter.type.daterange' => 'getSonata_Admin_Form_Filter_Type_DaterangeService.php',
            'sonata.admin.form.filter.type.datetime' => 'getSonata_Admin_Form_Filter_Type_DatetimeService.php',
            'sonata.admin.form.filter.type.datetime_range' => 'getSonata_Admin_Form_Filter_Type_DatetimeRangeService.php',
            'sonata.admin.form.filter.type.default' => 'getSonata_Admin_Form_Filter_Type_DefaultService.php',
            'sonata.admin.form.filter.type.number' => 'getSonata_Admin_Form_Filter_Type_NumberService.php',
            'sonata.admin.form.type.admin' => 'getSonata_Admin_Form_Type_AdminService.php',
            'sonata.admin.form.type.collection' => 'getSonata_Admin_Form_Type_CollectionService.php',
            'sonata.admin.form.type.model_autocomplete' => 'getSonata_Admin_Form_Type_ModelAutocompleteService.php',
            'sonata.admin.form.type.model_choice' => 'getSonata_Admin_Form_Type_ModelChoiceService.php',
            'sonata.admin.form.type.model_hidden' => 'getSonata_Admin_Form_Type_ModelHiddenService.php',
            'sonata.admin.form.type.model_list' => 'getSonata_Admin_Form_Type_ModelListService.php',
            'sonata.admin.form.type.model_reference' => 'getSonata_Admin_Form_Type_ModelReferenceService.php',
            'sonata.admin.global_template_registry' => 'getSonata_Admin_GlobalTemplateRegistryService.php',
            'sonata.admin.guesser.orm_datagrid' => 'getSonata_Admin_Guesser_OrmDatagridService.php',
            'sonata.admin.guesser.orm_datagrid_chain' => 'getSonata_Admin_Guesser_OrmDatagridChainService.php',
            'sonata.admin.guesser.orm_list' => 'getSonata_Admin_Guesser_OrmListService.php',
            'sonata.admin.guesser.orm_list_chain' => 'getSonata_Admin_Guesser_OrmListChainService.php',
            'sonata.admin.guesser.orm_show' => 'getSonata_Admin_Guesser_OrmShowService.php',
            'sonata.admin.guesser.orm_show_chain' => 'getSonata_Admin_Guesser_OrmShowChainService.php',
            'sonata.admin.helper' => 'getSonata_Admin_HelperService.php',
            'sonata.admin.label.strategy.bc' => 'getSonata_Admin_Label_Strategy_BcService.php',
            'sonata.admin.label.strategy.form_component' => 'getSonata_Admin_Label_Strategy_FormComponentService.php',
            'sonata.admin.label.strategy.native' => 'getSonata_Admin_Label_Strategy_NativeService.php',
            'sonata.admin.label.strategy.noop' => 'getSonata_Admin_Label_Strategy_NoopService.php',
            'sonata.admin.label.strategy.underscore' => 'getSonata_Admin_Label_Strategy_UnderscoreService.php',
            'sonata.admin.manager.orm' => 'getSonata_Admin_Manager_OrmService.php',
            'sonata.admin.manipulator.acl.admin' => 'getSonata_Admin_Manipulator_Acl_AdminService.php',
            'sonata.admin.manipulator.acl.object.orm' => 'getSonata_Admin_Manipulator_Acl_Object_OrmService.php',
            'sonata.admin.menu.group_provider' => 'getSonata_Admin_Menu_GroupProviderService.php',
            'sonata.admin.menu.matcher.voter.active' => 'getSonata_Admin_Menu_Matcher_Voter_ActiveService.php',
            'sonata.admin.menu.matcher.voter.admin' => 'getSonata_Admin_Menu_Matcher_Voter_AdminService.php',
            'sonata.admin.menu.matcher.voter.children' => 'getSonata_Admin_Menu_Matcher_Voter_ChildrenService.php',
            'sonata.admin.menu_builder' => 'getSonata_Admin_MenuBuilderService.php',
            'sonata.admin.object.manipulator.acl.admin' => 'getSonata_Admin_Object_Manipulator_Acl_AdminService.php',
            'sonata.admin.pool' => 'getSonata_Admin_PoolService.php',
            'sonata.admin.route.cache' => 'getSonata_Admin_Route_CacheService.php',
            'sonata.admin.route.cache_warmup' => 'getSonata_Admin_Route_CacheWarmupService.php',
            'sonata.admin.route.default_generator' => 'getSonata_Admin_Route_DefaultGeneratorService.php',
            'sonata.admin.route.path_info' => 'getSonata_Admin_Route_PathInfoService.php',
            'sonata.admin.route.query_string' => 'getSonata_Admin_Route_QueryStringService.php',
            'sonata.admin.route_loader' => 'getSonata_Admin_RouteLoaderService.php',
            'sonata.admin.search.handler' => 'getSonata_Admin_Search_HandlerService.php',
            'sonata.admin.security.handler' => 'getSonata_Admin_Security_HandlerService.php',
            'sonata.admin.sidebar_menu' => 'getSonata_Admin_SidebarMenuService.php',
            'sonata.admin.twig.extension' => 'getSonata_Admin_Twig_ExtensionService.php',
            'sonata.admin.twig.global' => 'getSonata_Admin_Twig_GlobalService.php',
            'sonata.admin.validator.inline' => 'getSonata_Admin_Validator_InlineService.php',
            'sonata.block.cache.handler.noop' => 'getSonata_Block_Cache_Handler_NoopService.php',
            'sonata.block.context_manager.default' => 'getSonata_Block_ContextManager_DefaultService.php',
            'sonata.block.exception.filter.debug_only' => 'getSonata_Block_Exception_Filter_DebugOnlyService.php',
            'sonata.block.exception.filter.ignore_block_exception' => 'getSonata_Block_Exception_Filter_IgnoreBlockExceptionService.php',
            'sonata.block.exception.filter.keep_all' => 'getSonata_Block_Exception_Filter_KeepAllService.php',
            'sonata.block.exception.filter.keep_none' => 'getSonata_Block_Exception_Filter_KeepNoneService.php',
            'sonata.block.exception.renderer.inline' => 'getSonata_Block_Exception_Renderer_InlineService.php',
            'sonata.block.exception.renderer.inline_debug' => 'getSonata_Block_Exception_Renderer_InlineDebugService.php',
            'sonata.block.exception.renderer.throw' => 'getSonata_Block_Exception_Renderer_ThrowService.php',
            'sonata.block.exception.strategy.manager' => 'getSonata_Block_Exception_Strategy_ManagerService.php',
            'sonata.block.form.type.block' => 'getSonata_Block_Form_Type_BlockService.php',
            'sonata.block.form.type.container_template' => 'getSonata_Block_Form_Type_ContainerTemplateService.php',
            'sonata.block.loader.chain' => 'getSonata_Block_Loader_ChainService.php',
            'sonata.block.loader.service' => 'getSonata_Block_Loader_ServiceService.php',
            'sonata.block.manager' => 'getSonata_Block_ManagerService.php',
            'sonata.block.menu.registry' => 'getSonata_Block_Menu_RegistryService.php',
            'sonata.block.renderer.default' => 'getSonata_Block_Renderer_DefaultService.php',
            'sonata.block.service.container' => 'getSonata_Block_Service_ContainerService.php',
            'sonata.block.service.empty' => 'getSonata_Block_Service_EmptyService.php',
            'sonata.block.service.menu' => 'getSonata_Block_Service_MenuService.php',
            'sonata.block.service.rss' => 'getSonata_Block_Service_RssService.php',
            'sonata.block.service.template' => 'getSonata_Block_Service_TemplateService.php',
            'sonata.block.service.text' => 'getSonata_Block_Service_TextService.php',
            'sonata.block.templating.helper' => 'getSonata_Block_Templating_HelperService.php',
            'sonata.block.twig.global' => 'getSonata_Block_Twig_GlobalService.php',
            'sonata.core.date.moment_format_converter' => 'getSonata_Core_Date_MomentFormatConverterService.php',
            'sonata.core.flashmessage.manager' => 'getSonata_Core_Flashmessage_ManagerService.php',
            'sonata.core.flashmessage.twig.extension' => 'getSonata_Core_Flashmessage_Twig_ExtensionService.php',
            'sonata.core.flashmessage.twig.runtime' => 'getSonata_Core_Flashmessage_Twig_RuntimeService.php',
            'sonata.core.form.type.array' => 'getSonata_Core_Form_Type_ArrayService.php',
            'sonata.core.form.type.array_legacy' => 'getSonata_Core_Form_Type_ArrayLegacyService.php',
            'sonata.core.form.type.boolean' => 'getSonata_Core_Form_Type_BooleanService.php',
            'sonata.core.form.type.boolean_legacy' => 'getSonata_Core_Form_Type_BooleanLegacyService.php',
            'sonata.core.form.type.collection' => 'getSonata_Core_Form_Type_CollectionService.php',
            'sonata.core.form.type.collection_legacy' => 'getSonata_Core_Form_Type_CollectionLegacyService.php',
            'sonata.core.form.type.color_legacy' => 'getSonata_Core_Form_Type_ColorLegacyService.php',
            'sonata.core.form.type.color_selector' => 'getSonata_Core_Form_Type_ColorSelectorService.php',
            'sonata.core.form.type.date_picker' => 'getSonata_Core_Form_Type_DatePickerService.php',
            'sonata.core.form.type.date_picker_legacy' => 'getSonata_Core_Form_Type_DatePickerLegacyService.php',
            'sonata.core.form.type.date_range' => 'getSonata_Core_Form_Type_DateRangeService.php',
            'sonata.core.form.type.date_range_legacy' => 'getSonata_Core_Form_Type_DateRangeLegacyService.php',
            'sonata.core.form.type.date_range_picker' => 'getSonata_Core_Form_Type_DateRangePickerService.php',
            'sonata.core.form.type.date_range_picker_legacy' => 'getSonata_Core_Form_Type_DateRangePickerLegacyService.php',
            'sonata.core.form.type.datetime_picker' => 'getSonata_Core_Form_Type_DatetimePickerService.php',
            'sonata.core.form.type.datetime_picker_legacy' => 'getSonata_Core_Form_Type_DatetimePickerLegacyService.php',
            'sonata.core.form.type.datetime_range' => 'getSonata_Core_Form_Type_DatetimeRangeService.php',
            'sonata.core.form.type.datetime_range_legacy' => 'getSonata_Core_Form_Type_DatetimeRangeLegacyService.php',
            'sonata.core.form.type.datetime_range_picker' => 'getSonata_Core_Form_Type_DatetimeRangePickerService.php',
            'sonata.core.form.type.datetime_range_picker_legacy' => 'getSonata_Core_Form_Type_DatetimeRangePickerLegacyService.php',
            'sonata.core.form.type.equal' => 'getSonata_Core_Form_Type_EqualService.php',
            'sonata.core.form.type.equal_legacy' => 'getSonata_Core_Form_Type_EqualLegacyService.php',
            'sonata.core.form.type.translatable_choice' => 'getSonata_Core_Form_Type_TranslatableChoiceService.php',
            'sonata.core.model.adapter.chain' => 'getSonata_Core_Model_Adapter_ChainService.php',
            'sonata.core.model.adapter.doctrine_orm' => 'getSonata_Core_Model_Adapter_DoctrineOrmService.php',
            'sonata.core.slugify.cocur' => 'getSonata_Core_Slugify_CocurService.php',
            'sonata.core.slugify.native' => 'getSonata_Core_Slugify_NativeService.php',
            'sonata.core.twig.deprecated_template_extension' => 'getSonata_Core_Twig_DeprecatedTemplateExtensionService.php',
            'sonata.core.twig.extension.text' => 'getSonata_Core_Twig_Extension_TextService.php',
            'sonata.core.twig.extension.wrapping' => 'getSonata_Core_Twig_Extension_WrappingService.php',
            'sonata.core.twig.status_extension' => 'getSonata_Core_Twig_StatusExtensionService.php',
            'sonata.core.twig.status_runtime' => 'getSonata_Core_Twig_StatusRuntimeService.php',
            'sonata.core.twig.template_extension' => 'getSonata_Core_Twig_TemplateExtensionService.php',
            'sonata.core.validator.inline' => 'getSonata_Core_Validator_InlineService.php',
            'sonata.templating' => 'getSonata_TemplatingService.php',
            'sonata.templating.locator' => 'getSonata_Templating_LocatorService.php',
            'sonata.templating.name_parser' => 'getSonata_Templating_NameParserService.php',
            'templating.helper.logout_url' => 'getTemplating_Helper_LogoutUrlService.php',
            'templating.helper.security' => 'getTemplating_Helper_SecurityService.php',
            'translation.dumper.csv' => 'getTranslation_Dumper_CsvService.php',
            'translation.dumper.ini' => 'getTranslation_Dumper_IniService.php',
            'translation.dumper.json' => 'getTranslation_Dumper_JsonService.php',
            'translation.dumper.mo' => 'getTranslation_Dumper_MoService.php',
            'translation.dumper.php' => 'getTranslation_Dumper_PhpService.php',
            'translation.dumper.po' => 'getTranslation_Dumper_PoService.php',
            'translation.dumper.qt' => 'getTranslation_Dumper_QtService.php',
            'translation.dumper.res' => 'getTranslation_Dumper_ResService.php',
            'translation.dumper.xliff' => 'getTranslation_Dumper_XliffService.php',
            'translation.dumper.yml' => 'getTranslation_Dumper_YmlService.php',
            'translation.extractor' => 'getTranslation_ExtractorService.php',
            'translation.extractor.php' => 'getTranslation_Extractor_PhpService.php',
            'translation.loader' => 'getTranslation_LoaderService.php',
            'translation.loader.csv' => 'getTranslation_Loader_CsvService.php',
            'translation.loader.dat' => 'getTranslation_Loader_DatService.php',
            'translation.loader.ini' => 'getTranslation_Loader_IniService.php',
            'translation.loader.json' => 'getTranslation_Loader_JsonService.php',
            'translation.loader.mo' => 'getTranslation_Loader_MoService.php',
            'translation.loader.php' => 'getTranslation_Loader_PhpService.php',
            'translation.loader.po' => 'getTranslation_Loader_PoService.php',
            'translation.loader.qt' => 'getTranslation_Loader_QtService.php',
            'translation.loader.res' => 'getTranslation_Loader_ResService.php',
            'translation.loader.xliff' => 'getTranslation_Loader_XliffService.php',
            'translation.loader.yml' => 'getTranslation_Loader_YmlService.php',
            'translation.reader' => 'getTranslation_ReaderService.php',
            'translation.warmer' => 'getTranslation_WarmerService.php',
            'translation.writer' => 'getTranslation_WriterService.php',
            'twig' => 'getTwigService.php',
            'twig.cache_warmer' => 'getTwig_CacheWarmerService.php',
            'twig.command.debug' => 'getTwig_Command_DebugService.php',
            'twig.command.lint' => 'getTwig_Command_LintService.php',
            'twig.controller.exception' => 'getTwig_Controller_ExceptionService.php',
            'twig.controller.preview_error' => 'getTwig_Controller_PreviewErrorService.php',
            'twig.exception_listener' => 'getTwig_ExceptionListenerService.php',
            'twig.form.renderer' => 'getTwig_Form_RendererService.php',
            'twig.loader' => 'getTwig_LoaderService.php',
            'twig.profile' => 'getTwig_ProfileService.php',
            'twig.runtime.httpkernel' => 'getTwig_Runtime_HttpkernelService.php',
            'twig.template_cache_warmer' => 'getTwig_TemplateCacheWarmerService.php',
            'twig.translation.extractor' => 'getTwig_Translation_ExtractorService.php',
            'uri_signer' => 'getUriSignerService.php',
            'validator' => 'getValidatorService.php',
            'validator.builder' => 'getValidator_BuilderService.php',
            'validator.email' => 'getValidator_EmailService.php',
            'validator.expression' => 'getValidator_ExpressionService.php',
            'validator.mapping.cache_warmer' => 'getValidator_Mapping_CacheWarmerService.php',
            'validator.validator_factory' => 'getValidator_ValidatorFactoryService.php',
        ];
        $this->privates = [
            'Sonata\\AdminBundle\\Admin\\Pool' => true,
            'session.storage' => true,
            'sonata.block.cache.handler' => true,
            'sonata.block.context_manager' => true,
            'sonata.block.renderer' => true,
            'sonata.doctrine.model.adapter.chain' => true,
            'App\\Repository\\AdjustingAngleRepository' => true,
            'App\\Repository\\BeamAngleRepository' => true,
            'App\\Repository\\BuildGroupRepository' => true,
            'App\\Repository\\ColourOfHousingRepository' => true,
            'App\\Repository\\ColourOfShadeRepository' => true,
            'App\\Repository\\ColourTemperatureRepository' => true,
            'App\\Repository\\CoverRepository' => true,
            'App\\Repository\\CriRepository' => true,
            'App\\Repository\\DataSheetRepository' => true,
            'App\\Repository\\DimmerTypeRepository' => true,
            'App\\Repository\\FixtureLightOutputRepository' => true,
            'App\\Repository\\FrequencyRepository' => true,
            'App\\Repository\\IPClassRepository' => true,
            'App\\Repository\\LightColourRepository' => true,
            'App\\Repository\\LightPointsRepository' => true,
            'App\\Repository\\MaterialOfHousingRepository' => true,
            'App\\Repository\\MaterialOfShadeRepository' => true,
            'App\\Repository\\NumberOfLampsRepository' => true,
            'App\\Repository\\PlugRepository' => true,
            'App\\Repository\\PowerOfFittingRepository' => true,
            'App\\Repository\\PowerOfLampRepository' => true,
            'App\\Repository\\SocketRepository' => true,
            'App\\Repository\\SupplierRepository' => true,
            'App\\Repository\\TypeOfBulbRepository' => true,
            'App\\Repository\\TypeOfLampRepository' => true,
            'App\\Repository\\VoltageRepository' => true,
            'Sonata\\BlockBundle\\Command\\DebugBlocksCommand' => true,
            'Sonata\\CoreBundle\\Command\\SonataDumpDoctrineMetaCommand' => true,
            'Sonata\\CoreBundle\\Command\\SonataListFormMappingCommand' => true,
            'annotation_reader' => true,
            'annotations.cache' => true,
            'annotations.cache_warmer' => true,
            'annotations.reader' => true,
            'argument_resolver.default' => true,
            'argument_resolver.request' => true,
            'argument_resolver.request_attribute' => true,
            'argument_resolver.service' => true,
            'argument_resolver.session' => true,
            'argument_resolver.variadic' => true,
            'assets.context' => true,
            'assets.packages' => true,
            'cache.annotations' => true,
            'cache.default_clearer' => true,
            'cache.property_access' => true,
            'cache.validator' => true,
            'config_cache_factory' => true,
            'console.command.about' => true,
            'console.command.assets_install' => true,
            'console.command.cache_clear' => true,
            'console.command.cache_pool_clear' => true,
            'console.command.cache_pool_prune' => true,
            'console.command.cache_warmup' => true,
            'console.command.config_debug' => true,
            'console.command.config_dump_reference' => true,
            'console.command.container_debug' => true,
            'console.command.debug_autowiring' => true,
            'console.command.event_dispatcher_debug' => true,
            'console.command.form_debug' => true,
            'console.command.router_debug' => true,
            'console.command.router_match' => true,
            'console.command.translation_debug' => true,
            'console.command.translation_update' => true,
            'console.command.xliff_lint' => true,
            'console.command.yaml_lint' => true,
            'console.error_listener' => true,
            'controller_name_converter' => true,
            'debug.debug_handlers_listener' => true,
            'debug.file_link_formatter' => true,
            'debug.stopwatch' => true,
            'deprecated.form.registry' => true,
            'deprecated.form.registry.csrf' => true,
            'doctrine.cache_clear_metadata_command' => true,
            'doctrine.cache_clear_query_cache_command' => true,
            'doctrine.cache_clear_result_command' => true,
            'doctrine.cache_collection_region_command' => true,
            'doctrine.clear_entity_region_command' => true,
            'doctrine.clear_query_region_command' => true,
            'doctrine.database_create_command' => true,
            'doctrine.database_drop_command' => true,
            'doctrine.database_import_command' => true,
            'doctrine.dbal.connection_factory' => true,
            'doctrine.ensure_production_settings_command' => true,
            'doctrine.generate_entities_command' => true,
            'doctrine.mapping_convert_command' => true,
            'doctrine.mapping_import_command' => true,
            'doctrine.mapping_info_command' => true,
            'doctrine.orm.default_entity_listener_resolver' => true,
            'doctrine.orm.default_listeners.attach_entity_listeners' => true,
            'doctrine.orm.default_manager_configurator' => true,
            'doctrine.orm.proxy_cache_warmer' => true,
            'doctrine.orm.validator.unique' => true,
            'doctrine.orm.validator_initializer' => true,
            'doctrine.query_dql_command' => true,
            'doctrine.query_sql_command' => true,
            'doctrine.result_cache_pool' => true,
            'doctrine.schema_create_command' => true,
            'doctrine.schema_drop_command' => true,
            'doctrine.schema_update_command' => true,
            'doctrine.schema_validate_command' => true,
            'doctrine.system_cache_pool' => true,
            'doctrine_cache.contains_command' => true,
            'doctrine_cache.delete_command' => true,
            'doctrine_cache.flush_command' => true,
            'doctrine_cache.stats_command' => true,
            'doctrine_migrations.diff_command' => true,
            'doctrine_migrations.dump_schema_command' => true,
            'doctrine_migrations.execute_command' => true,
            'doctrine_migrations.generate_command' => true,
            'doctrine_migrations.latest_command' => true,
            'doctrine_migrations.migrate_command' => true,
            'doctrine_migrations.rollup_command' => true,
            'doctrine_migrations.status_command' => true,
            'doctrine_migrations.up_to_date_command' => true,
            'doctrine_migrations.version_command' => true,
            'file_locator' => true,
            'form.registry' => true,
            'form.resolved_type_factory' => true,
            'form.server_params' => true,
            'form.type.choice' => true,
            'form.type.entity' => true,
            'form.type.form' => true,
            'form.type_extension.csrf' => true,
            'form.type_extension.form.http_foundation' => true,
            'form.type_extension.form.transformation_failure_handling' => true,
            'form.type_extension.form.validator' => true,
            'form.type_extension.repeated.validator' => true,
            'form.type_extension.submit.validator' => true,
            'form.type_extension.upload.validator' => true,
            'form.type_guesser.doctrine' => true,
            'form.type_guesser.validator' => true,
            'fragment.handler' => true,
            'fragment.renderer.inline' => true,
            'kernel.class_cache.cache_warmer' => true,
            'knp_menu.menu_provider' => true,
            'knp_menu.menu_provider.builder_alias' => true,
            'knp_menu.menu_provider.lazy' => true,
            'knp_menu.renderer.list' => true,
            'knp_menu.renderer.twig' => true,
            'knp_menu.renderer_provider' => true,
            'knp_menu.voter.router' => true,
            'locale_listener' => true,
            'logger' => true,
            'property_accessor' => true,
            'resolve_controller_name_subscriber' => true,
            'response_listener' => true,
            'router.cache_warmer' => true,
            'router.request_context' => true,
            'router_listener' => true,
            'security.access.authenticated_voter' => true,
            'security.access.decision_manager' => true,
            'security.access.expression_voter' => true,
            'security.access.simple_role_voter' => true,
            'security.access_listener' => true,
            'security.access_map' => true,
            'security.authentication.guard_handler' => true,
            'security.authentication.listener.anonymous.main' => true,
            'security.authentication.manager' => true,
            'security.authentication.provider.anonymous.main' => true,
            'security.authentication.session_strategy.main' => true,
            'security.authentication.trust_resolver' => true,
            'security.channel_listener' => true,
            'security.command.user_password_encoder' => true,
            'security.context_listener.0' => true,
            'security.encoder_factory' => true,
            'security.firewall' => true,
            'security.firewall.map.context.dev' => true,
            'security.firewall.map.context.main' => true,
            'security.logout_url_generator' => true,
            'security.rememberme.response_listener' => true,
            'security.request_matcher.zfhj2lw' => true,
            'security.user.provider.concrete.in_memory' => true,
            'security.user_value_resolver' => true,
            'security.validator.user_password' => true,
            'session.save_listener' => true,
            'session.storage.filesystem' => true,
            'session.storage.metadata_bag' => true,
            'session.storage.native' => true,
            'session.storage.php_bridge' => true,
            'session_listener' => true,
            'sonata.admin.builder.orm_datagrid' => true,
            'sonata.admin.builder.orm_form' => true,
            'sonata.admin.builder.orm_list' => true,
            'sonata.admin.builder.orm_show' => true,
            'sonata.admin.filter_persister.session' => true,
            'sonata.admin.guesser.orm_datagrid' => true,
            'sonata.admin.guesser.orm_datagrid_chain' => true,
            'sonata.admin.guesser.orm_list' => true,
            'sonata.admin.guesser.orm_list_chain' => true,
            'sonata.admin.guesser.orm_show' => true,
            'sonata.admin.guesser.orm_show_chain' => true,
            'sonata.admin.menu.group_provider' => true,
            'sonata.admin.security.handler' => true,
            'sonata.block.cache.handler.default' => true,
            'sonata.block.cache.handler.noop' => true,
            'sonata.block.exception.strategy.manager' => true,
            'sonata.block.form.type.block' => true,
            'sonata.block.form.type.container_template' => true,
            'sonata.block.loader.chain' => true,
            'sonata.block.loader.service' => true,
            'sonata.block.templating.helper' => true,
            'sonata.block.twig.global' => true,
            'sonata.core.date.moment_format_converter' => true,
            'sonata.core.flashmessage.twig.runtime' => true,
            'sonata.core.model.adapter.doctrine_orm' => true,
            'sonata.core.twig.deprecated_template_extension' => true,
            'sonata.core.twig.extension.text' => true,
            'sonata.core.twig.extension.wrapping' => true,
            'sonata.core.twig.status_extension' => true,
            'sonata.core.twig.status_runtime' => true,
            'sonata.core.twig.template_extension' => true,
            'sonata.core.validator.inline' => true,
            'sonata.templating' => true,
            'sonata.templating.locator' => true,
            'sonata.templating.name_parser' => true,
            'streamed_response_listener' => true,
            'templating.helper.logout_url' => true,
            'templating.helper.security' => true,
            'translation.dumper.csv' => true,
            'translation.dumper.ini' => true,
            'translation.dumper.json' => true,
            'translation.dumper.mo' => true,
            'translation.dumper.php' => true,
            'translation.dumper.po' => true,
            'translation.dumper.qt' => true,
            'translation.dumper.res' => true,
            'translation.dumper.xliff' => true,
            'translation.dumper.yml' => true,
            'translation.extractor' => true,
            'translation.extractor.php' => true,
            'translation.loader' => true,
            'translation.loader.csv' => true,
            'translation.loader.dat' => true,
            'translation.loader.ini' => true,
            'translation.loader.json' => true,
            'translation.loader.mo' => true,
            'translation.loader.php' => true,
            'translation.loader.po' => true,
            'translation.loader.qt' => true,
            'translation.loader.res' => true,
            'translation.loader.xliff' => true,
            'translation.loader.yml' => true,
            'translation.reader' => true,
            'translation.warmer' => true,
            'translation.writer' => true,
            'translator.default' => true,
            'translator_listener' => true,
            'twig.cache_warmer' => true,
            'twig.command.debug' => true,
            'twig.command.lint' => true,
            'twig.exception_listener' => true,
            'twig.form.renderer' => true,
            'twig.loader' => true,
            'twig.profile' => true,
            'twig.runtime.httpkernel' => true,
            'twig.template_cache_warmer' => true,
            'twig.translation.extractor' => true,
            'uri_signer' => true,
            'validate_request_listener' => true,
            'validator.builder' => true,
            'validator.email' => true,
            'validator.expression' => true,
            'validator.mapping.cache_warmer' => true,
            'validator.validator_factory' => true,
        ];
        $this->aliases = [
            'Sonata\\AdminBundle\\Admin\\Pool' => 'sonata.admin.pool',
            'cache.app_clearer' => 'cache.default_clearer',
            'console.command.doctrine_bundle_doctrinecachebundle_command_containscommand' => 'doctrine_cache.contains_command',
            'console.command.doctrine_bundle_doctrinecachebundle_command_deletecommand' => 'doctrine_cache.delete_command',
            'console.command.doctrine_bundle_doctrinecachebundle_command_flushcommand' => 'doctrine_cache.flush_command',
            'console.command.doctrine_bundle_doctrinecachebundle_command_statscommand' => 'doctrine_cache.stats_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsdiffdoctrinecommand' => 'doctrine_migrations.diff_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsdumpschemadoctrinecommand' => 'doctrine_migrations.dump_schema_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsexecutedoctrinecommand' => 'doctrine_migrations.execute_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsgeneratedoctrinecommand' => 'doctrine_migrations.generate_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationslatestdoctrinecommand' => 'doctrine_migrations.latest_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsmigratedoctrinecommand' => 'doctrine_migrations.migrate_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsrollupdoctrinecommand' => 'doctrine_migrations.rollup_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsstatusdoctrinecommand' => 'doctrine_migrations.status_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsuptodatedoctrinecommand' => 'doctrine_migrations.up_to_date_command',
            'console.command.doctrine_bundle_migrationsbundle_command_migrationsversiondoctrinecommand' => 'doctrine_migrations.version_command',
            'console.command.sonata_corebundle_command_sonatadumpdoctrinemetacommand' => 'Sonata\\CoreBundle\\Command\\SonataDumpDoctrineMetaCommand',
            'console.command.sonata_corebundle_command_sonatalistformmappingcommand' => 'Sonata\\CoreBundle\\Command\\SonataListFormMappingCommand',
            'database_connection' => 'doctrine.dbal.default_connection',
            'doctrine.orm.entity_manager' => 'doctrine.orm.default_entity_manager',
            'session.storage' => 'session.storage.native',
            'sonata.block.cache.handler' => 'sonata.block.cache.handler.default',
            'sonata.block.context_manager' => 'sonata.block.context_manager.default',
            'sonata.block.renderer' => 'sonata.block.renderer.default',
            'sonata.doctrine.model.adapter.chain' => 'sonata.core.model.adapter.chain',
            'translator' => 'translator.default',
        ];

        $this->privates['service_container'] = function () {
            include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/ControllerNameParser.php';
            include_once $this->targetDirs[3].'/vendor/symfony/event-dispatcher/EventSubscriberInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/ResponseListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/StreamedResponseListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/LocaleListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/ValidateRequestListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/EventListener/ResolveControllerNameSubscriber.php';
            include_once $this->targetDirs[3].'/vendor/symfony/event-dispatcher/EventDispatcherInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/event-dispatcher/EventDispatcher.php';
            include_once $this->targetDirs[3].'/vendor/symfony/event-dispatcher/ContainerAwareEventDispatcher.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/HttpKernelInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/TerminableInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/HttpKernel.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentResolverInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ControllerResolverInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ControllerResolver.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ContainerControllerResolver.php';
            include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/ControllerResolver.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentResolver.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/ControllerMetadata/ArgumentMetadataFactoryInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/ControllerMetadata/ArgumentMetadataFactory.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-foundation/RequestStack.php';
            include_once $this->targetDirs[3].'/vendor/symfony/config/ConfigCacheFactoryInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/config/ResourceCheckerConfigCacheFactory.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/AbstractSessionListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/SessionListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/dependency-injection/ServiceLocator.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/SaveSessionListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/TranslatorInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/TranslatorBagInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/Translator.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/CacheWarmer/WarmableInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Translation/Translator.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/Formatter/MessageFormatterInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/Formatter/ChoiceMessageFormatterInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/Formatter/MessageFormatter.php';
            include_once $this->targetDirs[3].'/vendor/symfony/translation/MessageSelector.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/TranslatorListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/DebugHandlersListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Debug/FileLinkFormatter.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/RequestContext.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/EventListener/RouterListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security/Core/Authentication/Token/Storage/TokenStorageInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security/Core/Authentication/Token/Storage/TokenStorage.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security/Http/Firewall.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security-bundle/EventListener/FirewallListener.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security-bundle/Security/FirewallMap.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security/Http/FirewallMapInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security/Http/Logout/LogoutUrlGenerator.php';
            include_once $this->targetDirs[3].'/vendor/symfony/security/Http/RememberMe/ResponseListener.php';
            include_once $this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Cache/HttpCacheHandlerInterface.php';
            include_once $this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Cache/HttpCacheHandler.php';
            include_once $this->targetDirs[3].'/vendor/psr/log/Psr/Log/LoggerInterface.php';
            include_once $this->targetDirs[3].'/vendor/psr/log/Psr/Log/AbstractLogger.php';
            include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Log/Logger.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/RequestContextAwareInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/Matcher/UrlMatcherInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/Generator/UrlGeneratorInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/RouterInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/Matcher/RequestMatcherInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/routing/Router.php';
            include_once $this->targetDirs[3].'/vendor/symfony/dependency-injection/ServiceSubscriberInterface.php';
            include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Routing/Router.php';
        };
    }

    public function getRemovedIds()
    {
        return require $this->containerDir.\DIRECTORY_SEPARATOR.'removed-ids.php';
    }

    public function compile()
    {
        throw new LogicException('You cannot compile a dumped container that was already compiled.');
    }

    public function isCompiled()
    {
        return true;
    }

    public function isFrozen()
    {
        @trigger_error(sprintf('The %s() method is deprecated since Symfony 3.3 and will be removed in 4.0. Use the isCompiled() method instead.', __METHOD__), E_USER_DEPRECATED);

        return true;
    }

    protected function load($file, $lazyLoad = true)
    {
        return require $this->containerDir.\DIRECTORY_SEPARATOR.$file;
    }

    /*
     * Gets the public 'admin.adjustingAngle' autowired service.
     *
     * @return \App\Admin\AdjustingAngleAdmin
     */
    protected function getAdmin_AdjustingAngleService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/AdjustingAngleAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\AdjustingAngleAdmin('admin.adjustingAngle', 'App\\Entity\\AdjustingAngle', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.adjusting_angle');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.adjustingAngle.template_registry']) ? $this->services['admin.adjustingAngle.template_registry'] : $this->load('getAdmin_AdjustingAngle_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.beamAngle' autowired service.
     *
     * @return \App\Admin\BeamAngleAdmin
     */
    protected function getAdmin_BeamAngleService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/BeamAngleAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\BeamAngleAdmin('admin.beamAngle', 'App\\Entity\\BeamAngle', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.beam_angle');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.beamAngle.template_registry']) ? $this->services['admin.beamAngle.template_registry'] : $this->load('getAdmin_BeamAngle_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.colourOfHousing' autowired service.
     *
     * @return \App\Admin\ColourOfHousingAdmin
     */
    protected function getAdmin_ColourOfHousingService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/ColourOfHousingAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\ColourOfHousingAdmin('admin.colourOfHousing', 'App\\Entity\\ColourOfHousing', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.colour_of_housing');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.colourOfHousing.template_registry']) ? $this->services['admin.colourOfHousing.template_registry'] : $this->load('getAdmin_ColourOfHousing_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.colourOfShade' autowired service.
     *
     * @return \App\Admin\ColourOfShadeAdmin
     */
    protected function getAdmin_ColourOfShadeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/ColourOfShadeAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\ColourOfShadeAdmin('admin.colourOfShade', 'App\\Entity\\ColourOfShade', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.colour_of_shade');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.colourOfShade.template_registry']) ? $this->services['admin.colourOfShade.template_registry'] : $this->load('getAdmin_ColourOfShade_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.cover' autowired service.
     *
     * @return \App\Admin\CoverAdmin
     */
    protected function getAdmin_CoverService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/CoverAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\CoverAdmin('admin.cover', 'App\\Entity\\Cover', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.cover');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.cover.template_registry']) ? $this->services['admin.cover.template_registry'] : $this->load('getAdmin_Cover_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.cri' autowired service.
     *
     * @return \App\Admin\CriAdmin
     */
    protected function getAdmin_CriService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/CriAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\CriAdmin('admin.cri', 'App\\Entity\\Cri', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.cri');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.cri.template_registry']) ? $this->services['admin.cri.template_registry'] : $this->load('getAdmin_Cri_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.datasheet' autowired service.
     *
     * @return \App\Admin\DataSheetAdmin
     */
    protected function getAdmin_DatasheetService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/DataSheetAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\DataSheetAdmin('admin.datasheet', 'App\\Entity\\DataSheet', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.datasheet');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.datasheet.template_registry']) ? $this->services['admin.datasheet.template_registry'] : $this->load('getAdmin_Datasheet_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.dimmerType' autowired service.
     *
     * @return \App\Admin\DimmerTypeAdmin
     */
    protected function getAdmin_DimmerTypeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/DimmerTypeAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\DimmerTypeAdmin('admin.dimmerType', 'App\\Entity\\DimmerType', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.dimmer_type');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.dimmerType.template_registry']) ? $this->services['admin.dimmerType.template_registry'] : $this->load('getAdmin_DimmerType_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.fixtureLightOutput' autowired service.
     *
     * @return \App\Admin\FixtureLightOutputAdmin
     */
    protected function getAdmin_FixtureLightOutputService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/FixtureLightOutputAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\FixtureLightOutputAdmin('admin.fixtureLightOutput', 'App\\Entity\\FixtureLightOutput', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.fixture_light_output');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.fixtureLightOutput.template_registry']) ? $this->services['admin.fixtureLightOutput.template_registry'] : $this->load('getAdmin_FixtureLightOutput_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.frequency' autowired service.
     *
     * @return \App\Admin\FrequencyAdmin
     */
    protected function getAdmin_FrequencyService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/FrequencyAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\FrequencyAdmin('admin.frequency', 'App\\Entity\\Frequency', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.frequency');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.frequency.template_registry']) ? $this->services['admin.frequency.template_registry'] : $this->load('getAdmin_Frequency_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.ipClass' autowired service.
     *
     * @return \App\Admin\IPClassAdmin
     */
    protected function getAdmin_IpClassService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/IPClassAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\IPClassAdmin('admin.ipClass', 'App\\Entity\\IPClass', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.ip_class');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.ipClass.template_registry']) ? $this->services['admin.ipClass.template_registry'] : $this->load('getAdmin_IpClass_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.lightColour' autowired service.
     *
     * @return \App\Admin\LightColourAdmin
     */
    protected function getAdmin_LightColourService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/LightColourAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\LightColourAdmin('admin.lightColour', 'App\\Entity\\LightColour', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.light_colour');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.lightColour.template_registry']) ? $this->services['admin.lightColour.template_registry'] : $this->load('getAdmin_LightColour_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.lightPoints' autowired service.
     *
     * @return \App\Admin\LightPointsAdmin
     */
    protected function getAdmin_LightPointsService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/LightPointsAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\LightPointsAdmin('admin.lightPoints', 'App\\Entity\\LightPoints', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.light_points');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.lightPoints.template_registry']) ? $this->services['admin.lightPoints.template_registry'] : $this->load('getAdmin_LightPoints_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.materialOfHousing' autowired service.
     *
     * @return \App\Admin\MaterialOfHousingAdmin
     */
    protected function getAdmin_MaterialOfHousingService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/MaterialOfHousingAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\MaterialOfHousingAdmin('admin.materialOfHousing', 'App\\Entity\\MaterialOfHousing', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.material_of_housing');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.materialOfHousing.template_registry']) ? $this->services['admin.materialOfHousing.template_registry'] : $this->load('getAdmin_MaterialOfHousing_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.materialOfShade' autowired service.
     *
     * @return \App\Admin\MaterialOfShadeAdmin
     */
    protected function getAdmin_MaterialOfShadeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/MaterialOfShadeAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\MaterialOfShadeAdmin('admin.materialOfShade', 'App\\Entity\\MaterialOfShade', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.material_of_shade');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.materialOfShade.template_registry']) ? $this->services['admin.materialOfShade.template_registry'] : $this->load('getAdmin_MaterialOfShade_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.numberOfLamps' autowired service.
     *
     * @return \App\Admin\NumberOfLampsAdmin
     */
    protected function getAdmin_NumberOfLampsService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/NumberOfLampsAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\NumberOfLampsAdmin('admin.numberOfLamps', 'App\\Entity\\NumberOfLamps', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.number_of_lamps');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.numberOfLamps.template_registry']) ? $this->services['admin.numberOfLamps.template_registry'] : $this->load('getAdmin_NumberOfLamps_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.plug' autowired service.
     *
     * @return \App\Admin\PlugAdmin
     */
    protected function getAdmin_PlugService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/PlugAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\PlugAdmin('admin.plug', 'App\\Entity\\Plug', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.plug');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.plug.template_registry']) ? $this->services['admin.plug.template_registry'] : $this->load('getAdmin_Plug_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.powerOfFitting' autowired service.
     *
     * @return \App\Admin\PowerOfFittingAdmin
     */
    protected function getAdmin_PowerOfFittingService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/PowerOfFittingAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\PowerOfFittingAdmin('admin.powerOfFitting', 'App\\Entity\\PowerOfFitting', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.power_of_fitting');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.powerOfFitting.template_registry']) ? $this->services['admin.powerOfFitting.template_registry'] : $this->load('getAdmin_PowerOfFitting_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.powerOfLamp' autowired service.
     *
     * @return \App\Admin\PowerOfLampAdmin
     */
    protected function getAdmin_PowerOfLampService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/PowerOfLampAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\PowerOfLampAdmin('admin.powerOfLamp', 'App\\Entity\\PowerOfLamp', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.power_of_lamp');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.powerOfLamp.template_registry']) ? $this->services['admin.powerOfLamp.template_registry'] : $this->load('getAdmin_PowerOfLamp_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.socket' autowired service.
     *
     * @return \App\Admin\SocketAdmin
     */
    protected function getAdmin_SocketService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/SocketAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\SocketAdmin('admin.socket', 'App\\Entity\\Socket', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.socket');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.socket.template_registry']) ? $this->services['admin.socket.template_registry'] : $this->load('getAdmin_Socket_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.supplier' autowired service.
     *
     * @return \App\Admin\SupplierAdmin
     */
    protected function getAdmin_SupplierService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/SupplierAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\SupplierAdmin('admin.supplier', 'App\\Entity\\Supplier', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.supplier');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.supplier.template_registry']) ? $this->services['admin.supplier.template_registry'] : $this->load('getAdmin_Supplier_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.typeOfBulb' autowired service.
     *
     * @return \App\Admin\TypeOfBulbAdmin
     */
    protected function getAdmin_TypeOfBulbService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/TypeOfBulbAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\TypeOfBulbAdmin('admin.typeOfBulb', 'App\\Entity\\TypeOfBulb', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.type_of_bulb');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.typeOfBulb.template_registry']) ? $this->services['admin.typeOfBulb.template_registry'] : $this->load('getAdmin_TypeOfBulb_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.typeOfLamp' autowired service.
     *
     * @return \App\Admin\TypeOfLampAdmin
     */
    protected function getAdmin_TypeOfLampService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/TypeOfLampAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\TypeOfLampAdmin('admin.typeOfLamp', 'App\\Entity\\TypeOfLamp', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.type_of_lamp');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.typeOfLamp.template_registry']) ? $this->services['admin.typeOfLamp.template_registry'] : $this->load('getAdmin_TypeOfLamp_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'admin.voltage' autowired service.
     *
     * @return \App\Admin\VoltageAdmin
     */
    protected function getAdmin_VoltageService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AccessRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/FieldDescriptionRegistryInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/LifecycleHookProviderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/MenuBuilderInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/ParentAdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/UrlGeneratorInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminInterface.php';
        include_once $this->targetDirs[3].'/vendor/symfony/security-acl/Model/DomainObjectInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AdminTreeInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Admin/AbstractAdmin.php';
        include_once $this->targetDirs[3].'/src/Admin/VoltageAdmin.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/SecurityHandlerInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Security/Handler/NoopSecurityHandler.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/LabelTranslatorStrategyInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Translator/NativeLabelTranslatorStrategy.php';

        $instance = new \App\Admin\VoltageAdmin('admin.voltage', 'App\\Entity\\Voltage', 'SonataAdminBundle:CRUD');

        $instance->setManagerType('orm');
        $instance->setModelManager(${($_ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->load('getSonata_Admin_Manager_OrmService.php')) && false ?: '_'});
        $instance->setFormContractor(${($_ = isset($this->services['sonata.admin.builder.orm_form']) ? $this->services['sonata.admin.builder.orm_form'] : $this->load('getSonata_Admin_Builder_OrmFormService.php')) && false ?: '_'});
        $instance->setShowBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_show']) ? $this->services['sonata.admin.builder.orm_show'] : $this->load('getSonata_Admin_Builder_OrmShowService.php')) && false ?: '_'});
        $instance->setListBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_list']) ? $this->services['sonata.admin.builder.orm_list'] : $this->load('getSonata_Admin_Builder_OrmListService.php')) && false ?: '_'});
        $instance->setDatagridBuilder(${($_ = isset($this->services['sonata.admin.builder.orm_datagrid']) ? $this->services['sonata.admin.builder.orm_datagrid'] : $this->load('getSonata_Admin_Builder_OrmDatagridService.php')) && false ?: '_'});
        $instance->setTranslator(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, false);
        $instance->setConfigurationPool(${($_ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->load('getSonata_Admin_PoolService.php')) && false ?: '_'});
        $instance->setRouteGenerator(${($_ = isset($this->services['sonata.admin.route.default_generator']) ? $this->services['sonata.admin.route.default_generator'] : $this->load('getSonata_Admin_Route_DefaultGeneratorService.php')) && false ?: '_'});
        $instance->setValidator(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->load('getValidatorService.php')) && false ?: '_'});
        $instance->setSecurityHandler(${($_ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : ($this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler())) && false ?: '_'});
        $instance->setMenuFactory(${($_ = isset($this->services['knp_menu.factory']) ? $this->services['knp_menu.factory'] : $this->load('getKnpMenu_FactoryService.php')) && false ?: '_'});
        $instance->setRouteBuilder(${($_ = isset($this->services['sonata.admin.route.path_info']) ? $this->services['sonata.admin.route.path_info'] : $this->load('getSonata_Admin_Route_PathInfoService.php')) && false ?: '_'});
        $instance->setLabelTranslatorStrategy(${($_ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : ($this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy())) && false ?: '_'});
        $instance->setPagerType('default');
        $instance->setLabel('label.voltage');
        $instance->showMosaicButton(true);
        $instance->setTemplateRegistry(${($_ = isset($this->services['admin.voltage.template_registry']) ? $this->services['admin.voltage.template_registry'] : $this->load('getAdmin_Voltage_TemplateRegistryService.php')) && false ?: '_'});
        $instance->setSecurityInformation([]);
        $instance->initialize();
        $instance->addExtension(${($_ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->load('getSonata_Admin_Event_ExtensionService.php')) && false ?: '_'});
        $instance->setFormTheme([0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig']);
        $instance->setFilterTheme([0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig']);

        return $instance;
    }

    /*
     * Gets the public 'event_dispatcher' shared service.
     *
     * @return \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher
     */
    protected function getEventDispatcherService()
    {
        $this->services['event_dispatcher'] = $instance = new \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher($this);

        $instance->addListener('kernel.response', [0 => function () {
            return ${($_ = isset($this->services['sonata.block.cache.handler.default']) ? $this->services['sonata.block.cache.handler.default'] : ($this->services['sonata.block.cache.handler.default'] = new \Sonata\BlockBundle\Cache\HttpCacheHandler())) && false ?: '_'};
        }, 1 => 'onKernelResponse'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ${($_ = isset($this->services['response_listener']) ? $this->services['response_listener'] : ($this->services['response_listener'] = new \Symfony\Component\HttpKernel\EventListener\ResponseListener('UTF-8'))) && false ?: '_'};
        }, 1 => 'onKernelResponse'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ${($_ = isset($this->services['streamed_response_listener']) ? $this->services['streamed_response_listener'] : ($this->services['streamed_response_listener'] = new \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener())) && false ?: '_'};
        }, 1 => 'onKernelResponse'], -1024);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['locale_listener']) ? $this->services['locale_listener'] : $this->getLocaleListenerService()) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 16);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ${($_ = isset($this->services['locale_listener']) ? $this->services['locale_listener'] : $this->getLocaleListenerService()) && false ?: '_'};
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['validate_request_listener']) ? $this->services['validate_request_listener'] : ($this->services['validate_request_listener'] = new \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener())) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 256);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['resolve_controller_name_subscriber']) ? $this->services['resolve_controller_name_subscriber'] : $this->getResolveControllerNameSubscriberService()) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 24);
        $instance->addListener('console.error', [0 => function () {
            return ${($_ = isset($this->services['console.error_listener']) ? $this->services['console.error_listener'] : $this->load('getConsole_ErrorListenerService.php')) && false ?: '_'};
        }, 1 => 'onConsoleError'], -128);
        $instance->addListener('console.terminate', [0 => function () {
            return ${($_ = isset($this->services['console.error_listener']) ? $this->services['console.error_listener'] : $this->load('getConsole_ErrorListenerService.php')) && false ?: '_'};
        }, 1 => 'onConsoleTerminate'], -128);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['session_listener']) ? $this->services['session_listener'] : $this->getSessionListenerService()) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 128);
        $instance->addListener('kernel.response', [0 => function () {
            return ${($_ = isset($this->services['session_listener']) ? $this->services['session_listener'] : $this->getSessionListenerService()) && false ?: '_'};
        }, 1 => 'onKernelResponse'], -1000);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ${($_ = isset($this->services['session_listener']) ? $this->services['session_listener'] : $this->getSessionListenerService()) && false ?: '_'};
        }, 1 => 'onFinishRequest'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ${($_ = isset($this->services['session.save_listener']) ? $this->services['session.save_listener'] : ($this->services['session.save_listener'] = new \Symfony\Component\HttpKernel\EventListener\SaveSessionListener())) && false ?: '_'};
        }, 1 => 'onKernelResponse'], -1000);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['translator_listener']) ? $this->services['translator_listener'] : $this->getTranslatorListenerService()) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 10);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ${($_ = isset($this->services['translator_listener']) ? $this->services['translator_listener'] : $this->getTranslatorListenerService()) && false ?: '_'};
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['debug.debug_handlers_listener']) ? $this->services['debug.debug_handlers_listener'] : $this->getDebug_DebugHandlersListenerService()) && false ?: '_'};
        }, 1 => 'configure'], 2048);
        $instance->addListener('console.command', [0 => function () {
            return ${($_ = isset($this->services['debug.debug_handlers_listener']) ? $this->services['debug.debug_handlers_listener'] : $this->getDebug_DebugHandlersListenerService()) && false ?: '_'};
        }, 1 => 'configure'], 2048);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['router_listener']) ? $this->services['router_listener'] : $this->getRouterListenerService()) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 32);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ${($_ = isset($this->services['router_listener']) ? $this->services['router_listener'] : $this->getRouterListenerService()) && false ?: '_'};
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ${($_ = isset($this->services['router_listener']) ? $this->services['router_listener'] : $this->getRouterListenerService()) && false ?: '_'};
        }, 1 => 'onKernelException'], -64);
        $instance->addListener('kernel.exception', [0 => function () {
            return ${($_ = isset($this->services['twig.exception_listener']) ? $this->services['twig.exception_listener'] : $this->load('getTwig_ExceptionListenerService.php')) && false ?: '_'};
        }, 1 => 'onKernelException'], -128);
        $instance->addListener('kernel.request', [0 => function () {
            return ${($_ = isset($this->services['security.firewall']) ? $this->services['security.firewall'] : $this->getSecurity_FirewallService()) && false ?: '_'};
        }, 1 => 'onKernelRequest'], 8);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ${($_ = isset($this->services['security.firewall']) ? $this->services['security.firewall'] : $this->getSecurity_FirewallService()) && false ?: '_'};
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ${($_ = isset($this->services['security.rememberme.response_listener']) ? $this->services['security.rememberme.response_listener'] : ($this->services['security.rememberme.response_listener'] = new \Symfony\Component\Security\Http\RememberMe\ResponseListener())) && false ?: '_'};
        }, 1 => 'onKernelResponse'], 0);

        return $instance;
    }

    /*
     * Gets the public 'http_kernel' shared service.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernel
     */
    protected function getHttpKernelService()
    {
        return $this->services['http_kernel'] = new \Symfony\Component\HttpKernel\HttpKernel(${($_ = isset($this->services['event_dispatcher']) ? $this->services['event_dispatcher'] : $this->getEventDispatcherService()) && false ?: '_'}, new \Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver($this, ${($_ = isset($this->services['controller_name_converter']) ? $this->services['controller_name_converter'] : ($this->services['controller_name_converter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel', 1)) && false ?: '_'}))) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : ($this->services['logger'] = new \Symfony\Component\HttpKernel\Log\Logger())) && false ?: '_'}), ${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())) && false ?: '_'}, new \Symfony\Component\HttpKernel\Controller\ArgumentResolver(new \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactory(), new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['argument_resolver.request_attribute']) ? $this->services['argument_resolver.request_attribute'] : ($this->services['argument_resolver.request_attribute'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestAttributeValueResolver())) && false ?: '_'};
            yield 1 => ${($_ = isset($this->services['argument_resolver.request']) ? $this->services['argument_resolver.request'] : ($this->services['argument_resolver.request'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver())) && false ?: '_'};
            yield 2 => ${($_ = isset($this->services['argument_resolver.session']) ? $this->services['argument_resolver.session'] : ($this->services['argument_resolver.session'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver())) && false ?: '_'};
            yield 3 => ${($_ = isset($this->services['security.user_value_resolver']) ? $this->services['security.user_value_resolver'] : $this->load('getSecurity_UserValueResolverService.php')) && false ?: '_'};
            yield 4 => ${($_ = isset($this->services['argument_resolver.service']) ? $this->services['argument_resolver.service'] : $this->load('getArgumentResolver_ServiceService.php')) && false ?: '_'};
            yield 5 => ${($_ = isset($this->services['argument_resolver.default']) ? $this->services['argument_resolver.default'] : ($this->services['argument_resolver.default'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver())) && false ?: '_'};
            yield 6 => ${($_ = isset($this->services['argument_resolver.variadic']) ? $this->services['argument_resolver.variadic'] : ($this->services['argument_resolver.variadic'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\VariadicValueResolver())) && false ?: '_'};
        }, 7)));
    }

    /*
     * Gets the public 'request_stack' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\RequestStack
     */
    protected function getRequestStackService()
    {
        return $this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack();
    }

    /*
     * Gets the public 'router' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected function getRouterService()
    {
        $this->services['router'] = $instance = new \Symfony\Bundle\FrameworkBundle\Routing\Router($this, 'kernel:loadRoutes', ['cache_dir' => $this->targetDirs[0], 'debug' => false, 'generator_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator', 'generator_base_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator', 'generator_dumper_class' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\PhpGeneratorDumper', 'generator_cache_class' => 'srcProdProjectContainerUrlGenerator', 'matcher_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher', 'matcher_base_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher', 'matcher_dumper_class' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\PhpMatcherDumper', 'matcher_cache_class' => 'srcProdProjectContainerUrlMatcher', 'strict_requirements' => NULL, 'resource_type' => 'service'], ${($_ = isset($this->services['router.request_context']) ? $this->services['router.request_context'] : $this->getRouter_RequestContextService()) && false ?: '_'});

        $instance->setConfigCacheFactory(${($_ = isset($this->services['config_cache_factory']) ? $this->services['config_cache_factory'] : ($this->services['config_cache_factory'] = new \Symfony\Component\Config\ResourceCheckerConfigCacheFactory())) && false ?: '_'});

        return $instance;
    }

    /*
     * Gets the public 'security.token_storage' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage
     */
    protected function getSecurity_TokenStorageService()
    {
        return $this->services['security.token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.boolean' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\BooleanFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_BooleanService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/BooleanFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\BooleanFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.callback' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_CallbackService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/CallbackFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.choice' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_ChoiceService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/ChoiceFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.class' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ClassFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_ClassService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/ClassFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\ClassFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.date' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_DateService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/AbstractDateFilter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/DateFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\DateFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.date_range' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_DateRangeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/AbstractDateFilter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/DateRangeFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.datetime' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateTimeFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_DatetimeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/AbstractDateFilter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/DateTimeFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\DateTimeFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.datetime_range' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateTimeRangeFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_DatetimeRangeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/AbstractDateFilter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/DateTimeRangeFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\DateTimeRangeFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.model' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ModelFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_ModelService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/ModelFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\ModelFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.model_autocomplete' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_ModelAutocompleteService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/ModelAutocompleteFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.number' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\NumberFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_NumberService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/NumberFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\NumberFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.string' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\StringFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_StringService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/StringFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\StringFilter();
    }

    /*
     * Gets the public 'sonata.admin.orm.filter.type.time' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\TimeFilter
     */
    protected function getSonata_Admin_Orm_Filter_Type_TimeService()
    {
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/FilterInterface.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/Filter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/AbstractDateFilter.php';
        include_once $this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src/Filter/TimeFilter.php';

        return new \Sonata\DoctrineORMAdminBundle\Filter\TimeFilter();
    }

    /*
     * Gets the private 'config_cache_factory' shared service.
     *
     * @return \Symfony\Component\Config\ResourceCheckerConfigCacheFactory
     */
    protected function getConfigCacheFactoryService()
    {
        return $this->services['config_cache_factory'] = new \Symfony\Component\Config\ResourceCheckerConfigCacheFactory();
    }

    /*
     * Gets the private 'controller_name_converter' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser
     */
    protected function getControllerNameConverterService()
    {
        return $this->services['controller_name_converter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel', 1)) && false ?: '_'});
    }

    /*
     * Gets the private 'debug.debug_handlers_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener
     */
    protected function getDebug_DebugHandlersListenerService()
    {
        return $this->services['debug.debug_handlers_listener'] = new \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener(NULL, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : ($this->services['logger'] = new \Symfony\Component\HttpKernel\Log\Logger())) && false ?: '_'}, -1, 0, false, ${($_ = isset($this->services['debug.file_link_formatter']) ? $this->services['debug.file_link_formatter'] : ($this->services['debug.file_link_formatter'] = new \Symfony\Component\HttpKernel\Debug\FileLinkFormatter(NULL))) && false ?: '_'}, false);
    }

    /*
     * Gets the private 'debug.file_link_formatter' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Debug\FileLinkFormatter
     */
    protected function getDebug_FileLinkFormatterService()
    {
        return $this->services['debug.file_link_formatter'] = new \Symfony\Component\HttpKernel\Debug\FileLinkFormatter(NULL);
    }

    /*
     * Gets the private 'locale_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\LocaleListener
     */
    protected function getLocaleListenerService()
    {
        return $this->services['locale_listener'] = new \Symfony\Component\HttpKernel\EventListener\LocaleListener(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())) && false ?: '_'}, 'en', ${($_ = isset($this->services['router']) ? $this->services['router'] : $this->getRouterService()) && false ?: '_'});
    }

    /*
     * Gets the private 'logger' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Log\Logger
     */
    protected function getLoggerService()
    {
        return $this->services['logger'] = new \Symfony\Component\HttpKernel\Log\Logger();
    }

    /*
     * Gets the private 'resolve_controller_name_subscriber' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\EventListener\ResolveControllerNameSubscriber
     */
    protected function getResolveControllerNameSubscriberService()
    {
        return $this->services['resolve_controller_name_subscriber'] = new \Symfony\Bundle\FrameworkBundle\EventListener\ResolveControllerNameSubscriber(${($_ = isset($this->services['controller_name_converter']) ? $this->services['controller_name_converter'] : ($this->services['controller_name_converter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel', 1)) && false ?: '_'}))) && false ?: '_'});
    }

    /*
     * Gets the private 'response_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ResponseListener
     */
    protected function getResponseListenerService()
    {
        return $this->services['response_listener'] = new \Symfony\Component\HttpKernel\EventListener\ResponseListener('UTF-8');
    }

    /*
     * Gets the private 'router.request_context' shared service.
     *
     * @return \Symfony\Component\Routing\RequestContext
     */
    protected function getRouter_RequestContextService()
    {
        return $this->services['router.request_context'] = new \Symfony\Component\Routing\RequestContext('', 'GET', 'localhost', 'http', 80, 443);
    }

    /*
     * Gets the private 'router_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\RouterListener
     */
    protected function getRouterListenerService()
    {
        return $this->services['router_listener'] = new \Symfony\Component\HttpKernel\EventListener\RouterListener(${($_ = isset($this->services['router']) ? $this->services['router'] : $this->getRouterService()) && false ?: '_'}, ${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())) && false ?: '_'}, ${($_ = isset($this->services['router.request_context']) ? $this->services['router.request_context'] : $this->getRouter_RequestContextService()) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : ($this->services['logger'] = new \Symfony\Component\HttpKernel\Log\Logger())) && false ?: '_'}, $this->targetDirs[3], false);
    }

    /*
     * Gets the private 'security.firewall' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener
     */
    protected function getSecurity_FirewallService()
    {
        return $this->services['security.firewall'] = new \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener(new \Symfony\Bundle\SecurityBundle\Security\FirewallMap(new \Symfony\Component\DependencyInjection\ServiceLocator(['security.firewall.map.context.dev' => function () {
            return ${($_ = isset($this->services['security.firewall.map.context.dev']) ? $this->services['security.firewall.map.context.dev'] : $this->load('getSecurity_Firewall_Map_Context_DevService.php')) && false ?: '_'};
        }, 'security.firewall.map.context.main' => function () {
            return ${($_ = isset($this->services['security.firewall.map.context.main']) ? $this->services['security.firewall.map.context.main'] : $this->load('getSecurity_Firewall_Map_Context_MainService.php')) && false ?: '_'};
        }]), new RewindableGenerator(function () {
            yield 'security.firewall.map.context.dev' => ${($_ = isset($this->services['security.request_matcher.zfhj2lw']) ? $this->services['security.request_matcher.zfhj2lw'] : ($this->services['security.request_matcher.zfhj2lw'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/(_(profiler|wdt)|css|images|js)/'))) && false ?: '_'};
            yield 'security.firewall.map.context.main' => NULL;
        }, 2)), ${($_ = isset($this->services['event_dispatcher']) ? $this->services['event_dispatcher'] : $this->getEventDispatcherService()) && false ?: '_'}, ${($_ = isset($this->services['security.logout_url_generator']) ? $this->services['security.logout_url_generator'] : $this->getSecurity_LogoutUrlGeneratorService()) && false ?: '_'});
    }

    /*
     * Gets the private 'security.logout_url_generator' shared service.
     *
     * @return \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator
     */
    protected function getSecurity_LogoutUrlGeneratorService()
    {
        return $this->services['security.logout_url_generator'] = new \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())) && false ?: '_'}, ${($_ = isset($this->services['router']) ? $this->services['router'] : $this->getRouterService()) && false ?: '_'}, ${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : ($this->services['security.token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage())) && false ?: '_'});
    }

    /*
     * Gets the private 'security.rememberme.response_listener' shared service.
     *
     * @return \Symfony\Component\Security\Http\RememberMe\ResponseListener
     */
    protected function getSecurity_Rememberme_ResponseListenerService()
    {
        return $this->services['security.rememberme.response_listener'] = new \Symfony\Component\Security\Http\RememberMe\ResponseListener();
    }

    /*
     * Gets the private 'session.save_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\SaveSessionListener
     */
    protected function getSession_SaveListenerService()
    {
        return $this->services['session.save_listener'] = new \Symfony\Component\HttpKernel\EventListener\SaveSessionListener();
    }

    /*
     * Gets the private 'session_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\SessionListener
     */
    protected function getSessionListenerService()
    {
        return $this->services['session_listener'] = new \Symfony\Component\HttpKernel\EventListener\SessionListener(new \Symfony\Component\DependencyInjection\ServiceLocator(['session' => function () {
            return ${($_ = isset($this->services['session']) ? $this->services['session'] : $this->load('getSessionService.php')) && false ?: '_'};
        }]));
    }

    /*
     * Gets the private 'sonata.block.cache.handler.default' shared service.
     *
     * @return \Sonata\BlockBundle\Cache\HttpCacheHandler
     */
    protected function getSonata_Block_Cache_Handler_DefaultService()
    {
        return $this->services['sonata.block.cache.handler.default'] = new \Sonata\BlockBundle\Cache\HttpCacheHandler();
    }

    /*
     * Gets the private 'streamed_response_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener
     */
    protected function getStreamedResponseListenerService()
    {
        return $this->services['streamed_response_listener'] = new \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener();
    }

    /*
     * Gets the private 'translator.default' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    protected function getTranslator_DefaultService()
    {
        $this->services['translator.default'] = $instance = new \Symfony\Bundle\FrameworkBundle\Translation\Translator(new \Symfony\Component\DependencyInjection\ServiceLocator(['translation.loader.csv' => function () {
            return ${($_ = isset($this->services['translation.loader.csv']) ? $this->services['translation.loader.csv'] : ($this->services['translation.loader.csv'] = new \Symfony\Component\Translation\Loader\CsvFileLoader())) && false ?: '_'};
        }, 'translation.loader.dat' => function () {
            return ${($_ = isset($this->services['translation.loader.dat']) ? $this->services['translation.loader.dat'] : ($this->services['translation.loader.dat'] = new \Symfony\Component\Translation\Loader\IcuDatFileLoader())) && false ?: '_'};
        }, 'translation.loader.ini' => function () {
            return ${($_ = isset($this->services['translation.loader.ini']) ? $this->services['translation.loader.ini'] : ($this->services['translation.loader.ini'] = new \Symfony\Component\Translation\Loader\IniFileLoader())) && false ?: '_'};
        }, 'translation.loader.json' => function () {
            return ${($_ = isset($this->services['translation.loader.json']) ? $this->services['translation.loader.json'] : ($this->services['translation.loader.json'] = new \Symfony\Component\Translation\Loader\JsonFileLoader())) && false ?: '_'};
        }, 'translation.loader.mo' => function () {
            return ${($_ = isset($this->services['translation.loader.mo']) ? $this->services['translation.loader.mo'] : ($this->services['translation.loader.mo'] = new \Symfony\Component\Translation\Loader\MoFileLoader())) && false ?: '_'};
        }, 'translation.loader.php' => function () {
            return ${($_ = isset($this->services['translation.loader.php']) ? $this->services['translation.loader.php'] : ($this->services['translation.loader.php'] = new \Symfony\Component\Translation\Loader\PhpFileLoader())) && false ?: '_'};
        }, 'translation.loader.po' => function () {
            return ${($_ = isset($this->services['translation.loader.po']) ? $this->services['translation.loader.po'] : ($this->services['translation.loader.po'] = new \Symfony\Component\Translation\Loader\PoFileLoader())) && false ?: '_'};
        }, 'translation.loader.qt' => function () {
            return ${($_ = isset($this->services['translation.loader.qt']) ? $this->services['translation.loader.qt'] : ($this->services['translation.loader.qt'] = new \Symfony\Component\Translation\Loader\QtFileLoader())) && false ?: '_'};
        }, 'translation.loader.res' => function () {
            return ${($_ = isset($this->services['translation.loader.res']) ? $this->services['translation.loader.res'] : ($this->services['translation.loader.res'] = new \Symfony\Component\Translation\Loader\IcuResFileLoader())) && false ?: '_'};
        }, 'translation.loader.xliff' => function () {
            return ${($_ = isset($this->services['translation.loader.xliff']) ? $this->services['translation.loader.xliff'] : ($this->services['translation.loader.xliff'] = new \Symfony\Component\Translation\Loader\XliffFileLoader())) && false ?: '_'};
        }, 'translation.loader.yml' => function () {
            return ${($_ = isset($this->services['translation.loader.yml']) ? $this->services['translation.loader.yml'] : ($this->services['translation.loader.yml'] = new \Symfony\Component\Translation\Loader\YamlFileLoader())) && false ?: '_'};
        }]), new \Symfony\Component\Translation\Formatter\MessageFormatter(new \Symfony\Component\Translation\MessageSelector()), 'en', ['translation.loader.php' => [0 => 'php'], 'translation.loader.yml' => [0 => 'yaml', 1 => 'yml'], 'translation.loader.xliff' => [0 => 'xlf', 1 => 'xliff'], 'translation.loader.po' => [0 => 'po'], 'translation.loader.mo' => [0 => 'mo'], 'translation.loader.qt' => [0 => 'ts'], 'translation.loader.csv' => [0 => 'csv'], 'translation.loader.res' => [0 => 'res'], 'translation.loader.dat' => [0 => 'dat'], 'translation.loader.ini' => [0 => 'ini'], 'translation.loader.json' => [0 => 'json']], ['cache_dir' => ($this->targetDirs[0].'/translations'), 'debug' => false, 'resource_files' => ['af' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.af.xlf')], 'ar' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.ar.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.ar.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.ar.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.ar.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.ar.xliff')], 'az' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.az.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.az.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.az.xlf')], 'be' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.be.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.be.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.be.xlf')], 'bg' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.bg.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.bg.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.bg.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.bg.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.bg.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.bg.xliff')], 'ca' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.ca.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.ca.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.ca.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.ca.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.ca.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.ca.xliff')], 'cs' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.cs.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.cs.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.cs.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.cs.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.cs.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.cs.xliff')], 'cy' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.cy.xlf')], 'da' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.da.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.da.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.da.xlf')], 'de' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.de.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.de.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.de.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.de.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.de.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.de.xliff'), 6 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/validators.de.xliff'), 7 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.de.xliff')], 'el' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.el.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.el.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.el.xlf')], 'en' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.en.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.en.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.en.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.en.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.en.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.en.xliff'), 6 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/validators.en.xliff'), 7 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.en.xliff'), 8 => ($this->targetDirs[3].'/translations/SonataAdminBundle.en.yml'), 9 => ($this->targetDirs[3].'/translations/messages.en.yml')], 'es' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.es.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.es.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.es.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.es.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.es.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.es.xliff')], 'et' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.et.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.et.xlf')], 'eu' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.eu.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.eu.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.eu.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.eu.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.eu.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.eu.xliff')], 'fa' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.fa.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.fa.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.fa.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.fa.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.fa.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.fa.xliff')], 'fi' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.fi.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.fi.xlf'), 2 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.fi.xliff')], 'fr' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.fr.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.fr.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.fr.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.fr.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.fr.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.fr.xliff'), 6 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/validators.fr.xliff'), 7 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.fr.xliff')], 'gl' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.gl.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.gl.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.gl.xlf')], 'he' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.he.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.he.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.he.xlf')], 'hr' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.hr.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.hr.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.hr.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.hr.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.hr.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.hr.xliff')], 'hu' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.hu.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.hu.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.hu.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.hu.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.hu.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.hu.xliff'), 6 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.hu.xliff')], 'hy' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.hy.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.hy.xlf')], 'id' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.id.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.id.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.id.xlf')], 'it' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.it.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.it.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.it.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.it.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.it.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.it.xliff'), 6 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.it.xliff')], 'ja' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.ja.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.ja.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.ja.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.ja.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.ja.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.ja.xliff')], 'lb' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.lb.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.lb.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.lb.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.lb.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.lb.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.lb.xliff')], 'lt' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.lt.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.lt.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.lt.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.lt.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.lt.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.lt.xliff')], 'lv' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.lv.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.lv.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.lv.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.lv.xliff')], 'mn' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.mn.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.mn.xlf')], 'nb' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.nb.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.nb.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.nb.xlf')], 'nl' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.nl.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.nl.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.nl.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.nl.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.nl.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.nl.xliff')], 'nn' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.nn.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.nn.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.nn.xlf')], 'no' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.no.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.no.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.no.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.no.xliff')], 'pl' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.pl.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.pl.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.pl.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.pl.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.pl.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.pl.xliff')], 'pt' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.pt.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.pt.xlf'), 2 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.pt.xliff'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.pt.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.pt.xliff')], 'pt_BR' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.pt_BR.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.pt_BR.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.pt_BR.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.pt_BR.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.pt_BR.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.pt_BR.xliff')], 'ro' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.ro.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.ro.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.ro.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.ro.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.ro.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.ro.xliff')], 'ru' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.ru.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.ru.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.ru.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.ru.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.ru.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.ru.xliff'), 6 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.ru.xliff')], 'sk' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.sk.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.sk.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.sk.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.sk.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.sk.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.sk.xliff')], 'sl' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.sl.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.sl.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.sl.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.sl.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.sl.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.sl.xliff')], 'sq' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.sq.xlf')], 'sr_Cyrl' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.sr_Cyrl.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.sr_Cyrl.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.sr_Cyrl.xlf')], 'sr_Latn' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.sr_Latn.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.sr_Latn.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.sr_Latn.xlf')], 'sv' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.sv.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.sv.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.sv.xlf')], 'th' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.th.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.th.xlf')], 'tl' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.tl.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.tl.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.tl.xlf')], 'tr' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.tr.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.tr.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.tr.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.tr.xliff')], 'uk' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.uk.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.uk.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.uk.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.uk.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.uk.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.uk.xliff')], 'vi' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.vi.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.vi.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.vi.xlf')], 'zh_CN' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.zh_CN.xlf'), 1 => ($this->targetDirs[3].'/vendor/symfony/form/Resources/translations/validators.zh_CN.xlf'), 2 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.zh_CN.xlf'), 3 => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src/Resources/translations/SonataDatagridBundle.zh_CN.xliff'), 4 => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.zh_CN.xliff'), 5 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.zh_CN.xliff')], 'zh_TW' => [0 => ($this->targetDirs[3].'/vendor/symfony/validator/Resources/translations/validators.zh_TW.xlf')], 'pt_PT' => [0 => ($this->targetDirs[3].'/vendor/symfony/security/Core/Resources/translations/security.pt_PT.xlf')], 'sv_SE' => [0 => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src/Resources/translations/SonataAdminBundle.sv_SE.xliff')]]]);

        $instance->setConfigCacheFactory(${($_ = isset($this->services['config_cache_factory']) ? $this->services['config_cache_factory'] : ($this->services['config_cache_factory'] = new \Symfony\Component\Config\ResourceCheckerConfigCacheFactory())) && false ?: '_'});
        $instance->setFallbackLocales([0 => 'en', 1 => 'en']);

        return $instance;
    }

    /*
     * Gets the private 'translator_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\TranslatorListener
     */
    protected function getTranslatorListenerService()
    {
        return $this->services['translator_listener'] = new \Symfony\Component\HttpKernel\EventListener\TranslatorListener(${($_ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->getTranslator_DefaultService()) && false ?: '_'}, ${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())) && false ?: '_'});
    }

    /*
     * Gets the private 'validate_request_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener
     */
    protected function getValidateRequestListenerService()
    {
        return $this->services['validate_request_listener'] = new \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener();
    }

    public function getParameter($name)
    {
        $name = (string) $name;
        if (isset($this->buildParameters[$name])) {
            return $this->buildParameters[$name];
        }
        if (!(isset($this->parameters[$name]) || isset($this->loadedDynamicParameters[$name]) || array_key_exists($name, $this->parameters))) {
            $name = $this->normalizeParameterName($name);

            if (!(isset($this->parameters[$name]) || isset($this->loadedDynamicParameters[$name]) || array_key_exists($name, $this->parameters))) {
                throw new InvalidArgumentException(sprintf('The parameter "%s" must be defined.', $name));
            }
        }
        if (isset($this->loadedDynamicParameters[$name])) {
            return $this->loadedDynamicParameters[$name] ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
        }

        return $this->parameters[$name];
    }

    public function hasParameter($name)
    {
        $name = (string) $name;
        if (isset($this->buildParameters[$name])) {
            return true;
        }
        $name = $this->normalizeParameterName($name);

        return isset($this->parameters[$name]) || isset($this->loadedDynamicParameters[$name]) || array_key_exists($name, $this->parameters);
    }

    public function setParameter($name, $value)
    {
        throw new LogicException('Impossible to call set() on a frozen ParameterBag.');
    }

    public function getParameterBag()
    {
        if (null === $this->parameterBag) {
            $parameters = $this->parameters;
            foreach ($this->loadedDynamicParameters as $name => $loaded) {
                $parameters[$name] = $loaded ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
            }
            foreach ($this->buildParameters as $name => $value) {
                $parameters[$name] = $value;
            }
            $this->parameterBag = new FrozenParameterBag($parameters);
        }

        return $this->parameterBag;
    }

    private $loadedDynamicParameters = [
        'kernel.root_dir' => false,
        'kernel.project_dir' => false,
        'kernel.cache_dir' => false,
        'kernel.logs_dir' => false,
        'kernel.bundles_metadata' => false,
        'kernel.secret' => false,
        'session.save_path' => false,
        'validator.mapping.cache.file' => false,
        'translator.default_path' => false,
        'twig.default_path' => false,
        'doctrine.orm.proxy_dir' => false,
        'doctrine_migrations.dir_name' => false,
    ];
    private $dynamicParameters = [];

    /*
     * Computes a dynamic parameter.
     *
     * @param string $name The name of the dynamic parameter to load
     *
     * @return mixed The value of the dynamic parameter
     *
     * @throws InvalidArgumentException When the dynamic parameter does not exist
     */
    private function getDynamicParameter($name)
    {
        switch ($name) {
            case 'kernel.root_dir': $value = ($this->targetDirs[3].'/src'); break;
            case 'kernel.project_dir': $value = $this->targetDirs[3]; break;
            case 'kernel.cache_dir': $value = $this->targetDirs[0]; break;
            case 'kernel.logs_dir': $value = ($this->targetDirs[2].'/log'); break;
            case 'kernel.bundles_metadata': $value = [
                'FrameworkBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/framework-bundle'),
                    'namespace' => 'Symfony\\Bundle\\FrameworkBundle',
                ],
                'TwigBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/twig-bundle'),
                    'namespace' => 'Symfony\\Bundle\\TwigBundle',
                ],
                'SecurityBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/security-bundle'),
                    'namespace' => 'Symfony\\Bundle\\SecurityBundle',
                ],
                'SonataDatagridBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sonata-project/datagrid-bundle/src'),
                    'namespace' => 'Sonata\\DatagridBundle',
                ],
                'SonataCoreBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sonata-project/core-bundle/src/CoreBundle'),
                    'namespace' => 'Sonata\\CoreBundle',
                ],
                'SonataBlockBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sonata-project/block-bundle/src'),
                    'namespace' => 'Sonata\\BlockBundle',
                ],
                'KnpMenuBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/knplabs/knp-menu-bundle/src'),
                    'namespace' => 'Knp\\Bundle\\MenuBundle',
                ],
                'SonataAdminBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sonata-project/admin-bundle/src'),
                    'namespace' => 'Sonata\\AdminBundle',
                ],
                'KinulabSonataGentellelaThemeBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/kinulab/sonata-gentellela-theme-bundle'),
                    'namespace' => 'Kinulab\\SonataGentellelaThemeBundle',
                ],
                'DoctrineCacheBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/doctrine/doctrine-cache-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\DoctrineCacheBundle',
                ],
                'DoctrineBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/doctrine/doctrine-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\DoctrineBundle',
                ],
                'DoctrineMigrationsBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/doctrine/doctrine-migrations-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\MigrationsBundle',
                ],
                'SonataDoctrineORMAdminBundle' => [
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sonata-project/doctrine-orm-admin-bundle/src'),
                    'namespace' => 'Sonata\\DoctrineORMAdminBundle',
                ],
            ]; break;
            case 'kernel.secret': $value = $this->getEnv('APP_SECRET'); break;
            case 'session.save_path': $value = ($this->targetDirs[0].'/sessions'); break;
            case 'validator.mapping.cache.file': $value = ($this->targetDirs[0].'/validation.php'); break;
            case 'translator.default_path': $value = ($this->targetDirs[3].'/translations'); break;
            case 'twig.default_path': $value = ($this->targetDirs[3].'/templates'); break;
            case 'doctrine.orm.proxy_dir': $value = ($this->targetDirs[0].'/doctrine/orm/Proxies'); break;
            case 'doctrine_migrations.dir_name': $value = ($this->targetDirs[3].'/src/Migrations'); break;
            default: throw new InvalidArgumentException(sprintf('The dynamic parameter "%s" must be defined.', $name));
        }
        $this->loadedDynamicParameters[$name] = true;

        return $this->dynamicParameters[$name] = $value;
    }

    private $normalizedParameterNames = [];

    private function normalizeParameterName($name)
    {
        if (isset($this->normalizedParameterNames[$normalizedName = strtolower($name)]) || isset($this->parameters[$normalizedName]) || array_key_exists($normalizedName, $this->parameters)) {
            $normalizedName = isset($this->normalizedParameterNames[$normalizedName]) ? $this->normalizedParameterNames[$normalizedName] : $normalizedName;
            if ((string) $name !== $normalizedName) {
                @trigger_error(sprintf('Parameter names will be made case sensitive in Symfony 4.0. Using "%s" instead of "%s" is deprecated since Symfony 3.4.', $name, $normalizedName), E_USER_DEPRECATED);
            }
        } else {
            $normalizedName = $this->normalizedParameterNames[$normalizedName] = (string) $name;
        }

        return $normalizedName;
    }

    /*
     * Gets the default parameters.
     *
     * @return array An array of the default parameters
     */
    protected function getDefaultParameters()
    {
        return [
            'kernel.environment' => 'prod',
            'kernel.debug' => false,
            'kernel.name' => 'src',
            'kernel.bundles' => [
                'FrameworkBundle' => 'Symfony\\Bundle\\FrameworkBundle\\FrameworkBundle',
                'TwigBundle' => 'Symfony\\Bundle\\TwigBundle\\TwigBundle',
                'SecurityBundle' => 'Symfony\\Bundle\\SecurityBundle\\SecurityBundle',
                'SonataDatagridBundle' => 'Sonata\\DatagridBundle\\SonataDatagridBundle',
                'SonataCoreBundle' => 'Sonata\\CoreBundle\\SonataCoreBundle',
                'SonataBlockBundle' => 'Sonata\\BlockBundle\\SonataBlockBundle',
                'KnpMenuBundle' => 'Knp\\Bundle\\MenuBundle\\KnpMenuBundle',
                'SonataAdminBundle' => 'Sonata\\AdminBundle\\SonataAdminBundle',
                'KinulabSonataGentellelaThemeBundle' => 'Kinulab\\SonataGentellelaThemeBundle\\KinulabSonataGentellelaThemeBundle',
                'DoctrineCacheBundle' => 'Doctrine\\Bundle\\DoctrineCacheBundle\\DoctrineCacheBundle',
                'DoctrineBundle' => 'Doctrine\\Bundle\\DoctrineBundle\\DoctrineBundle',
                'DoctrineMigrationsBundle' => 'Doctrine\\Bundle\\MigrationsBundle\\DoctrineMigrationsBundle',
                'SonataDoctrineORMAdminBundle' => 'Sonata\\DoctrineORMAdminBundle\\SonataDoctrineORMAdminBundle',
            ],
            'kernel.charset' => 'UTF-8',
            'kernel.container_class' => 'srcProdProjectContainer',
            'container.autowiring.strict_mode' => true,
            'container.dumper.inline_class_loader' => true,
            'locale' => 'en',
            'fragment.renderer.hinclude.global_template' => '',
            'fragment.path' => '/_fragment',
            'kernel.http_method_override' => true,
            'kernel.trusted_hosts' => [

            ],
            'kernel.default_locale' => 'en',
            'templating.helper.code.file_link_format' => NULL,
            'debug.file_link_format' => NULL,
            'session.metadata.storage_key' => '_sf2_meta',
            'session.storage.options' => [
                'cache_limiter' => '0',
                'cookie_httponly' => true,
                'gc_probability' => 1,
                'use_strict_mode' => true,
            ],
            'session.metadata.update_threshold' => '0',
            'form.type_extension.csrf.enabled' => true,
            'form.type_extension.csrf.field_name' => '_token',
            'asset.request_context.base_path' => '',
            'asset.request_context.secure' => false,
            'validator.mapping.cache.prefix' => '',
            'validator.translation_domain' => 'validators',
            'translator.logging' => false,
            'data_collector.templates' => [

            ],
            'debug.error_handler.throw_at' => 0,
            'router.options.generator_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator',
            'router.options.generator_base_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator',
            'router.options.generator_dumper_class' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\PhpGeneratorDumper',
            'router.options.matcher_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher',
            'router.options.matcher_base_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher',
            'router.options.matcher_dumper_class' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\PhpMatcherDumper',
            'router.options.matcher.cache_class' => 'srcProdProjectContainerUrlMatcher',
            'router.options.generator.cache_class' => 'srcProdProjectContainerUrlGenerator',
            'router.request_context.host' => 'localhost',
            'router.request_context.scheme' => 'http',
            'router.request_context.base_url' => '',
            'router.resource' => 'kernel:loadRoutes',
            'router.cache_class_prefix' => 'srcProdProjectContainer',
            'request_listener.http_port' => 80,
            'request_listener.https_port' => 443,
            'twig.exception_listener.controller' => 'twig.controller.exception:showAction',
            'twig.form.resources' => [
                0 => 'form_div_layout.html.twig',
            ],
            'security.authentication.trust_resolver.anonymous_class' => 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\AnonymousToken',
            'security.authentication.trust_resolver.rememberme_class' => 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\RememberMeToken',
            'security.role_hierarchy.roles' => [

            ],
            'security.access.denied_url' => NULL,
            'security.authentication.manager.erase_credentials' => true,
            'security.authentication.session_strategy.strategy' => 'migrate',
            'security.access.always_authenticate_before_granting' => false,
            'security.authentication.hide_user_not_found' => true,
            'sonata.core.flashmessage.manager.class' => 'Sonata\\Twig\\FlashMessage\\FlashManager',
            'sonata.core.twig.extension.flashmessage.class' => 'Sonata\\Twig\\Extension\\FlashMessageExtension',
            'sonata.core.form_type' => 'standard',
            'sonata.block.service.container.class' => 'Sonata\\BlockBundle\\Block\\Service\\ContainerBlockService',
            'sonata.block.service.empty.class' => 'Sonata\\BlockBundle\\Block\\Service\\EmptyBlockService',
            'sonata.block.service.text.class' => 'Sonata\\BlockBundle\\Block\\Service\\TextBlockService',
            'sonata.block.service.rss.class' => 'Sonata\\BlockBundle\\Block\\Service\\RssBlockService',
            'sonata.block.service.menu.class' => 'Sonata\\BlockBundle\\Block\\Service\\MenuBlockService',
            'sonata.block.service.template.class' => 'Sonata\\BlockBundle\\Block\\Service\\TemplateBlockService',
            'sonata.block.exception.strategy.manager.class' => 'Sonata\\BlockBundle\\Exception\\Strategy\\StrategyManager',
            'sonata.block.container.types' => [
                0 => 'sonata.block.service.container',
                1 => 'sonata.page.block.container',
                2 => 'sonata.dashboard.block.container',
                3 => 'cmf.block.container',
                4 => 'cmf.block.slideshow',
            ],
            'sonata_block.blocks' => [
                'sonata.admin.block.admin_list' => [
                    'contexts' => [
                        0 => 'admin',
                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                    'settings' => [

                    ],
                ],
            ],
            'sonata_block.blocks_by_class' => [

            ],
            'sonata_blocks.block_types' => [
                0 => 'sonata.admin.block.admin_list',
            ],
            'sonata_block.cache_blocks' => [
                'by_type' => [
                    'sonata.admin.block.admin_list' => 'sonata.cache.noop',
                ],
            ],
            'knp_menu.factory.class' => 'Knp\\Menu\\MenuFactory',
            'knp_menu.factory_extension.routing.class' => 'Knp\\Menu\\Integration\\Symfony\\RoutingExtension',
            'knp_menu.helper.class' => 'Knp\\Menu\\Twig\\Helper',
            'knp_menu.matcher.class' => 'Knp\\Menu\\Matcher\\Matcher',
            'knp_menu.menu_provider.chain.class' => 'Knp\\Menu\\Provider\\ChainProvider',
            'knp_menu.menu_provider.container_aware.class' => 'Knp\\Bundle\\MenuBundle\\Provider\\ContainerAwareProvider',
            'knp_menu.menu_provider.builder_alias.class' => 'Knp\\Bundle\\MenuBundle\\Provider\\BuilderAliasProvider',
            'knp_menu.renderer_provider.class' => 'Knp\\Bundle\\MenuBundle\\Renderer\\ContainerAwareProvider',
            'knp_menu.renderer.list.class' => 'Knp\\Menu\\Renderer\\ListRenderer',
            'knp_menu.renderer.list.options' => [

            ],
            'knp_menu.listener.voters.class' => 'Knp\\Bundle\\MenuBundle\\EventListener\\VoterInitializerListener',
            'knp_menu.voter.router.class' => 'Knp\\Menu\\Matcher\\Voter\\RouteVoter',
            'knp_menu.twig.extension.class' => 'Knp\\Menu\\Twig\\MenuExtension',
            'knp_menu.renderer.twig.class' => 'Knp\\Menu\\Renderer\\TwigRenderer',
            'knp_menu.renderer.twig.options' => [

            ],
            'knp_menu.renderer.twig.template' => '@KnpMenu/menu.html.twig',
            'knp_menu.default_renderer' => 'twig',
            'sonata.admin.twig.extension.x_editable_type_mapping' => [
                'choice' => 'select',
                'boolean' => 'select',
                'text' => 'text',
                'textarea' => 'textarea',
                'html' => 'textarea',
                'email' => 'email',
                'string' => 'text',
                'smallint' => 'text',
                'bigint' => 'text',
                'integer' => 'number',
                'decimal' => 'number',
                'currency' => 'number',
                'percent' => 'number',
                'url' => 'url',
                'date' => 'date',
            ],
            'sonata.admin.configuration.global_search.empty_boxes' => 'show',
            'sonata.admin.configuration.templates' => [
                'user_block' => '@KinulabSonataGentellelaTheme/Core/user_block.html.twig',
                'add_block' => '@KinulabSonataGentellelaTheme/Core/add_block.html.twig',
                'layout' => '@KinulabSonataGentellelaTheme/standard_layout.html.twig',
                'ajax' => '@KinulabSonataGentellelaTheme/ajax_layout.html.twig',
                'dashboard' => '@KinulabSonataGentellelaTheme/Core/dashboard.html.twig',
                'search' => '@KinulabSonataGentellelaTheme/Core/search.html.twig',
                'list' => '@KinulabSonataGentellelaTheme/CRUD/list.html.twig',
                'filter' => '@KinulabSonataGentellelaTheme/Form/filter_admin_fields.html.twig',
                'show' => '@KinulabSonataGentellelaTheme/CRUD/show.html.twig',
                'show_compare' => '@KinulabSonataGentellelaTheme/CRUD/show_compare.html.twig',
                'edit' => '@KinulabSonataGentellelaTheme/CRUD/edit.html.twig',
                'history' => '@KinulabSonataGentellelaTheme/CRUD/history.html.twig',
                'history_revision_timestamp' => '@KinulabSonataGentellelaTheme/CRUD/history_revision_timestamp.html.twig',
                'acl' => '@KinulabSonataGentellelaTheme/CRUD/acl.html.twig',
                'action' => '@KinulabSonataGentellelaTheme/CRUD/action.html.twig',
                'short_object_description' => '@KinulabSonataGentellelaTheme/Helper/short-object-description.html.twig',
                'preview' => '@KinulabSonataGentellelaTheme/CRUD/preview.html.twig',
                'list_block' => '@KinulabSonataGentellelaTheme/Block/block_admin_list.html.twig',
                'delete' => '@KinulabSonataGentellelaTheme/CRUD/delete.html.twig',
                'batch' => '@KinulabSonataGentellelaTheme/CRUD/list__batch.html.twig',
                'select' => '@KinulabSonataGentellelaTheme/CRUD/list__select.html.twig',
                'batch_confirmation' => '@KinulabSonataGentellelaTheme/CRUD/batch_confirmation.html.twig',
                'inner_list_row' => '@KinulabSonataGentellelaTheme/CRUD/list_inner_row.html.twig',
                'base_list_field' => '@KinulabSonataGentellelaTheme/CRUD/base_list_field.html.twig',
                'pager_links' => '@KinulabSonataGentellelaTheme/Pager/links.html.twig',
                'pager_results' => '@KinulabSonataGentellelaTheme/Pager/results.html.twig',
                'tab_menu_template' => '@KinulabSonataGentellelaTheme/Core/tab_menu_template.html.twig',
                'knp_menu_template' => '@KinulabSonataGentellelaTheme/Menu/sonata_menu.html.twig',
                'search_result_block' => '@KinulabSonataGentellelaTheme/Block/block_search_result.html.twig',
                'outer_list_rows_mosaic' => '@KinulabSonataGentellelaTheme/CRUD/list_outer_rows_mosaic.html.twig',
                'outer_list_rows_list' => '@KinulabSonataGentellelaTheme/CRUD/list_outer_rows_list.html.twig',
                'outer_list_rows_tree' => '@KinulabSonataGentellelaTheme/CRUD/list_outer_rows_tree.html.twig',
                'button_acl' => '@KinulabSonataGentellelaTheme/Button/acl_button.html.twig',
                'button_create' => '@KinulabSonataGentellelaTheme/Button/create_button.html.twig',
                'button_edit' => '@KinulabSonataGentellelaTheme/Button/edit_button.html.twig',
                'button_history' => '@KinulabSonataGentellelaTheme/Button/history_button.html.twig',
                'button_list' => '@KinulabSonataGentellelaTheme/Button/list_button.html.twig',
                'button_show' => '@KinulabSonataGentellelaTheme/Button/show_button.html.twig',
                'action_create' => '@SonataAdmin/CRUD/dashboard__action_create.html.twig',
            ],
            'sonata.admin.configuration.admin_services' => [

            ],
            'sonata.admin.configuration.dashboard_groups' => [

            ],
            'sonata.admin.configuration.dashboard_blocks' => [
                0 => [
                    'type' => 'sonata.admin.block.admin_list',
                    'position' => 'left',
                    'roles' => [

                    ],
                    'settings' => [

                    ],
                    'class' => 'col-md-4',
                ],
            ],
            'sonata.admin.configuration.sort_admins' => false,
            'sonata.admin.configuration.breadcrumbs' => [
                'child_admin_route' => 'edit',
            ],
            'sonata.admin.security.acl_user_manager' => NULL,
            'sonata.admin.configuration.security.role_admin' => 'ROLE_SONATA_ADMIN',
            'sonata.admin.configuration.security.role_super_admin' => 'ROLE_SUPER_ADMIN',
            'sonata.admin.configuration.security.information' => [

            ],
            'sonata.admin.configuration.security.admin_permissions' => [
                0 => 'CREATE',
                1 => 'LIST',
                2 => 'DELETE',
                3 => 'UNDELETE',
                4 => 'EXPORT',
                5 => 'OPERATOR',
                6 => 'MASTER',
            ],
            'sonata.admin.configuration.security.object_permissions' => [
                0 => 'VIEW',
                1 => 'EDIT',
                2 => 'DELETE',
                3 => 'UNDELETE',
                4 => 'OPERATOR',
                5 => 'MASTER',
                6 => 'OWNER',
            ],
            'sonata.admin.security.handler.noop.class' => 'Sonata\\AdminBundle\\Security\\Handler\\NoopSecurityHandler',
            'sonata.admin.security.handler.role.class' => 'Sonata\\AdminBundle\\Security\\Handler\\RoleSecurityHandler',
            'sonata.admin.security.handler.acl.class' => 'Sonata\\AdminBundle\\Security\\Handler\\AclSecurityHandler',
            'sonata.admin.security.mask.builder.class' => 'Sonata\\AdminBundle\\Security\\Acl\\Permission\\MaskBuilder',
            'sonata.admin.manipulator.acl.admin.class' => 'Sonata\\AdminBundle\\Util\\AdminAclManipulator',
            'sonata.admin.object.manipulator.acl.admin.class' => 'Sonata\\AdminBundle\\Util\\AdminObjectAclManipulator',
            'sonata.admin.extension.map' => [
                'admins' => [

                ],
                'excludes' => [

                ],
                'implements' => [

                ],
                'extends' => [

                ],
                'instanceof' => [

                ],
                'uses' => [

                ],
            ],
            'sonata.admin.configuration.filters.persist' => false,
            'sonata.admin.configuration.filters.persister' => 'sonata.admin.filter_persister.session',
            'sonata.admin.configuration.show.mosaic.button' => true,
            'sonata.admin.configuration.translate_group_label' => false,
            'doctrine_cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine_cache.apcu.class' => 'Doctrine\\Common\\Cache\\ApcuCache',
            'doctrine_cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine_cache.chain.class' => 'Doctrine\\Common\\Cache\\ChainCache',
            'doctrine_cache.couchbase.class' => 'Doctrine\\Common\\Cache\\CouchbaseCache',
            'doctrine_cache.couchbase.connection.class' => 'Couchbase',
            'doctrine_cache.couchbase.hostnames' => 'localhost:8091',
            'doctrine_cache.file_system.class' => 'Doctrine\\Common\\Cache\\FilesystemCache',
            'doctrine_cache.php_file.class' => 'Doctrine\\Common\\Cache\\PhpFileCache',
            'doctrine_cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine_cache.memcache.connection.class' => 'Memcache',
            'doctrine_cache.memcache.host' => 'localhost',
            'doctrine_cache.memcache.port' => 11211,
            'doctrine_cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine_cache.memcached.connection.class' => 'Memcached',
            'doctrine_cache.memcached.host' => 'localhost',
            'doctrine_cache.memcached.port' => 11211,
            'doctrine_cache.mongodb.class' => 'Doctrine\\Common\\Cache\\MongoDBCache',
            'doctrine_cache.mongodb.collection.class' => 'MongoCollection',
            'doctrine_cache.mongodb.connection.class' => 'MongoClient',
            'doctrine_cache.mongodb.server' => 'localhost:27017',
            'doctrine_cache.predis.client.class' => 'Predis\\Client',
            'doctrine_cache.predis.scheme' => 'tcp',
            'doctrine_cache.predis.host' => 'localhost',
            'doctrine_cache.predis.port' => 6379,
            'doctrine_cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine_cache.redis.connection.class' => 'Redis',
            'doctrine_cache.redis.host' => 'localhost',
            'doctrine_cache.redis.port' => 6379,
            'doctrine_cache.riak.class' => 'Doctrine\\Common\\Cache\\RiakCache',
            'doctrine_cache.riak.bucket.class' => 'Riak\\Bucket',
            'doctrine_cache.riak.connection.class' => 'Riak\\Connection',
            'doctrine_cache.riak.bucket_property_list.class' => 'Riak\\BucketPropertyList',
            'doctrine_cache.riak.host' => 'localhost',
            'doctrine_cache.riak.port' => 8087,
            'doctrine_cache.sqlite3.class' => 'Doctrine\\Common\\Cache\\SQLite3Cache',
            'doctrine_cache.sqlite3.connection.class' => 'SQLite3',
            'doctrine_cache.void.class' => 'Doctrine\\Common\\Cache\\VoidCache',
            'doctrine_cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine_cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine_cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine_cache.security.acl.cache.class' => 'Doctrine\\Bundle\\DoctrineCacheBundle\\Acl\\Model\\AclCache',
            'doctrine.dbal.logger.chain.class' => 'Doctrine\\DBAL\\Logging\\LoggerChain',
            'doctrine.dbal.logger.profiling.class' => 'Doctrine\\DBAL\\Logging\\DebugStack',
            'doctrine.dbal.logger.class' => 'Symfony\\Bridge\\Doctrine\\Logger\\DbalLogger',
            'doctrine.dbal.configuration.class' => 'Doctrine\\DBAL\\Configuration',
            'doctrine.data_collector.class' => 'Doctrine\\Bundle\\DoctrineBundle\\DataCollector\\DoctrineDataCollector',
            'doctrine.dbal.connection.event_manager.class' => 'Symfony\\Bridge\\Doctrine\\ContainerAwareEventManager',
            'doctrine.dbal.connection_factory.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ConnectionFactory',
            'doctrine.dbal.events.mysql_session_init.class' => 'Doctrine\\DBAL\\Event\\Listeners\\MysqlSessionInit',
            'doctrine.dbal.events.oracle_session_init.class' => 'Doctrine\\DBAL\\Event\\Listeners\\OracleSessionInit',
            'doctrine.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Registry',
            'doctrine.entity_managers' => [
                'default' => 'doctrine.orm.default_entity_manager',
            ],
            'doctrine.default_entity_manager' => 'default',
            'doctrine.dbal.connection_factory.types' => [

            ],
            'doctrine.connections' => [
                'default' => 'doctrine.dbal.default_connection',
            ],
            'doctrine.default_connection' => 'default',
            'doctrine.orm.configuration.class' => 'Doctrine\\ORM\\Configuration',
            'doctrine.orm.entity_manager.class' => 'Doctrine\\ORM\\EntityManager',
            'doctrine.orm.manager_configurator.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ManagerConfigurator',
            'doctrine.orm.cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine.orm.cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine.orm.cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine.orm.cache.memcache_host' => 'localhost',
            'doctrine.orm.cache.memcache_port' => 11211,
            'doctrine.orm.cache.memcache_instance.class' => 'Memcache',
            'doctrine.orm.cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine.orm.cache.memcached_host' => 'localhost',
            'doctrine.orm.cache.memcached_port' => 11211,
            'doctrine.orm.cache.memcached_instance.class' => 'Memcached',
            'doctrine.orm.cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine.orm.cache.redis_host' => 'localhost',
            'doctrine.orm.cache.redis_port' => 6379,
            'doctrine.orm.cache.redis_instance.class' => 'Redis',
            'doctrine.orm.cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine.orm.cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine.orm.cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine.orm.metadata.driver_chain.class' => 'Doctrine\\Common\\Persistence\\Mapping\\Driver\\MappingDriverChain',
            'doctrine.orm.metadata.annotation.class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
            'doctrine.orm.metadata.xml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedXmlDriver',
            'doctrine.orm.metadata.yml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedYamlDriver',
            'doctrine.orm.metadata.php.class' => 'Doctrine\\ORM\\Mapping\\Driver\\PHPDriver',
            'doctrine.orm.metadata.staticphp.class' => 'Doctrine\\ORM\\Mapping\\Driver\\StaticPHPDriver',
            'doctrine.orm.proxy_cache_warmer.class' => 'Symfony\\Bridge\\Doctrine\\CacheWarmer\\ProxyCacheWarmer',
            'form.type_guesser.doctrine.class' => 'Symfony\\Bridge\\Doctrine\\Form\\DoctrineOrmTypeGuesser',
            'doctrine.orm.validator.unique.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\Constraints\\UniqueEntityValidator',
            'doctrine.orm.validator_initializer.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\DoctrineInitializer',
            'doctrine.orm.security.user.provider.class' => 'Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider',
            'doctrine.orm.listeners.resolve_target_entity.class' => 'Doctrine\\ORM\\Tools\\ResolveTargetEntityListener',
            'doctrine.orm.listeners.attach_entity_listeners.class' => 'Doctrine\\ORM\\Tools\\AttachEntityListenersListener',
            'doctrine.orm.naming_strategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultNamingStrategy',
            'doctrine.orm.naming_strategy.underscore.class' => 'Doctrine\\ORM\\Mapping\\UnderscoreNamingStrategy',
            'doctrine.orm.quote_strategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultQuoteStrategy',
            'doctrine.orm.quote_strategy.ansi.class' => 'Doctrine\\ORM\\Mapping\\AnsiQuoteStrategy',
            'doctrine.orm.entity_listener_resolver.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Mapping\\ContainerEntityListenerResolver',
            'doctrine.orm.second_level_cache.default_cache_factory.class' => 'Doctrine\\ORM\\Cache\\DefaultCacheFactory',
            'doctrine.orm.second_level_cache.default_region.class' => 'Doctrine\\ORM\\Cache\\Region\\DefaultRegion',
            'doctrine.orm.second_level_cache.filelock_region.class' => 'Doctrine\\ORM\\Cache\\Region\\FileLockRegion',
            'doctrine.orm.second_level_cache.logger_chain.class' => 'Doctrine\\ORM\\Cache\\Logging\\CacheLoggerChain',
            'doctrine.orm.second_level_cache.logger_statistics.class' => 'Doctrine\\ORM\\Cache\\Logging\\StatisticsCacheLogger',
            'doctrine.orm.second_level_cache.cache_configuration.class' => 'Doctrine\\ORM\\Cache\\CacheConfiguration',
            'doctrine.orm.second_level_cache.regions_configuration.class' => 'Doctrine\\ORM\\Cache\\RegionsConfiguration',
            'doctrine.orm.auto_generate_proxy_classes' => false,
            'doctrine.orm.proxy_namespace' => 'Proxies',
            'doctrine_migrations.namespace' => 'DoctrineMigrations',
            'doctrine_migrations.table_name' => 'migration_versions',
            'doctrine_migrations.column_name' => 'version',
            'doctrine_migrations.column_length' => 14,
            'doctrine_migrations.executed_at_column_name' => 'executed_at',
            'doctrine_migrations.all_or_nothing' => false,
            'doctrine_migrations.name' => 'Application Migrations',
            'doctrine_migrations.custom_template' => NULL,
            'doctrine_migrations.organize_migrations' => false,
            'sonata.admin.manipulator.acl.object.orm.class' => 'Sonata\\DoctrineORMAdminBundle\\Util\\ObjectAclManipulator',
            'sonata_doctrine_orm_admin.entity_manager' => NULL,
            'sonata_doctrine_orm_admin.templates' => [
                'types' => [
                    'list' => [
                        'array' => '@SonataAdmin/CRUD/list_array.html.twig',
                        'boolean' => '@SonataAdmin/CRUD/list_boolean.html.twig',
                        'date' => '@SonataAdmin/CRUD/list_date.html.twig',
                        'time' => '@SonataAdmin/CRUD/list_time.html.twig',
                        'datetime' => '@SonataAdmin/CRUD/list_datetime.html.twig',
                        'text' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'textarea' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'email' => '@SonataAdmin/CRUD/list_email.html.twig',
                        'trans' => '@SonataAdmin/CRUD/list_trans.html.twig',
                        'string' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'smallint' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'bigint' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'integer' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'decimal' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'identifier' => '@SonataAdmin/CRUD/list_string.html.twig',
                        'currency' => '@SonataAdmin/CRUD/list_currency.html.twig',
                        'percent' => '@SonataAdmin/CRUD/list_percent.html.twig',
                        'choice' => '@SonataAdmin/CRUD/list_choice.html.twig',
                        'url' => '@SonataAdmin/CRUD/list_url.html.twig',
                        'html' => '@SonataAdmin/CRUD/list_html.html.twig',
                    ],
                    'show' => [
                        'array' => '@SonataAdmin/CRUD/show_array.html.twig',
                        'boolean' => '@SonataAdmin/CRUD/show_boolean.html.twig',
                        'date' => '@SonataAdmin/CRUD/show_date.html.twig',
                        'time' => '@SonataAdmin/CRUD/show_time.html.twig',
                        'datetime' => '@SonataAdmin/CRUD/show_datetime.html.twig',
                        'text' => '@SonataAdmin/CRUD/base_show_field.html.twig',
                        'email' => '@SonataAdmin/CRUD/show_email.html.twig',
                        'trans' => '@SonataAdmin/CRUD/show_trans.html.twig',
                        'string' => '@SonataAdmin/CRUD/base_show_field.html.twig',
                        'smallint' => '@SonataAdmin/CRUD/base_show_field.html.twig',
                        'bigint' => '@SonataAdmin/CRUD/base_show_field.html.twig',
                        'integer' => '@SonataAdmin/CRUD/base_show_field.html.twig',
                        'decimal' => '@SonataAdmin/CRUD/base_show_field.html.twig',
                        'currency' => '@SonataAdmin/CRUD/show_currency.html.twig',
                        'percent' => '@SonataAdmin/CRUD/show_percent.html.twig',
                        'choice' => '@SonataAdmin/CRUD/show_choice.html.twig',
                        'url' => '@SonataAdmin/CRUD/show_url.html.twig',
                        'html' => '@SonataAdmin/CRUD/show_html.html.twig',
                    ],
                ],
                'form' => [
                    0 => '@SonataDoctrineORMAdmin/Form/form_admin_fields.html.twig',
                ],
                'filter' => [
                    0 => '@SonataDoctrineORMAdmin/Form/filter_admin_fields.html.twig',
                ],
            ],
            'sonata.core.form.types' => [
                0 => 'form.type.form',
                1 => 'form.type.choice',
                2 => 'form.type.file',
                3 => 'sonata.core.form.type.array_legacy',
                4 => 'sonata.core.form.type.boolean_legacy',
                5 => 'sonata.core.form.type.collection_legacy',
                6 => 'sonata.core.form.type.translatable_choice',
                7 => 'sonata.core.form.type.date_range_legacy',
                8 => 'sonata.core.form.type.datetime_range_legacy',
                9 => 'sonata.core.form.type.date_picker_legacy',
                10 => 'sonata.core.form.type.datetime_picker_legacy',
                11 => 'sonata.core.form.type.date_range_picker_legacy',
                12 => 'sonata.core.form.type.datetime_range_picker_legacy',
                13 => 'sonata.core.form.type.equal_legacy',
                14 => 'sonata.core.form.type.color_selector',
                15 => 'sonata.core.form.type.color_legacy',
                16 => 'sonata.core.form.type.array',
                17 => 'sonata.core.form.type.boolean',
                18 => 'sonata.core.form.type.collection',
                19 => 'sonata.core.form.type.date_range',
                20 => 'sonata.core.form.type.datetime_range',
                21 => 'sonata.core.form.type.date_picker',
                22 => 'sonata.core.form.type.datetime_picker',
                23 => 'sonata.core.form.type.date_range_picker',
                24 => 'sonata.core.form.type.datetime_range_picker',
                25 => 'sonata.core.form.type.equal',
                26 => 'sonata.block.form.type.block',
                27 => 'sonata.block.form.type.container_template',
                28 => 'sonata.admin.form.type.admin',
                29 => 'sonata.admin.form.type.model_choice',
                30 => 'sonata.admin.form.type.model_list',
                31 => 'sonata.admin.form.type.model_reference',
                32 => 'sonata.admin.form.type.model_hidden',
                33 => 'sonata.admin.form.type.model_autocomplete',
                34 => 'sonata.admin.form.type.collection',
                35 => 'sonata.admin.doctrine_orm.form.type.choice_field_mask',
                36 => 'sonata.admin.form.filter.type.number',
                37 => 'sonata.admin.form.filter.type.choice',
                38 => 'sonata.admin.form.filter.type.default',
                39 => 'sonata.admin.form.filter.type.date',
                40 => 'sonata.admin.form.filter.type.daterange',
                41 => 'sonata.admin.form.filter.type.datetime',
                42 => 'sonata.admin.form.filter.type.datetime_range',
                43 => 'form.type.entity',
            ],
            'sonata.core.form.type_extensions' => [
                0 => 'form.type_extension.form.transformation_failure_handling',
                1 => 'form.type_extension.form.http_foundation',
                2 => 'form.type_extension.form.validator',
                3 => 'form.type_extension.repeated.validator',
                4 => 'form.type_extension.submit.validator',
                5 => 'form.type_extension.upload.validator',
                6 => 'form.type_extension.csrf',
                7 => 'sonata.admin.form.extension.field',
                8 => 'sonata.admin.form.extension.field.mopa',
                9 => 'sonata.admin.form.extension.choice',
            ],
            'console.command.ids' => [
                'console.command.symfony_bundle_frameworkbundle_command_aboutcommand' => 'console.command.about',
                'console.command.symfony_bundle_frameworkbundle_command_assetsinstallcommand' => 'console.command.assets_install',
                'console.command.symfony_bundle_frameworkbundle_command_cacheclearcommand' => 'console.command.cache_clear',
                'console.command.symfony_bundle_frameworkbundle_command_cachepoolclearcommand' => 'console.command.cache_pool_clear',
                'console.command.symfony_bundle_frameworkbundle_command_cachepoolprunecommand' => 'console.command.cache_pool_prune',
                'console.command.symfony_bundle_frameworkbundle_command_cachewarmupcommand' => 'console.command.cache_warmup',
                'console.command.symfony_bundle_frameworkbundle_command_configdebugcommand' => 'console.command.config_debug',
                'console.command.symfony_bundle_frameworkbundle_command_configdumpreferencecommand' => 'console.command.config_dump_reference',
                'console.command.symfony_bundle_frameworkbundle_command_containerdebugcommand' => 'console.command.container_debug',
                'console.command.symfony_bundle_frameworkbundle_command_debugautowiringcommand' => 'console.command.debug_autowiring',
                'console.command.symfony_bundle_frameworkbundle_command_eventdispatcherdebugcommand' => 'console.command.event_dispatcher_debug',
                'console.command.symfony_bundle_frameworkbundle_command_routerdebugcommand' => 'console.command.router_debug',
                'console.command.symfony_bundle_frameworkbundle_command_routermatchcommand' => 'console.command.router_match',
                'console.command.symfony_bundle_frameworkbundle_command_translationdebugcommand' => 'console.command.translation_debug',
                'console.command.symfony_bundle_frameworkbundle_command_translationupdatecommand' => 'console.command.translation_update',
                'console.command.symfony_bundle_frameworkbundle_command_xlifflintcommand' => 'console.command.xliff_lint',
                'console.command.symfony_bundle_frameworkbundle_command_yamllintcommand' => 'console.command.yaml_lint',
                'console.command.symfony_component_form_command_debugcommand' => 'console.command.form_debug',
                'console.command.symfony_bridge_twig_command_debugcommand' => 'twig.command.debug',
                'console.command.symfony_bundle_twigbundle_command_lintcommand' => 'twig.command.lint',
                'console.command.symfony_bundle_securitybundle_command_userpasswordencodercommand' => 'security.command.user_password_encoder',
                'console.command.sonata_corebundle_command_sonatadumpdoctrinemetacommand' => 'console.command.sonata_corebundle_command_sonatadumpdoctrinemetacommand',
                'console.command.sonata_corebundle_command_sonatalistformmappingcommand' => 'console.command.sonata_corebundle_command_sonatalistformmappingcommand',
                'console.command.sonata_blockbundle_command_debugblockscommand' => 'Sonata\\BlockBundle\\Command\\DebugBlocksCommand',
                'console.command.sonata_adminbundle_command_createclasscachecommand' => 'Sonata\\AdminBundle\\Command\\CreateClassCacheCommand',
                'console.command.sonata_adminbundle_command_explainadmincommand' => 'Sonata\\AdminBundle\\Command\\ExplainAdminCommand',
                'console.command.sonata_adminbundle_command_generateadmincommand' => 'Sonata\\AdminBundle\\Command\\GenerateAdminCommand',
                'console.command.sonata_adminbundle_command_generateobjectaclcommand' => 'Sonata\\AdminBundle\\Command\\GenerateObjectAclCommand',
                'console.command.sonata_adminbundle_command_listadmincommand' => 'Sonata\\AdminBundle\\Command\\ListAdminCommand',
                'console.command.sonata_adminbundle_command_setupaclcommand' => 'Sonata\\AdminBundle\\Command\\SetupAclCommand',
                'console.command.doctrine_bundle_doctrinecachebundle_command_containscommand' => 'console.command.doctrine_bundle_doctrinecachebundle_command_containscommand',
                'console.command.doctrine_bundle_doctrinecachebundle_command_deletecommand' => 'console.command.doctrine_bundle_doctrinecachebundle_command_deletecommand',
                'console.command.doctrine_bundle_doctrinecachebundle_command_flushcommand' => 'console.command.doctrine_bundle_doctrinecachebundle_command_flushcommand',
                'console.command.doctrine_bundle_doctrinecachebundle_command_statscommand' => 'console.command.doctrine_bundle_doctrinecachebundle_command_statscommand',
                'console.command.doctrine_bundle_doctrinebundle_command_createdatabasedoctrinecommand' => 'doctrine.database_create_command',
                'console.command.doctrine_bundle_doctrinebundle_command_dropdatabasedoctrinecommand' => 'doctrine.database_drop_command',
                'console.command.doctrine_bundle_doctrinebundle_command_generateentitiesdoctrinecommand' => 'doctrine.generate_entities_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_runsqldoctrinecommand' => 'doctrine.query_sql_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_clearmetadatacachedoctrinecommand' => 'doctrine.cache_clear_metadata_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_clearquerycachedoctrinecommand' => 'doctrine.cache_clear_query_cache_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_clearresultcachedoctrinecommand' => 'doctrine.cache_clear_result_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_collectionregiondoctrinecommand' => 'doctrine.cache_collection_region_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_convertmappingdoctrinecommand' => 'doctrine.mapping_convert_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_createschemadoctrinecommand' => 'doctrine.schema_create_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_dropschemadoctrinecommand' => 'doctrine.schema_drop_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_ensureproductionsettingsdoctrinecommand' => 'doctrine.ensure_production_settings_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_entityregioncachedoctrinecommand' => 'doctrine.clear_entity_region_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_importdoctrinecommand' => 'doctrine.database_import_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_infodoctrinecommand' => 'doctrine.mapping_info_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_queryregioncachedoctrinecommand' => 'doctrine.clear_query_region_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_rundqldoctrinecommand' => 'doctrine.query_dql_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_updateschemadoctrinecommand' => 'doctrine.schema_update_command',
                'console.command.doctrine_bundle_doctrinebundle_command_proxy_validateschemacommand' => 'doctrine.schema_validate_command',
                'console.command.doctrine_bundle_doctrinebundle_command_importmappingdoctrinecommand' => 'doctrine.mapping_import_command',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsdiffdoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsdiffdoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsdumpschemadoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsdumpschemadoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsexecutedoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsexecutedoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsgeneratedoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsgeneratedoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationslatestdoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationslatestdoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsmigratedoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsmigratedoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsrollupdoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsrollupdoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsstatusdoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsstatusdoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsuptodatedoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsuptodatedoctrinecommand',
                'console.command.doctrine_bundle_migrationsbundle_command_migrationsversiondoctrinecommand' => 'console.command.doctrine_bundle_migrationsbundle_command_migrationsversiondoctrinecommand',
            ],
            'console.lazy_command.ids' => [
                'console.command.about' => true,
                'console.command.assets_install' => true,
                'console.command.cache_clear' => true,
                'console.command.cache_pool_clear' => true,
                'console.command.cache_pool_prune' => true,
                'console.command.cache_warmup' => true,
                'console.command.config_debug' => true,
                'console.command.config_dump_reference' => true,
                'console.command.container_debug' => true,
                'console.command.debug_autowiring' => true,
                'console.command.event_dispatcher_debug' => true,
                'console.command.router_debug' => true,
                'console.command.router_match' => true,
                'console.command.translation_debug' => true,
                'console.command.translation_update' => true,
                'console.command.xliff_lint' => true,
                'console.command.yaml_lint' => true,
                'console.command.form_debug' => true,
                'twig.command.debug' => true,
                'twig.command.lint' => true,
                'security.command.user_password_encoder' => true,
                'Sonata\\BlockBundle\\Command\\DebugBlocksCommand' => true,
                'doctrine.database_create_command' => true,
                'doctrine.database_drop_command' => true,
                'doctrine.generate_entities_command' => true,
                'doctrine.query_sql_command' => true,
                'doctrine.cache_clear_metadata_command' => true,
                'doctrine.cache_clear_query_cache_command' => true,
                'doctrine.cache_clear_result_command' => true,
                'doctrine.cache_collection_region_command' => true,
                'doctrine.mapping_convert_command' => true,
                'doctrine.schema_create_command' => true,
                'doctrine.schema_drop_command' => true,
                'doctrine.ensure_production_settings_command' => true,
                'doctrine.clear_entity_region_command' => true,
                'doctrine.database_import_command' => true,
                'doctrine.mapping_info_command' => true,
                'doctrine.clear_query_region_command' => true,
                'doctrine.query_dql_command' => true,
                'doctrine.schema_update_command' => true,
                'doctrine.schema_validate_command' => true,
                'doctrine.mapping_import_command' => true,
            ],
        ];
    }
}
