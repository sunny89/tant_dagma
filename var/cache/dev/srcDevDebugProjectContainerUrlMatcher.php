<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        elseif (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/audit')) {
                // app_doctrine_audit_show_entity_history
                if (0 === strpos($pathinfo, '/audit_new') && preg_match('#^/audit_new/(?P<entity>[^/]++)(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'app_doctrine_audit_show_entity_history']), array (  'id' => NULL,  '_controller' => 'App\\Controller\\AuditController::getHistory',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_app_doctrine_audit_show_entity_history;
                    }

                    return $ret;
                }
                not_app_doctrine_audit_show_entity_history:

                // get_history_pdf
                if (0 === strpos($pathinfo, '/audit_pdf') && preg_match('#^/audit_pdf/(?P<id>[^/]++)/(?P<transactionHash>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'get_history_pdf']), array (  '_controller' => 'App\\Controller\\AuditController::getHistoryPDF',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_get_history_pdf;
                    }

                    return $ret;
                }
                not_get_history_pdf:

                // dh_doctrine_audit_list_audits
                if ('/audit' === $pathinfo) {
                    $ret = array (  '_controller' => 'DH\\DoctrineAuditBundle\\Controller\\AuditController::listAuditsAction',  '_route' => 'dh_doctrine_audit_list_audits',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_dh_doctrine_audit_list_audits;
                    }

                    return $ret;
                }
                not_dh_doctrine_audit_list_audits:

                // dh_doctrine_audit_show_transaction
                if (0 === strpos($pathinfo, '/audit/transaction') && preg_match('#^/audit/transaction/(?P<hash>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'dh_doctrine_audit_show_transaction']), array (  '_controller' => 'DH\\DoctrineAuditBundle\\Controller\\AuditController::showTransactionAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_dh_doctrine_audit_show_transaction;
                    }

                    return $ret;
                }
                not_dh_doctrine_audit_show_transaction:

                // dh_doctrine_audit_show_entity_history
                if (preg_match('#^/audit/(?P<entity>[^/]++)(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'dh_doctrine_audit_show_entity_history']), array (  'id' => NULL,  '_controller' => 'DH\\DoctrineAuditBundle\\Controller\\AuditController::showEntityHistoryAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_dh_doctrine_audit_show_entity_history;
                    }

                    return $ret;
                }
                not_dh_doctrine_audit_show_entity_history:

            }

            elseif (0 === strpos($pathinfo, '/auth')) {
                // 2fa_login
                if ('/auth' === $pathinfo) {
                    return array (  '_controller' => 'scheb_two_factor.form_controller:form',  '_route' => '2fa_login',);
                }

                // 2fa_login_check
                if ('/auth_check' === $pathinfo) {
                    return ['_route' => '2fa_login_check'];
                }

            }

            elseif (0 === strpos($pathinfo, '/admin')) {
                if (0 === strpos($pathinfo, '/admin/dimmerrack')) {
                    // dimmerrack
                    if ('/admin/dimmerrack' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\DefaultController::dimmerrack',  '_route' => 'dimmerrack',);
                    }

                    // dimmerracksou
                    if ('/admin/dimmerracksou' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\DefaultController::dimmerracksou',  '_route' => 'dimmerracksou',);
                    }

                }

                // dimmingChannelCalculation
                if ('/admin/dimmingChannelCalculation' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\DefaultController::dimmingChannelCalculation',  '_route' => 'dimmingChannelCalculation',);
                }

                // lightPointCalculation
                if ('/admin/lightPointCalculation' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\DefaultController::lightPointCalculation',  '_route' => 'lightPointCalculation',);
                }

                // powerLoadConsumtion
                if ('/admin/powerLoadConsumtion' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\DefaultController::powerLoadConsumtion',  '_route' => 'powerLoadConsumtion',);
                }

                // videoTutorials
                if ('/admin/videoTutorials' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\DefaultController::videoTutorials',  '_route' => 'videoTutorials',);
                }

                // manuals
                if ('/admin/manuals' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\DefaultController::manuals',  '_route' => 'manuals',);
                }

                // sonata_admin_redirect
                if ('/admin' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_sonata_admin_redirect;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'sonata_admin_redirect'));
                    }

                    return $ret;
                }
                not_sonata_admin_redirect:

                // sonata_admin_dashboard
                if ('/admin/dashboard' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
                }

                if (0 === strpos($pathinfo, '/admin/core')) {
                    if (0 === strpos($pathinfo, '/admin/core/get-')) {
                        // sonata_admin_retrieve_form_element
                        if ('/admin/core/get-form-field-element' === $pathinfo) {
                            return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                        }

                        // sonata_admin_short_object_information
                        if (0 === strpos($pathinfo, '/admin/core/get-short-object-description') && preg_match('#^/admin/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'sonata_admin_short_object_information']), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                        }

                        // sonata_admin_retrieve_autocomplete_items
                        if ('/admin/core/get-autocomplete-items' === $pathinfo) {
                            return array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  '_route' => 'sonata_admin_retrieve_autocomplete_items',);
                        }

                    }

                    // sonata_admin_append_form_element
                    if ('/admin/core/append-form-field-element' === $pathinfo) {
                        return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
                    }

                    // sonata_admin_set_object_field_value
                    if ('/admin/core/set-object-field-value' === $pathinfo) {
                        return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
                    }

                }

                // sonata_admin_search
                if ('/admin/search' === $pathinfo) {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
                }

                if (0 === strpos($pathinfo, '/admin/app')) {
                    if (0 === strpos($pathinfo, '/admin/app/user')) {
                        // admin_app_user_list
                        if ('/admin/app/user/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_list',  '_route' => 'admin_app_user_list',);
                        }

                        // admin_app_user_create
                        if ('/admin/app/user/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_create',  '_route' => 'admin_app_user_create',);
                        }

                        // admin_app_user_batch
                        if ('/admin/app/user/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_batch',  '_route' => 'admin_app_user_batch',);
                        }

                        // admin_app_user_edit
                        if (preg_match('#^/admin/app/user/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_user_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_edit',));
                        }

                        // admin_app_user_delete
                        if (preg_match('#^/admin/app/user/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_user_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_delete',));
                        }

                        // admin_app_user_show
                        if (preg_match('#^/admin/app/user/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_user_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_show',));
                        }

                        // admin_app_user_export
                        if ('/admin/app/user/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.user',  '_sonata_name' => 'admin_app_user_export',  '_route' => 'admin_app_user_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/datasheet')) {
                        // admin_app_datasheet_list
                        if ('/admin/app/datasheet/list' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\DatasheetActionsController:listAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_list',  '_route' => 'admin_app_datasheet_list',);
                        }

                        // admin_app_datasheet_create
                        if ('/admin/app/datasheet/create' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\DatasheetActionsController:createAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_create',  '_route' => 'admin_app_datasheet_create',);
                        }

                        // admin_app_datasheet_batch
                        if ('/admin/app/datasheet/batch' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\DatasheetActionsController:batchAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_batch',  '_route' => 'admin_app_datasheet_batch',);
                        }

                        // admin_app_datasheet_edit
                        if (preg_match('#^/admin/app/datasheet/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_edit']), array (  '_controller' => 'App\\Controller\\DatasheetActionsController:editAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_edit',));
                        }

                        // admin_app_datasheet_delete
                        if (preg_match('#^/admin/app/datasheet/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_delete']), array (  '_controller' => 'App\\Controller\\DatasheetActionsController:deleteAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_delete',));
                        }

                        // admin_app_datasheet_show
                        if (preg_match('#^/admin/app/datasheet/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_show']), array (  '_controller' => 'App\\Controller\\DatasheetActionsController:showAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_show',));
                        }

                        // admin_app_datasheet_export
                        if ('/admin/app/datasheet/export' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\DatasheetActionsController:exportAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_export',  '_route' => 'admin_app_datasheet_export',);
                        }

                        // admin_app_datasheet_createPDF
                        if (preg_match('#^/admin/app/datasheet/(?P<id>[^/]++)/createPDF$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_createPDF']), array (  '_controller' => 'App\\Controller\\DatasheetActionsController:createPDFAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_createPDF',));
                        }

                        // admin_app_datasheet_copyDatasheet
                        if (preg_match('#^/admin/app/datasheet/(?P<id>[^/]++)/copyDatasheet$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_copyDatasheet']), array (  '_controller' => 'App\\Controller\\DatasheetActionsController:copyDatasheetAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_copyDatasheet',));
                        }

                        // admin_app_datasheet_auditList
                        if (preg_match('#^/admin/app/datasheet/(?P<id>[^/]++)/auditList$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_datasheet_auditList']), array (  '_controller' => 'App\\Controller\\DatasheetActionsController:auditListAction',  '_sonata_admin' => 'admin.datasheet',  '_sonata_name' => 'admin_app_datasheet_auditList',));
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/dimmertype')) {
                        // admin_app_dimmertype_list
                        if ('/admin/app/dimmertype/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_list',  '_route' => 'admin_app_dimmertype_list',);
                        }

                        // admin_app_dimmertype_create
                        if ('/admin/app/dimmertype/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_create',  '_route' => 'admin_app_dimmertype_create',);
                        }

                        // admin_app_dimmertype_batch
                        if ('/admin/app/dimmertype/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_batch',  '_route' => 'admin_app_dimmertype_batch',);
                        }

                        // admin_app_dimmertype_edit
                        if (preg_match('#^/admin/app/dimmertype/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_dimmertype_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_edit',));
                        }

                        // admin_app_dimmertype_delete
                        if (preg_match('#^/admin/app/dimmertype/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_dimmertype_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_delete',));
                        }

                        // admin_app_dimmertype_show
                        if (preg_match('#^/admin/app/dimmertype/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_dimmertype_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_show',));
                        }

                        // admin_app_dimmertype_export
                        if ('/admin/app/dimmertype/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.dimmerType',  '_sonata_name' => 'admin_app_dimmertype_export',  '_route' => 'admin_app_dimmertype_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/adjustingangle')) {
                        // admin_app_adjustingangle_list
                        if ('/admin/app/adjustingangle/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_list',  '_route' => 'admin_app_adjustingangle_list',);
                        }

                        // admin_app_adjustingangle_create
                        if ('/admin/app/adjustingangle/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_create',  '_route' => 'admin_app_adjustingangle_create',);
                        }

                        // admin_app_adjustingangle_batch
                        if ('/admin/app/adjustingangle/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_batch',  '_route' => 'admin_app_adjustingangle_batch',);
                        }

                        // admin_app_adjustingangle_edit
                        if (preg_match('#^/admin/app/adjustingangle/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_adjustingangle_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_edit',));
                        }

                        // admin_app_adjustingangle_delete
                        if (preg_match('#^/admin/app/adjustingangle/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_adjustingangle_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_delete',));
                        }

                        // admin_app_adjustingangle_show
                        if (preg_match('#^/admin/app/adjustingangle/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_adjustingangle_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_show',));
                        }

                        // admin_app_adjustingangle_export
                        if ('/admin/app/adjustingangle/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.adjustingAngle',  '_sonata_name' => 'admin_app_adjustingangle_export',  '_route' => 'admin_app_adjustingangle_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/beamangle')) {
                        // admin_app_beamangle_list
                        if ('/admin/app/beamangle/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_list',  '_route' => 'admin_app_beamangle_list',);
                        }

                        // admin_app_beamangle_create
                        if ('/admin/app/beamangle/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_create',  '_route' => 'admin_app_beamangle_create',);
                        }

                        // admin_app_beamangle_batch
                        if ('/admin/app/beamangle/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_batch',  '_route' => 'admin_app_beamangle_batch',);
                        }

                        // admin_app_beamangle_edit
                        if (preg_match('#^/admin/app/beamangle/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_beamangle_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_edit',));
                        }

                        // admin_app_beamangle_delete
                        if (preg_match('#^/admin/app/beamangle/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_beamangle_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_delete',));
                        }

                        // admin_app_beamangle_show
                        if (preg_match('#^/admin/app/beamangle/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_beamangle_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_show',));
                        }

                        // admin_app_beamangle_export
                        if ('/admin/app/beamangle/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.beamAngle',  '_sonata_name' => 'admin_app_beamangle_export',  '_route' => 'admin_app_beamangle_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/co')) {
                        if (0 === strpos($pathinfo, '/admin/app/colourofhousing')) {
                            // admin_app_colourofhousing_list
                            if ('/admin/app/colourofhousing/list' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_list',  '_route' => 'admin_app_colourofhousing_list',);
                            }

                            // admin_app_colourofhousing_create
                            if ('/admin/app/colourofhousing/create' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_create',  '_route' => 'admin_app_colourofhousing_create',);
                            }

                            // admin_app_colourofhousing_batch
                            if ('/admin/app/colourofhousing/batch' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_batch',  '_route' => 'admin_app_colourofhousing_batch',);
                            }

                            // admin_app_colourofhousing_edit
                            if (preg_match('#^/admin/app/colourofhousing/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofhousing_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_edit',));
                            }

                            // admin_app_colourofhousing_delete
                            if (preg_match('#^/admin/app/colourofhousing/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofhousing_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_delete',));
                            }

                            // admin_app_colourofhousing_show
                            if (preg_match('#^/admin/app/colourofhousing/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofhousing_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_show',));
                            }

                            // admin_app_colourofhousing_export
                            if ('/admin/app/colourofhousing/export' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.colourOfHousing',  '_sonata_name' => 'admin_app_colourofhousing_export',  '_route' => 'admin_app_colourofhousing_export',);
                            }

                        }

                        elseif (0 === strpos($pathinfo, '/admin/app/colourofshade')) {
                            // admin_app_colourofshade_list
                            if ('/admin/app/colourofshade/list' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_list',  '_route' => 'admin_app_colourofshade_list',);
                            }

                            // admin_app_colourofshade_create
                            if ('/admin/app/colourofshade/create' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_create',  '_route' => 'admin_app_colourofshade_create',);
                            }

                            // admin_app_colourofshade_batch
                            if ('/admin/app/colourofshade/batch' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_batch',  '_route' => 'admin_app_colourofshade_batch',);
                            }

                            // admin_app_colourofshade_edit
                            if (preg_match('#^/admin/app/colourofshade/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofshade_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_edit',));
                            }

                            // admin_app_colourofshade_delete
                            if (preg_match('#^/admin/app/colourofshade/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofshade_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_delete',));
                            }

                            // admin_app_colourofshade_show
                            if (preg_match('#^/admin/app/colourofshade/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_colourofshade_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_show',));
                            }

                            // admin_app_colourofshade_export
                            if ('/admin/app/colourofshade/export' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.colourOfShade',  '_sonata_name' => 'admin_app_colourofshade_export',  '_route' => 'admin_app_colourofshade_export',);
                            }

                        }

                        elseif (0 === strpos($pathinfo, '/admin/app/cover')) {
                            // admin_app_cover_list
                            if ('/admin/app/cover/list' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_list',  '_route' => 'admin_app_cover_list',);
                            }

                            // admin_app_cover_create
                            if ('/admin/app/cover/create' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_create',  '_route' => 'admin_app_cover_create',);
                            }

                            // admin_app_cover_batch
                            if ('/admin/app/cover/batch' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_batch',  '_route' => 'admin_app_cover_batch',);
                            }

                            // admin_app_cover_edit
                            if (preg_match('#^/admin/app/cover/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cover_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_edit',));
                            }

                            // admin_app_cover_delete
                            if (preg_match('#^/admin/app/cover/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cover_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_delete',));
                            }

                            // admin_app_cover_show
                            if (preg_match('#^/admin/app/cover/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cover_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_show',));
                            }

                            // admin_app_cover_export
                            if ('/admin/app/cover/export' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.cover',  '_sonata_name' => 'admin_app_cover_export',  '_route' => 'admin_app_cover_export',);
                            }

                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/cri')) {
                        // admin_app_cri_list
                        if ('/admin/app/cri/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_list',  '_route' => 'admin_app_cri_list',);
                        }

                        // admin_app_cri_create
                        if ('/admin/app/cri/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_create',  '_route' => 'admin_app_cri_create',);
                        }

                        // admin_app_cri_batch
                        if ('/admin/app/cri/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_batch',  '_route' => 'admin_app_cri_batch',);
                        }

                        // admin_app_cri_edit
                        if (preg_match('#^/admin/app/cri/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cri_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_edit',));
                        }

                        // admin_app_cri_delete
                        if (preg_match('#^/admin/app/cri/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cri_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_delete',));
                        }

                        // admin_app_cri_show
                        if (preg_match('#^/admin/app/cri/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_cri_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_show',));
                        }

                        // admin_app_cri_export
                        if ('/admin/app/cri/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.cri',  '_sonata_name' => 'admin_app_cri_export',  '_route' => 'admin_app_cri_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/fixturelightoutput')) {
                        // admin_app_fixturelightoutput_list
                        if ('/admin/app/fixturelightoutput/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_list',  '_route' => 'admin_app_fixturelightoutput_list',);
                        }

                        // admin_app_fixturelightoutput_create
                        if ('/admin/app/fixturelightoutput/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_create',  '_route' => 'admin_app_fixturelightoutput_create',);
                        }

                        // admin_app_fixturelightoutput_batch
                        if ('/admin/app/fixturelightoutput/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_batch',  '_route' => 'admin_app_fixturelightoutput_batch',);
                        }

                        // admin_app_fixturelightoutput_edit
                        if (preg_match('#^/admin/app/fixturelightoutput/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_fixturelightoutput_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_edit',));
                        }

                        // admin_app_fixturelightoutput_delete
                        if (preg_match('#^/admin/app/fixturelightoutput/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_fixturelightoutput_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_delete',));
                        }

                        // admin_app_fixturelightoutput_show
                        if (preg_match('#^/admin/app/fixturelightoutput/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_fixturelightoutput_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_show',));
                        }

                        // admin_app_fixturelightoutput_export
                        if ('/admin/app/fixturelightoutput/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.fixtureLightOutput',  '_sonata_name' => 'admin_app_fixturelightoutput_export',  '_route' => 'admin_app_fixturelightoutput_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/frequency')) {
                        // admin_app_frequency_list
                        if ('/admin/app/frequency/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_list',  '_route' => 'admin_app_frequency_list',);
                        }

                        // admin_app_frequency_create
                        if ('/admin/app/frequency/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_create',  '_route' => 'admin_app_frequency_create',);
                        }

                        // admin_app_frequency_batch
                        if ('/admin/app/frequency/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_batch',  '_route' => 'admin_app_frequency_batch',);
                        }

                        // admin_app_frequency_edit
                        if (preg_match('#^/admin/app/frequency/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_frequency_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_edit',));
                        }

                        // admin_app_frequency_delete
                        if (preg_match('#^/admin/app/frequency/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_frequency_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_delete',));
                        }

                        // admin_app_frequency_show
                        if (preg_match('#^/admin/app/frequency/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_frequency_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_show',));
                        }

                        // admin_app_frequency_export
                        if ('/admin/app/frequency/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.frequency',  '_sonata_name' => 'admin_app_frequency_export',  '_route' => 'admin_app_frequency_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/lightcolour')) {
                        // admin_app_lightcolour_list
                        if ('/admin/app/lightcolour/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_list',  '_route' => 'admin_app_lightcolour_list',);
                        }

                        // admin_app_lightcolour_create
                        if ('/admin/app/lightcolour/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_create',  '_route' => 'admin_app_lightcolour_create',);
                        }

                        // admin_app_lightcolour_batch
                        if ('/admin/app/lightcolour/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_batch',  '_route' => 'admin_app_lightcolour_batch',);
                        }

                        // admin_app_lightcolour_edit
                        if (preg_match('#^/admin/app/lightcolour/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightcolour_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_edit',));
                        }

                        // admin_app_lightcolour_delete
                        if (preg_match('#^/admin/app/lightcolour/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightcolour_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_delete',));
                        }

                        // admin_app_lightcolour_show
                        if (preg_match('#^/admin/app/lightcolour/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightcolour_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_show',));
                        }

                        // admin_app_lightcolour_export
                        if ('/admin/app/lightcolour/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.lightColour',  '_sonata_name' => 'admin_app_lightcolour_export',  '_route' => 'admin_app_lightcolour_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/lightpoints')) {
                        // admin_app_lightpoints_list
                        if ('/admin/app/lightpoints/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_list',  '_route' => 'admin_app_lightpoints_list',);
                        }

                        // admin_app_lightpoints_create
                        if ('/admin/app/lightpoints/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_create',  '_route' => 'admin_app_lightpoints_create',);
                        }

                        // admin_app_lightpoints_batch
                        if ('/admin/app/lightpoints/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_batch',  '_route' => 'admin_app_lightpoints_batch',);
                        }

                        // admin_app_lightpoints_edit
                        if (preg_match('#^/admin/app/lightpoints/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightpoints_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_edit',));
                        }

                        // admin_app_lightpoints_delete
                        if (preg_match('#^/admin/app/lightpoints/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightpoints_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_delete',));
                        }

                        // admin_app_lightpoints_show
                        if (preg_match('#^/admin/app/lightpoints/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_lightpoints_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_show',));
                        }

                        // admin_app_lightpoints_export
                        if ('/admin/app/lightpoints/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.lightPoints',  '_sonata_name' => 'admin_app_lightpoints_export',  '_route' => 'admin_app_lightpoints_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/materialofhousing')) {
                        // admin_app_materialofhousing_list
                        if ('/admin/app/materialofhousing/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_list',  '_route' => 'admin_app_materialofhousing_list',);
                        }

                        // admin_app_materialofhousing_create
                        if ('/admin/app/materialofhousing/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_create',  '_route' => 'admin_app_materialofhousing_create',);
                        }

                        // admin_app_materialofhousing_batch
                        if ('/admin/app/materialofhousing/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_batch',  '_route' => 'admin_app_materialofhousing_batch',);
                        }

                        // admin_app_materialofhousing_edit
                        if (preg_match('#^/admin/app/materialofhousing/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofhousing_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_edit',));
                        }

                        // admin_app_materialofhousing_delete
                        if (preg_match('#^/admin/app/materialofhousing/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofhousing_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_delete',));
                        }

                        // admin_app_materialofhousing_show
                        if (preg_match('#^/admin/app/materialofhousing/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofhousing_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_show',));
                        }

                        // admin_app_materialofhousing_export
                        if ('/admin/app/materialofhousing/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.materialOfHousing',  '_sonata_name' => 'admin_app_materialofhousing_export',  '_route' => 'admin_app_materialofhousing_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/materialofshade')) {
                        // admin_app_materialofshade_list
                        if ('/admin/app/materialofshade/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_list',  '_route' => 'admin_app_materialofshade_list',);
                        }

                        // admin_app_materialofshade_create
                        if ('/admin/app/materialofshade/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_create',  '_route' => 'admin_app_materialofshade_create',);
                        }

                        // admin_app_materialofshade_batch
                        if ('/admin/app/materialofshade/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_batch',  '_route' => 'admin_app_materialofshade_batch',);
                        }

                        // admin_app_materialofshade_edit
                        if (preg_match('#^/admin/app/materialofshade/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofshade_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_edit',));
                        }

                        // admin_app_materialofshade_delete
                        if (preg_match('#^/admin/app/materialofshade/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofshade_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_delete',));
                        }

                        // admin_app_materialofshade_show
                        if (preg_match('#^/admin/app/materialofshade/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_materialofshade_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_show',));
                        }

                        // admin_app_materialofshade_export
                        if ('/admin/app/materialofshade/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.materialOfShade',  '_sonata_name' => 'admin_app_materialofshade_export',  '_route' => 'admin_app_materialofshade_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/numberoflamps')) {
                        // admin_app_numberoflamps_list
                        if ('/admin/app/numberoflamps/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_list',  '_route' => 'admin_app_numberoflamps_list',);
                        }

                        // admin_app_numberoflamps_create
                        if ('/admin/app/numberoflamps/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_create',  '_route' => 'admin_app_numberoflamps_create',);
                        }

                        // admin_app_numberoflamps_batch
                        if ('/admin/app/numberoflamps/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_batch',  '_route' => 'admin_app_numberoflamps_batch',);
                        }

                        // admin_app_numberoflamps_edit
                        if (preg_match('#^/admin/app/numberoflamps/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_numberoflamps_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_edit',));
                        }

                        // admin_app_numberoflamps_delete
                        if (preg_match('#^/admin/app/numberoflamps/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_numberoflamps_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_delete',));
                        }

                        // admin_app_numberoflamps_show
                        if (preg_match('#^/admin/app/numberoflamps/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_numberoflamps_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_show',));
                        }

                        // admin_app_numberoflamps_export
                        if ('/admin/app/numberoflamps/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.numberOfLamps',  '_sonata_name' => 'admin_app_numberoflamps_export',  '_route' => 'admin_app_numberoflamps_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/p')) {
                        if (0 === strpos($pathinfo, '/admin/app/plug')) {
                            // admin_app_plug_list
                            if ('/admin/app/plug/list' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_list',  '_route' => 'admin_app_plug_list',);
                            }

                            // admin_app_plug_create
                            if ('/admin/app/plug/create' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_create',  '_route' => 'admin_app_plug_create',);
                            }

                            // admin_app_plug_batch
                            if ('/admin/app/plug/batch' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_batch',  '_route' => 'admin_app_plug_batch',);
                            }

                            // admin_app_plug_edit
                            if (preg_match('#^/admin/app/plug/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_plug_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_edit',));
                            }

                            // admin_app_plug_delete
                            if (preg_match('#^/admin/app/plug/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_plug_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_delete',));
                            }

                            // admin_app_plug_show
                            if (preg_match('#^/admin/app/plug/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_plug_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_show',));
                            }

                            // admin_app_plug_export
                            if ('/admin/app/plug/export' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.plug',  '_sonata_name' => 'admin_app_plug_export',  '_route' => 'admin_app_plug_export',);
                            }

                        }

                        elseif (0 === strpos($pathinfo, '/admin/app/poweroffitting')) {
                            // admin_app_poweroffitting_list
                            if ('/admin/app/poweroffitting/list' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_list',  '_route' => 'admin_app_poweroffitting_list',);
                            }

                            // admin_app_poweroffitting_create
                            if ('/admin/app/poweroffitting/create' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_create',  '_route' => 'admin_app_poweroffitting_create',);
                            }

                            // admin_app_poweroffitting_batch
                            if ('/admin/app/poweroffitting/batch' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_batch',  '_route' => 'admin_app_poweroffitting_batch',);
                            }

                            // admin_app_poweroffitting_edit
                            if (preg_match('#^/admin/app/poweroffitting/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroffitting_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_edit',));
                            }

                            // admin_app_poweroffitting_delete
                            if (preg_match('#^/admin/app/poweroffitting/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroffitting_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_delete',));
                            }

                            // admin_app_poweroffitting_show
                            if (preg_match('#^/admin/app/poweroffitting/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroffitting_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_show',));
                            }

                            // admin_app_poweroffitting_export
                            if ('/admin/app/poweroffitting/export' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.powerOfFitting',  '_sonata_name' => 'admin_app_poweroffitting_export',  '_route' => 'admin_app_poweroffitting_export',);
                            }

                        }

                        elseif (0 === strpos($pathinfo, '/admin/app/poweroflamp')) {
                            // admin_app_poweroflamp_list
                            if ('/admin/app/poweroflamp/list' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_list',  '_route' => 'admin_app_poweroflamp_list',);
                            }

                            // admin_app_poweroflamp_create
                            if ('/admin/app/poweroflamp/create' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_create',  '_route' => 'admin_app_poweroflamp_create',);
                            }

                            // admin_app_poweroflamp_batch
                            if ('/admin/app/poweroflamp/batch' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_batch',  '_route' => 'admin_app_poweroflamp_batch',);
                            }

                            // admin_app_poweroflamp_edit
                            if (preg_match('#^/admin/app/poweroflamp/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroflamp_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_edit',));
                            }

                            // admin_app_poweroflamp_delete
                            if (preg_match('#^/admin/app/poweroflamp/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroflamp_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_delete',));
                            }

                            // admin_app_poweroflamp_show
                            if (preg_match('#^/admin/app/poweroflamp/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_poweroflamp_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_show',));
                            }

                            // admin_app_poweroflamp_export
                            if ('/admin/app/poweroflamp/export' === $pathinfo) {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.powerOfLamp',  '_sonata_name' => 'admin_app_poweroflamp_export',  '_route' => 'admin_app_poweroflamp_export',);
                            }

                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/supplier')) {
                        // admin_app_supplier_list
                        if ('/admin/app/supplier/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_list',  '_route' => 'admin_app_supplier_list',);
                        }

                        // admin_app_supplier_create
                        if ('/admin/app/supplier/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_create',  '_route' => 'admin_app_supplier_create',);
                        }

                        // admin_app_supplier_batch
                        if ('/admin/app/supplier/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_batch',  '_route' => 'admin_app_supplier_batch',);
                        }

                        // admin_app_supplier_edit
                        if (preg_match('#^/admin/app/supplier/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_supplier_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_edit',));
                        }

                        // admin_app_supplier_delete
                        if (preg_match('#^/admin/app/supplier/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_supplier_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_delete',));
                        }

                        // admin_app_supplier_show
                        if (preg_match('#^/admin/app/supplier/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_supplier_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_show',));
                        }

                        // admin_app_supplier_export
                        if ('/admin/app/supplier/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.supplier',  '_sonata_name' => 'admin_app_supplier_export',  '_route' => 'admin_app_supplier_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/socket')) {
                        // admin_app_socket_list
                        if ('/admin/app/socket/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_list',  '_route' => 'admin_app_socket_list',);
                        }

                        // admin_app_socket_create
                        if ('/admin/app/socket/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_create',  '_route' => 'admin_app_socket_create',);
                        }

                        // admin_app_socket_batch
                        if ('/admin/app/socket/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_batch',  '_route' => 'admin_app_socket_batch',);
                        }

                        // admin_app_socket_edit
                        if (preg_match('#^/admin/app/socket/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_socket_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_edit',));
                        }

                        // admin_app_socket_delete
                        if (preg_match('#^/admin/app/socket/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_socket_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_delete',));
                        }

                        // admin_app_socket_show
                        if (preg_match('#^/admin/app/socket/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_socket_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_show',));
                        }

                        // admin_app_socket_export
                        if ('/admin/app/socket/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.socket',  '_sonata_name' => 'admin_app_socket_export',  '_route' => 'admin_app_socket_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/typeoflamp')) {
                        // admin_app_typeoflamp_list
                        if ('/admin/app/typeoflamp/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_list',  '_route' => 'admin_app_typeoflamp_list',);
                        }

                        // admin_app_typeoflamp_create
                        if ('/admin/app/typeoflamp/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_create',  '_route' => 'admin_app_typeoflamp_create',);
                        }

                        // admin_app_typeoflamp_batch
                        if ('/admin/app/typeoflamp/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_batch',  '_route' => 'admin_app_typeoflamp_batch',);
                        }

                        // admin_app_typeoflamp_edit
                        if (preg_match('#^/admin/app/typeoflamp/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeoflamp_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_edit',));
                        }

                        // admin_app_typeoflamp_delete
                        if (preg_match('#^/admin/app/typeoflamp/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeoflamp_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_delete',));
                        }

                        // admin_app_typeoflamp_show
                        if (preg_match('#^/admin/app/typeoflamp/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeoflamp_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_show',));
                        }

                        // admin_app_typeoflamp_export
                        if ('/admin/app/typeoflamp/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.typeOfLamp',  '_sonata_name' => 'admin_app_typeoflamp_export',  '_route' => 'admin_app_typeoflamp_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/typeofbulb')) {
                        // admin_app_typeofbulb_list
                        if ('/admin/app/typeofbulb/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_list',  '_route' => 'admin_app_typeofbulb_list',);
                        }

                        // admin_app_typeofbulb_create
                        if ('/admin/app/typeofbulb/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_create',  '_route' => 'admin_app_typeofbulb_create',);
                        }

                        // admin_app_typeofbulb_batch
                        if ('/admin/app/typeofbulb/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_batch',  '_route' => 'admin_app_typeofbulb_batch',);
                        }

                        // admin_app_typeofbulb_edit
                        if (preg_match('#^/admin/app/typeofbulb/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeofbulb_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_edit',));
                        }

                        // admin_app_typeofbulb_delete
                        if (preg_match('#^/admin/app/typeofbulb/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeofbulb_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_delete',));
                        }

                        // admin_app_typeofbulb_show
                        if (preg_match('#^/admin/app/typeofbulb/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_typeofbulb_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_show',));
                        }

                        // admin_app_typeofbulb_export
                        if ('/admin/app/typeofbulb/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.typeOfBulb',  '_sonata_name' => 'admin_app_typeofbulb_export',  '_route' => 'admin_app_typeofbulb_export',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/app/voltage')) {
                        // admin_app_voltage_list
                        if ('/admin/app/voltage/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_list',  '_route' => 'admin_app_voltage_list',);
                        }

                        // admin_app_voltage_create
                        if ('/admin/app/voltage/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_create',  '_route' => 'admin_app_voltage_create',);
                        }

                        // admin_app_voltage_batch
                        if ('/admin/app/voltage/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_batch',  '_route' => 'admin_app_voltage_batch',);
                        }

                        // admin_app_voltage_edit
                        if (preg_match('#^/admin/app/voltage/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_voltage_edit']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_edit',));
                        }

                        // admin_app_voltage_delete
                        if (preg_match('#^/admin/app/voltage/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_voltage_delete']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_delete',));
                        }

                        // admin_app_voltage_show
                        if (preg_match('#^/admin/app/voltage/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_app_voltage_show']), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_show',));
                        }

                        // admin_app_voltage_export
                        if ('/admin/app/voltage/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.voltage',  '_sonata_name' => 'admin_app_voltage_export',  '_route' => 'admin_app_voltage_export',);
                        }

                    }

                }

            }

        }

        // index
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\DefaultController::index',  '_route' => 'index',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_index;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'index'));
            }

            return $ret;
        }
        not_index:

        if (0 === strpos($pathinfo, '/login')) {
            // fos_user_security_login
            if ('/login' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:loginAction',  '_route' => 'fos_user_security_login',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_security_login;
                }

                return $ret;
            }
            not_fos_user_security_login:

            // fos_user_security_check
            if ('/login_check' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:checkAction',  '_route' => 'fos_user_security_check',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_security_check;
                }

                return $ret;
            }
            not_fos_user_security_check:

        }

        elseif (0 === strpos($pathinfo, '/logout')) {
            // fos_user_security_logout
            if ('/logout' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.security.controller:logoutAction',  '_route' => 'fos_user_security_logout',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_security_logout;
                }

                return $ret;
            }
            not_fos_user_security_logout:

            // _security_logout
            if ('/logout' === $pathinfo) {
                return ['_route' => '_security_logout'];
            }

        }

        elseif (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if ('/profile' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'fos_user.profile.controller:showAction',  '_route' => 'fos_user_profile_show',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_user_profile_show;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_profile_show'));
                }

                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_profile_show;
                }

                return $ret;
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ('/profile/edit' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.profile.controller:editAction',  '_route' => 'fos_user_profile_edit',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_profile_edit;
                }

                return $ret;
            }
            not_fos_user_profile_edit:

            // fos_user_change_password
            if ('/profile/change-password' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.change_password.controller:changePasswordAction',  '_route' => 'fos_user_change_password',);
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_change_password;
                }

                return $ret;
            }
            not_fos_user_change_password:

        }

        elseif (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if ('/register' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'fos_user.registration.controller:registerAction',  '_route' => 'fos_user_registration_register',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_fos_user_registration_register;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'fos_user_registration_register'));
                }

                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_registration_register;
                }

                return $ret;
            }
            not_fos_user_registration_register:

            // fos_user_registration_check_email
            if ('/register/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.registration.controller:checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_registration_check_email;
                }

                return $ret;
            }
            not_fos_user_registration_check_email:

            if (0 === strpos($pathinfo, '/register/confirm')) {
                // fos_user_registration_confirm
                if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_registration_confirm']), array (  '_controller' => 'fos_user.registration.controller:confirmAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_registration_confirm;
                    }

                    return $ret;
                }
                not_fos_user_registration_confirm:

                // fos_user_registration_confirmed
                if ('/register/confirmed' === $pathinfo) {
                    $ret = array (  '_controller' => 'fos_user.registration.controller:confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_fos_user_registration_confirmed;
                    }

                    return $ret;
                }
                not_fos_user_registration_confirmed:

            }

        }

        elseif (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ('/resetting/request' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:requestAction',  '_route' => 'fos_user_resetting_request',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_request;
                }

                return $ret;
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'fos_user_resetting_reset']), array (  '_controller' => 'fos_user.resetting.controller:resetAction',));
                if (!in_array($canonicalMethod, ['GET', 'POST'])) {
                    $allow = array_merge($allow, ['GET', 'POST']);
                    goto not_fos_user_resetting_reset;
                }

                return $ret;
            }
            not_fos_user_resetting_reset:

            // fos_user_resetting_send_email
            if ('/resetting/send-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_fos_user_resetting_send_email;
                }

                return $ret;
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ('/resetting/check-email' === $pathinfo) {
                $ret = array (  '_controller' => 'fos_user.resetting.controller:checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                if (!in_array($canonicalMethod, ['GET'])) {
                    $allow = array_merge($allow, ['GET']);
                    goto not_fos_user_resetting_check_email;
                }

                return $ret;
            }
            not_fos_user_resetting_check_email:

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
